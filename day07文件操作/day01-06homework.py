#!/usr/bin/env python
# -*- coding:utf-8 -*-

# day01
# 1.操作系统的作用?
'''
人操作软件,软件控制操作系统,操作系统控制硬件运行.
'''

# 2.列举你听过的操作系统及区别？
'''
Windows:xp/7/8/10   使用广泛,界面美观,操作流畅,收费
Linux:Ubuntu,CentOS,RedHat  免费,开源,长时间开机不卡顿,适合搭载服务器
Unix:MacOS  适合办公,操作方便
'''

# 3.列举你了解的编码及他们之间的区别？
'''
1.ASCII:只有英文,8bit,1byte
2.Unicode:万国码,能使用所有语言,浪费内存,32bit,4byte
3.UTF-8:简化万国码,英文8bit,欧文16bit,中文24bit
4.GBK:国标码,16bit
'''

# 4.列举你了解的Python2和Python3的区别？
'''
1.默认解释器编码不同:2:ASCII,3:UTF-8
2.输入语法不同:2:raw_input,3:input
3.输出语法不同:2:print "",3.print("")
4.int整型:2:有范围,3:没有范围
5.long长整型:2:有,3:没有
'''

# 5.你了解的python都有那些数据类型？
'''
1.int:整型
2.str:字符串
3.list:列表
4.tuple:元组
5.dict:字典
6.bool:布尔值
7.set:集合
'''

# 6.补充代码，实现以下功能。
'''
value =  'alex"烧饼'
print(value)  # 要求输出  alex"烧饼
'''

# 7.用print打印出下面内容：
# ⽂能提笔安天下,
# 武能上⻢定乾坤.
# ⼼存谋略何⼈胜,
# 古今英雄唯是君。
"""
print('''
⽂能提笔安天下,
武能上⻢定乾坤.
⼼存谋略何⼈胜,
古今英雄唯是君。
''')
"""

# 8.变量名的命名规范和建议？
'''
1.由数字,字母和下划线组成.
2.不能用数字开头
3.不能用关键字
4.词之间用_连接或驼峰命名法
'''

# 9.如下那个变量名是正确的？
#
# name = '武沛齐'  正确
# _ = 'alex'    正确
# _9 = "老男孩"    正确
# 9name = "景女神"
# oldboy(edu = 666

# 10.简述你了解if条件语句的基本结构。
'''
if 条件1:
    代码块1
elif 条件2:
    代码块2
...
else:
    代码块n
'''

# 11.设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；如果比66小，则显示猜测的结果小了;只有
# 等于66，显示猜测结果正确。
'''
a = int(input('请猜:'))
if a == 66:
    print('猜对了')
elif a > 66:
    print('猜大了')
else:
    print('猜小了')
'''

# 12.提⽰⽤户输入⿇花藤. 判断⽤户输入的对不对。如果对, 提⽰真聪明, 如果不对, 提⽰你 是傻逼么。
'''
a = input('请输入⿇花藤:')
if a == '⿇花藤':
    print('真聪明')
else:
    print('你 是傻逼么')
'''

# day02
# 1.猜数字，设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；如果比66小，则显示猜测的结果小
# 了;只有等于66，显示猜测结果正确，然后退出循环。
'''
while 1:
    a = int(input('请猜:'))
    if a == 66:
        print('猜测结果正确')
        break
    elif a > 66:
        print('猜大了')
    else:
        print('猜小了')
'''

# 2.在上一题的基础，设置：给用户三次猜测机会，如果三次之内猜测对了，则显示猜测正确，退出循环，如果三次之内没有猜测正确，
# 则自动退出循环，并显示‘大笨蛋’。
'''
count = 0
while count < 3:
    a = int(input('请猜:'))
    if a == 66:
        print('猜对了')
        break
    elif a > 66:
        print('猜大了')
    else:
        print('猜小了')
    count += 1
if count == 3:
    print('大笨蛋')
'''

# 3.使用两种方法实现输出 1 2 3 4 5 6 8 9 10 。
'''
# 1.
a = 1
while a <= 10:
    if a == 7:
        pass
    else:
        print(a)
    a+=1
# 2.
a = 1
while a<=10:
    if a!=7:
        print(a)
    a+=1
'''

# 4.求1-100的所有数的和.
'''
sum = 0
for a in range(101):
    sum += a
print(sum)
'''

# 5.输出 1-100 内的所有奇数.
'''
for i in range(100):
    if i % 2 == 1:
        print(i)
'''

# 6.输出 1-100 内的所有偶数.
'''
for i in range(100):
    if i % 2 == 0:
        print(i)
'''

# 7.求1-2+3-4+5 ... 99的所有数的和.
'''
sum = 0
for i in range(100):
    if i % 2 == 0:
        sum -= i
    else:
        sum += i
print(sum)
'''

# 8.⽤户登陆（三次输错机会）且每次输错误时显示剩余错误次数（提示：使⽤字符串格式化）
'''
for i in range(2,-1,-1):
    a = input('帐号')
    b = input('密码')
    if a == 'qwer' and b == '123':
        print('登陆成功')
    else:
        print('登录失败,剩余%s次.' % i)
'''

# 9.简述ASCII、Unicode、utf-8编码
'''
ASCII:只有英文,8bit,1byte
Unicode:万国码,可使用所有语言,浪费资源.32bit,4byte.
UTF-8:简化万国码,英文8bit,欧洲16bit,中文24bit
'''

# 10.简述位和字节的关系？
'''
1位即1bit,指一个0或1,一个字节指单个字符,8bit=1byte.
'''

# 11.猜年龄游戏,要求：允许用户最多尝试3次，3次都没猜对的话，就直接退出，如果猜对了，打印恭喜信息并退出.
'''
for i in range(2,-1,-1):
    age = int(input('请猜:'))
    if age == 18:
        print('恭喜你!')
        break
    else:
        print('猜错了,剩余%s次' % i)
'''

# 12..猜年龄游戏升级版
# 要求：允许用户最多尝试3次，每尝试3次后，如果还没猜对，就问用户是否还想继续玩，如果回答Y，就继续让其猜3次，以此往复，如
# 果回答N，就退出程序，如何猜对了，就直接退出。
'''
while 1:
    for i in range(2,-1,-1):
        age = int(input('请猜:'))
        if age == 18:
            print('猜对了')
    ans = input('是否继续?y/n')
    if ans.upper()=='Y':
        continue
    else:
        break
'''

# 13.判断下列逻辑语句的True,False
# 1 > 1 or 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6  #True
# not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6 # False

# 14.求出下列逻辑语句的值。
# 8 or 3 and 4 or 2 and 0 or 9 and 7    # 8
# 0 or 2 and 3 and 4 or 6 and 0 or 3    # 4

# 15.下列结果是什么？
# 6 or 2 > 1    # 6
# 3 or 2 > 1    # 3
# 0 or 5 < 4    # f
# 5 < 4 or 3    # 3
# 2 > 1 or 6    # t
# 3 and 2 > 1   # t
# 0 and 3 > 1   # 0
# 2 > 1 and 3   # 3
# 3 > 1 and 0   # 0
# 3 > 1 and 2 or 2 < 3 and 3 and 4 or 3 > 2 # 2

# day03
# 1.有变量name = "aleX leNb " 完成如下操作：
# name = "aleX leNb "
# 移除 name 变量对应的值两边的空格,并输出处理结果
'''
name2 = name.strip()
print(name2)
'''
# 判断 name 变量是否以 "al" 开头,并输出结果（用切片）
'''
if name[:2]=='al':
    print('Y')
'''
# 判断name变量是否以"Nb"结尾,并输出结果（用切片）
'''
if name[:-3:-1]=='Nb':
    print('Y')
'''
# 将 name 变量对应的值中的 所有的"l" 替换为 "p",并输出结果
'''
name2 = name.replace('l','p')
print(name2)
'''
# 将name变量对应的值中的第一个"l"替换成"p",并输出结果
'''
name2 = name.replace('l','p',1)
print(name2)
'''
# 将 name 变量对应的值根据 所有的"l" 分割,并输出结果
'''
name2 = name.split('l')
print(name2)
'''
# 将name变量对应的值根据第一个"l"分割,并输出结果
'''
name2 = name.split('l',1)
print(name2)
'''
# 将 name 变量对应的值变大写,并输出结果
'''
name2 = name.upper()
print(name2)
'''
# 将 name 变量对应的值变小写,并输出结果
'''
name2 = name.lower()
print(name2)
'''
# 请输出 name 变量对应的值的第 2 个字符?
'''
print(name[:2])
'''
# 请输出 name 变量对应的值的前 3 个字符?
'''
print(name[:3])
'''
# 请输出 name 变量对应的值的后 2 个字符?
'''
print(name[-2:])
'''

# 2.有字符串s = "123a4b5c"
# s = "123a4b5c"
# 通过对s切片形成新的字符串 "123"
'''
s2 = s[:3]
print(s2)
'''
# 通过对s切片形成新的字符串 "a4b"
'''
print(s[3:6])
'''
# 通过对s切片形成字符串s5,s5 = "c"
'''
s5 = s[-1]
print(s5)
'''
# 通过对s切片形成字符串s6,s6 = "ba2"
'''
s6 = s[-3::-2]
print(s6)
'''

# 3.使用while循环字符串 s="asdfer" 中每个元素。
'''
s="asdfer"
i = 0
while i < len(s):
    print(s[i])
    i += 1
'''

# 4.使用while循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"。
'''
s = "321"
i = 0
while i < len(s):
    print('倒计时%s秒' % s[i])
    i += 1
print('出发!')
'''

# 5.实现一个整数加法计算器(两个数相加)：
# 如：content = input("请输入内容:") 用户输入：5+9或5+ 9或5 + 9（含空白），然后进行分割转换最终进行整数的计算得到结果。
'''
content = input("请输入内容:")
s1,s2 = content.split('+')
print(int(s1)+int(s2))
'''

# 6.计算用户输入的内容中有几个 h 字符？
# 如：content = input("请输入内容：") # 如fhdal234slfh98769fjdla
'''
content = input("请输入内容：")
s = content.count('h')
print(s)
'''

# 7.计算用户输入的内容中有几个 h 或 H 字符？
# 如：content = input("请输入内容：") # 如fhdal234slfH9H769fjdla
'''
content = input("请输入内容：")
s = 0
for i in content:
    if i.upper() == 'H':
        s += 1
print(s)
'''

# 8.使用while循环分别正向和反向对字符串 message = "伤情最是晚凉天，憔悴厮人不堪言。" 进行打印。
'''
message = "伤情最是晚凉天，憔悴厮人不堪言。"
i = 0
while i < len(message):
    print(message[i])
    i += 1
i = 0
while i < len(message):
    print(message[len(message)-i-1])
    i += 1
'''

# 9.获取用户输入的内容中 前4个字符中 有几个 A ？
# 如：content = input("请输入内容：") # 如fAdal234slfH9H769fjdla
'''
content = input("请输入内容：")
s = content[:4].count('A')
print(s)
'''

# 10.获取用户输入的内容，并计算前四位"l"出现几次,并输出结果。
'''
content = input("请输入内容：")
s = content[:4].count('1')
print(s)
'''

# 11.获取用户两次输入的内容，并将所有的数据获取并进行相加，如：
# """
# 要求：
# 	将num1中的的所有数字找到并拼接起来：1232312
# 	将num1中的的所有数字找到并拼接起来：1218323
# 	然后将两个数字进行相加。
# """
'''
num1 = input("请输入：") # asdfd123sf2312
num2 = input("请输入：") # a12dfd183sf23
s1 = ''
s2 = ''
for i in num1:
    if i.isdigit():
        s1 += i
for i in num2:
    if i.isdigit():
        s2 += i
print(int(s1)+int(s2))
'''

# day04
# 1.简述解释性语言和编译型语言的区别？
'''
解释型语言:按行执行代码,报错之前都会执行,与计算机交流频繁
编译型语言:报错不能编译,编译完成才能执行,与计算机交流少
'''

# 2.列举你了解的Python的数据类型？
'''
1.int整型
2.bool布尔值
3.str字符串
4.list列表
5,dict字典
6.tuple元组
7.set集合
'''

# 3.写代码，有如下列表，按照要求实现每一个功能。
# li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
# 计算列表的长度并输出
'''
print(len(li))
'''
# 请通过步长获取索引为偶数的所有值，并打印出获取后的列表
'''
li2 = li[::2]
print(li2)
'''
# 列表中追加元素"seven",并输出添加后的列表
'''
li.append('seven')
print(li)
'''
# 请在列表的第1个位置插入元素"Tony",并输出添加后的列表
'''
li.insert(0,'Tony')
print(li)
'''
# 请修改列表第2个位置的元素为"Kelly",并输出修改后的列表
'''
li[1] = 'Kelly'
print(li)
'''
# 请将列表的第3个位置的值改成 "太白"，并输出修改后的列表
'''
li[2] = '太白'
print(li)
'''
# 请将列表 l2=[1,"a",3,4,"heart"] 的每一个元素追加到列表li中，并输出添加后的列表
'''
l2=[1,"a",3,4,"heart"]
for i in l2:
    li.append(i)
print(li)
'''
# 请将字符串 s = "qwert"的每一个元素添加到列表li中，一行代码实现，不允许循环添加。
'''
s = "qwert"
li.extend(s)
print(li)
'''
# 请删除列表中的元素"ritian",并输出添加后的列表
'''
li.remove('ritian')
print(li)
'''
# 请删除列表中的第2个元素，并输出删除元素后的列表
'''
li.pop(1)
print(li)
'''
# 请删除列表中的第2至第4个元素，并输出删除元素后的列表
'''
del li[1:4]
print(li)
'''

# 4.请用三种方法实现字符串反转 name = "小黑半夜三点在被窝玩愤怒的小鸟"（步长、while、for）
'''
name = "小黑半夜三点在被窝玩愤怒的小鸟"
# 1.print(name[::-1])
# 2.i = 0
s = ''
while i < len(name):
    s += name[len(name)-i-1]
    i += 1
print(s)
# 3.s = ''
for i in range(len(name)):
    s += name[len(name)-i-1]
print(s)
'''

# 5.写代码，有如下列表，利用切片实现每一个功能
# li = [1, 3, 2, "a", 4, "b", 5,"c"]
# 通过对li列表的切片形成新的列表 [1,3,2]
'''
a = li[:3]
print(a)
'''
# 通过对li列表的切片形成新的列表 ["a",4,"b"]
'''
a = li[3:6]
print(a)
'''
# 通过对li列表的切片形成新的列表 [1,2,4,5]
'''
a = li[::2]
print(a)
'''
# 通过对li列表的切片形成新的列表 [3,"a","b"]
'''
a = li[1:6:2]
print(a)
'''
# 通过对li列表的切片形成新的列表 [3,"a","b","c"]
'''
a = li[1::2]
print(a)
'''
# 通过对li列表的切片形成新的列表 ["c"]
'''
a = li[-1:-2:-1]
print(a)
'''
# 通过对li列表的切片形成新的列表 ["b","a",3]
'''
a = li[-3::-2]
print(a)
'''

# 6.请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
# 0 武沛齐
# 1 景女神
# 2 肖大侠
'''
users = ["武沛齐","景女神","肖大侠"]
for i in range(len(users)):
    print(i,users[i])
'''

# 7.请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
# 1 武沛齐
# 2 景女神
# 3 肖大侠
'''
users = ["武沛齐","景女神","肖大侠"]
for i in range(len(users)):
    print(i+1,users[i])
'''

# 8.写代码，有如下列表，按照要求实现每一个功能。
# lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
# 将列表lis中的"k"变成大写，并打印列表。
'''
lis[2] = 'K'
lis[3][2][0] = 'K1'
print(lis)
'''
# 将列表中的数字3变成字符串"100"
'''
lis[3][2][1][1] = '100'
print(lis)
'''
# 将列表中的字符串"tt"变成数字 101
'''
lis[3][2][1][0] = 101
print(lis)
'''
# 在 "qwe"前面插入字符串："火车头"
'''
lis[3].insert(0,"火车头")
print(lis)
'''

# 9.写代码实现以下功能
# 如有变量 googs = ['汽车','飞机','火箭'] 提示用户可供选择的商品：
# 0,汽车
# 1,飞机
# 2,火箭
'''
googs = ['汽车','飞机','火箭']
for i in range(len(googs)):
    print(i,googs[i])

# 用户输入索引后，将指定商品的内容拼接打印，如：用户输入0，则打印 您选择的商品是汽车。
a = int(input('请输入:'))
print(googs[a])
'''

# 10.请用代码实现
# li = "alex"
# 利用下划线将列表的每一个元素拼接成字符串"a_l_e_x"
'''
s = '_'.join(li)
print(s)
'''

# 11.利用for循环和range找出 0 ~ 100 以内所有的偶数，并追加到一个列表。
'''
lst = []
for i in range(101):
    if i % 2 == 0:
        lst.append(i)
print(lst)
'''

# 12.利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并追加到一个列表。
'''
lst = []
for i in range(51):
    if i % 3 == 0:
        lst.append(i)
print(lst)
'''

# 13.利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并插入到列表的第0个索引位置，最终结果如下：
# [48,45,42...]
'''
lst = []
for i in range(51):
    if i % 3 == 0:
        lst.insert(0,i)
print(lst)
'''

# 14.查找列表li中的元素，移除每个元素的空格，并找出以"a"开头，并添加到一个新列表中,最后循环打印这个新列表。
'''
li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", "  aqc"]
li2 = []
for i in li:
    a = i.strip()
    if a.startswith('a'):
        li2.append(a)
    print(li2)
'''

# 15.判断是否可以实现，如果可以请写代码实现。
# li = ["alex",[11,22,(88,99,100,),33], "WuSir", ("ritian", "barry",), "wenzhou"]
# 请将 "WuSir" 修改成 "武沛齐"
'''
li[2] = '武沛齐'
print(li)
'''
# 请将 ("ritian", "barry",) 修改为 ['日天','日地']
'''
li[3] = ['日天','日地']
print(li)
'''
# 请将 88 修改为 87
'''
li[1][2] = (87,99,100,)
print(li)
'''
# 请将 "wenzhou" 删除，然后再在列表第0个索引位置插入 "文周"
'''
li.remove("wenzhou")
li.insert(0,"文周")
print(li)
'''

# day05
# 1.请将列表中的每个元素通过 "_" 链接起来。
'''
users = ['李少奇','李启航','渣渣辉']
a = '_'.join(users)
print(a)
'''

# 2.请将列表中的每个元素通过 "_" 链接起来。
'''
users = ['李少奇','李启航',666,'渣渣辉']
users[2] = '666'
a = '_'.join(users)
print(a)
'''

# 3.请将元组 v1 = (11,22,33) 中的所有元素追加到列表 v2 = [44,55,66] 中。
'''
v1 = (11,22,33)
v2 = [44,55,66]
v2.extend(v1)
print(v2)
'''

# 4.请将元组 v1 = (11,22,33,44,55,66,77,88,99) 中的所有偶数索引位置的元素 追加到列表 v2 = [44,55,66] 中。
'''
v1 = (11,22,33,44,55,66,77,88,99)
v2 = [44,55,66]
for i in range(len(v1)):
    if i % 2 == 0:
        v2.append(v1[i])
print(v2)
'''

# 5.将字典的键和值分别追加到 key_list 和 value_list 两个列表中，如：
'''
key_list = []
value_list = []
info = {'k1':'v1','k2':'v2','k3':'v3'}
for i in info.keys():
    key_list.append(i)
for i in info.values():
    value_list.append(i)
print(key_list,value_list)
'''

# 6.字典
# dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
# a. 请循环输出所有的key
'''
for i in dic:
    print(i)
'''
# b. 请循环输出所有的value
'''
for i in dic.values():
    print(i)
'''
# c. 请循环输出所有的key和value
'''
for i in dic.items():
    print(i)
'''
# d. 请在字典中添加一个键值对，"k4": "v4"，输出添加后的字典
'''
dic['k4'] = 'v4'
print(dic)
'''
# e. 请在修改字典中 "k1" 对应的值为 "alex"，输出修改后的字典
'''
dic['k1'] = 'alex'
print(dic)
'''
# f. 请在k3对应的值中追加一个元素 44，输出修改后的字典
'''
dic['k3'].append(44)
print(dic)
'''
# g. 请在k3对应的值的第 1 个位置插入个元素 18，输出修改后的字典
'''
dic['k3'].insert(0,18)
print(dic)
'''

# 7.请循环打印k2对应的值中的每个元素。
'''
info = {
    'k1':'v1',
    'k2':[('alex'),('wupeiqi'),('oldboy')],
}
for i in info['k2']:
    print(i)
'''

# 8.有字符串"k: 1|k1:2|k2:3 |k3 :4" 处理成字典 {'k':1,'k1':2....}
'''
s = "k: 1|k1:2|k2:3 |k3 :4"
a = s.split('|')
dic = {}
for i in a:
    k,v = i.split(':')
    v = int(v)
    dic[k] = v
print(dic)
'''

# 9.写代码,有如下值 li= [11,22,33,44,55,66,77,88,99,90] ,将所有大于 66 的值保存至字典的第一个key对应的列表中，将小于 66
# 的值保存至第二个key对应的列表中。
'''
li = [11,22,33,44,55,66,77,88,99,90]
result = {'k1':[],'k2':[]}
for i in li:
    if i > 66:
        result['k1'].append(i)
    else:
        result['k2'].append(i)
print(result)
'''

# 10.输出商品列表，用户输入序号，显示用户选中的商品
'''
# 商品列表：
goods = [
    {"name": "电脑", "price": 1999},
    {"name": "鼠标", "price": 10},
    {"name": "游艇", "price": 20},
    {"name": "美女", "price": 998}
]
# 要求:
# 1：页面显示 序号 + 商品名称 + 商品价格，如：
#       1 电脑 1999
#       2 鼠标 10
# 	    ...
# 2：用户输入选择的商品序号，然后打印商品名称及商品价格
# 3：如果用户输入的商品序号有误，则提示输入有误，并重新输入。
# 4：用户输入Q或者q，退出程序。
for i in range(len(goods)):
    print(i + 1, goods[i]['name'], goods[i]['price'])
while 1:
    a = input('请输入序号:')
    if a.upper() == 'Q':
        break
    if int(a) in range(1,len(goods)+1):
        print(goods[int(a)-1]['name'],goods[int(a)-1]['price'])
    else:
        print('输入有误!重新输入.')
'''

# 看代码写结果
'''
v = {}
for index in range(10):
    v['users'] = index
print(v)    # v = {'users':9}
'''

# day06
# 1.列举你了解的字典中的功能（字典独有）。
'''
1..keys()取所有键
2..values()取所有值
3..items()取键值对
4..get()取对应键的值
5..update()更新
'''

# 2.列举你了解的集合中的功能（集合独有）。
'''
1.intersection()交集
2.union()合集
3.difference()差集
4.symmetric_difference()对称差集
5.add()添加
6.discard()删除
7.update()更新
'''

# 3.列举你了解的可以转换为 布尔值且为False的值。
'''
0,'',{},[].set(),(,)
'''

# 4.请用代码实现
# info = {'name': '王刚蛋', 'hobby': '铁锤'}
# 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出。
# 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出（如果key不存在，则获取默认“键不存在”，并输出）。
# 注意：无需考虑循环终止（写死循环即可）
'''
while 1:
    a = input('请输入:')
    if a in info.keys():
        print(info[a])
    else:
        print('键不存在.')
'''

# 5.请用代码验证 "name" 是否在字典的键中？
'''
info = {'name': '王刚蛋', 'hobby': '铁锤', 'age': '18', }
for i in info.keys():
    if 'name' == i:
        print('存在')
'''

# 6.请用代码验证 "alex" 是否在字典的值中？
'''
info = {'name':'王刚蛋','hobby':'铁锤','age':'18',}
if 'alex' in info.values():
    print('存在')
'''

# 7.有如下
# v1 = {'武沛齐', '李杰', '太白', '景女神'}
# v2 = {'李杰', '景女神'}
# 请得到 v1 和 v2 的交集并输出
'''
v = v1.intersection(v2)
print(v)
'''
# 请得到 v1 和 v2 的并集并输出
'''
v = v1.union(v2)
print(v)
'''
# 请得到 v1 和 v2 的 差集并输出
'''
v = v1.difference(v2)
print(v)
'''
# 请得到 v2 和 v1 的 差集并输出
'''
v = v2.difference(v1)
print(v)
'''

# 8.循环提示用户输入，并将输入内容追加到列表中（如果输入N或n则停止循环）
'''
lst = []
while 1:
    a = input('请输入:')
    if a.upper() == 'N':
        break
    lst.append(a)
print(lst)
'''

# 9.循环提示用户输入，并将输入内容添加到集合中（如果输入N或n则停止循环）
'''
st = set()
while 1:
    a = input('请输入:')
    if a.upper() == 'N':
        break
    st.add(a)
print(st)
'''

# 10.写代码实现
'''
v1 = {'alex','武sir','肖大'}
v2 = []
# 循环提示用户输入，如果输入值在v1中存在，则追加到v2中，如果v1中不存在，则添加到v1中。（如果输入N或n则停止循环）
while 1:
    a = input('请输入:')
    if a.upper()=='N':
        break
    if a in v1:
        v2.append(a)
    else:
        v1.add(a)
print(v1,v2)
'''

# 11.判断以下值那个能做字典的key ？那个能做集合的元素？
# 1 ✔✔
# -1    ✔✔
# ""    ✔✔
# None  ✔✔
# [1,2] ✖✖
# (1,)  ✔✔
# {11,22,33,4}  ✖✖
# {'name':'wupeiq','age':18}    ✖✖

# 12.is 和 == 的区别？
'''
is查看内存地址是否相同
==查看值是否相同
'''

# 13.type使用方式及作用？
'''
type()里放入内容,可以查看数据类型.
'''

# 14.id的使用方式及作用？
'''
id()里放入内容,可查看内存地址.
'''

# 15.看代码写结果并解释原因
'''
v1 = {'k1': 'v1', 'k2': [1, 2, 3]}
v2 = {'k1': 'v1', 'k2': [1, 2, 3]}

result1 = v1 == v2
result2 = v1 is v2
print(result1)  # True
print(result2)  # False
'''

# 16.看代码写结果并解释原因
'''
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = v1

result1 = v1 == v2
result2 = v1 is v2 
print(result1)  # True
print(result2)  # True
'''

# 17.看代码写结果并解释原因
'''
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = v1

v1['k1'] = 'wupeiqi'
print(v2)   # {'k1':'wupeiqi','k2':[1,2,3]}
'''

# 18.看代码写结果并解释原因
'''
v1 = '人生苦短，我用Python'
v2 = [1, 2, 3, 4, v1]
v1 = "人生苦短，用毛线Python"
print(v2)   # v2 = [1, 2, 3, 4, '人生苦短，我用Python']
'''

# 19.看代码写结果并解释原因
'''
info = [1, 2, 3]
userinfo = {'account': info, 'num': info, 'money': info}
info.append(9)
print(userinfo) # {'account': [1, 2, 3,9], 'num': [1, 2, 3,9], 'money': [1, 2, 3,9]}
info = "题怎么这么多"
print(userinfo) # {'account': [1, 2, 3,9], 'num': [1, 2, 3,9], 'money': [1, 2, 3,9]}
'''

# 20.看代码写结果并解释原因
'''
info = [1, 2, 3]
userinfo = [info, info, info, info, info]
info[0] = '不仅多，还特么难呢'
print(info, userinfo)   # ['不仅多，还特么难呢', 2, 3]   [['不仅多，还特么难呢', 2, 3], ['不仅多，还特么难呢', 2, 3], ['不仅多，还特么难呢', 2, 3], ['不仅多，还特么难呢', 2, 3], ['不仅多，还特么难呢', 2, 3]]
'''

# 21.看代码写结果并解释原因
'''
info = [1, 2, 3]
userinfo = [info, info, info, info, info]
userinfo[2][0] = '闭嘴'
print(info, userinfo)   # ['闭嘴', 2, 3]  [['闭嘴', 2, 3], ['闭嘴', 2, 3], ['闭嘴', 2, 3], ['闭嘴', 2, 3], ['闭嘴', 2, 3]]
'''

# 22.看代码写结果并解释原因
'''
info = [1, 2, 3]
user_list = []
for item in range(10):
    user_list.append(info)
info[1] = "是谁说Python好学的？"
print(user_list)    # [[1, "是谁说Python好学的？", 3]*10]
'''

# 23.看代码写结果并解释原因
'''
data = {}
for i in range(10):
    data['user'] = i
print(data) # {'user':9}
循环给user值
'''

# 24.看代码写结果并解释原因
'''
data_list = []
data = {}
for i in range(10):
    data['user'] = i
    data_list.append(data)
print(data_list) # [{'user':9}*10]
'''

# 25.看代码写结果并解释原因
'''
data_list = []
for i in range(10):
    data = {}
    data['user'] = i
    data_list.append(data)
print(data_list)    # [{'user':0},{'user':1}...{'user':9}]
'''