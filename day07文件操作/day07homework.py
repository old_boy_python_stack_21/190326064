#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.看代码写结果
'''
v1 = [1, 2, 3, 4, 5]
v2 = [v1, v1, v1]
v1.append(6)    # 内部修改,都改变
print(v1)       # [1, 2, 3, 4, 5,6]
print(v2)       # v2 = [[1, 2, 3, 4, 5,6], [1, 2, 3, 4, 5,6], [1, 2, 3, 4, 5,6]]
'''

# 2.看代码写结果
'''
v1 = [1, 2, 3, 4, 5]
v2 = [v1, v1, v1]
v2[1][0] = 111      # 内部修改 v1 = [111, 2, 3, 4, 5]
v2[2][0] = 222      # 内部修改 v1 = [222, 2, 3, 4, 5]
print(v1)           # v1 = [222, 2, 3, 4, 5]
print(v2)           # v2 = [[222, 2, 3, 4, 5], [222, 2, 3, 4, 5], [222, 2, 3, 4, 5]]
'''

# 3.看代码写结果，并解释每一步的流程。
'''
v1 = [1,2,3,4,5,6,7,8,9]
v2 = {}
for item in v1:
    if item < 6:
        continue
    if 'k1' in v2:
        v2['k1'].append(item)
    else:
        v2['k1'] = [item]      # v2 = {'k1':[6,7,8,9]}
print(v2)
'''

# 4.简述深浅拷贝？
'''
深拷贝:拷贝嵌套层次中的所有可变类型.
浅拷贝:只拷贝第一层.
'''

# 5.看代码写结果
'''
import copy

v1 = "alex"
v2 = copy.copy(v1)          # 浅拷贝 v2 = v1,指向同一内存地址
v3 = copy.deepcopy(v1)      # 深拷贝 v3 = v1
print(v1 is v2)             # True
print(v1 is v3)             # True
'''

# 6.看代码写结果
'''
import copy

v1 = [1, 2, 3, 4, 5]
v2 = copy.copy(v1)      # v2 == v1
v3 = copy.deepcopy(v1)  # v3 == v1
print(v1 is v2)         # False
print(v1 is v3)         # False
'''

# 7.看代码写结果
'''
import copy

v1 = [1, 2, 3, 4, 5]
v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)
print(v1[0] is v2[0])   # True
print(v1[0] is v3[0])   # True
print(v2[0] is v3[0])   # True
'''

# 8.看代码写结果
'''
import copy

v1 = [1, 2, 3, 4, 5]
v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)
print(v1[0] is v2[0])  # True
print(v1[0] is v3[0])  # True
print(v2[0] is v3[0])  # True
'''

# 9.看代码写结果
'''
import copy

v1 = [1, 2, 3, {"name": '武沛齐', "numbers": [7, 77, 88]}, 4, 5]
v2 = copy.copy(v1)
print(v1 is v2)     # False
print(v1[0] is v2[0])   # True
print(v1[3] is v2[3])   # True
print(v1[3]['name'] is v2[3]['name'])   # True
print(v1[3]['numbers'] is v2[3]['numbers']) # True
print(v1[3]['numbers'][1] is v2[3]['numbers'][1])   # True
'''

# 10.看代码写结果
'''
import copy

v1 = [1, 2, 3, {"name": '武沛齐', "numbers": [7, 77, 88]}, 4, 5]
v2 = copy.deepcopy(v1)
print(v1 is v2)     # False
print(v1[0] is v2[0])   # True
print(v1[3] is v2[3])   # False
print(v1[3]['name'] is v2[3]['name'])   # True
print(v1[3]['numbers'] is v2[3]['numbers'])     # False
print(v1[3]['numbers'][1] is v2[3]['numbers'][1])   # True
'''

# 11.简述文件操作的打开模式
'''
r,只读模式,无文件报错.
w,只写模式,写入会先清空并覆盖原内容,无文件新建.
a,追加模式,无文件新建,在文件最后追加内容.
r+,读写模式,从光标位置读取,seek()定位光标,写入可能会覆盖原内容.
w+,写读模式,写入前先清空原内容,读取需要seek()定位光标.
a+,追加写读,根据光标位置读取,写入在最后.
'''

# 12.请将info中的值使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = ['骗子，','不是','说','只有','10','个题吗？']
s = '_'.join(info)
f = open('readme.txt',mode='w',encoding='utf-8')
f.write(s)
f.close()
'''

# 13.请将info中的值使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = ['骗子，','不是','说','只有',10,'个题吗？']
info[4] = str(info[4])
s = '_'.join(info)
f = open('readme.txt',mode='w',encoding='utf-8')
f.write(s)
f.close()
'''

# 14.请将info中的所有键 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
# info = {'name': '骗子', 'age': 18, 'gender': '性别'}
# 1. 请将info中的所有键 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
s = '_'.join(info)
f = open('readme.txt',mode='w',encoding='utf-8')
f.write(s)
f.close()
'''
# 2. 请将info中的所有值 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
lst = list(info.values())
lst[1] = str(lst[1])
s = '_'.join(lst)
f = open('readme.txt', mode='w', encoding='utf-8')
f.write(s)
f.close()
'''
# 3. 请将info中的所有键和值按照 "键|值,键|值,键|值" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = {'name': '骗子', 'age': 18, 'gender': '性别'}
s = ''
info['age'] = str(info['age'])
for k,v in info.items():
    a = k +'|' + v + ','
    s += a
f = open('readme.txt',mode='w',encoding='utf-8')
f.write(s[0:-1])
f.close()
'''

# 15.写代码
# 要求：
#     如文件 data.txt 中有内容如下：
#     wupeiqi|oldboy|wupeiqi@xx.com
#     alex|oldboy|66s@xx.com
#     xxxx|oldboy|yuy@xx.com
#     请用代码实现读入文件的内容，并构造成如下格式：
# info = [
#         {'name':'wupeiqi','pwd':'oldboy','email':'wupeiqi@xx.com'},
#         {'name':'alex','pwd':'oldboy','email':'66s@xx.com'},
#         {'name':'xxxx','pwd':'oldboy','email':'yuy@xx.com'},
# ]
'''
info = []
f = open('data.txt',mode='r',encoding='utf-8')
for i in f.readlines():
    dic = {}
    new_i = i.strip()
    new2_i = new_i.split('|')
    dic['name'] = new2_i[0]
    dic['pwd'] = new2_i[1]
    dic['email'] = new2_i[2]
    info.append(dic)
print(info)
f.close()
'''
