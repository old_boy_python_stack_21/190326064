#!/usr/bin/env python
# -*- coding:utf-8 -*-
from day23.obj.src.register import register
from day23.obj.src.login import login
from day23.obj.src.article_list import article_list


def run():
    """
    主函数
    :return:
    """
    print('=================== 系统首页 ===================')
    func_dict = {'1': register, '2': login, '3': article_list}
    while True:
        print('1.注册；2.登录；3.文章列表')
        choice = input('请选择序号：')
        if choice.upper() == 'N':
            return
        func = func_dict.get(choice)
        if not func:
            print('序号选择错误')
            continue
        func()


if __name__ == '__main__':
    run()
