#!/usr/bin/env python
# -*- coding:utf-8 -*-
import hashlib
from day23.obj.config import settings
import importlib
importlib.reload(settings)

def encrypt_md5(arg):
    """
    md5加密
    :param arg:
    :return:
    """
    m = hashlib.md5()
    m.update(arg.encode('utf-8'))
    return m.hexdigest()
