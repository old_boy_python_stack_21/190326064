#!/usr/bin/env python
# -*- coding:utf-8 -*-
from day23.obj.db.usermsg import USER_DICT
from day23.obj.lib.md import encrypt_md5
from day23.obj.config import settings
import importlib
importlib.reload(settings)

def register():
    """
    用户注册
    :return:
    """
    print('用户注册')
    while True:
        user = input('请输入用户名(N返回上一级)：')
        if user.upper() == 'N':
            return
        pwd = input('请输入密码：')
        if user in USER_DICT:
            print('用户已经存在，请重新输入。')
            continue
        USER_DICT[user] = encrypt_md5(pwd)
        print('%s 注册成功' % user)
        print(USER_DICT)

