#!/usr/bin/env python
# -*- coding:utf-8 -*-
from day23.obj.config import settings
import importlib
importlib.reload(settings)


def show_article_detail(row_dict):
    print('=================== 文章详细 ===================')
    msg = '%s\n%s\n赞(%s) 评论(%s)' % (row_dict['title'], row_dict['content'], row_dict['up'], len(row_dict['comment']))
    print(msg)
    if len(row_dict['comment']):
        print('评论列表(%s)' % len(row_dict['comment']))
        for item in row_dict['comment']:
            comment = "    %s - %s" % (item['data'], item['user'])
            print(comment)
