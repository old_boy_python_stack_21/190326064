#!/usr/bin/env python
# -*- coding:utf-8 -*-
from day23.obj.src.show_article_detail import show_article_detail
from day23.obj.src.up_comment import article_up, article_comment
from day23.obj.src.login import login
from day23.obj.config import settings
import importlib
importlib.reload(settings)


def article_detail(row_dict):
    """
    文章详细
    :param row_dict:
    :return:
    """
    show_article_detail(row_dict)
    func_dict = {'1': article_up, '2': article_comment}
    while True:
        print('1.赞；2.评论；')
        choice = input('请选择（N返回上一级）：')
        if choice.upper() == 'N':
            return
        func = func_dict.get(choice)
        if not func:
            print('选择错误，请重新输入。')
        result = func(row_dict)
        if result:
            show_article_detail(row_dict)
            continue

        print('用户未登录，请登录后再进行点赞和评论。')
        to_login = input('是否进行登录？yes/no：')
        if to_login == 'yes':
            login()
