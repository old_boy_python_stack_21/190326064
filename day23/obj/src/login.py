#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
from day23.obj.db.usermsg import USER_DICT
from day23.obj.lib.md import encrypt_md5
from day23.obj.config import settings
import importlib
importlib.reload(settings)


def login():
    """
    用户登录
    :return:
    """
    print('用户登录')
    while True:
        user = input('请输入用户名(N返回上一级)：')
        if user.upper() == 'N':
            return
        pwd = input('请输入密码：')
        if user not in USER_DICT:
            print('用户名不存在')
            continue

        encrypt_password = USER_DICT.get(user)
        if encrypt_md5(pwd) != encrypt_password:
            print('密码错误')
            continue

        print('登录成功')
        global CURRENT_USER
        with open(r'D:\homework\day23\obj\config\settings.py', 'r', encoding='utf-8') as f1,\
                open(r'D:\homework\day23\obj\config\settings2.py', 'w', encoding='utf-8') as f2:
            f2.write("CURRENT_USER = '%s'" % user)
        os.remove(r'D:\homework\day23\obj\config\settings.py')
        os.rename(r'D:\homework\day23\obj\config\settings2.py', r'D:\homework\day23\obj\config\settings.py')
        return
