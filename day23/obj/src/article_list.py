#!/usr/bin/env python
# -*- coding:utf-8 -*-
from day23.obj.db.user import ARTICLE_LIST
from day23.obj.src.article_detail import article_detail
from day23.obj.config import settings
import importlib
importlib.reload(settings)


def article_list():
    """
    文章列表
    :return:
    """
    while True:
        print('=================== 文章列表 ===================')
        for i in range(len(ARTICLE_LIST)):
            row = ARTICLE_LIST[i]
            msg = """%s.%s \n  赞(%s) 评论(%s)\n""" % (i + 1, row['title'], row['up'], len(row['comment']))
            print(msg)
        choice = input('请选择要查看的文章(N返回上一级)：')
        if choice.upper() == 'N':
            return
        choice = int(choice)
        choice_row_dict = ARTICLE_LIST[choice - 1]
        article_detail(choice_row_dict)
