#!/usr/bin/env python
# -*- coding:utf-8 -*-
from day23.obj.config.settings import CURRENT_USER
from day23.obj.config import settings
import importlib
importlib.reload(settings)


def auth(func):
    def inner(*args, **kwargs):
        if CURRENT_USER:
            return func(*args, **kwargs)
        return False

    return inner


@auth
def article_up(row_dict):
    """
    点赞文章
    :param row_dict:
    :return:
    """
    row_dict['up'] += 1
    print('点赞成功')
    return True


@auth
def article_comment(row_dict):
    """
    评论文章
    :param row_dict:
    :return:
    """
    while True:
        comment = input('请输入评论（N返回上一级）：')
        if comment.upper() == 'N':
            return True
        row_dict['comment'].append({'data': comment, 'user': CURRENT_USER})
        print('评论成功')
