# day23笔记

## 一.补充,作业

### 1.字符串格式化

```python
msg = "我是%(n1)s,年龄%(n2)s" % {'n1': 'alex', 'n2': 123, }
print(msg)
```

```python
# v1 = "我是{0},年龄{1}".format('alex',19)位置参数
v1 = "我是{0},年龄{1}".format(*('alex',19,))
print(v1)

# v2 = "我是{name},年龄{age}".format(name='alex',age=18)关键字参数
v2 = "我是{name},年龄{age}".format(**{'name':'alex','age':18})
print(v2)
```

### 2.有序字典

```python
from collections import OrderedDict

info = OrderedDict()
info['k1'] = 123
info['k2'] = 456

print(info.keys())
print(info.values())
print(info.items())
```

###    3.反射

```python
class Cloud(object):

    def upload(self):
        pass

    def download(self):
        pass

    def run(self):
        # up|C:/xxx/xxx.zip
        # down|xxxx.py
        value = input('请用户输入要干什么？')
        action = value.split('|')[0]
        # 最low的形式
        if action == 'up':
            self.upload()
		elif action == 'down':
            self.download()
		else:
            print('输入错误')
		
		# 构造字典 (*)
		method_dict = {'up':self.upload, 'down':self.download}
         method = method_dict.get(action)
		method()
         
		# 反射（*）
         method = getattr(self,action) # upload/download(方法名)
		method()
```

## 二.单例模式(23种)

- 无论实例化多少次，永远用的都是第一次实例化出的对象.

- 标准(单例类名用Singleton)

  ```python
  class Singleton(object):
      instance = None
      def __new__(cls, *args, **kwargs):
          if not cls.instance:
              cls.instance = object.__new__(cls)
          return cls.instance
  
  obj1 = Singleton()
  obj2 = Singleton()
  
  # 不是最终，还有加锁。
  ```

- 文件的连接池

  ```python
  class FileHelper(object):
      instance = None
      def __init__(self, path):
          self.file_object = open(path,mode='r',encoding='utf-8')
  
      def __new__(cls, *args, **kwargs):
          if not cls.instance:
              cls.instance = object.__new__(cls)
          return cls.instance
  
  obj1 = FileHelper('x')
  obj2 = FileHelper('x')
  ```

## 三.模块导入

- 多次导入重新加载

  ```python
  import jd # 第一次加载：会加载一遍jd中所有的内容。
  import jd # 由已经加载过，就不在加载。
  print(456)
  ```

## 三.模块导入

- 非要加载的话

  ```python
  import importlib
  import jd
  importlib.reload(jd)
  print(456)
  ```

- 通过模块导入实现单例模式:

```python
# jd.py
class Foo(object):
    pass

obj = Foo()
```

```python
# app.py
import jd # 加载jd.py，加载最后会实例化一个Foo对象并赋值给obj
print(jd.obj)
```

## 四.日志(logging)

- 日志处理本质 : Logger/FileHandler/Formatter

- 处理日志方式 :

  ```python
  import logging
  
  file_handler = logging.FileHandler(filename='x1.log', mode='a', encoding='utf-8',)
  logging.basicConfig(
      format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
      datefmt='%Y-%m-%d %H:%M:%S %p',
      handlers=[file_handler,],
      level=logging.ERROR
  )
  
  logging.error('你好')
  ```

- 推荐处理日志方式 + 日志分割

  ```python
  import time
  import logging
  from logging import handlers
  # file_handler = logging.FileHandler(filename='x1.log', mode='a', encoding='utf-8',)
  file_handler = handlers.TimedRotatingFileHandler(filename='x3.log', when='s', interval=5, encoding='utf-8')
  logging.basicConfig(
      format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
      datefmt='%Y-%m-%d %H:%M:%S %p',
      handlers=[file_handler,],
      level=logging.ERROR
  )
  
  for i in range(1,100000):
      time.sleep(1)
      logging.error(str(i))
  ```

- 注意事项

  ```python
  # 在应用日志时，如果想要保留异常的堆栈信息。
  import logging
  import requests
  
  logging.basicConfig(
      filename='wf.log',
      format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
      datefmt='%Y-%m-%d %H:%M:%S %p',
      level=logging.ERROR
  )
  
  try:
      requests.get('http://www.xxx.com')
  except Exception as e:
      msg = str(e) # 调用e.__str__方法
      logging.error(msg,exc_info=True)
  ```



## 五.项目结构目录

- 脚本

  ```python
  import re		# 内置函数从短到长
  import json
  import datetime
  
  import xlrd		# 第三方放在下面
  import requests
  
  class 类名(object):	# 类
      pass
  
  def run():		# 函数
      pass
  
  if __name__ == '__main__':		# 主函数
      run()
  ```

  

- 单可执行文件

  ```python
  山西-高启芝 2019/4/28 19:58:58
  pro   			# 项目名称总目录
  ├── app.py  	 # application程序入口
  ├── config 		# 配置文件目录
  ├── db 			# datebase数据目录
  │   └── user.txt
  ├── lib  		# 公有性质的模块放置目录
  │   └── page.py
  └── src  		# 功能性函数目录
      ├── account.py  # 其他类或函数文件
      ├── order.py  	# 其他函数
      └── run.py  	# 主函数文件
  ```

  

- 多可执行文件

  ```python
  pro 					# 项目名称总目录
  ├── bin					# 程序入口目录
  │   ├── admin.py		 # 管理员入口
  │   ├── student.py		 # 学生入口
  │   └── teacher.py		 # 教师入口
  ├── config				# 配置文件夹
  │   └── settings.py		 # 配置
  ├── db					# datebase数据目录
  │   └── user.txt
  ├── lib					# 公有性质的模块放置目录
  │   └── page.py
  ├── log
  │   └── test.log
  └── src 				# 功能性函数目录
      ├── account.py 		 # 其他类或函数文件
      ├── order.py  	     # 其他函数
      └── run.py			 # 主函数文件
  
  ```

  