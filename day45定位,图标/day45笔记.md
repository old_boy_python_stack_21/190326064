# day45笔记

## 1.回顾

### 1.伪类选择器

```css
a:link{} 没有被访问过时a标签的样式
a:visited{} 访问过后的
a:hover{} 悬浮时
a:active{} 摁住的时候
```

### 2.如何在p标签的后面添加''&'内容？

```html
<style>
    p::after{
        /*行内元素*/
        content:'&',
        color:red;
        font-size: 20px;
    }
</style>
<p>wusir</p>
```

### 3.设置网页字体使用哪个属性，备选字体如何写？

```css
font-family:'宋体','楷体';
```

### 4.如何设置文字间距和英文单词间距？

```css
文字间距:letter-spacing
英文单词间距:word-spacing
```

### 5.字体加粗使用哪个属性，它的取值有哪些？

```css
font-weight:lighter| normal | bold |bolder| 100~900 字体加粗
font-style:italic;/*斜体*/
```

### 6.文本修饰用哪个属性，它的取值有哪些？

```css
text-decoration:none| underline | overline | line-through
```

### 7.分别说明px,em,rem单位的意思

```
px: 绝对单位  固定不变的尺寸
em和rem :相对单位     跟随font-size
	em:相对于当前的盒子
	rem:相对于根元素（html）
```

### 8.如何设置首行缩进，一般使用什么单位？

```
em
```

### 9.文本水平排列方式是哪个属性，它的取值有？

```css
text-align:left | center | right | justify(仅限于英文，两端对齐)
```

### 10.如何让一个盒子水平居中？

```css
盒子必须有宽度和高度，再设置margin: 0 auto;
让文本水平居中： text-align:center;
让文本垂直居中：line-height = height
```

### 11.margin在垂直方向上会出现什么现象？

```css
外边距合并，“塌陷”
尽量避免出现塌陷问题，只要设置一个方向的margin
```

### 12.如果让两个盒子并排在一行上显示？

```html
1.float浮动属性
2.设置盒子的显示方式 display:inline | inline-block;
```

### 13.简单阐述一下，浮动对盒子产生了什么现象？

```
1.脱离标准文档流，不在页面上占位置 “脱标”
2.文字环绕 设置浮动属性的初衷
3.“贴边” 现象： 给盒子设置了浮动，会找浮动盒子的边，如果找不到浮动盒子的边，会贴到父元素的边，如果找到了，就会贴到浮动盒子的边上
4.收缩效果

有点像 一个块级元素转成行内块
```

## 2.今日内容

### 1.浮动

- 布局方案
- 作用:实现元素并排

#### 1.浮动的现象

- 脱离了标准文档流,不在页面上占位置
- 贴边现象
- 收缩效果
- 文字环绕效果

#### 2.浮动带来的问题

- 撑不起父盒子的高度
- 给父元素添加固定高度 （不灵活，后期不易维护）
- 应用:万年不变导航栏

```css
<head>
    <style>
        *{
            padding: 0;
            margin: 0;
        }
        .father{
            width: 800px;
            margin: 100px auto;
            height: 400px;
            border: 1px solid #000;
        }
        .child1{
            /*width: 200px;*/
            height: 400px;
            background-color: red;
            float: left;
        }
        .child2{
            /*width: 300px;*/
            height: 200px;
            background-color: green;
            float: right;
        }
    </style>
</head>
<body>
    <div class="father">
        <div class="child1">mjj</div>
        <div class="child2">wusir</div>
    </div>
</body>
```



#### 3.清除浮动带来的问题的方式

- 内墙法
  - 给最后一个浮动元素的后面添加一个空的块级标签，并且设置该标签的属性为clear:both;
  - 问题：冗余

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .clear{
            clear: both;
        }
    </style>
</head>
<body>
    <div class="father clearfix">
        <div class="child1">mjj</div>
        <div class="child2">wusir</div>
        <div class="clear"></div> #设置空块级标签
    </div>
</body>
```

- 伪元素清除法 推荐

```css
.clearfix::after{
    content:'';
    display: block;
    clear: both;
    /*visibility: hidden;*/
    /*height: 0;*/
}
```

- overflow  超出部分默认可见
  - visible  默认可见
  - hidden  隐藏
  - scroll  滚动
  - inherit  继承

```html
<head>
    <style>
        body{
            overflow: hidden;
        }
        .box{
            width: 200px;
            height: 200px;
            border: 1px solid #000;
            /*overflow: visible;!*默认可见*!*/
            /*overflow: hidden;*/
            overflow: scroll;
            /*overflow: inherit;*/
        }
    </style>
</head>
<body>
    <div class="box">
        脱离标准文档流，不在页面上占位置 “脱标”
2.文字环绕 设置浮动属性的初衷
3.“贴边” 现象： 给盒子设置了浮动，会找浮动盒子的边，如果找不到浮动盒子的边，会贴到父元素的边，如果找到了，就会贴到浮动盒子的边上
4.收缩效果脱离标准文档流，不在页面上占位置 “脱标”
2.文字环绕 设置浮动属性的初衷
3.“贴边” 现象： 给盒子设置了浮动，会找浮动盒子的边，如果找不到浮动盒子的边，会贴到父元素的边，如果找到了，就会贴到浮动盒子的边上
4.收缩效果脱离标准文档流，不在页面上占位置 “脱标”
    </div>
</body>
```

- 一旦设置一个Box盒子 除了overflow：visible；它会形成一个BFC,BFC它有布局规则： 它会让内部浮动元素计算高度

```
overflow:hidden;会形成BFC区域，形成BFC区域之后，内部有它的布局规则
计算BFC的高度时，浮动元素也参与计算,但是小心overflow：hidden它自己的本意
```

#### 4.BFC 块级格式化上下文

- 哪些元素会生成BFC

```
1.根元素
2.float属性不为none
3.position为absolute或fixed
4.display为inline-block
5.overflow不为visible
```

- BFC布局规则

```
1.内部的Box会在垂直方向，一个接一个地放置。
2.Box垂直方向的距离由margin决定。属于同一个BFC的两个相邻Box的margin会发生重叠(塌陷)
3.每个元素的margin box的左边， 与包含块border box的左边相接触(对于从左往右的格式化，否则相反)。即使存在浮动也是如此。
4.BFC的区域不会与float 元素重叠。
5.BFC就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此。
6.计算BFC的高度时，浮动元素也参与计算
```



### 2.定位

- position:static静态  relative相对  absolute绝对  fixed固定
- top/left/right都是在有position时才起作用

#### 1.相对定位 relative

- 特征:
  - 与标准文档流下的盒子没有区别
  - 留坑,会影响页面布局
- 作用
  - 做“子绝父相”布局方案的参考
- 参考点
  - 以原来的盒子为参考点

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .box {
            width: 500px;
            height: 600px;
            border: 1px solid #000;
        }
        .box .a {
            width: 200px;
            height: 200px;
            background-color: red;
        }
        .box .b {
            width: 200px;
            height: 200px;
            background-color: green;
            position: relative;
            top: 30px;
            left: 50px;
        }
        .box .c {
            width: 200px;
            height: 200px;
            background-color: blue;
        }
    </style>
</head>
<body>
<div class="box">
    <div class="a"></div>
    <div class="b"></div>
    <div class="c"></div>
</div>
</body>
```

#### 2.绝对定位 absolute

##### 1.单独设置一个盒子为绝对定位参考点

```
以top描述，它的参考点是以body的（0，0）为参考点
以bottom描述，它的参考点是以浏览器的左下角为参考点
```

##### 2.子绝父相

- 子代position:absolute  父代relative
- 以最近的父辈元素的左上角为参考点进行定位

##### 3.特征

- 1.脱标 - 脱离了标准文档流
- 2.压盖 - 层级提高
- 3.子绝父相

```html
<head>
    <style>
        .box {
            width: 500px;
            height: 600px;
            border: 1px solid #000;
            position: relative;
            float: right;
        }
        .box .a {
            width: 200px;
            height: 200px;
            background-color: red;
        }
        .box .b {
            width: 200px;
            height: 200px;
            background-color: green;
            position: absolute;
            top: 20px;
            left: 20px;
        }
        .box .c {
            width: 300px;
            height: 200px;
            background-color: blue;
        }
    </style>
</head>
<body>
<div class="box">
    <div class="a"></div>
    <div class="b"></div>
    <div class="c"></div>
</div>
</body>
```



## 3.iconfont图标

- 进入iconfont,选择图标,右侧添加项目,选择下载使用或在线链接
- 网页上方帮助或项目中复制代码即可

```html
<head>
    <link rel="stylesheet" href="css地址">
    <style>
        i.active{
            font-size: 30px;# 调整大小
        }
    </style>
</head>
<body>
    <i class="iconfont active">图标编号</i>
    <input type="submit" value="图标编号" class="iconfont">
</body>
```


