#!/usr/bin/env python
# -*- coding:utf-8 -*-
#!/usr/bin/env python
# -*- coding:utf-8 -*-

# day08 exam
# 12.反转 v = '全栈21期'
'''
v = '全栈21期'
print(v[::-1])
'''

# 15.写代码将 v = 'k1|v1,k2|v2,k3|v3...'转换成字典{'k1':'v1','k2':'v2','k3':'v3'...}
'''
v = 'k1|v1,k2|v2,k3|v3'
dic = {}
lst = v.split(',')
for i in lst:
    k,v = i.split('|')
    dic[k] = v
print(dic)
'''

# 16.实现整数乘法计算器.
'''
content = input('内容:')
lst = content.split('*')
res = 1
for i in lst:
    res *= int(i)
print(res)
'''

# 17.看代码写结果
'''
v1 = [1, 2, 3, 4, 5]
v2 = [v1, v1, v1]
v2[1][0] = 111
v2[2][0] = 222
print(v1,v2)        # [222, 2, 3, 4, 5] [[222, 2, 3, 4, 5],[222, 2, 3, 4, 5],[222, 2, 3, 4, 5]]
'''

# 18.将info中偶数位索引使用*拼接,写入到a.log中.
'''
info = ['你是','到底','是','不是','一个','魔鬼']
info2 = info[::2]
s = '*'.join(info2)
f = open('a.log',mode='w',encoding='utf-8')
f.write(s)
f.close()
'''

# 19.读取100g的文件,检测文件中是否有关键字keys=['苍老师','小泽老师','alex'],如果有则替换为***,并写入到另外一个文件中.
'''
keys=['苍老师','小泽老师','alex']
with open('goods.txt', mode='r', encoding='utf-8') as f1, open('res', mode='w', encoding='utf-8') as f2:
    for line in f1:
        for i in keys:
            if i in line:
                line = line.replace(i,'***')
        f2.write(line)
'''

# 20.车牌区域划分.写入字典.
'''
cars = ['鲁A32444', '鲁B12333', '京B8989M', '黑C40678', '黑C46555', '沪B25041','鲁SDKJ124']
dic = {}
for i in cars:
    if i[0] not in dic:
        dic[i[0]] = 1
    elif i[0] in dic:
        dic[i[0]] += 1
print(dic)
'''

# 21.将data.txt利用文件操作构成如下类型:info = [{'id':'1','name':'alex','age':'22','phone':'13231213132','job':'IT'}...]
'''
lst = []
with open('data.txt',mode='r',encoding='utf-8') as f:
    first_line = f.readline().split(',')
    for line in f:
        dic = {}
        val = line.split(',')
        dic[first_line[0]] = val[0]
        dic[first_line[1]] = val[1]
        dic[first_line[2]] = val[2]
        dic[first_line[3]] = val[3]
        dic[first_line[4].strip()] = val[4].strip()
        lst.append(dic)
print(lst)
'''

# 22.for循环打印9*9乘法表.
'''
for a in range(1, 10):
    for b in range(1, a+1):
        print('%s*%s=%s ' % (b, a, a * b),end='')
        if a == b:
            print('')
'''

