# day08笔记

## 一.进制

二进制:0,1,10,11,...

八进制:0,1,...,7,10...

十进制.

十六进制:0,1,...,9,a,b,c,d,e,f,10...	\x

## 二.文件操作

1.一般用于文字写入:

f = open('file_path',mode='r/w/a',encoding='utf-8')

data = f.read()

f.write()

f.close()

2.一般用于图片/音频/视频/未知编码

f = open('a.txt',mode = 'wb')

date = ''

content = date.encode('utf-8')

f.write(content)

f.close()

3.模式

只读只写:r /w /a

可读可写:r+ / w+ / a+

只读只写二进制:rb / wb / ab

