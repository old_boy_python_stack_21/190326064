#!/usr/bin/env python
# -*- coding:utf-8 -*-
# info = {'name':'刘伟达','age':18,'gender':'男','hobby':'同桌'}
# for k,v in info.items():
#     print(k,v)
# k = input('请输入:')
# print(info[k])

# dic = {}
# while 1:
#     start = input('开始输入,输入N退出:')
#     if start.upper() == 'N':
#         break
#     k = input('key:')
#     v = input('value:')
#     dic[k] = v
# print(dic)


# info = {}
# message = "k1|v1,k2|v2,k3|123"
# a = message.split(',')
# for i in a:
#     k,v = i.split('|')
#     info[k] = v
# print(info)


# li = []
# while 1:
#     u = input('请输入用户名:')
#     if u.upper() == 'N':
#         break
#     p = input('请输入密码:')
#     v = {}
#     v['user'] = u
#     v['pwd'] = p
#     li.append(v)
# print(li)
#
# user = input('用户名:')
# pwd = input('密码:')
# for i in li:
#     if user == i['user'] and pwd == i['pwd']:
#         s = True
#     else:
#         s = False
# if s:
#     print('登陆成功!')
# else:
#     print('用户名或密码错误')

'''
构建用户列表,格式为:
[{'user': 'k1', 'value': 'v1'},
{'user': 'k2', 'value': 'v2'},
{'user': 'k3', 'value': 'v3'}]
Q为停止录入,然后使用用户列表中的内容登陆,失败则重新输入.
'''
lst = []
while 1:
    dic = {}
    a = input('用户名:')
    if a.upper() == "Q":
        break
    b = input('密码:')
    dic['user'] = a
    dic['value'] = b
    lst.append(dic)

print(lst)
while 1:
    username = input('请输入用户名:')
    password = input('请输入密码:')
    message = '登录失败!请重试!'
    for i in lst:
        if i['user'] == username and i['value'] == password:
            message = '登陆成功!'
            print(message)
            break
    if message == '登陆成功!':
        break
    print(message)
