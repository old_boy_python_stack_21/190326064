# day05笔记

## 1.字典

### 1.格式

dic = {键:值,键:值...}

独有功能:

1.dic.keys():取所有键

2.dic.values():取所有值

3.dic.items():取所有键值对



公共功能:

1.len():字符串长度

2.索引:dic[key]

3.for循环

4.修改:dic[k] = v,有k则更改,无k则添加

5.删除:del dic[k]