# Day40笔记

## 1.回顾

- 约束
  - not null    非空(严格模式会影响非空设置的效果)
  - default    默认值
  - unique    唯一
    - 联合唯一 unique(字段1,字段2)
  - auto_increment   自增
    - 前提:数字且唯一
    - 自带:非空 自增
  - unsigned    无符号正数
  - primary key   主键
    - 一张表只能有一个,且最好有一个主键.
    - 约束:非空且唯一
    - 联合主键:primary key(字段1,字段2)
  - foreign key   外键
    - foreign key(本表字段外键名) references 外表名(外表字段)
    - 外键关联的表中的字段必须unique
    - 级联操作:on update cascade on delete cascade
- 表与表之间的关系
  - 多对一
    - foreign key(多) references 表(一)
    - 多个学生对应一个班级
  - 一对一
    - 后一的类型+unique
    - foreign key(后1) references 表(先1)
    - 一个商品对应一个商品详情
  - 多对多
    - 第三张表
    - foreign key(外键名1) references 表1(主键)
    - foreign key(外键名2) references 表2(主键)
    - 多本书对应一个作者,多个作者对应一本书
- 数据的操作
  - 增
    - insert into 表 values(数据),(数据);
    - insert into 表(字段1,字段2) values(值1,值2),(值1,值2);
  - 删
    - delete from 表 where 条件;
  - 改
    - update 表 set 字段=值 where 条件;
  - 查
    - 单表查select
    - select * from 表
    - select 字段 from 表
    - select 字段1,字段2 from 表
    - select 字段 as 新名 from 表
    - select 字段 新名 from 表
    - select distinct 字段 from 表;     去重
    - select 字段*10 from 表;       计算



## 2.where语句

### 1.比较运算

- ``` > < = >= <= != <> ```

### 2.范围筛选

- 字段名 in (值1,值2,值3)

  - select * from 表名 where 字段 in(2000,3000...)
- 在一个模糊的范围里 between 1000 and 2000
  - 在一个数值区间 1k-2k之间的所有人的名字
    - select 字段 from 表名 where salary between 1000 and 2000;

- 字符串的模糊查询 like

  - 通配符 % 匹配任意长度的任意内容
  - 通配符 _ 匹配一个字符长度的任意内容
  - select 字段 from 表名 where name like's%';

- 正则匹配 regexp

  - select * from 表 where 字段 regexp 正则;

- 查看岗位是teacher且名字是jin开头的员工姓名、年薪

  ```python
  select emp_name,salary*12 from employee where post='teacher' and emp_name like 'jin%';
  select emp_name,salary*12 from employee where post='teacher' and emp_name regexp '^jin.*';
  ```

### 3.逻辑运算 - 条件的拼接

- 与and

- 或or

- 非not

  ```Python
  select * from employee where salary not in (20000,30000,3000,19000,18000,17000)
  ```

### 4.身份运算

- 关于null   is null / is not null
- 查看岗位不为null的员工信息
  - select * from employee where post_comment is not null;



## 3.分组聚合

### 1.分组group by

- select *from 表名 group by 字段;
- 会把在group by后面的这个字段中的每一个不同的项都保留下来,并且把值是这一项的所有行归为一组.

```Python
SELECT post FROM employee GROUP BY post; # 把post分组(相当于去重)

SELECT post,emp_name FROM employee GROUP BY post; # 只显示分组中的第一项emp_name

SELECT post,GROUP_CONCAT(emp_name) FROM employee GROUP BY post; # 显示全部分组中的内容emp_name

select post,count(id) as count from employee group by post; # 按照岗位分组，并查看每个组有多少人
```

### 2.聚合

- 把很多行的同一个字段进行一些统计,最终得到一个结果.
- count(字段)  统计这个字段有多少项
- sum(字段)   统计这个字段对应的数值的和
- avg(字段)   统计这个字段对应的数值的平均值
- min(字段)   字段最小值
- max(字段)   字段最大值

```Python
SELECT COUNT(*) FROM employee;
SELECT COUNT(*) FROM employee WHERE depart_id=1;
SELECT MAX(salary) FROM employee;
SELECT MIN(salary) FROM employee;
SELECT AVG(salary) FROM employee;
SELECT SUM(salary) FROM employee;
SELECT SUM(salary) FROM employee WHERE depart_id=3;
```

### 3.分组聚合

- 求各个部门的人数
  - select post,count(*) from employee group by post;
- 求公司男生和女生的人数
  - select count(id) from employee group by sex;
- 求各部门年龄最小的
  - select post,min(age) from employee group by post;

- 通过聚合取,但要得到对应的人,就必须通过多表查询.
- 总是根据会重复的项来分组
- 分组总是和聚合函数一起用

## 4.having过滤

### 1.having条件   过滤 组

- 部门人数大于3的
  - select post from employee group by post having count(*) > 3;
- 1.执行顺序
  - where > group by > having 
- 2.只能用having来完成

### 2.HAVING与WHERE不一样的地方

- Where 发生在分组group by之前，因而Where中可以有任意字段，但是绝对不能使用聚合函数。
- Having发生在分组group by之后，因而Having中可以使用分组的字段，无法直接取到其他字段,可以使用聚合函数

## 5.order by排序

- 1.order by 某一个字段 asc;    默认是升序asc 从小到大
- 2.order by 某一个字段 desc;    指定降序排列desc 从大到小
- 3.order by 第一个字段 asc,第二个字段 desc;     指定先根据第一个字段升序排列，在第一个字段相同的情况下，再根据第二个字段排列

## 6.limit限制查询的记录数

- 取前n个  limit n
  - 与limit 0,n相同
- 分页    limit m,n
  - 从m+1开始取n个
  - 与limit n offset m相同

## 7.总结

- select执行顺序

```Python
select distinct 需要显示的列(第五步) from 表 (第一步)
                             where 条件 (第二步)
                             group by 分组 (第三步)
                             having 过滤组条件 (第四步)
                             order by 排序 (第六步)
                             limit 前n 条 (第七步)
```

## 8.pymysql模块(第三方)

- 提交操作
- .connect()    连接数据库
- .cursor()     数据库操作符
- .commit()     提交操作
- .execute()     对数据库操作
- .close()     关闭连接

```Python
import pymysql

conn = pymysql.connect(host='127.0.0.1', user='root', password="123",database='day40')	# 连接mysql中的database
cur = conn.cursor()   # 数据库操作符,游标,和f=open相同
cur.execute('要执行的操作')
conn.commit()	# 提交操作
conn.close()
```

- 获取结果,不用commit提交
- .fetchone()     获取一个查询结果
- .fetchall()     接收全部的返回结果行
- .fetchmany(n)	拿到n行结果

```Python
conn = pymysql.connect(host='127.0.0.1', user='root', password="123",
                 database='day40')
cur = conn.cursor(pymysql.cursors.DictCursor)	# 拿到字典格式的结果
cur.execute('select * from employee where id > 10')
# ret = cur.fetchone()	# 拿到一行结果
# print(ret['emp_name'])	# 打印ret中的字段项
# # ret = cur.fetchmany(5)	# 拿到5行结果
ret = cur.fetchall()	    # 拿到所有
print(ret)
conn.close()
```

- 报错时回滚

```Python
try:
   cursor.execute(sql) # 执行sql语句
   conn.commit()         # 提交到数据库执行
except:
   conn.rollback()       # 如果发生错误则回滚
```

- rowcount     一个只读属性，并返回执行execute()方法后影响的行数

## 9.多表查询

### 1.交叉连接

- 不适用任何匹配条件,生成笛卡尔积
- select * from 表1,表2;

### 2.内连接  inner join

- 只连接匹配的行
- 找两张表共有的部分，相当于利用条件从笛卡尔积结果中筛选出了正确的结果
- select 表1.字段1,表1.字段2,表2.字段 from 表1 inner join b表2 on 表1.dep_id=表2.id; 
- 同上:select 表1.字段1,表1.字段2,,表2.字段 from 表1,表2 where 表1.dep_id=表2.id;

### 3.外连接

- 左外连接left join
  - select * from emp left join department on emp.dep_id = department.id;
  - 优先显示左表全部记录
  - 以左表为准，即找出所有员工信息，当然包括没有部门的员工
  - 本质就是：在内连接的基础上增加左边有右边没有的结果

- 右外连接right join
  - select * from emp right join department on emp.dep_id = department.id;
  - 优先显示右表全部记录
  - 以右表为准，即找出所有部门信息，包括没有员工的部门
  - 本质就是：在内连接的基础上增加右边有左边没有的结果

- 全外连接
  - select * from employee left join department on employee.dep_id = department.id
    union
    select * from employee right join department on employee.dep_id = department.id;
  - 在内连接的基础上增加左边有右边没有的和右边有左边没有的结果
  - 注意：mysql不支持全外连接 full JOIN
  - 强调：mysql可以使用此种方式间接实现全外连接
  - 注意 union与union all的区别：union会去掉相同的纪录

### 4.子查询

- 1：子查询是将一个查询语句嵌套在另一个查询语句中。
- 2：内层查询语句的查询结果，可以为外层查询语句提供查询条件。
- 3：子查询中可以包含：IN、NOT IN、ANY、ALL、EXISTS 和 NOT EXISTS等关键字
- 4：还可以包含比较运算符：= 、 !=、> 、<等

```Python
找技术部门的所有人的姓名
1.先找到部门表技术部门的部门id
select id from department where name = '技术';
2.再找emp表中部门id = 200
select name from emp where dep_id = (select id from department where name = '技术');
```

```Python
找到技术部门和销售部门所有人的姓名
1.先找到技术部门和销售部门的的部门id
select id from department where name = '技术' or name='销售'
2.找到emp表中部门id = 200或者202的人名
select name from emp where dep_id in (select id from department where name = '技术' or name='销售');
或
select emp.name from emp inner join department on emp.dep_id = department.id where department.name in ('技术','销售');
```



- 优先使用连表查询，因为连表查询的效率高



