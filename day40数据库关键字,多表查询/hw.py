
# 中午
"""
1.select 书名,价格 from book where 作者='egon';

2.select max(价格) from book;

3.select avg(价格) from book;

4.select * from book order by 出版日期;

5.select avg(价格) from book where 作者='alex';

6.select * from book where 出版社='人民音乐不好听出版社';

7.select 书名,价格 from book where 出版社='人民音乐不好听出版社' and 作者='alex';

8.select 作者 from book group by 作者 order by avg(价格) desc limit 1;

9.select 作者,出版社 from book order by 出版日期 desc limit 1;

10.select 书名,出版社 from book order by 出版社;

11.select max(价格) from book;
update book set 价格=50 where 价格=70;

12.delete from book where 价格=5;

13.update book set 作者='alexsb' where 作者='alex';

14.
"""

# 晚上
'''
select post,name,hire_date from emp where hire_date in(select max(hire_date) from emp group by post);
'''
