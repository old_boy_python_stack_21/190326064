# day63笔记

## 1.作业讲解

https://www.cnblogs.com/maple-shaw/articles/9414626.html?tdsourcetag=s_pcqq_aiomsg

- 底部有答案,display为none没显示.

## 2.今日内容

博客:https://www.cnblogs.com/maple-shaw/articles/9502602.html

### 1.cookie

#### 定义

http是无状态的,每次请求是独立的,为了保持状态,通过保存在浏览器本地上一组组键值对来在下次访问服务器时自动携带,提供给服务器信息.

#### 特点

1. 由服务器让浏览器进行设置的
2. 浏览器保存在浏览器本地
3. 下次访问时自动携带

#### 应用

1. 登录
2. 保存浏览习惯
3. 简单的投票

#### django中操作cookie

##### 设置：

- set_cookie(key,value,max_age=10,path='/',domain=None,secure=False,httponly=False)
  - max_age	cookie超时时间
  - path	cookie生效的路径, / 表示根路径
  - domain	cookie生效的域名
  - secure	False表示支持http,True只支持https
  - httponly	False只能http协议传输,无法被js获取
- set_signed_cookie(key,value,salt)
  - salt为加密盐

```python
ret = reditrect('index') # Httpresponse/render都可以
ret.set_cookie(key,value)   #    set-cookie:  key=value
ret.set_signed_cookie('is_login', '1', 's21')  # Set-Cookie: is_login=1; Path=/
```

##### 获取：

- get_signed_cookie
  - default: 默认值
  - salt: 加密盐,需要和set_signed_cookie的盐一样
  - max_age: 后台控制过期时间

```python
request.COOKIES['key']
request.COOKIES.get('key')
request.get_signed_cookie('key', default=RAISE_ERROR, salt='', max_age=None)
```

##### 删除：

- 设置删除函数,在url中添加即可.
- ret = redirect('/login/')
  ret.delete_cookie('is_login')

```python
def logout(request):
    ret = redirect('/login/')
    # ret.delete_cookie('is_login')
    # request.session.delete()    # 删除session数据  不删除cookie
    # request.session.flush()      # 删除session数据  删除cookie
    return ret
```



### 2.session

#### 定义

cookie最大支持4096字节,而且保存在客户端可能被拦截.session是保存在服务器上的一组组键值对(必须依赖cookie),有较高的安全性.

#### 为什么要有session？

- cookie保存在浏览器本地
- 大小个数受到限制

#### django中操作session

```python
# 获取、设置
request.session['k1'] = 值
request.session.get('k1',None) # 找不到k1返回None
request.session.setdefault('k1',123) # 存在则不设置

# 删除
del request.session['k1']
request.session.delete()    # 删除session数据  不删除cookie
request.session.flush()      # 删除session数据  删除cookie
request.session.pop('键') # 删除单个

# 所有键,值,键值对
request.session.keys()
request.session.values()
request.session.items()
request.session.iterkeys()
request.session.itervalues()
request.session.iteritems()

# 将所有Session失效日期小于当前日期的数据删除
request.session.clear_expired()

# 检查会话session的key在数据库中是否存在
request.session.exists("session_key")

# 删除当前会话的所有Session数据
request.session.delete()

# 删除当前的会话数据并删除会话的Cookie
request.session.flush() 
	#这用于确保前面的会话数据不可以再次被用户的浏览器访问,例如,django.contrib.auth.logout() 函数中就会调用它。

# 设置会话Session和Cookie的超时时间
request.session.set_expiry(value)
	# 如果value是个整数，session会在些秒数后失效。
    # 如果value是个datatime或timedelta，session就会在这个时间后失效。
    # 如果value是0,用户关闭浏览器session就会失效。
    # 如果value是None,session会依赖全局session失效策略。
```

#### session的配置

- 见默认配置,全局配置
  - from django.conf import global_settings

#### 其他views需要用到cookie或session时需要加装饰器

```python
def login_required(func):
    def inner(request, *args, **kwargs):
        # is_login = request.COOKIES.get('is_login')
        # is_login = request.get_signed_cookie('is_login', salt='s21', default='')
        is_login = request.session.get('is_login')
        url = request.path_info
        if is_login != 1:
            return redirect('/login/?return_url={}'.format(url))
        # 已经登录
        ret = func(request, *args, **kwargs)
        return ret
    return inner

@login_required
def index(request):
    return ...
```









