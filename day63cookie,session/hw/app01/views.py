from django.shortcuts import render, HttpResponse, redirect
from django.views import View
from django.http import JsonResponse
from app01 import models


def login_required(func):
    def inner(request, *args, **kwargs):
        # is_login = request.COOKIES.get('is_login')
        # is_login = request.get_signed_cookie('is_login', salt='s21', default='')
        is_login = request.session.get('is_login')
        url = request.path_info
        if is_login != '1':
            return redirect('/login/?return_url={}'.format(url))
        ret = func(request, *args, **kwargs)
        return ret

    return inner


@login_required
def index(request):
    return HttpResponse('首页')


class Login(View):
    def get(self, request):
        return render(request, 'login.html')

    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        if username == 'alex' and password == '123':
            url = request.GET.get('return_url')
            if url:
                ret = redirect(url)
            else:
                ret = redirect('/index/')
            request.session['is_login'] = '1'
            return ret
        return render(request, 'login.html')


def logout(request):
    ret = redirect('/login/')
    # ret.delete_cookie('is_login')
    # request.session.delete()    # 删除session数据  不删除cookie
    request.session.flush()  # 删除session数据  删除cookie
    return ret


def ajax_demo1(request):
    return render(request, "ajax_demo1.html")


def ajax_add(request):
    i1 = int(request.GET.get("i1"))
    i2 = int(request.GET.get("i2"))
    ret = i1 + i2
    return JsonResponse(ret, safe=False)


def calc(request):
    un = request.POST.get('username')
    if not un:
        return HttpResponse('请输入账号!')
    user = models.User.objects.filter(username=un)
    if not user:
        return HttpResponse('账号错误')
    return HttpResponse('ok')


def mdf(request):
    all_user = models.User.objects.all()
    return render(request, 'mdf.html', {'all_user': all_user})


def del_user(request):
    pk = request.GET.get('del_id')
    print(pk)
    models.User.objects.get(pk=pk).delete()
    return JsonResponse({'status': 0})
