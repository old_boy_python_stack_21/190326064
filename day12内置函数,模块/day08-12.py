#!/usr/bin/env python
# -*- coding:utf-8 -*-

# day08 exam
# 12.反转 v = '全栈21期'
'''
v = '全栈21期'
print(v[::-1])
'''

# 15.写代码将 v = 'k1|v1,k2|v2,k3|v3...'转换成字典{'k1':'v1','k2':'v2','k3':'v3'...}
'''
v = 'k1|v1,k2|v2,k3|v3'
dic = {}
lst = v.split(',')
for i in lst:
    k,v = i.split('|')
    dic[k] = v
print(dic)
'''

# 16.实现整数乘法计算器.
'''
content = input('内容:')
lst = content.split('*')
res = 1
for i in lst:
    res *= int(i)
print(res)
'''

# 17.看代码写结果
'''
v1 = [1, 2, 3, 4, 5]
v2 = [v1, v1, v1]
v2[1][0] = 111
v2[2][0] = 222
print(v1,v2)        # [222, 2, 3, 4, 5] [[222, 2, 3, 4, 5],[222, 2, 3, 4, 5],[222, 2, 3, 4, 5]]
'''

# 18.将info中偶数位索引使用*拼接,写入到a.log中.
'''
info = ['你是','到底','是','不是','一个','魔鬼']
info2 = info[::2]
s = '*'.join(info2)
f = open('a.log',mode='w',encoding='utf-8')
f.write(s)
f.close()
'''

# 19.读取100g的文件,检测文件中是否有关键字keys=['苍老师','小泽老师','alex'],如果有则替换为***,并写入到另外一个文件中.
'''
keys=['苍老师','小泽老师','alex']
with open('goods.txt', mode='r', encoding='utf-8') as f1, open('res', mode='w', encoding='utf-8') as f2:
    for line in f1:
        for i in keys:
            if i in line:
                line = line.replace(i,'***')
        f2.write(line)
'''

# 20.车牌区域划分.写入字典.
'''
cars = ['鲁A32444', '鲁B12333', '京B8989M', '黑C40678', '黑C46555', '沪B25041','鲁SDKJ124']
dic = {}
for i in cars:
    if i[0] not in dic:
        dic[i[0]] = 1
    elif i[0] in dic:
        dic[i[0]] += 1
print(dic)
'''

# 21.将data.txt利用文件操作构成如下类型:info = [{'id':'1','name':'alex','age':'22','phone':'13231213132','job':'IT'}...]
'''
lst = []
with open('data.txt',mode='r',encoding='utf-8') as f:
    first_line = f.readline().split(',')
    for line in f:
        dic = {}
        val = line.split(',')
        dic[first_line[0]] = val[0]
        dic[first_line[1]] = val[1]
        dic[first_line[2]] = val[2]
        dic[first_line[3]] = val[3]
        dic[first_line[4].strip()] = val[4].strip()
        lst.append(dic)
print(lst)
'''

# 22.for循环打印9*9乘法表.
'''
for a in range(1, 10):
    for b in range(1, a+1):
        print('%s*%s=%s ' % (b, a, a * b),end='')
        if a == b:
            print('')
'''

# day09
# 2.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新列表返回。
'''
def func(a):
    lst = a[1::2]
    print(lst)
func((1,2,3,2,1))
'''

# 3.写函数，判断用户传入的一个对象（字符串或列表或元组任意）长度是否大于5，并返回真假。
'''
def func(a):
    return len(a) > 5


print(func('123'))
'''

# 4.写函数，接收两个数字参数，返回比较大的那个数字。
'''
def func(a, b):
    return a if a > b else b


print(func(22, 25))
'''

# 5.写函数，函数接收四个参数分别是：姓名，性别，年龄，学历。用户通过输入这四个内容，然后将这四个内容传入到函数中，此函
# 数接收到这四个内容，将内容根据"*"拼接起来并追加到一个student_msg文件中。
'''
def func(name, gender, age, edu):
    lst = [name, gender, age, edu]
    s = '*'.join(lst)
    f = open('student.msg', mode='w', encoding='utf-8')
    f.write(s)
    f.close()


func('2', '3', '5', '6')
'''

# 6.写函数，在函数内部生成如下规则的列表 [1,1,2,3,5,8,13,21,34,55…]（斐波那契数列），并返回。 注意：函数可接收一个参
# 数用于指定列表中元素最大不可以超过的范围。
'''
lst = [1, 1]


def func(max):
    while 1:
        a = lst[-1] + lst[-2]
        if a < max:
            lst.append(a)
        else:
            break
    print(lst)


func(999)
'''

# 7.写函数，验证用户名在文件 data.txt 中是否存在，如果存在则返回True，否则返回False。（函数有一个参数，用于接收用户输入的用户
# 名）
'''
def func(username):
    tip = False
    with open('username.txt',mode='r',encoding='utf-8') as f:
        for line in f:
            if username in line:
                tip = True
                break
    return tip

print(func('alex'))
'''

# day10
# 1.写函数，函数可以支持接收任意数字（位置传参）并将所有数据相加并返回。
'''
def func(*args):
    res = sum(args)
    print(res)


func(1, 2, 3, 4, 5, 5, 8, 8, 7)
'''

# 2.看代码写结果
'''
def func():
    return 1, 2, 3

val = func()
print(type(val) == tuple)   # True
print(type(val) == list)    # False
'''

# 3.看代码写结果
# def func(*args,**kwargs):
#     pass

# a. 请将执行函数，并实现让args的值为 (1,2,3,4)
'''
def func(*args,**kwargs):
    return args
print(func(1,2,3,4))
'''
# b. 请将执行函数，并实现让args的值为 ([1,2,3,4],[11,22,33])
'''
def func(*args,**kwargs):
    return args
print(func([1,2,3,4],[11,22,33]))
'''
# c. 请将执行函数，并实现让args的值为 ([11,22],33]) 且 kwargs的值为{'k1':'v1','k2':'v2'}
'''
def func(*args,**kwargs):
    return args,kwargs
print(func(*([11,22],33),k1 = 'v1',k2 = 'v2'))
'''
# d. 如执行 func(*{'武沛齐','金鑫','女神'})，请问 args和kwargs的值分别是？
'''
args = ('武沛齐','金鑫','女神')    kwargs = {}
'''
# e. 如执行 func({'武沛齐','金鑫','女神'},[11,22,33])，请问 args和kwargs的值分别是？
'''
args = ({'武沛齐','金鑫','女神'},[11,22,33])    kwargs = {}
'''
# f. 如执行 func('武沛齐','金鑫','女神',[11,22,33],**{'k1':'栈'})，请问 args和kwargs的值分别是？
'''
args = ('武沛齐','金鑫','女神',[11,22,33])    kwargs = {'k1':'栈'}
'''

# 4.看代码写结果
'''
def func(name, age=19, email='123@qq.com'):
    return name,age,email
print(func())
'''
# a. 执行 func('alex') ,判断是否可执行，如可以请问 name、age、email 的值分别是？
'''
可执行,name = 'alex', age = 19, email = 123@qq.com
'''
# b. 执行 func('alex',20) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
'''
可执行,name = 'alex', age = 20, email = 123@qq.com
'''
# c. 执行 func('alex',20,30) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
'''
可执行,name = 'alex', age = 20, email = 30
'''
# d. 执行 func('alex',email='x@qq.com') ,判断是否可执行，如可以请问 name、age、email 的值分别是？
'''
可执行,name = 'alex', age = 19, email = x@qq.com
'''
# e. 执行 func('alex',email='x@qq.com',age=99) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
'''
可执行,name = 'alex', age = 99, email = x@qq.com
'''
# f. 执行 func(name='alex',99) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
'''
不可执行,位置参数必须在关键字参数前面.
'''
# g. 执行 func(name='alex',99,'111@qq.com') ,判断是否可执行，如可以请问 name、age、email 的值分别是？
'''
不可执行,位置参数必须在关键字参数前面.
'''

# 5.看代码写结果
'''
def func(users,name):
    users.append(name)
    return users

result = func(['武沛齐','李杰'],'alex')
print(result)       # ['武沛齐','李杰','alex']
'''

# 6.看代码写结果
'''
def func(v1):
    return v1 * 2

def bar(arg):
    return "%s 是什么玩意？" % (arg,)

val = func('你')
data = bar(val)
print(data)     # 你你 是什么玩意？
'''

# 7.看代码写结果
'''
def func(v1):
    return v1 * 2

def bar(arg):
    msg = "%s 是什么玩意？" %(arg,)
    print(msg)

val = func('你')
data = bar(val)
print(data)         # 你你 是什么玩意？     None
'''

# 8.看代码写结果
'''
v1 = '武沛齐'

def func():
    print(v1)

func()
v1 = '老男人'
func()              # 武沛齐     老男人
'''

# 9.看代码写结果
'''
v1 = '武沛齐'

def func():
    v1 = '景女神'
    def inner():
        print(v1)
    v1 = '肖大侠'
    inner()
func()
v1 = '老男人'
func()              # 肖大侠   肖大侠
'''

# 10.看代码写结果【可选】注意：函数类似于变量，func代指一块代码的内存地址。
'''
def func():
    data = 2*2
    return data

new_name = func
val = new_name()
print(val)    # 4
'''

# 11.看代码写结果【可选】注意：函数类似于变量，func代指一块代码的内存地址。
'''
def func():
    data = 2*2
    return data

data_list = [func,func,func]
for item in data_list:
    v = item()
    print(v)        # 4     4     4
'''

# 12.看代码写结果（函数可以做参数进行传递）【可选】
'''
def func(arg):
    arg()

def show():
    print('show函数')

func(show)      # show函数
'''

# day11
# 2.列举你了解的常见内置函数 【面试题】
'''
输入输出:input() / print()
数学相关:
sum()求和     max()最大值       min()最小值     divmod()整除和余数     pow()幂运算      float()浮点     abs()绝对值
强制转换:
int()整型     list()列表     str()字符串     tuple()元组     dict()字典     bool()布尔值      set()集合
其他:
len()长度     range()范围      open()打开文件     type()查看数据类型      id()查看内存地址
'''

# 3.看代码分析结果
'''
def func(arg):
    return arg.replace('苍老师', '***')             # 4.返回"Alex的女朋友***和大家都是好朋友"给result

def run():                                          # 2.run()
    msg = "Alex的女朋友苍老师和大家都是好朋友"
    result = func(msg)                              # 3.调用func("Alex的女朋友苍老师和大家都是好朋友")
    print(result)                                   # 5.输出Alex的女朋友***和大家都是好朋友

run()                                               # 1.调用run()
'''

# 4.看代码分析结果
'''
def func(arg):
    return arg.replace('苍老师', '***')                # 3."Alex的女朋友***和大家都是好朋友"

def run():
    msg = "Alex的女朋友苍老师和大家都是好朋友"
    result = func(msg)                                 # 2.调用func("Alex的女朋友苍老师和大家都是好朋友")
    print(result)                                      # 4.输出Alex的女朋友***和大家都是好朋友

data = run()                                           # 1.调用run()
print(data)                                            # 5.输出None
'''

# 5.看代码分析结果
'''
DATA_LIST = []


def func(arg):
    return DATA_LIST.insert(0, arg)  # 2.insert没有返回值,返回None


data = func('绕不死你')  # 1.调用func()
print(data)  # 3.None
print(DATA_LIST)  # 4.DATA_LIST = ['绕不死你']
'''

# 6.看代码分析结果
'''
def func():
    print('你好呀')
    return '好你妹呀'

func_list = [func, func, func]      # 1.func为内存地址,没有调用

for item in func_list:              # 2.循环列表
    val = item()                    # 3.val = func()
    print(val)                      # 4.输出func的返回值

# 输出三次 你好呀\n好你妹呀
'''

# 7.看代码分析结果
'''
def func():
    print('你好呀')
    return '好你妹呀'


func_list = [func, func, func]      # 1.func为内存地址,没有调用

for i in range(len(func_list)):     # 2.循环range(3),i=0,1,2
    val = func_list[i]()            # 3.func_list[i]都执行func
    print(val)                      # 4.输出func()返回值

# 输出三次 你好呀\n好你妹呀
'''

# 8.看代码写结果
'''
tips = "啦啦啦啦"
def func():
    print(tips)
    return '好你妹呀'
func_list = [func, func, func]      # 1.func为内存地址,没有调用
tips = '你好不好'                    # 2.tips被重新赋值
for i in range(len(func_list)):     # 3.循环3次
    val = func_list[i]()            # 4.执行func()
    print(val)                      # 5.输出func()返回值

# 输出三次 你好不好\n好你妹呀
'''

# 9.看代码写结果
'''
def func():
    return '烧饼'  # 4.返回 烧饼


def bar():
    return '豆饼'  # 4.返回 豆饼


def base(a1, a2):
    return a1() + a2()  # 2.返回func() + bar()


result = base(func, bar)  # 1.调用base()    3.res=func() + bar()    5.res=烧饼豆饼
print(result)  # 6.输出 烧饼豆饼
'''

# 10.看代码写结果
'''
def func():
    return '烧饼'
    
    
def bar():
    return '豆饼'
    
    
def base(a1, a2):
    return a1 + a2
result = base(func(), bar())    # 1.调用base(),执行里面的func和bar
print(result)                   # 2.烧饼豆饼
'''

# 11.看代码写结果
'''
v1 = lambda :100
print(v1())         # 100

v2 = lambda vals: max(vals) + min(vals)
print(v2([11,22,33,44,55]))             # 66

v3 = lambda vals: '大' if max(vals)>5 else '小'
print(v3([1,2,3,4]))                            # 小
'''

# 12.看代码写结果
'''
def func():
    num = 10
    v4 = [lambda :num+10,lambda :num+100,lambda :num+100,]  # 2.三个函数
    for item in v4:                                         # 3.循环v4的三个函数
        print(item())                                       # 4.输出20,110,110
func()                                                      # 1.调用func
'''

# 13.看代码写结果
'''
for item in range(10):
    print(item) # 0-9

print(item)     # 9
'''

# 14.看代码写结果
'''
def func():
    for item in range(10):
        pass
    print(item)
func()              # 9
'''

# 15.看代码写结果
'''
item = '老男孩'
def func():
    item = 'alex'
    def inner():
        print(item)
    for item in range(10):          # 9
        pass
    inner()
func()
'''

# 16.看代码写结果【新浪微博面试题】
'''
def func():
    for num in range(10):                                       # num = 9
        pass
    v4 = [lambda :num+10,lambda :num+100,lambda :num+100,]
    result1 = v4[1]()                                           # res=109
    result2 = v4[2]()                                           # res=109
    print(result1,result2)
func()
'''

# 17.通过代码实现如下转换
# 二进制转换成十进制：
'''
v = '0b1111011'
print(int(v,base=2))
'''
# 十进制转换成二进制：
'''
v = 18
print(bin(v))
'''
# 八进制转换成十进制：
'''
v = '011'
print(int(v,base=8))
'''
# 十进制转换成八进制：
'''
v = 30
print(oct(v))
'''
# 十六进制转换成十进制：
'''
v = '0x12'
print(int(v,base=16))
'''
# 十进制转换成十六进制：
'''
v = 87
print(hex(v))
'''


# 18.请编写一个函数实现将IP地址转换成一个整数。【面试题】
# 如 10.3.9.12 转换规则为二进制：
#         10            00001010
#          3            00000011
#          9            00001001
#         12            00001100
# 再将以上二进制拼接起来计算十进制结果：00001010 00000011 00001001 00001100 = ？
"""
def func(ip):
    lst = ip.split('.')
    s = ''
    for i in lst:
        a = bin(int(i))[2:].rjust(8, '0')
        s += a
    print(int(s,base=2))


func('10.3.9.12')
"""