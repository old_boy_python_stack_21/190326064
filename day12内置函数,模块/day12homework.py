#!/usr/bin/env python
# -*- coding:utf-8 -*-


# 1.写出三元运算的基本格式及作用？
'''
a if a>b else b
'''

# 2.什么是匿名函数？
'''
lambda,一行代码写出简单函数,无需定义函数名称.
'''

# 3.尽量多的列举你了解的内置函数？【默写】
'''
1.输入输出:
input() / print()
2.数学相关:
sum()求和     max()取最大值     min()取最小值     float()浮点      divmod()商和余数     pow()幂运算    abs()绝对值
round(a,b)a保留b位小数(四舍五入)
3.其他:
len()长度   open()打开文件    type()查看数据类型      id()查看内存地址    range()范围
4.强制转换:
int()整型     str()字符串    bool()布尔值      list()列表     tuple()元组     set()集合     dict()字典
5.进制转换:
int()十进制    bin()二进制    oct()八进制    hex()十六进制
'''

# 4.filter/map/reduce函数的作用分别是什么？
'''
filter:筛选,(函数,可迭代对象),循环可迭代对象,符合函数要求的输出到新列表
map:(函数,可迭代对象),循环可迭代对象,执行函数后返回原长度列表.
reduce:累积,(函数,可迭代对象),传入的函数必须接收两个参数，对每个元素反复调用函数，并返回最终结果值。
'''

# 5.看代码写结果

# def func(*args, **kwargs):
#     print(args, kwargs)

# a. 执行 func(12,3,*[11,22]) ，输出什么？
'''
func(12,3,*[11,22])     # (12,3,11,22) {}
'''
# b. 执行 func(('alex','武沛齐',),name='eric')
'''
func(('alex','武沛齐',),name='eric')       # (('alex','武沛齐'),) {'name':'eric'}
'''

# 6.看代码分析结果
'''
def func(arg):
    return arg.pop(1)


result = func([11, 22, 33, 44])
print(result)               # 22
'''

# 7.看代码分析结果
'''
func_list = []

for i in range(10):
    func_list.append(lambda: i)             # 每次把函数传入列表

v1 = func_list[0]()             # 调用函数
v2 = func_list[5]()             # 调用函数
print(v1, v2)                   # 9 9
'''

# 8.看代码分析结果
'''
func_list = []

for i in range(10):
    func_list.append(lambda x: x + i)

v1 = func_list[0](2)        # 11
v2 = func_list[5](1)        # 10
print(v1, v2)
'''

# 9.看代码分析结果
'''
func_list = []

for i in range(10):
    func_list.append(lambda x: x + i)   # 列表加入十个地址,函数不执行,i不变

for i in range(0, len(func_list)):      # 循环10次
    result = func_list[i](i)
    print(result)                       # 0 2 4 6 8 10 12 14 16 18
'''

# 10.看代码分析结果
'''
def f1():
    print('f1')


def f2():
    print('f2')
    return f1


func = f2()         # 调用f2,输出f2,func=f1
result = func()     # result=f1(),输出f1
print(result)       # 输出None
'''

# 11.看代码分析结果【面试题】
'''
def f1():
    print('f1')
    return f3()


def f2():
    print('f2')
    return f1


def f3():
    print('f3')


func = f2()         # 调用f2,输出f2,func=f1
result = func()     # f1(),输出f1,return 调用f3(),输出f3,返回None
print(result)
'''

# 12.看代码分析结果
'''
name = '景女神'


def func():
    def inner():
        print(name)

    return inner()


v = func()          # 调用func(),return调用inner,输出景女神,返回None
print(v)            # 输出None
'''

# 13.看代码分析结果
'''
name = '景女神'


def func():
    def inner():
        print(name)
        return '老男孩'

    return inner()


v = func()              # 调用func,return调用inner,输出景女神,返回老男孩给func的return.
print(v)                # 输出老男孩
'''

# 14.看代码分析结果
'''
name = '景女神'


def func():
    def inner():
        print(name)
        return '老男孩'

    return inner


v = func()          # 调用func,返回inner
result = v()        # 调用inner,输出景女神,返回老男孩给result.
print(result)       # 输出老男孩
'''

# 15.看代码分析结果
'''
def func():
    name = '武沛齐'

    def inner():
        print(name)
        return '老男孩'

    return inner


v1 = func()         # 调用func,返回inner
v2 = func()         # 调用func,返回inner
print(v1, v2)       # 输出两个不同的inner内存地址
'''

# 16.看代码写结果
'''
def func(name):
    def inner():
        print(name)
        return '老男孩'

    return inner


v1 = func('金老板')        # 调用func,v1=inner
v2 = func('alex')          # 调用func,v2=inner
print(v1, v2)              # 输出两个不同的inner内存地址
'''

# 17.看代码写结果
'''
def func(name=None):
    if not name:
        name = '武沛齐'

    def inner():
        print(name)
        return '老男孩'

    return inner


v1 = func()         # 调用func,name=武沛齐,返回inner
v2 = func('alex')   # 调用func,
print(v1, v2)       # inner的内存地址 inner另一个内存地址
'''

# 18.看代码写结果【面试题】
'''
def func(name):
    v = lambda x: x + name
    return v


v1 = func('武沛齐')        # 调用func,v1 = lambda x: x + name
v2 = func('alex')          # 调用func,v1 = lambda x: x + name
v3 = v1('银角')            # 银角武沛齐
v4 = v2('金角')            # 金角alex
print(v1, v2, v3, v4)      # 内存地址 另一个内存地址  银角武沛齐 金角alex
'''

# 19.看代码写结果
'''
NUM = 100
result = []
for i in range(10):      # 循环0-9
    func = lambda: NUM   # 注意：函数不执行，内部代码不会执行。
    result.append(func)  # result=[100,100...]

print(i)                 # 9
print(result)            # result=[十个不同的lambda内存地址]
v1 = result[0]()         # 100
v2 = result[9]()         # 100
print(v1, v2)
'''

# 20.看代码写结果【面试题】
'''
result = []
for i in range(10):         # 0-9
    func = lambda: i # 注意：函数不执行，内部代码不会执行。
    result.append(func)     # result=[十个不同的lambda内存地址,i=0-9]
print(i)            # 9
print(result)       # result=[十个不同的lambda内存地址,i=0-9]
v1 = result[0]()    # 9
v2 = result[9]()    # 9
print(v1, v2)
'''

# 21.看代码分析结果【面试题】
'''
def func(num):
    def inner():
        print(num)

    return inner


result = []
for i in range(10):     # 循环十次
    f = func(i)
    result.append(f)    # result=[十个不同的inner内存地址对应十个不同的num(0-9)]

print(i)                # 9
print(result)           # [十个不同的inner内存地址对应十个不同的num(0-9)]
v1 = result[0]()        # 0
v2 = result[9]()        # 9
print(v1, v2)           # None None
'''


# 22.程序设计题
# 请设计实现一个商城系统，商城主要提供两个功能：商品管理、会员管理。
# 商品管理：
# 1.查看商品列表
# 2.根据关键字搜索指定商品
# 3.录入商品
# 4.会员管理：【无需开发，如选择则提示此功能不可用，正在开发中，让用户重新选择】
#
# 需求细节：
# 1.启动程序让用户选择进行商品管理 或 会员管理.
# 2.用户选择 【1】 则进入商品管理页面，进入之后显示商品管理相关的菜单.
# 3.用户选择【2】则提示此功能不可用，正在开发中，让用户重新选择。
# 4.如果用户在【商品管理】中选择【1】，则按照分页去文件 goods.txt 中读取所有商品，并全部显示出来【分页功能可选】。
# 5.如果用户在【商品管理】中选择【2】，则让提示让用户输入关键字，输入关键字后根据商品名称进行模糊匹配.
# 6.如果用户在【商品管理】中选择【3】，则提示让用户输入商品名称、价格、数量 然后写入到 goods.txt 文件.


def goods_list():  # 查看商品列表
    content = []
    f = open('goods.txt', mode='r', encoding='utf-8')
    for i in f:
        content.append(i.strip())
    f.seek(0)
    a = len(f.readlines())
    max_page, a = divmod(a, 5)
    if a > 0:
        max_page += 1
    while 1:
        pager = input('要查看第几页(输入N返回)：')
        if pager.upper() == 'N':
            return class_two()
        if pager.isdigit():
            int_pager = int(pager)
            if int_pager < 1 or int_pager > max_page:
                print('页码不合法，必须是 1 ~ %s' % max_page)
            else:
                start = (int_pager - 1) * 5
                end = int_pager * 5
                data = content[start:end]
                for item in data:
                    print(item)
        else:
            print('输入有误!请重新输入!')


def key_word_find():  # 关键字查找
    c = input('''
*****欢迎使用劳资的购物商城【商品管理】【根据关键字搜索】*****
请输入关键字(输入N返回上一级):''')
    if c.upper() == 'N':
        return class_two()
    f = open('goods.txt', mode='r', encoding='utf-8')
    x = '输入有误!'
    for i in f:
        if c in i.split(' ')[0]:
            print(i)




def input_goods():  # 录入商品
    name = input('请输入商品名称(输出N返回上一级):')
    if name.upper() == 'N':
        return class_two()
    price = input('请输入商品价格:')
    count = input('请输入商品数量:')
    f = open('goods.txt', mode='a', encoding='utf-8')
    f.write('\n' + name + ' ' + price + ' ' + count)
    f.close()
    print('录入成功!')


def class_two():
    b = input('''
*****欢迎使用劳资的购物商城【商品管理】*****
1.查看商品列表
2.根据关键字搜索指定商品
3.录入商品
请选择(输入N返回上一级):''')
    if a.upper() == 'N':
        return
    if b == '1':  # 查看商品列表
        goods_list()
    elif b == '2':  # 根据关键字查找
        key_word_find()
    elif b == '3':  # 录入商品
        input_goods()
    else:
        print('输入有误!')


while 1:
    a = input('''
*****欢迎使用劳资的购物商城*****
1.商品管理
2.会员管理(开发中...)
请选择(输入N返回上一级):''')  # 首页
    if a == '1':  # 进入商品管理界面
        class_two()
    if a == '2':
        print('此功能不可用!')
        continue
    if a.upper() == 'N':
        break
