# day12笔记

## 一.函数中高级

### 1.函数作返回值

```python
def func():
    print(123)

def bar():
    return func

v = bar()			# 调用bar,v=func

v()					# v()=func(),输出123
```

```python
name = 'oldboy'
def func():
    print(name)
    
def bar():
    return func

v = bar()			#调用bar,v=func

v()					# v()=func(),输出oldboy.
```

```python
def bar():
    def inner():
        print(123)
    return inner
v = bar()			#调用bar,v=inner
v()					#inner()输出123
```

```python
name = 'oldboy'
def bar():
    name = 'alex'
    def inner():
        print(name)
    return inner
v = bar()			#调用bar,v=inner
v()					#inner()输出alex
```

```python
name = 'oldboy'
def bar(name):
    def inner():
        print(name)
    return inner
v1 = bar('alex') 	# 调用bar,v1=inner
v2 = bar('eric') 	# 调用bar,v2=inner
v1()	# inner()输出alex
v2()	# inner()输出eric
```

### 2.闭包

为函数创建一块区域并为其维护自己数据，以后执行时方便调用。

```python
def func(name):
    def inner():
        print(name)
	return inner 

v1 = func('alex')	# 调用func,v1=func
v1()				# func()输出alex
v2 = func('eric')	# 调用func,v2=func
v2()				# func()输出eric
```

### 3.高阶函数

- 把函数当作参数传递
- 把函数当作返回值

## 二.内置函数

### 1.编码相关

- chr，将十进制数字转换成 unicode 编码中的对应字符串。

```python
v = chr(99)
print(v)	# c
```

- ord，根据字符在unicode编码中找到其对应的十进制。

```python
num = ord('中')
print(num)		# '中'在unicode中的编码20013
```

- 应用于生成随机验证码

```python
import random		# 导入模块

def get_random_code(length=6):	# 默认验证码长度6
    data = []
    for i in range(length):		# 循环length次
        v = random.randint(65,90)	# 随机A到Z字母
        data.append(chr(v))		# 添加到data

    return  ''.join(data)		# 返回拼接的字符串


code = get_random_code()		# 调用函数
print(code)
```

### 2.高级内置函数

- map

循环让每个元素执行函数，将每个函数执行的结果保存到新的列表中，并返回。

```python
v1 = [11,22,33,44]
result = map(lambda x:x+100,v1)	# 第一个参数为执行的函数,第二个参数为可迭代元素.
print(list(result)) # [111,122,133,144]
```

- filter

按条件筛选.

```python
v1 = [11,22,33,'asd',44,'xf']

# 一般做法
def func(x):
    if type(x) == int:
        return True
    return False
result = filter(func,v1)
print(list(result))		# [11,22,33,44]

# 简化做法
result = filter(lambda x: True if type(x) == int else False ,v1)
print(list(result))

# 极简做法
result = filter(lambda x: type(x) == int ,v1)
print(list(result))
```

- reduce

对参数序列中元素进行累积.

```python
import functools
v1 = ['wo','hao','e']

def func(x,y):
    return x+y
result = functools.reduce(func,v1) 
print(result)

result = functools.reduce(lambda x,y:x+y,v1)
print(result)
```

### 3.匿名函数

lambda

## 三.模块

### 1.md5加密字符串

```python
import hashlib		# 引入模块

def get_md5(data):
    obj = hashlib.md5()		# 简单加密,易被破解
    obj.update(data.encode('utf-8'))
    result = obj.hexdigest()
    return result

val = get_md5('123')	# md5加密'123'
print(val)
```

### 2.加盐

```python
import hashlib

def get_md5(data):
    obj = hashlib.md5("resdy54436jgfdsjdxff123ad".encode('utf-8'))
    obj.update(data.encode('utf-8'))
    result = obj.hexdigest()
    return result

val = get_md5('123')
print(val)
```

### 3.应用于用户注册登录

```python
import hashlib
USER_LIST = []		# 注册用户列表
def get_md5(data):	# 加密函数
    obj = hashlib.md5("12:;idy54436jgfdsjdxff123ad".encode('utf-8'))
    obj.update(data.encode('utf-8'))
    result = obj.hexdigest()
    return result


def register():		# 注册函数
    print('**************用户注册**************')
    while True:
        user = input('请输入用户名:')
        if user == 'N':
            return
        pwd = input('请输入密码:')
        temp = {'username':user,'password':get_md5(pwd)}
        USER_LIST.append(temp)

def login():		# 登录函数
    print('**************用户登陆**************')
    user = input('请输入用户名:')
    pwd = input('请输入密码:')

    for item in USER_LIST:
        if item['username'] == user and item['password'] == get_md5(pwd):
            return True


register()
result = login()
if result:
    print('登陆成功')
else:
    print('登陆失败')
```

### 4.密码不显示

pycharm里不起作用,cmd可用

```python
import getpass

pwd = getpass.getpass('请输入密码：')
if pwd == '123':
    print('输入正确')
```

