# day51笔记

## 1.回顾

### 1.html值的操作

- html()
  - innerHTML实现,对文本和标签进行渲染
  - 如果有参数表示设置值,如果没有参数表示获取值,下面方法一样效果
- text()
  - innerText实现,只对文本进行渲染
- val()
  - value()的实现,只对标签中有value属性的有效,比如input等.

### 2.html对象属性操作

- prop()
  - 对JSDOM对象属性操作,比如单选框中checked属性的设置
- removeProp()
  - 移除JSDOM对象属性



## 2.jQuery插件

- www.jq22.com
- 右键检查,Sources下即可找到源码



## 3.bootstrap

- www.bootcss.com

### 1.可视化布局layoutit

- https://www.bootcss.com/p/layoutit/
- 可拖放排序在线编辑的Bootstrap可视化布局系统,自动生成代码.

### 2.下载使用

- 用于生产环境的bootstrap
- 右侧基本模版复制到html文件中
- head中的meta设置对移动设备兼容:

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```

- 修改link href为本地bootstrap.css路径
- 在项目文件中先引入jQuery再引入bootstrap

### 3.栅格

- Class中写入每列占用格数
  - 顺序大屏>中屏>小屏>手机
- thumbnail自定义面板

```html
<div class="col-lg-3 col-md-4 col-sm-6">
    <div class="thumbnail">
        内容
    </div>
</div>
```

- 列偏移
  - 偏移x格col-lg-offset-x

```html
<div class="col-lg-3 col-md-4 col-sm-6 col-lg-offset-6">
```

### 4.排版

- 文本样式 / 对齐样式 / 大小写 / 缩略语 / 地址 / 引用 / 列表 都可以用类或标签使用.
- https://v3.bootcss.com/css/

### 5.表格

- 基本样式
  - 为任意 `<table>` 标签添加 `.table` 类可以为其赋予基本的样式 — 少量的内补（padding）和水平方向的分隔线。

```html
<table class="table">
  ...
</table>
```

- 条纹状表格
  - 通过 `.table-striped` 类可以给 `<tbody>` 之内的每一行增加斑马条纹样式。

```html
<table class="table table-striped">
  ...
</table>
```

- 带边框的表格
  - 添加 `.table-bordered` 类为表格和其中的每个单元格增加边框。

```html
<table class="table table-bordered">
  ...
</table>
```

- 鼠标悬停
  - 通过添加 `.table-hover` 类可以让 `<tbody>` 中的每一行对鼠标悬停状态作出响应。

```html
<table class="table table-hover">
  ...
</table>
```

- 紧缩表格
  - 通过添加 `.table-condensed` 类可以让表格更加紧凑，单元格中的内补（padding）均会减半。

```html
<table class="table table-condensed">
  ...
</table>
```

- 状态类
  - 通过这些状态类可以为行或单元格设置颜色。
  - .active 鼠标悬停在行或单元格上时所设置的颜色
  - .success 标识成功或积极的动作(绿)
  - .info 标识普通的提示信息或动作(蓝)
  - .warning 标识警告或需要用户注意(黄)
  - .danger 标识危险或潜在的带来负面影响的动作(红)

```html
<tr class="active">...</tr>
<tr>
  <td class="active">...</td>
</tr>
```

### 6.表单

- 基本实例

```html
<form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="exampleInputFile">File input</label>
    <input type="file" id="exampleInputFile">
    <p class="help-block">Example block-level help text here.</p>
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox"> Check me out
    </label>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
```

## 4.组件

- 字体图标
  - 不要和其他组件混合使用，嵌套一个span标签，加上对应字体图标的类名.
  - font-awesome ： 字体图标库
- 下拉菜单、按钮组、按钮式下拉菜单、输入框组、导航、导航条、路径导航、分页、标签、徽章、巨幕、页头、缩略图、警告框、进度条、媒体对象、列表组、面板
- https://v3.bootcss.com/components/



## 5.js插件

### 1.模态框

- 模态框经过了优化，更加灵活，以弹出对话框的形式出现，具有最小和最实用的功能集。

- 实例见网页。

- 方法

  - .modal(options) 将页面中的某块内容作为模态框激活。接受可选参数 `object`

  ```js
  $('#myModal').modal({
    keyboard: false
  })
  ```

  - .modal('toggle') 手动打开或关闭模态框
  - .modal('show') 手动打开模态框
  - .modal('hide') 手动隐藏模态框

### 2.滚动监听

- 实例

```html
<body data-spy="scroll" data-target="#navbar-example">
  ...
  <div id="navbar-example">
    <ul class="nav nav-tabs" role="tablist">
      ...
    </ul>
  </div>
  ...
</body>
```

### 3.标签页

- 用法

```js
$('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
```

### 4.工具提示 / 弹出框 / 警告框 / 按钮 / 侧边栏 / 轮播图

- https://v3.bootcss.com/javascript/



## 6.万能模版

- adminlte

