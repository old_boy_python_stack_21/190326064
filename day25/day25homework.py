#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 一、正则表达式练习
# 1、匹配整数或者小数（包括正数和负数）
"""
-?\d+(.\d+)?
"""

# 2、匹配年月日日期 格式2018-12-6
'''
\d{4}-(0[1-9]|1[0-2])-([1-2][0-9]|0[1-9]|3[01])
'''

# 3、匹配qq号
'''
[1-9]\d{4,9}
'''

# 4、11位的电话号码
'''
1[3456789]\d{9}
'''

# 5、长度为8-10位的用户密码 ： 包含数字字母下划线
'''
\w{8,10}
'''

# 6、匹配验证码：4位数字字母组成的
'''
(\d|[a-z]|[A-Z]){4}
'''

# 7、匹配邮箱地址
'''
\w+@\w+\.\w+
'''

# 8、1-2*((60-30+(-40/5)(9-25/3+7/399/42998+10568/14))-(-43)/(16-32))
# 从上面算式中匹配出最内层小括号以及小括号内的表达式
'''
\([^()]+\)
'''

# 9、从类似9-25/3+7/399/42998+10*568/14的表达式中匹配出乘法或除法
'''
\d+((\*|\/)\d+)+
'''

# 10、从类似
# <a>wahaha</a>
# <b>banana</b>
# <h1>qqxing</h1>
# 这样的字符串中，
# 1）匹配出<a>,<b>,<h1>这样的内容
'''
<\w+>
'''

# 2）匹配出wahaha，banana，qqxing内容。(思考题)
'''
import re
s = '<a>wahaha</a>\n<b>banana</b>\n<h1>qqxing</h1>'
lst = s.split('\n')
for i in lst:
    res = re.search(r'<(\w+)>(\w+)</(\w+)>', i)
    print(res.group(2))
'''

# 自学以下内容，完成10、2)
'''
import re
ret = re.search(r"<(?P<tag_name>\w+)>(\w+)</\w+>", "<h1>hello</h2>")
# 还可以在分组中利用?的形式给分组起名字
# 获取的匹配结果可以直接用group('名字')拿到对应的值
print(ret.group('tag_name'))  # 结果 ：h1
print(ret.group())  # 结果 ：<h1>hello</h1>
print(ret.group(1))  # 结果 ：<h1>hello</h1>
print(ret.group(2))  # 结果 ：<h1>hello</h1>
'''

# 二、使用listdir完成计算文件夹大小
'''
import os


class A:
    size = 0

    def get_size(self, path):
        content = os.listdir(path)
        for i in content:
            file = os.path.join(path, i)
            if os.path.isdir(file):
                A().get_size(file)
            else:
                A.size += os.path.getsize(file)
        return A.size


res = A().get_size('D:\\homework')
print(res)
'''

# 三、根据以下需求，完成选课系统作业，5月4号晚上10点之前提交
# https://www.cnblogs.com/Eva-J/articles/9235899.html
