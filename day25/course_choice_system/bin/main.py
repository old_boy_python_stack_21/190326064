#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import sys
from day25.course_choice_system.src.manager import Manager
from day25.course_choice_system.src.students import Students


class Course:

    def __init__(self, num, name, price, days, teach):
        """
        增加课程
        :param name: 课程名
        :param price: 价格
        :param days: 周期天数
        :param teach: 老师
        """
        self.num = num
        self.name = name
        self.price = price
        self.days = days
        self.teach = teach


def run():
    """
    主程序
    :return:
    """
    path = os.path.join(os.path.dirname(os.path.dirname(__file__)), r'db\usermsg')
    while 1:
        user = input('请输入用户名:')
        pwd = input('请输入密码:')
        status = False
        with open(path, 'r', encoding='utf-8')as f:
            for line in f:
                a, b, c, d = line.split('----')
                if a == user and b == pwd:
                    dic = {'stu': '学生', 'admin': '管理员'}
                    print('登陆成功,当前身份为%s' % dic[c])
                    if c == 'stu':
                        cls = Students()
                        print('1.查看所有课程\n2.选择课程\n3.查看所选课程')
                        choice = input('请选择:')
                        dic1 = {'1': cls.course_list, '2': cls.choice_course, '3': cls.choiced}
                        dic1.get(choice)()
                    elif c == 'admin':
                        cls = Manager()
                        print('1.创建课程\n2.创建学生账号\n3.查看所有课程\n4.查看所有学生\n5.查看所有学生的选课情况\n')
                        choice = input('请选择:')
                        dic2 = {'1': cls.mkcourse, '2': cls.mk_stu, '3': cls.check_all_course, '4': cls.check_all_stu,
                                '5': cls.check_status}
                        dic2.get(choice)()

                    return
            if not status:
                print('账号或密码错误,请重新输入!')


if __name__ == '__main__':
    run()
