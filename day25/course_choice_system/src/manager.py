#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import sys


class Manager:

    def mkcourse(self):
        """
        创建课程
        :return:
        """
        course_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'db\\courselist')
        while 1:
            s1 = input('请输入序号:')
            s2 = input('请输入课程名:')
            s3 = input('请输入价格:')
            s4 = input('请输入周期:')
            s5 = input('请输入老师:')
            with open(course_path, 'a', encoding='utf-8')as f:
                f.write('%s----%s----%s----%s----%s\n' % (s1, s2, s3, s4, s5))
            print('创建成功 ! 继续创建请按Y/y , 退出按N/n.')
            user = input('请选择:')
            if user.upper() == 'Y':
                self.mkcourse()
            elif user.upper() == 'N':
                return
            else:
                print('输入错误!')

    def mk_stu(self):
        """
        创建学生账号
        :return:
        """
        path = os.path.join(os.path.dirname(os.path.dirname(__file__)), r'db\usermsg')
        while 1:
            q1 = input('请输入账号:')
            q2 = input('请输入密码:')
            q3 = input('请输入身份:')
            with open(path, 'a', encoding='utf-8')as f:
                f.write('%s----%s----%s----\n' % (q1, q2, q3))
            print('创建成功 ! 继续创建请按Y/y , 退出按N/n.')
            user = input('请选择:')
            if user.upper() == 'Y':
                self.mkcourse()
            elif user.upper() == 'N':
                return
            else:
                print('输入错误!')

    def check_all_course(self):
        """
        查看所有课程
        :return:
        """
        course_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'db\\courselist')
        with open(course_path, 'r', encoding='utf-8')as f:
            for line in f:
                num, kemu, jiage, shichang, jiaoshi = line.split('----')
                print('序号:%s , 科目:%s , 价格:%s , 时长:%s , 教师:%s' % (num, kemu, jiage, shichang, jiaoshi))

    def check_all_stu(self):
        """
        查看所有学生
        :return:
        """
        path = os.path.join(os.path.dirname(os.path.dirname(__file__)), r'db\usermsg')
        with open(path, 'r', encoding='utf-8')as f:
            for line in f:
                q, w, e, r = line.split('----')
                if e == 'stu':
                    print(line)

    def check_status(self):
        """
        查看所有学生的选课情况
        :return:
        """