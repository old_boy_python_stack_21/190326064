#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import sys


class Students:
    stu_list = []

    def course_list(self):
        """
        查看所有课程
        :return:
        """
        course_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),'db\\courselist')
        with open(course_path, 'r', encoding='utf-8')as f:
            for line in f:
                num, kemu, jiage, shichang, jiaoshi = line.split('----')
                print('序号:%s , 科目:%s , 价格:%s , 时长:%s , 教师:%s' % (num, kemu, jiage, shichang, jiaoshi))
        x = input('请输入序号选择课程(返回N/n):')
        if x.upper() == 'N':
            return
        self.choice_course(x)

    def choice_course(self, x=None):
        """
        选课
        :return:
        """
        course_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'db\\courselist')
        if x:
            with open(course_path, 'r', encoding='utf-8')as f:
                for line in f:
                    num, kemu, jiage, shichang, jiaoshi = line.split('----')
                    if x == num:
                        print('您已选择%s !' % kemu)
                        self.stu_list.append(kemu)
                        break
        if not x:
            xx = int(input('请输入课程序号:'))
            with open(course_path, 'r', encoding='utf-8')as f:
                for line in f:
                    num, kemu, jiage, shichang, jiaoshi = line.split('----')
                    if xx == num:
                        print('您已选择%s !' % kemu)
                        self.stu_list.append(kemu)
                        break
        while 1:
            print('继续选课请按Y/y , 退出请按N/n.')
            user = input('请选择:')
            if user.upper() == 'Y':
                self.course_list()
            elif user.upper() == 'N':
                sys.exit()
            else:
                print('输入错误!')

    def choiced(self):
        """
        查看所选课程
        :return:
        """
        print(self.stu_list)
        while 1:
            print('继续选课请按Y/y , 退出请按N/n.')
            user = input('请选择:')
            if user.upper() == 'Y':
                self.course_list()
            elif user.upper() == 'N':
                sys.exit()
            else:
                print('输入错误!')