# 1.有变量name = "aleX leNb " 完成如下操作：
'''
name = "aleX leNb "
'''
# 移除 name 变量对应的值两边的空格,并输出处理结果
'''
name1 = name.strip()
print(name1)
'''
# 判断 name 变量是否以 "al" 开头,并输出结果（用切片）
'''
if name[:2] == 'al':
    print(True)
else:
    print(False)
'''
# 判断name变量是否以"Nb"结尾,并输出结果（用切片）
'''
if name[-2:] == 'Nb':
    print(True)
else:
    print(False)
'''
# 将 name 变量对应的值中的 所有的"l" 替换为 "p",并输出结果
'''
name2 = name.replace('l','p')
print(name2)
'''
# 将name变量对应的值中的第一个"l"替换成"p",并输出结果
'''
name3 = name.replace('l','p',1)
print(name3)
'''
# 将 name 变量对应的值根据 所有的"l" 分割,并输出结果
'''
name4 = name.split('l')
print(name4)
'''
# 将name变量对应的值根据第一个"l"分割,并输出结果
'''
name5 = name.split('l',1)
print(name5)
'''
# 将 name 变量对应的值变大写,并输出结果
'''
name6 = name.upper()
print(name6)
'''
# 将 name 变量对应的值变小写,并输出结果
'''
name7 = name.lower()
print(name7)
'''
# 请输出 name 变量对应的值的第 2 个字符?
'''
name8 = name[1]
print(name8)
'''
# 请输出 name 变量对应的值的前 3 个字符?
'''
name9 = name[:3]
print(name9)
'''
# 请输出 name 变量对应的值的后 2 个字符?
'''
name10 = name[-2:]
print(name10)
'''

# 2.有字符串s = "123a4b5c"
'''
s = "123a4b5c"
'''
# 通过对s切片形成新的字符串 "123"
'''
s1 = s[:3]
print(s1)
'''
# 通过对s切片形成新的字符串 "a4b"
'''
s2 = s[3:6]
print(s2)
'''
# 通过对s切片形成字符串s5,s5 = "c"
'''
s5 = s[-2]
print(s5)
'''
# 通过对s切片形成字符串s6,s6 = "ba2"
'''
s6 = s[-3::-2]
print(s6)
'''

# 3.使用while循环字符串 s="asdfer" 中每个元素。
'''
s = "asdfer"
count = 0
while count < len(s):
    print(s[count])
    count += 1
'''

# 4.使用while循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"。
'''
s = "321"
count = 0
while count < len(s):
    print('倒计时%s秒' % s[count])
    count += 1
print('出发!')
'''

# 5.实现一个整数加法计算器(两个数相加)：如：content = input("请输入内容:") 用户输入：5+9或5+ 9或5 + 9（含空白），然后进
# 行分割转换最终进行整数的计算得到结果。
'''
content = input("请输入内容:")
s = content.split('+')
print(int(s[0])+int(s[1]))
'''

# 6.计算用户输入的内容中有几个 h 字符？如：content = input("请输入内容：") # 如fhdal234slfh98769fjdla
'''
content = input("请输入内容：")
count = 0
i = 0
while i < len(content):
    if content[i] == 'h':
        count += 1
    i += 1
print(count)
'''

# 7.计算用户输入的内容中有几个 h 或 H 字符？如：content = input("请输入内容：") #
'''
content = input("请输入内容：")
count = 0
i = 0
while i < len(content):
    sss = content[i].upper()
    if  sss == 'H':
        count += 1
    i += 1
print(count)
'''

# 8.使用while循环分别正向和反向对字符串 message = "伤情最是晚凉天，憔悴厮人不堪言。"。
'''
message = "伤情最是晚凉天，憔悴厮人不堪言。"
count = 0
s1 = ''
while count < len(message):
    a = message[count]
    s1 += a
    count += 1
print(s1)
s2 = ''
count = 0
while count < len(message):
    b = message[len(message)-count-1]
    s2 += b
    count += 1
print(s2)
'''

# 9.获取用户输入的内容中 前4个字符中 有几个 A ？如：content = input("请输入内容：") # 如fAdal234slfH9H769fjdla
'''
content = input("请输入内容：")
new_content = content[:4]
i = 0
sum = 0
while i < len(new_content):
    if new_content[i] == 'A':
        sum += 1
    i += 1
print(sum)
'''

# 10.如果判断name变量对应的值前四位"l"出现几次,并输出结果。
'''
name = input('请输入:')
new_name = name[:4]
i = 0
count = 0
while i < len(new_name):
    if new_name[i] == '1':
        count += 1
    i += 1
print(count)
'''

# 11.获取用户两次输入的内容，并将所有的数据获取并进行相加，如：
"""
要求：
	将num1中的的所有数字倒找并拼接起来：1232312
	将num1中的的所有数字倒找并拼接起来：1218323
	然后将两个数字进行相加。
"""
'''
num1 = input("请输入：")  # asdfd123sf2312
num2 = input("请输入：")  # a12dfd183sf23
count = 0
new_num1 = ''
while count < len(num1):
    s1 = num1[count]
    if s1.isdigit():
        new_num1 += s1
    count += 1
n1 = int(new_num1)
count = 0
new_num2 = ''
while count < len(num2):
    s2 = num2[count]
    if s2.isdigit():
        new_num2 += s2
    count += 1
n2 = int(new_num2)
print(n1 + n2)
'''

#12.思维导图