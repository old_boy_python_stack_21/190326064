# day33笔记

## 1.回顾

### 1.1并发编程

1.进程 在我们目前完成的一些项目里是不常用到的

2.线程 后面的爬虫阶段经常用(前端的障碍)

3.协程  异步的框架 异步的爬虫模块

- 线程

  ```python
  线程是进程的一部分，每个进程中至少有一个线程
  能被CPU调度的最小单位
  一个进程中的多个线程是可以共享这个进程的数据的  —— 数据共享
  线程的创建、销毁、切换 开销远远小于进程  —— 开销小
  ```

### 1.2multiprocessing进程

p = Process(target=函数名,args=(参数1,))

#### 1.如何创建一个进程对象

- 对象和进程之间的关系
  - 进程对象和进程并没有直接的关系,只是存储了一些和进程相关的内容
  - 此时此刻，操作系统还没有接到创建进程的指令

#### 2.如何开启一个进程

通过p.start()开启了一个进程--这个方法相当于给了操作系统一条指令.

- start方法 的 非阻塞和异步的特点
  - 在执行开启进程这个方法的时候,我们既不等待这个进程开启，也不等待操作系统给我们的响应
  - 这里只是负责通知操作系统去开启一个进程,开启了一个子进程之后，主进程的代码和子进程的代码完全异步

#### 3.父进程和子进程之间的关系

- 父进程会等待子进程结束之后才结束,为了回收子进程的资源

#### 4.不同操作系统中进程开启的方式

- windows 通过（模块导入）再一次执行父进程文件中的代码来获取父进程中的数据
  - 所以只要是不希望被子进程执行的代码，就写在if __ name__ == '__ main__ '下,因为在进行导入的时候父进程文件中的__ name__ != '__ main__'
- linux/ios
  - 正常的写就可以，没有if __ name__ == '__ main__'这件事情了

#### 5.如何确认一个子进程执行完毕

- join方法
- 开启了多个子进程，等待所有子进程结束

## 2.守护进程

```python
import time
from multiprocessing import Process

def son1(a,b):
    while True:
        print('is alive')
        time.sleep(0.5)

def son2():
    for i in range(5):
        print('in son2')
        time.sleep(1)

if __name__ == '__main__':
    p = Process(target=son1,args=(1,2))
    p.daemon = True
    p.start()      # 把p子进程设置成了一个守护进程
    p2 = Process(target=son2)
    p2.start()
    time.sleep(2)
```

daemon,把子进程设置为守护进程.

- 守护进程是随着主进程的代码结束而结束的
  - 生产者消费者模型的时候
  - 和守护线程做对比的时候
- 所有的子进程都必须在主进程结束之前结束，由主进程来负责回收资源

## 3.Process对象的其他方法

terminate结束进程

```python
import time
from multiprocessing import Process

def son1():
    while True:
        print('is alive')
        time.sleep(0.5)

if __name__ == '__main__':
    p = Process(target=son1)
    p.start()      # 异步 非阻塞
    print(p.is_alive())		# 判断是活着的进程
    time.sleep(1)
    p.terminate()   # 结束进程,异步的 非阻塞
    print(p.is_alive())   # 进程还活着 因为操作系统还没来得及关闭进程
    time.sleep(0.01)
    print(p.is_alive())   # 操作系统已经响应了我们要关闭进程的需求，再去检测的时候，得到的结果是进程已经结束了
```



## 4.使用面向对象的方式

```python
import os
import time
from multiprocessing import Process

class MyProcecss2(Process):
    def run(self):
        while True:
            print('is alive')
            time.sleep(0.5)

class MyProcecss1(Process):
    def __init__(self,x,y):
        self.x = x
        self.y = y
        super().__init__()     # 必须带
    def run(self):
        print(self.x,self.y,os.getpid())
        for i in range(5):
            print('in son2')
            time.sleep(1)

if __name__ == '__main__':
    mp = MyProcecss1(1,2)
    mp.daemon = True
    mp.start()
    print(mp.is_alive())    # True
    mp.terminate()
    mp2 = MyProcecss2()
    mp2.daemon = True   # 设置为守护进程
    mp2.start()
    print('main :',os.getpid())
    time.sleep(2)
```

## 5.Process类总结

### 5.1开启进程的方式

- 面向函数

  - def 函数名 : 要在子进程中执行的代码
  - p = Process(target=函数名,args=(参数1,))
  - p.start()  开启了进程

- 面向对象

  ```python
  class 类名(Process):
  	def __init__(self,参数1，参数2):   # 如果子进程不需要参数可以不写
  		self.a = 参数1
  		self.b = 参数2
  		super().__init__()
      def run(self):
          # 要在子进程中执行的代码
  p = 类名(参数1，参数2)
  ```

- Process提供的操作进程的方法

  - p.start()  开启进程    异步非阻塞
  - p.terminate()  结束进程    异步非阻塞
  - p.join()  同步阻塞
  - p.is_alive()  获取当前进程的状态
  - daemon = True 设置为守护进程，守护进程永远在主进程的代码结束之后自动结束

## 6.锁

- 1.实现能够响应多个client端的server
- 2.抢票系统

```python
import time
import json
from multiprocessing import Process,Lock

def search_ticket(user):
    with open('ticket_count') as f:
        dic = json.load(f)
        print('%s查询结果  : %s张余票'%(user,dic['count']))

def buy_ticket(user,lock):
    with lock:      # with lock和lock.acquire建议with lock
    # lock.acquire()   # 给这段代码加上一把锁,release解锁
        time.sleep(0.02)
        with open('ticket_count') as f:
            dic = json.load(f)
        if dic['count'] > 0:
            print('%s买到票了'%(user))
            dic['count'] -= 1
        else:
            print('%s没买到票' % (user))
        time.sleep(0.02)
        with open('ticket_count','w') as f:
            json.dump(dic,f)
    # lock.release()   # 给这段代码解锁

def task(user, lock):
    search_ticket(user)
    with lock:
        buy_ticket(user, lock)

if __name__ == '__main__':
    lock = Lock()
    for i in range(10):
        p = Process(target=task,args=('user%s'%i,lock))
        p.start()
```

- 1.如果在一个并发的场景下，涉及到某部分内容
  - 是需要修改一些所有进程共享数据资源,需要加锁来维护数据的安全
- 2.在数据安全的基础上，才考虑效率问题
- 3.同步存在的意义 : 数据的安全性

- 在主进程中实例化 lock = Lock() , 把这把锁传递给子进程 , 在子进程中 对需要加锁的代码 进行 with lock：
  - with lock相当于lock.acquire()和lock.release()
- 在进程中需要加锁的场景
  - 共享的数据资源（文件、数据库）
  - 对资源进行修改、删除操作
- 加锁之后能够保证数据的安全性 但是也降低了程序的执行效率

## 7.进程之间的通信

### 7.1进程之间的数据隔离

```python
from multiprocessing import Process
n = 100
def func():
    global n
    n -= 1

if __name__ == '__main__':
    p_l = []
    for i in range(10):
        p = Process(target=func)    # 创建多个进程,每个进程在自己的作用域n-=1
        p.start()
        p_l.append(p)
    for p in p_l:p.join()
    print(n)    # 输出的n是全局变量的n
```

### 7.2通信

进程之间的通信 - IPC(inter process communication)

- 先进先出

  ```python
  from multiprocessing import Queue,Process
  def func(exp,q):
      ret = eval(exp)
      q.put({ret,2,3})	# 一个一个放入队列
      q.put(ret*2)
      q.put(ret*4)
  
  if __name__ == '__main__':
      q = Queue()
      Process(target=func,args=('1+2+3',q)).start()
      print(q.get())	# 一个个取出
      print(q.get())
      print(q.get())	# 没有则阻塞
  ```

Queue基于  数据安全

pipe 管道(不安全的) = 文件家族的socket pickle

队列 = 管道 + 锁

```python
from multiprocessing import Pipe
pip = Pipe()
pip.send()
pip.recv()
```

```python
import queue

from multiprocessing import Queue
q = Queue(5)    # 设置队列长度
q.put(1)     # 放入数据
q.put(2)
q.put(3)
q.put(4)
q.put(5)   # 当队列为满的时候用put向队列中放数据 队列会阻塞
print('5555555')
try:
    q.put_nowait(6)  # 当队列为满的时候用put_nowait向队列中放数据 会报错并且会丢失数据
except queue.Full:
    pass
print('6666666')

print(q.get())
print(q.get())
print(q.get())   # 在队列为空的时候会发生阻塞
print(q.get())   # 在队列为空的时候会发生阻塞
print(q.get())   # 在队列为空的时候会发生阻塞
try:
    print(q.get_nowait())   # 在队列为空的时候 直接报错
except queue.Empty:
    pass
```


