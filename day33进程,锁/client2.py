#!/usr/bin/env python
# -*- coding:utf-8 -*-
import time
import json
import socket
import struct

sk = socket.socket()
sk.connect(('127.0.0.1', 9000))
while 1:
    msg = '666'
    bmsg = json.dumps(msg).encode('utf-8')
    len_msg = struct.pack('i', len(bmsg))
    sk.send(len_msg)
    sk.send(bmsg)
    time.sleep(2)
