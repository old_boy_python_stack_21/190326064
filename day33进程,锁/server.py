#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 0、整理两天的笔记和每一个课上例题
# 1、进程间内存是否共享？如何实现通讯？
# 不共享,网络和文件.

# 2、请聊聊进程队列的特点和实现原理？
# Queue,特点:先进先出,Queue()可传入长度,用put传入超出则阻塞,用put_nowait传入超出则报错.取出用get
# 原理: 队列 = 管道 + 锁

# 3、请画出进程的三状态转换图
# 见进程三状态图.png

# 4、从你的角度说说进程在计算机中扮演什么角色？
# 进程（Process）是计算机中的程序关于某数据集合上的一次运行活动，是系统进行资源分配和调度的基本单位，是操作系统结构的基础。

# 5、使用进程实现并发的socket的server端
import json
import socket
import struct
from multiprocessing import Process


def func(conn):
    while 1:
        a = conn.recv(4)
        len_msg = struct.unpack('i', a)[0]
        dic_msg = conn.recv(len_msg).decode('utf-8')
        msg = json.loads(dic_msg)
        print(msg)


if __name__ == '__main__':
    sk = socket.socket()
    sk.bind(('127.0.0.1', 9000))
    sk.listen()
    while 1:
        conn, addr = sk.accept()
        p = Process(target=func, args=(conn,))
        p.start()
