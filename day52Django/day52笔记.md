# day52笔记

## 1.HTTP协议

### 1.http版本

普遍使用http 1.1

### 2.在浏览器地址栏输入URL,按下回车后会经历哪些流程

```
1.浏览器向 DNS 服务器请求解析该 URL 中的域名所对应的 IP 地址;
2.解析出 IP 地址后，根据该 IP 地址和默认端口 80，和服务器建立TCP连接;
3.浏览器发出读取文件(URL 中域名后面部分对应的文件)的HTTP 请求，该请求报文作为 TCP 三次握手的第三个报文的数据发送给服务器;
4.服务器对浏览器请求作出响应，并把对应的 html 文本发送给浏览器;
5.释放 TCP连接;
6.浏览器将该 html 文本并显示内容; 
```

### 3.http请求方法(8种)数据

```
*get获取资源,post提交
head请求,put上传,delete删除,trace回显请求,options回传,connect代理服务器
```

### 4.http状态码

- 所有HTTP响应的第一行都是状态行，依次是当前HTTP版本号，3位数字组成的状态代码，以及描述状态的短语，彼此由空格分隔。
- 状态代码的第一个数字代表当前响应的类型

```
1xx消息——请求已被服务器接收，继续处理
2xx成功——请求已成功被服务器接收、理解、并接受
3xx重定向——需要后续操作才能完成这一请求
4xx请求错误——请求含有词法错误或者无法被执行
5xx服务器错误——服务器在处理某个正确请求时发生错误
```

### 5.url 统一资源定位符

```
传送协议:http/https
url标记符://
域名或ip:www.xxx.com/127.xxx.xxx.xxxx
端口号:http默认80,https默认443
路径:以“/”字符区别路径中的每一个目录名称
查询:GET模式的窗体参数，以“?”字符为起点，每个参数以“&”隔开，再以“=”分开参数名称与数据，通常以UTF8的URL编码，避开字符冲突的问题
片段:以“#”字符为起点
```

### 6.http请求格式

- 浏览器 ->服务器 请求(request)
- get请求没有请求数据

![1560219130805](C:\Users\建雄\AppData\Roaming\Typora\typora-user-images\1560219130805.png)

### 7.http响应格式

- 服务器 ->浏览器 响应(response)

![1560219635435](C:\Users\建雄\AppData\Roaming\Typora\typora-user-images\1560219635435.png)



## 2.web框架

- 本质:所有的Web应用本质上就是一个socket服务端，而用户的浏览器就是一个socket客户端。 

### 1.socket收发消息

- wsgiref测试使用
- uWSGI线上使用

### 2.根据不同的路径返回不同的内容

- /index
- /home

### 3.返回动态页面(字符串的替换)

- jinjia2

### 不同框架实现的功能

- Django实现2和3
- flask 实现2
- tornado 1 2 3



## 3.Django的下载安装使用

### 1.下载

- 命令行
  - pip3 install django==1.11.21 -i https://pypi.tuna.tsinghua.edu.cn/simple

### 2.常见命令(无界面使用)

- 创建项目
  - django-admin startproject mysite
  - 创建好项目之后，可以查看当前目录下多出一个名为mysite的文件夹.
- 启动Django项目
  - 启动项目的时候，需要切换到mysite目录下，执行如下命令：
    - python manage.py runserver　　#默认使用8000端口
  - 命令后面还可以指定参数：
    - python manage.py runserver 8888　　#8888为新指定的端口
    - python manage.py runserver 127.0.0.1:8000　　#还可以指定IP和端口，冒号分割
- 创建APP
  - 一个Django项目可以分为很多个APP，用来隔离不同功能模块的代码。
  - python manage.py startapp app01
  - 执行命令后，项目目录下多出一个app01的文件夹.
- 数据库迁移
  - python manage.py makemigrations
  - python manage.py migrate
- 创建超级用户
  - python manage.py createsuperuser
  - 输入以上命令后，根据提示输入用户名、邮箱、密码、确认密码。密码的要求至少是不八位，不能和邮箱太接近，两次密码需要一致。
- 需要在setting中设置DIR目录.

### 3.使用

- new project创建新项目
- templates文件夹里放入子目录
- urls.py中导入模块并写入相关路径

```Python
from django.shortcuts import HttpResponse,render
def index(request):
    return render(request,'html文件')
urlpatterns = [
    url(r'^admin/', admin.site.urls),  #默认
    url(r'^index/', index),  #自己添加
]
```





