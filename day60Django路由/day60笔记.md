# day60笔记

## 1.回顾

### 1.FBV   CBV  

- 定义CBV

```python 
from  django.views import View
class  AddPublisher(View):
    
    def get(self,request,*args,**kwargs):
        # 处理get请求
        return response
    
    def post(self,request,*args,**kwargs):
        # 处理post请求
        return response
```

- 使用：

```python
form app01 import views
url(r'add_publisher/',views.AddPublisher.as_view())
```

### 2.as_view的流程

1. 项目启动，执行AddPublisher.as_view()       ——》  view

   ​	url(r'add_publisher/',views.AddPublisher.as_view())

   ​	url(r'add_publisher/', view )

2. 请求到来时执行view函数：

   1. 实例化AddPublisher   ——》 self

   2. self.request = reqeust

   3. 执行self.dispatch(request,*args,**kwargs)  

      1. 判断请求方式是否被允许     http_method_names  = []

         1. 允许

            通过反射获取到当前请求方式对应的方法   ——》 handler

         2. 不允许

            self.http_method_not_allowed    ——》  handler

      2. 执行handler(request,*args,**kwargs)     ——》  返回响应

### 3.加装饰器

- from  django.utils.decorators  import method_decorator
- 加方法上：

```python
@method_decorator(timer)
def  get（self,request）
```

- 加在类上

```python 
@method_decorator(timer,name='get')
@method_decorator(timer,name='post')
class  AddPublisher(View):
    
@method_decorator(timer,name='dispatch')
class  AddPublisher(View):
```

- 加在dispatch方法上

```python
@method_decorator(timer)
def dispatch(self,request,*args,**kwargs):
    ret = super().dispatch(request,*args,**kwargs)
    return ret 
```

### 4.request

- request.method   ——》 当前的请求方式  GET  POST
- request.GET       ——》 url上的参数    ?k1=v1&k2=v2
- request.POST    ——》 POST请求提交的数据  
- request.path_info   ——》 路径信息   不包括 ip和端口  不包括参数
- request.body    ——》  请求体 字节a
- request.FILES   ——》  上传的文件

```
1.enctype = 'multipart/form-data'
2.f1.chunks()
```

- request.META   ——》  请求头    全大写    HTTP_      -  ——》 _
- request.cookies    ——》 cookie
- request.session    ——》 session



- request.get_host()     获取主机的IP和端口
- request.get_full_path()    ——》  路径信息   不包括 ip和端口  包括参数
- request.is_ajax()     ——》 是否是ajax请求

### 5.response

HttpResponse('字符串')       ——》  字符串     content-type ='text/html' 

render(request,'模板的文件名',{})    ——》  返回一个完整的页面  

redirect(重定向的地址)     ——》   Location：地址

JsonResponse({})         content-type ='application/json' 

JsonResponse(非字典，safe=False)   



## 2.今日内容

https://www.cnblogs.com/maple-shaw/articles/9282718.html

### 1.urlconf

- url(正则表达式, views视图，参数，别名)
  - 正则表达式：一个正则表达式字符串
  - views视图：一个可调用对象，通常为一个视图函数
  - 参数：可选的要传递给视图函数的默认参数（字典形式）
  - 别名：一个可选的name参数

```python
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^blog/$', views.blog),
    url(r'^blog/[0-9]{4}/$', views.blogs),
]
```

- django2.0写法
  - path新写法
  - re_path同旧版写法

```python
from django.urls import path，re_path
urlpatterns = [
    path('articles/2003/', views.special_case_2003),
    re_path(r'^blog/[0-9]{4}/$', views.blogs)
]
```

### 2.正则表达式

- ^   $    [0-9]   [a-zA-Z]     \d   \w   .    *  +   ？
- 注意事项
  - urlpatterns中的元素按照书写顺序从上往下逐一匹配正则表达式，一旦匹配成功则不再继续。
  - 若要从URL中捕获一个值，只需要在它周围放置一对圆括号（分组匹配）。
  - 不需要添加一个前导的反斜杠，因为每个URL 都有。例如，应该是^articles 而不是 ^/articles。
  - 每个正则表达式前面的'r' 是可选的但是建议加上。
- 是否开启URL访问地址后面不为/跳转至带有/的路径的配置项在settings中
  - APPEND_SLASH=True
  - True默认自动加 / , 改为False则不自动加.

### 3.分组和命名分组

- 分组

```
url(r'^blog/([0-9]{4})/$', views.blogs),    —— 》 分组  将捕获的参数按位置传参传递给视图函数
```

- 命名分组

```python
url(r'^blog/(?P<year>[0-9]{4})/$', views.blogs),   —— 》 命名分组  将捕获的参数按关键字传参传递给视图函数
```

- URLconf匹配的位置

```
URLconf 在请求的URL 上查找，将它当做一个普通的Python 字符串。不包括GET和POST参数以及域名。
例如，http://www.example.com/myapp/ 请求中，URLconf 将查找 /myapp/ 。
在http://www.example.com/myapp/?page=3 请求中，URLconf 仍将查找 /myapp/ 。
URLconf 不检查请求的方法。换句话讲，所有的请求方法 —— 同一个URL的POST、GET、HEAD等等 —— 都将路由到相同的函数。
```

- 捕获的参数永远都是字符串

```
每个在URLconf中捕获的参数都作为一个普通的Python字符串传递给视图，无论正则表达式使用的是什么匹配方式。例如，下面这行URLconf 中：
url(r'^articles/(?P<year>[0-9]{4})/$', views.year_archive),
传递到视图函数views.year_archive() 中的year参数永远是一个字符串类型。
```

- 视图函数中指定默认值

### 4.include

- 通常，我们会在每个app里，各自创建一个urls.py路由模块，然后从根路由出发，将app所属的url请求，全部转发到相应的urls.py模块中。

```python
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls')),
]
```

- app01.urls

```python
from django.conf.urls import url
from . import views
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^blog/$', views.blog),
    url(r'^blog/(?P<year>[0-9]{4})/(?P<month>\d{2})/$', views.blogs),
]
```

### 5.URL的命名和反向解析

#### 1.静态路由

命名

```python
url(r'^blog/$', views.blog, name='blog'),
```

反向解析
- 模板html中

```html
{% url 'blog' %}    ->  /blog/
```

- py文件中
  - shortcut中也可以导入reverse,不过它也是从django.urls中导入的
  - ()中写谁就返回谁的地址

```python
from django.urls import reverse
reverse('blog')   -> '/blog/'
```

#### 2.动态路由

分组,生成多个参数

```python
url(r'^blog/([0-9]{4})/(\d{2})/$', views.blogs, name='blogs')
```

反向解析

- 模板中传参数

```Django
{% url 'blogs' 2222 12 %}"   ——》  /blog/2222/12/
```

- py文件中

```python
from django.urls import reverse
reverse('blogs',args=('2019','06'))   ——》 /app01/blog/2019/06/
```

#### 3.命名分组

命名分组

```python
url(r'^blog/([0-9]{4})/(\d{2})/$', views.blogs, name='blogs'),
```

反向解析

- 模板

```django
{% url 'blogs' 2222 12 %}"   ——》  /blog/2222/12/
{% url 'blogs' year=2222 month=12 %}"   ——》  /blog/2222/12/
```

- py文件中

```python
from django.urls import reverse
reverse('blogs',args=('2019','06'))   ——》 /app01/blog/2019/06/ 
reverse('blogs',kwargs={'year':'2019','month':'06'})   ——》 /app01/blog/2019/06/ 
```

### 6.namespace命名空间

- 即使不同的APP使用相同的URL名称，URL的命名空间模式也可以让你唯一反转命名的URL。
- 项目中的urls.py

```python
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls', namespace='app01')),
    url(r'^app02/', include('app02.urls', namespace='app02')),
]
```

- 模板中使用

```Django
{% url 'app01:blogs' 2222 12 %}
```

- views中使用

```python
reverse('app01:blogs',args=('2019','06'))
```

