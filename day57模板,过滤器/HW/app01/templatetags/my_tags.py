#!/usr/bin/env python
# -*- coding:utf-8 -*-

from django import template

register = template.Library()


@register.filter
def jian(value, arg):
    return '{}'.format(value - arg)


@register.filter
def cheng(value, arg):
    return '{}'.format(value * arg)


@register.filter
def chu(value, arg):
    return '{}'.format(value / arg)


@register.filter
def show_a(value, arg):
    return '<a href="https://{}">{}</a>'.format(value, arg)
