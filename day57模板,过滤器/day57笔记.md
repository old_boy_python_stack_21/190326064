# day57笔记

## 1.回顾

### 1.Django中所有命令

- 下载安装
  - pip  install django==1.11.21  -i 源
- 创建项目
  - django-admin startproject   项目名称
- 启动项目
  - cd 项目根目录下
  - python manage.py  runserver   # 127.0.0.1:8000
  - python manage.py  runserver   80 # 127.0.0.1:80
  - python manage.py  runserver   0.0.0.0:80 # 0.0.0.0:80
- 创建app
  - python manage.py  startapp  app名称
- 数据库迁移
  - pyhton manage.py makemigrations
  - pyhton manage.py migrate

### 2.配置

- 静态文件
  - STATIC_URL = '/static/' 
  - STATICFILES_DIRS = [os.path.join(BASE_DIR,‘static’)，]
- 模板  
  - TEMPLATES  DIRS  [os.path.join(BASE_DIR,‘templates’)，]
- 数据库
  - DATABASES = [ENGINE 引擎,NAME   数据库的名称,HOST   ip,PORT   端口   3306,USER  用户民,PASSWORD 密码]
- app
  - INSTALLED_APPS  =[  'app01' / 'app01.apps.App01Config' ]
- 中间件
  - MIDDLEWARE     注释csrf相关的中间   提交POST请求

### 3.request

- request.method    ——》  请求方式 GET POST 
- request.GET           ——》   url上携带的参数     ?name=ddd&id=11    {}
- request.POST        ——》   form表单提交的POST请求的数据   {}

### 4.响应

- HttpResponse('字符串')       ——》  字符串
- render(request,'模板的文件名',{k1:v1})       ——》 返回的是一个完整的页面
- redirect('/index/')    ——》  重定向     Location ： /index/

### 5.ORM操作

```python
from django.db import models

class Publisher(models.Model):   # app01_publisher
    pid = models.AutoField(primary_key=Ture)
    name = models.CharField(max_length=32)   # varchar(32)

class Book(models.Model):
    title = models.CharField(max_length=32)
    pub = models.ForeignKey('Publisher',on_delete=models.CASCADE)  # pub_id

class Author(models.Model):
    name = models.CharField(max_length=32)   # varchar(32)
    books = models.ManyToManyField('Book')
```

```python
from app01 import models
# 查询
models.Publisher.objects.all()  # 查询所有的数据  queryset   对象列表
models.Publisher.objects.get(name='xxx',pid=1)   # Publisher对象  查询不到或者多个 就报错
models.Publisher.objects.filter(name='xxx',pid=1)   # 查询满足条件的所有对象    对象列表
models.Publisher.objects.all().order_by('id')  # 排序  默认升序

pub_obj.pid  pub_obj.pk 
pub_obj.name

book_obj.pub   ——》 所关联的对象
book_obj.pub_id   ——》所关联的对象id

author_obj.books    ——》 关系管理对象
author_obj.books.all()    ——》 所关联的所有对象

# 新增
models.Publisher.objects.create(name='xxxx')

models.Book.objects.create(title='xxxx',pub=Publisher的对象)
models.Book.objects.create(title='xxxx',pub_id=Publisher的对象id)

author_obj = models.Author.objects.create(name='xxxx')
author_obj.books.set([书籍对象的id,书籍对象的id,书籍对象的id])   #  每次都是重新设置

# 删除
models.Publisher.objects.get(pk=1).delete()
models.Publisher.objects.filter(pk=1).delete()

# 修改
pub_obj.name = 'xxxx'
pub_obj.save()

book_obj.title = 'xxx'
book_obj.pub = publisher的对象
book_obj.pub_id = publisher的对象的id
book_obj.save() 

author_obj.name = 'xxx'
author_obj.save()
author_obj.books.set([id,id])
```



## 2.MVC和MTV

### 1.MVC和MTV

- M model 模型 / V view 视图 / C controller 控制器
- M model 模型 / T template 模板 / V view 业务逻辑

### 2.模板 - 变量

- 变量 {{ }}

- .索引  .key  .属性  .方法

  ```django
  {# 取l中的第一个参数 #}
  {{ l.0 }}
  {# 取字典中key的值 #}
  {{ d.key }}
  {# 取对象的name属性 #}
  {{ person_list.0.name }}
  {# .操作只能调用不带参数的方法 #}
  {{ person_list.0.dream }}
  ```

- 当模板系统遇到一个（.）时，会按照如下的顺序去查询：

  1. 在字典中查询
  2. 属性或者方法
  3. 数字索引

### 3.filters过滤器

Django官方文档:https://docs.djangoproject.com/en/1.11/ref/templates/language/

语法 : {{ value|filter_name:参数 }}

- default

```
{{ value|default:"nothing"}}  # 变量不存在或者为空  显示默认值
```

- filesizeformat

  - 将值格式化为一个 “人类可读的” 文件尺寸 （例如 '13 KB', '4.1 MB', '102 bytes', 等等）
  - 如果 value 是 123456789，输出将会是 117.7 MB。

- add
  - 相当于+    默认加法    字符串的拼接   列表的拼接

  - ```django
    {{ value|add:"2" }}
    ```

    value是数字4，则输出结果为6。(能转为int自动转换,不能则字符串拼接)

    ```django
    {{ first|add:second }}
    ```

    如果first是 [1,.2,3] ，second是 [4,5,6] ，那输出结果是 [1,2,3,4,5,6] 。

- lower小写 / upper大写 / title标题 / ljust左对齐 / rjust右对齐 / center居中 / length返回长度 / first第一个元素 / last最后一个元素 / join拼接 / truncatechars截断,参数设置长度,以...结尾

  - rjust/ljust/center后面跟的是百分百数字

- slice切片,可传3个参数

```
{{ hobby|slice:'-2:0:-1' }} 
```

- date日期格式化

  - 方法1

  ```django
  {{ now|date:'Y-m-d H:i:s' }}
  ```

  - 方法2 : 在setting中的配置：

    ```python
    USE_L10N = False
    DATETIME_FORMAT = 'Y-m-d H:i:s'
    ```

    然后{{ now }}即可.

- safe
  - 告诉django不需要转义

    ```django
    value = "<a href='#'>点我</a>"
    {{ value|safe }}
    ```

### 4.自定义filter

- 带有一个或两个参数的python函数.

1.在app下创建一个名为templatetags(固定名称)的python包；

2.在python中创建py文件,文件名自定义（my_tags.py）;

3.在my_tags.py文件中写：

```python
from django import template
register = template.Library()  # register也不能变
```

4.然后写函数+装饰器

```python
@register.filter
def add_xx(value, arg):  # 最多有两个
    return '{}-{}'.format(value, arg)
```

使用：

```django
{% load my_tags %}
{{ 'alex'|add_xx:'dsb' }}
```

输出: alex-dasb

