#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.计算任意一个文件夹的大小（考虑绝对路径的问题）
# 基础需求  这个文件夹中只有文件
# 进阶需求  这个文件夹中可能有文件夹，并且文件夹中还可能有文件夹...不知道有多少层
"""
import os


def filesize(user_path):
    s = 0
    wenjian = os.walk(user_path)
    for a, b, c in wenjian:
        # s += os.path.getsize(a)
        for i in c:
            s += os.path.getsize(os.path.join(a, i))
    print('%sKB' % (s / 1024))


filesize('D:\\homework\\day23')
"""

# 2.校验大文件的一致性
# 基础需求 : 普通的文字文件
# 进阶需求 : 视频或者是图片
'''
import hashlib

file1 = input('请输入文件1路径')
file2 = input('请输入文件2路径')
md51 = hashlib.md5()
md52 = hashlib.md5()
with open(file1, 'rb')as f1, open(file2, 'rb')as f2:
    for line in f1:
        if not line:
            break
        md51.update(line)
    md51 = md51.hexdigest()
    for line in f2:
        if not line:
            break
        md52.update(line)
    md52 = md52.hexdigest()

print(md51 == md52)
'''

# 3.发红包
# 每一个人能够抢到的金额的概率都是平均的
# 小数的不准确性的问题
'''
import random

money = int(input('发多少钱呀?'))
num = int(input('发几个呀?'))
b = []
s = 0
while num > 1:
    a = round(random.uniform(0.01, money/num*2), 2)
    b.append(a)
    s += a
    money -= a
    num -= 1
b.append(round(money, 2))
print(b)
print(sum(b))
'''
