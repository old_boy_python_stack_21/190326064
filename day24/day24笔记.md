# day24笔记

## 一.回顾和补充

### 1.logging模块

- 用于记录日志
- 程序员:
  - 用于统计
  - 做故障排除的debug
  - 记录错误,优化代码
- logging.basicconfig
  - 使用方便
  - 不能实现编码问题 ; 不能同时向文件和屏幕上输出
  - logging.debug , logging.warning
- logger对象
  - 复杂
    - 创建一个logger对象
    - 创建一个文件操作符
    - 创建一个屏幕操作符
    - 创建一个格式
    - 给logger对象绑定 文件操作符
    - 给logger对象绑定 屏幕操作符
    - 给文件操作符 设定格式
    - 给屏幕操作符 设定格式
    - 用logger对象来操作

```python
import logging

logger = logging.getLogger()
fh = logging.FileHandler('log.log')
sh = logging.StreamHandler()
logger.addHandler(fh)
logger.addHandler(sh)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
sh.setFormatter(formatter)
logger.warning('message')
```

### 2.collections模块

- OrderedDict有序字典

  ```python
  from collections import OrderedDict
  odic = OrderedDict([('a', 1), ('b', 2), ('c', 3)])
  print(odic)
  for k in odic:
      print(k,odic[k])
  ```

- defaultdict

- deque

- namedtuple可命名元组

  - 创建一个类，这个类没有方法，所有属性的值都不能修改.

  ```python
  from collections import namedtuple
  
  Course = namedtuple('Course', ['name', 'price', 'teacher'])
  python = Course('python', 19800, 'alex')
  print(python)
  print(python.name)
  print(python.price)
  ```

### 3.模块和包

- 模块

  - 写好了的py文件,对程序员直接提供功能

  - import / from import

  - os     和操作系统打交道

    ```python
    # 计算文件夹大小
    import os
    print(os.path.getsize(路径))
    ```

  - sys

    - sys.path	模块搜索路径 模块是否能导入看sys.path是否有这个模块所在的路径.
    - sys.argv	获取命令行参数  python c://aaa.py remove 路径
    - sys.modules	存储了当前程序中用到的所有模块,反射本文件中的内容

  - datetime

    - now()	当前时间

    - utc()	伦敦时间

    - strftime('%Y-%m-%d %H:%M:%S') 格式化时间

    - strptime('2019-1-1 10:23:23','%Y-%m-%d %H:%M:%S')) 获取到一个datetime对象

    - timedelta(days=140)  时间的加减

      ```python
      import datetime
      s = datetime.datetime.now()
      print(s)
      s2 = datetime.datetime.timestamp(s)
      print(s2)
      res = s - datetime.timedelta(10)
      print(res)
      ```

    - fromtimestamp(时间戳时间)  时间戳转datetime

    - timestamp(datetime对象)  datetime转时间戳

      ```python
      from datetime import datetime
      dt = datetime.now()
      print(datetime.timestamp(dt))
      ```

  - time

    - time() 时间戳时间
    - sleep(n)	程序暂停n秒

  - hashlib	算法模块

    - 密文验证

    - 校验文件的一致性

    - md5

    - sha

      ```python
      import hashlib
      md5 = hashlib.sha1('盐'.encode())
      md5.update(b'str')	# 加密内容
      print(md5.hexdigest())
      ```
    - 序列化	把其他数据类型转换成 str/bytes类型

    - 反序列化	str/bytes类型 转换回去

  - json	所有语言都支持

    - json格式
      - 1.所有的字符串都是双引号
      - 2.最外层只能是列表或者字典
      - 3.只支持 int float str list dict bool
      - 4.存在字典字典的key只能是str
      - 5.不能连续load多次

  - pickle	只支持python语言

    -  1.几乎所有的数据类型都可以写到文件中
    -  2.支持连续load多次

  - random	随机

    - randint	随机数

      ```python
      import random
      print(random.uniform(1,5))	# 带多位小数
      print(random.choice([1,2,3,4,5]))   # 验证码 抽奖
      print(random.sample([1,2,3,4],3))   # 抽取多个不重复的项
      ```

    - choice
    - sample
    - shuffle	# 洗牌  算法

  - logging

    - 两种配置方式
    - basicconfig
    - logger对象

  - collections

    - OderedDict
    - namedtuple
    - deque 双端队列
    - defaultDict 默认字典，可以给字典的value设置一个默认值

  - shutil

    - shutil.make_archive()  压缩文件
    - shutil.unpack_archive() 解压文件
    - shutil.rmtree() 删除目录
    - shutil.move()  重命名 移动文件

  - getpass 在命令行密文显示输入的内容

  - copy.deepcopy

  - importlib

    - importlib.import_module('模块名')

  - functools

    - reduce(add,iterable)

- 包
  - 存储多个py文件的文件夹
  - 如果导入的是一个包,这个包里的默认模块是不能用的
  - 导入一个包相当于执行__ init__.py文件中的内容

### 4.项目名

```python
bin # start
	import os
    from src import core
conf	# 配置文件
	settings
src	# 业务逻辑
	student.py
    	from src import core
    core.py
    	from conf import settings
db	# 数据文件
lib	# 扩展模块
log	# 日志文件
```

### 5.栈和队列

- 栈Stack	lifo  last in first out
- 队列Queue        fifo  first in first out

### 6.反射

- hasattr

- getattr

  ```python
  import demo
  import sys
  ab = 'test'
  
  print(sys.modules)
  print(getattr(sys.modules[__name__], 'ab'))
  ```

- 通过 对象 来获取 实例变量 / 绑定方法

- 通过 类 来获取 类变量/ 类方法 / 静态方法

- 通过 模块名 来获取 模块中的任意变量（普通变量 函数  类）

- 通过 本文件 来获取 本文件中的任意变量

  ```python
  getattr(sys.modules[__name__],'变量名')
  ```

### 7.Foo抽象类 / 接口类(约束)

- 给字类一个规范，让子类必须按照抽象类的规范来实现方法.

### 8.面向对象

#### 8.1基础概念

- 什么是类 : 具有相同方法和属性的一类事物
- 什么是对象、实例 : 一个拥有具体属性值和动作的具体个体
- 实例化 ：从一个类 得到一个具体对象的过程

#### 8.2组合

- 一个类的对象作为另一个类对象的实例变量.

#### 8.3三大特性

- 继承

  - 先找自己的，自己没有找父类
  - 如果自己和父类都有，希望自己和父类都调用，super()/指定类名直接调
  - 单继承 : 子类可以使用父类的方法
  - 多继承
    - 查找顺序 : 深度优先 , 广度优先

- 多态

  - 一个类表现出来的多种状态 --> 多个类表现出相似的状态
  - 鸭子类型

- 封装

  - 广义的封装 ：类中的成员

  - 狭义的封装 ：私有成员

    - __名字
    - 只能在类的内部使用，既不能在类的外部调用，也不能在子类中使用
    - _类名__名字

    ```python
    class A:
        def __func(self):
            print('A')
        def funca(self):
            self.__func()
    
    
    A().funca()		# 内部调用
    print(A()._A__func())		# 强制调用
    ```



#### 8.4类成员

- 类变量
- 绑定方法
- 特殊成员
  - 类方法 classmethod
  - 静态方法 staticmethod
  - 属性 property

#### 8.5双下方法/魔术方法/内置方法

- __ str
- __ new__  构造方法
- __ init__ 初始化方法
- __ call__ 源码中很容易写这个用法
- __ enter__ __ exit__  with上下文管理
- __ getitem__
- __ settitem__
- __ delitem__
- __ add__
- __ iter__
- __ dict__

#### 8.6相关内置函数

- isinstance 对象和类
- issubclass 类和类
- type 类型 类 = type(对象)
- super 遵循mro顺序查找上一个类的

#### 8.7新式类和经典类

- py2 继承object就是新式类 , 默认是经典类
- py3 都是新式类，默认继承object

- 新式类
  - 继承object
  - 支持super
  - 多继承 广度优先C3算法
  - mro方法
- 经典类
  - py2中不继承object
  - 没有super语法
  - 多继承 深度优先
  - 没有mro方法



