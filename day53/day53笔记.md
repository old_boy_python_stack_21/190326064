# day53笔记

## 1.回顾

### 1.浏览器发送请求和接受响应的过程

1. 在浏览器上的地址栏种输入URL，回车，发送get请求；
2. 服务器接收到请求，获取URL的路径，根据路径做不同的操作，把返回的数据封装到响应体中，返回给浏览器
3. 浏览器接受到响应，双方断开连接
4. 浏览器从响应体中获取数据，进行解析渲染。



## 2.静态文件的配置和使用

- settings.py中最后写入代码

```python
STATIC_URL = '/static/'   # 别名
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static1'),
    os.path.join(BASE_DIR, 'static'),
    os.path.join(BASE_DIR, 'static2'),
] # 多个文件夹作为静态文件时也用/static/开头,从前往后找,重名先找前面的
```

- 之后即可使用本地文件 , 按照STATICFILES_DIRS列表的顺序进行查找
- 拿静态文件时要以别名开头:
  - /static/...



## 3.简单的登录的实例

- form表单提交数据注意的问题
  - 1.提交的地址action="" 请求的方式method=""
  - 2.所有的input框有name属性
    - name="username/pwd"才会形成键值对
  - 3.有一个input框的type="submit"  或者有一个button
- input中写required说明该项必填
- 提交POST请求，把settings中MIDDLEWARE的 'django.middleware.csrf.CsrfViewMiddleware' 注释掉



## 4.app

- 创建app命令行

  - python manage.py startapp app名称

- pycharm中操作

  - Tools -> run manage.py task -> startapp app名称

- 注册app

  - 在setting中设置

  ```python
  INSTALLED_APPS = [
  	...
      'app名称',
      'app名称.apps.App名称Config',  # 推荐写法
  ]
  ```

  

## 5.ORM

### 1.概念

对象关系映射（Object Relational Mapping，简称ORM）模式是一种为了解决面向对象与关系数据库存在的互不匹配的现象的技术 , 通过使用描述对象和数据库之间映射的元数据，将程序中的对象自动持久化到关系数据库中。

### 2.优缺点

- 优势
  - ORM解决的主要问题是对象和关系的映射。它通常将一个类和一张表一一对应，类的每个实例对应表中的一条记录，类的每个属性对应表中的每个字段。
  - ORM提供了对数据库的映射，不用直接编写SQL代码，只需操作对象就能对数据库操作数据。
  - 让软件开发人员专注于业务逻辑的处理，提高了开发效率。
- 劣势
  - ORM的缺点是会在一定程度上牺牲程序的执行效率。
  - ORM的操作是有限的，也就是ORM定义好的操作是可以完成的，一些复杂的查询操作是完成不了。
  - ORM用多了SQL语句就不会写了，关系数据库相关技能退化 .

### 3.Django中使用

1. 创建一个Mysql数据库.

2. 在setting中配置,Django连接Mysql数据库:

   ```python
   DATABASES = {
       'default': {
           'ENGINE': 'django.db.backends.mysql',    # 引擎	
           'NAME': 'day53',						# 数据库名称
           'HOST': '127.0.0.1',					# ip地址
           'PORT':3306,							# 端口
           'USER':'root',							# 用户
           'PASSWORD':'123'						# 密码
       }
   }
   ```

3. 在与settings同级目录下的init文件中写:

   ```Python
   import pymysql
   pymysql.install_as_MySQLdb()
   ```

4. 创建表(在app下的models.py中写类)

   ```Python
   from django.db import models
   
   class User(models.Model):
       username = models.CharField(max_length=32)   # username varchar(32)
       password = models.CharField(max_length=32)   # username varchar(32)
   ```

5. 执行数据库迁移的命令

   - python manage.py makemigrations   #  检测每个注册app下的model.py   记录model的变更记录
   - python manage.py migrate   #  同步变更记录到数据库中

6. orm操作

```python
# 获取表中所有的数据
ret = models.User.objects.all()  # QuerySet 对象列表  【对象】
# 获取一个对象（有且唯一）
obj = models.User.objects.get(username='alex')   # 获取不到或者获取到多个对象会报错
# 获取满足条件的对象
ret = models.User.objects.filter(username='alex1',password='dasb')  # QuerySet 对象列表
```




