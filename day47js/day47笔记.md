# day47笔记

## 1.js的引入方式

- 行内

  ```html
  <p onclick="console.log(2);">xxx</p>
  ```

  - onclick 单击p标签

- 内嵌

  - 可以写在任意位置

  ```html
  <script src='链接js文件'></script>
  ```

### 测试语句

- console.log(a)=print(a)
  - 控制台输出
- alert('内容');
  - 网页弹出框
- prompt(message:'弹出问题');
  - 弹出框弹出问题,可以输入回答,返回输入内容
  - var 变量名=prompt(...);

## 2.变量

- //  单行注释
- /* */  多行注释

#### 2.1基本数据类型 

- 整型number var a=2;
- 字符串string var a='2';
  - 可以与数字拼接为字符串
- 布尔值Boolean
- 未定义undefined
  - 先声明后定义 var e;console.log(e);会提示undefined
- 空值null
  - var f=null;相当于字符串

#### 2.2引用数据类型

- array数组

  - var arr=['']
  - console.log(arr);  输出数组,索引和内容,长度.

- object对象

  - var obj={name:'mjj', age:19};
  - console.log(obj);  输出内容

- function函数

  - 全局作用域和函数作用域
  - 1.普通函数的声明

  ```js
  function add(a,b){
  	return a+b
  };
  add(a,b)
  ```

  - 2.函数表达式

  ```js
  var add2=function(){};
  ```

  - 3.自执行函数

  ```javascript
  (function(){})();
  ```

- a++ 先赋值再加1
- ++a 先加1再赋值

```js
var a=4;
var c=a++; //先对c赋值,再a+1
//a=5,c=4
```

#### 2.3字符串拼接

```js
var name = 'wusir', age = 28;
var str = name + '今年是' + age + '岁了，快要结婚了，娶了个黑姑娘';
console.log(str);
//    es6的模板字符串 ``
var str2 = `${name}今年是${age}岁了，快要结婚了，娶了个黑姑娘`;
console.log(str2);
```

## 3.数组

```js
var arr = [1,'2','mjj'];
//解释器遇到var声明的变量 会把var声明的变量提升到全局作用域下
var i;
for(i = 0; i<arr.length; i++){
    console.log(arr[i]);
}
function fn(){
    var d = 4;
} //打印d会报错
var c;
console.log(i,c); //3,undefined
```

## 4.流程控制

```js
//if判断
var score = 100;
if(score > 80){
	console.log('可以吃鸡了');}
else if(条件){
	console.log('在家呆着');}
else{};
//根据输入判断
var weather = prompt('请输入今天的天气');
switch (weather) {
    case '晴天':
        console.log('可以去打篮球');
        break;
    case '下雨':
        console.log('可以睡觉');
        break;
    default:
        console.log('学习');
        break;}
//==和===的区别
var a = 2;
var b = '2';
console.log(a == b);//比较的是值
console.log(a === b); //比较是值和数据类型
```

## 5.循环

```js
//循环输出
//1.初始化循环变量  2.循环条件  3.更新循环变量
var arr = [8,9,0];
for(var i = 0; i<arr.length; i++){
	console.log(arr[i]);}

//1到100之间的数
//while
var a = 1;
while(a <= 100){
    console.log(a);
    a+=1;}
```

## 6.函数

```js
//接收多个参数
function fn() {
	switch (arguments.length) {
		case 2:
            console.log('2个参数')
            break;
		case 3:
            console.log('3个参数')
            break;
        default:
            break;
	}
}

fn(2, 3);
fn(2, 3, 4)
```

## 7.对象object

```js
//1.字面量创建方式
//点语法 set设置值 和get获取值
var obj = {};
obj.name = 'mjj'; //给对象添加内容
obj.fav = function(){
    console.log(this);
}; //给对象添加内容
obj.fav(); //调用obj中的fav,输出this(this=Python中的self)
console.log(obj.name);
//------------------------
function  add(x,y) {
    console.log(this.name);
    console.log(x);
    console.log(y);
}
console.dir(add); //dir查看add属性和方法
add();
add.call(obj,1,2); //add.call()等于add(),但.call能改变this指向
add.apply(obj,[1,2]);//同call,传参要用[]
//------------------------
(function () {
    console.log(this);
})();//自执行函数
//------------------------
//2.构造函数
var obj2 = new Object();
console.log(obj2);
obj2.name = 'wusir';
```

## 8.数组

### 1.数组创建方式

- 使用Array构造函数方式创建
  - var colors = new Array();
- 使用字面量方式创建数组
  - var colors = [ ];

### 2.检测数组

- 确定某个值是否是数组
  - Array.isArray(变量名)

### 3.方法

- 变量名.toString() : 返回由数组中每个值的字符串形式拼接而成的一个以逗号分隔的字符串。

  ```js
  var colors = ['red','green','blue'];
  alert(colors.toString());//red,green,blue
  ```

- 变量名.toLocalString()

  ```js
  var person1 = {
      toLocaleString : function () {
          return '马大帅';
      },
      toString : function () {
          return '马小帅';
      }
  }
  var person2 = {
      toLocaleString : function () {
          return '隔壁老王';
      },
      toString : function () {
          return '隔壁老李';
      }
  }
  var people = [person1,person2];
  alert(people);//马小帅,隔壁老李
  alert(people.toString());//马小帅,隔壁老李
  alert(people.toLocaleString()); //马大帅,隔壁老王
  ```

- 分割字符串

  - toLocalString() 方法和 toString() 方法，在默认情况下都会以逗号分割的字符串的形式返回。而如果使用 join() 方法，join() 方法只接收一个参数。

  ```js
  var colors = ['red','blue','green'];
  colors.join('||'); //red||blue||green
  ```

- 栈方法 - 后进先出

  - push() 可以接收任意数量的参数，把它们逐个添加到数组末尾，并返回修改后数组的长度。

  ```js
  var colors = [];
  var count = colors.push('red','blue','green');
  alert(count); //3
  ```

  - pop()方法 从数组末尾移除最后一项，减少数组的 length 值，然后返回移除的项 。

  ```js
  var item = colors.pop(); //取最后一项
  alert(item); //green
  alert(colors.length); //2
  ```

- 队列方法 - 先进先出

  - shift() 移除数组中的第一个项并返回该项，同时将数组长度减 1。

  ```js
  var colors = ['red','blue','green'];
  var item = colors.shift();//取得第一项
  alert(item); //"red"
  alert(colors.length); //2
  ```

  - unshift() 在数组前端添加任意个项并返回新数组的长度。

  ```js
  var colors = [];
  var count  = colors.unshift('red','green'); //推入两项
  alert(count); //2
  console.log(colors); // ["red", "green"]
  ```

- 重排序方法

  - reverse() 翻转数组项的顺序

  ```js
  var values = [1,2,3,4,5];
  values.reverse();
  alert(values); // 5,4,3,2,1
  ```

  - sort() 默认升序排列,按字符串比较

  ```js
  var values = [0,1,5,10,15];
  varlus.sort();
  alert(values); //0,1,10,15,5
  ```

- 操作方法

  - concat() 数组合并方法，一个数组调用concat()方法去合并另一个数组，返回一个新的数组。concat()接收的参数是可以是任意的。
  - 参数为一个或多个数组，则该方法会将这些数组中每一项都添加到结果数组中。
  - 参数不是数组，这些值就会被简单地添加到结果数组的末尾

  ```js
  var colors = ['red','blue','green'];
  colors.concat('yello');//["red", "blue", "green", "yello"]
  colors.concat({'name':'张三'});//["red", "blue", "green", {…}]
  colors.concat({'name':'李四'},['black','brown']);// ["red", "blue", "green", {…}, "black", "brown"]
  ```

  - slice() 相当于切片,不影响原始数组
  - 一个参数的情况下，slice()方法会返回从该参数指定位置开始到当前数组默认的所有项
  - 两个参数的情况下，该方法返回起始和结束位置之间的项——但不包括结束位置的项。

  ```js
  var colors = ['red','blue','green','yellow','purple'];
  colors.slice(1);//["blue", "green", "yellow", "purple"]
  colors.slice(1,4);// ["blue", "green", "yellow"]
  ```

  - 如果包含负数

  ```js
  var colors = ['red','blue','green','yellow','purple'];
  colors.slice(-2,-1);//["yellow"] 
  colors.slice(-1,-2);//[]
  ```

  - splice() 主要用途是向数组的中插入项,返回数组。使用这种方法的方式则有3种。
  - 1.删除 可以删除任意数量的项，指定2个参数：要删除的第一项的位置,要删除的个数。例如splice(0,2)会删除数组中的前两项
  - 2.插入 可以向指定位置插入任意数量的项，提供3个参数：起始位置、0（要删除的个数）和要插入的项。如果要插入多个项，可以再传入第四、第五、以至任意多个项。例如，splice(2,0,'red','green')会从当前数组的位置2开始插入字符串'red'和'green'。
  - 3.替换 向指定位置插入任意数量的项，同时删除任意数量的项，指定 3 个参数:起始位置、要删除的项数,要插入的任意数量的项。插入的项数不必与删除的项数相等。例如，splice (2,1,"red","green")会删除当前数组位置 2 的项，然后再从位置 2 开始插入字符串"red"和"green"。

  ```js
  var colors = ["red", "green", "blue"];
  var removed = colors.splice(0,1); 
  alert(colors); // green,blue 
  alert(removed); // red，返回的数组中只包含一项
  removed = colors.splice(1, 0, "yellow", "orange"); 
  alert(colors); // green,yellow,orange,blue
  alert(removed);//返回的是一个空数组
  removed = colors.splice(1, 1, "red", "purple"); 
  alert(colors); // green,red,purple,orange,blue
  alert(removed); // yellow，返回的数组中只包含一项
  ```

- 位置方法

  - indexOf() 从前找内容的索引值,差不到返回-1
  - lastIndexOf() 从后找内容的索引值,差不到返回-1
  - 这两个方法都接收两个参数:要查找的项和(可选的)表示查找起点位置的索引。

  ```js
  var numbers = [1,2,3,4,5,4,3,2,1];
  alert(numbers.indexOf(4)); //3
  alert(numbers.lastIndexOf(4));// 5
  alert(numbers.indexOf(4,4));// 5
  alert(numbers.lastIndexOf(4,4));//3
  ```

- 迭代方法

  - forEach() 对数组中的每一项运行传入的函数(回调函数),没有返回值.

  ```js
  var numbers = [1,2,3,4,5,4,3,2,1];
  numbers.forEach(function(...){
  });
  ```

## 9.字符串

### 1.字符方法

- charAt() charCodeAt()
  - 都接收一个位置,返回该位置的字符.

### 2.字符串操作方法

- concat() 将一或多个字符串拼接起来,返回新字符串。

  ```js
  var stringValue = "hello ";
  var result = stringValue.concat("world"); alert(result); //"hello world" 
  alert(stringValue); //"hello"
  ```

- slice() substring() substr()

  - 一个参数,三者都从参数位置分割到字符串最后
  - 两个参数,前两者取到第二个参数的前一位,substr(a,b)从a取b个

  ```js
  ar stringValue = "hello world";
  alert(stringValue.slice(3));//"lo world"
  alert(stringValue.substring(3));//"lo world"
  alert(stringValue.substr(3));//"lo world"
  alert(stringValue.slice(3, 7));//"lo w"
  alert(stringValue.substring(3,7));//"lo w"
  alert(stringValue.substr(3, 7));//"lo worl"
  ```

  - 参数为负时
  - 1个参数 substring()方法将数值转换成0，返回全部字符串
  - 2个参数 substring()方法会把第二个参数转换为 0，使调用变成了substring(3,0)，而由于这个方法会将较小的数作为开始位置，将较大的数作为结束位置， 因此最终相当于调用了substring(0,3)。substr()也会将第二个参数转换为 0，这也就意味着返回包含零个字符的字符串，也就是一个空字符串。

### 3.字符串位置方法

- indexOf() lastIndexOf()

  ```js
  var stringValue = "hello world";
  alert(stringValue.indexOf("o"));             //4
  alert(stringValue.lastIndexOf("o"));         //7
  alert(stringValue.indexOf("o", 6));         //7
  alert(stringValue.lastIndexOf("o", 6));     //4
  ```

### 4.trim() 方法

- 去前后空格

### 5.字符串大小写转换

- toLowerCase()  toUpperCase()

```js
var stringValue = "hello world";
alert(stringValue.toUpperCase());        //"HELLO WORLD"
alert(stringValue.toLowerCase());        //"hello world"
```

### 6.Date日期

![](D:\homework\day47\image-20190526151544338.png)

- toLocaleString() 获取如下格式时间
  - 2019/4/15 上午10:11:53
- 定时器
  - setInterval(function(){},1000)
  - 每1000ms执行一次function

### 7.字符串,数值转换

- 字符串转数值

- 内容中包含非数字时,输出NaN(not a number)

  ```js
  var str = '123.0000111';
  console.log(parseInt(str));//输出数字整型
  console.log(parseFloat(str));//输出浮点型
  ```

- 数值转字符串

  ```js
  var num  = 1233.006;
  console.log(String(num));
  console.log(num.toString());
  ```

### 8.Global对象

```js
var url = `http://www.apeland/web index.html`;
console.log(encodeURI(url));//http://www.apeland/web%20index.html
console.log(encodeURIComponent(url));//http%3A%2F%2Fwww.apeland%2Fweb%20index.html
```

### 9.Window对象

- 在全局作用域中声明的所有变量和函数，都成为了 window 对象的属性。

### 10.Math对象

- min()  max() 最小最大值

  ```js
  var max = Math.max(3, 54, 32, 16);
  alert(max);    //54
  var min = Math.min(3, 54, 32, 16);
  alert(min);    //3
  ```

- apply()

  - apply(变量名,数组);

  ```js
  var values = [1,2,36,23,43,3,41];
  var max = Math.max.apply(null,values);
  console.log(max);
  ```

- 舍入方法

  - Math.ceil()向上舍入,天花板函数
  - Math.floor()向下地板舍入,天花板函数
  - Math.round()四舍五入

- random()方法

  - Math.random()返回大于等于0小于1的一个随机数.
  - 获取min到max范围整数

  ```js
  function random(lower, upper) {
      return Math.floor(Math.random() * (upper - lower)) + lower;
  }
  ```

  - 获取随机色

  ```js
  /*产生一个随机的rgb颜色
  @return {String}  返回颜色rgb值字符串内容，如：rgb(201, 57, 96)
  */
  function randomColor() {
      // 随机生成 rgb 值，每个颜色值在 0 - 255 之间
      var r = random(0, 256),
          g = random(0, 256),
          b = random(0, 256);
      // 连接字符串的结果
      var result = "rgb("+ r +","+ g +","+ b +")";
      // 返回结果
      return result;
  }
  ```

  - 获取随机验证码

  ```js
  function createCode(){
      //首先默认code为空字符串
      var code = '';
      //设置长度，这里看需求，我这里设置了4
      var codeLength = 4;
      //设置随机字符
      var random = new Array(0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R', 'S','T','U','V','W','X','Y','Z');
      //循环codeLength 我设置的4就是循环4次
      for(var i = 0; i < codeLength; i++){
          //设置随机数范围,这设置为0 ~ 36
          var index = Math.floor(Math.random()*36);
          //字符串拼接 将每次随机的字符 进行拼接
          code += random[index]; 
      }
      //将拼接好的字符串赋值给展示的Value
      return code
  }
  ```

  