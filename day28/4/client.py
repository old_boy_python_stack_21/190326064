#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import json
import socket

sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

file_path = input('>>>')                    # 输入文件路径
if os.path.isabs(file_path):                # 判断文件路径是否为绝对路径
    filename = os.path.basename(file_path)      # 返回文件名
    filesize = os.path.getsize(file_path)       # 返回文件大小
    dic = {'filename': filename, 'filesize': filesize}      # 存入字典
    str_dic = json.dumps(dic)                   # 转为str格式
    sk.send(str_dic.encode('utf-8'))
    with open(file_path, mode='rb') as f:       # 打开文件,模式为读取字节
        content = f.read()
        sk.send(content)
sk.close()
