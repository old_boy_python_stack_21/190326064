#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
import socket

sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

conn, addr = sk.accept()
print(addr)
str_dic = conn.recv(1024).decode('utf-8')       # 接收str字典
dic = json.loads(str_dic)                       # 转换为字典
content = conn.recv(dic['filesize'])            # 接收字典中的文件大小项
with open(dic['filename'], mode='wb') as f:     # 创建字典中文件名的项,模式为写字节
    f.write(content)
conn.close()
sk.close()
