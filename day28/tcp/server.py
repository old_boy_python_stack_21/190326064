#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
import socket


def chat(conn):     # 聊天模块
    while True:
        msg = conn.recv(1024).decode('utf-8')       # 接收消息,msg字典的json形式
        dic_msg = json.loads(msg)
        uid = dic_msg['id']                         # 获取msg的id账号
        if dic_msg['msg'].upper() == 'Q':           # 如果对方输入的msg的msg项为Q则退出
            print('%s已经下线' % color_dic[uid]['name'])
            break
        print('%s%s : %s\033[0m' % (color_dic[uid]['color'], color_dic[uid]['name'], dic_msg['msg']))   # color字典的id项的color项
        inp = input('>>>')
        conn.send(inp.encode('utf-8'))  # 发送内容
        if inp.upper() == 'Q':          # Q退出
            print('您已经断开和%s的聊天' % color_dic[uid]['name'])
            break


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()
color_dic = {
    '12345': {'color': '\033[32m', 'name': 'alex'},
    '12346': {'color': '\033[31m', 'name': 'wusir'},
    '12347': {'color': '\033[33m', 'name': 'yuan'},
}       # 各个账号颜色
while True:
    conn, _ = sk.accept()       # 等待接入  conn是连接的数据
    chat(conn)                  # 调用chat聊天模块
    conn.close()                # 关闭连接
sk.close()
