#!/usr/bin/env python
# -*- coding:utf-8 -*-

import socket
import json

sk = socket.socket()

id = '12345'                                # 登录的id账号
sk.connect(('127.0.0.1', 9000))             # 连接到服务器
while True:
    inp = input('>>>')
    dic = {'msg': inp, 'id': id}            # 把内容和id账号放入字典
    str_dic = json.dumps(dic)               # 把字典变成str格式
    sk.send(str_dic.encode('utf-8'))        # 发送
    if inp.upper() == 'Q':
        print('您已经断开和server的聊天')
        break
    msg = sk.recv(1024).decode('utf-8')     # 接收服务器的消息
    if msg.upper() == 'Q':
        break
    print(msg)
sk.close()
