# day28笔记

## 1.TCP协议(打电话)

- 可靠  慢  全双工通信
- 建立连接的时候:三次握手
- 断开连接的时候:四次挥手
- 在建立起连接之后
  - 发送的每一条信息都有回执
  - 为了保证数据的完整性,还有重传机制.
- 长连接:会一直占用双方的端口
- IO(input , output)操作,输入输出是相对内存来说的.
  - write  send
  - read  recv
- 应用场景
  - 文件的上传下载
- 能够传递的数据长度几乎没有限制

## 2.UDP协议(发短信)

- 无连接  速度快
- 可能会丢消息

- 能传递的数据长度是有限的,根据数据传递设备的设置有关.
- 应用场景
  - 即时通信类

## 3.osi七层模型

- 应用层
  - 表示层
  - 会话层
- 传输层
- 网络层
- 数据链路层
- 物理层

```python
# osi五层协议     协议                  物理设备

# 5应用层      http https ftp smtp

#             python代码  hello

# 4传输层      tcp/udp协议 端口      四层交换机、四层路由器

# 3网络层      ipv4/ipv6协议         路由器、三层交换机

# 2数据链路层  mac地址  arp协议      网卡 、交换机

# 1物理层
```



## 4.socket模块 (套接字)

- 工作在应用层和传输层之间的抽象层
  - 帮助我们完成了所有信息的组织和拼接
- socket对于程序员来说是网络操作的最底层

tcp协议

- server端

  ```python
  import socket
  
  
  sk = socket.socket()
  sk.bind(('127.0.0.1',9000))
  sk.listen()    # n 允许多少客户端等待
  print('*'*20)
  while True:   # outer
      conn,addr = sk.accept()    # 阻塞
      print(addr)
      while True:   # inner
          msg = conn.recv(1024).decode('utf-8')
          if msg.upper() == 'Q': break
          print(msg)
          inp = input('>>>')
          conn.send(inp.encode('utf-8'))
          if inp.upper() == 'Q':
              break
      conn.close()
  sk.close()
  ```

- client端

  ```python
  import socket
  
  
  sk = socket.socket()
  sk.connect(('127.0.0.1',9000))
  while True:
      inp = input('>>>')
      sk.send(inp.encode('utf-8'))
      if inp.upper() == 'Q':
          break
      msg = sk.recv(1024).decode('utf-8')
      if msg.upper() == 'Q': break
      print(msg)
  sk.close()
  ```

udp协议

- server端

  ```python
  import socket
  
  sk = socket.socket(type = socket.SOCK_DGRAM)
  sk.bind(('127.0.0.1',9000))
  while True:
      msg,client_addr = sk.recvfrom(1024)
      print(msg.decode('utf-8'))
      msg = input('>>>').encode('utf-8')
      sk.sendto(msg,client_addr)
  sk.close()
  ```

- client端

  ```python
  import socket
  
  sk = socket.socket(type=socket.SOCK_DGRAM)
  while True:
      inp = input('>>>').encode('utf-8')
      sk.sendto(inp,('127.0.0.1',9000))
      ret = sk.recv(1024)
      print(ret)
  sk.close()
  ```

