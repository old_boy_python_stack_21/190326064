#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
import socket
import hashlib


def get_md5(username, password):
    md5 = hashlib.md5(username.encode('utf-8'))     # 加的盐使用 用户名
    md5.update(password.encode('utf-8'))            # 把密码转为md5
    return md5.hexdigest()


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

conn, _ = sk.accept()
str_dic = conn.recv(1024).decode('utf-8')
dic = json.loads(str_dic)                           # 接收用户字典信息
with open('userinfo', encoding='utf-8') as f:       # 打开用户信息文件
    for line in f:
        user, passwd = line.strip().split('|')
        if user == dic['usr'] and passwd == get_md5(dic['usr'], dic['pwd']):    # 判断账号密码相同
            res_dic = {'opt': 'login', 'result': True}
            break
    else:
        res_dic = {'opt': 'login', 'result': False}
sdic = json.dumps(res_dic)          # 把登录结果转为str
conn.send(sdic.encode('utf-8'))     # 发送
conn.close()
sk.close()
