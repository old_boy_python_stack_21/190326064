#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
import socket

usr = input('username : ')
pwd = input('password : ')
dic = {'usr': usr, 'pwd': pwd}
str_dic = json.dumps(dic)       # 把字典转为str
sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

sk.send(str_dic.encode('utf-8'))        # 发送账号密码
ret = sk.recv(1024).decode('utf-8')     # 接收客户端的返回内容
ret_dic = json.loads(ret)               # str转换为字典
if ret_dic['result']:   # 字典的result项为True
    print('登陆成功')
sk.close()
