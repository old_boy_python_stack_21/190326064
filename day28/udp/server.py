#!/usr/bin/env python
# -*- coding:utf-8 -*-

import socket

dic = {'707554978': '\033[3;34m', '541315469': '\033[5;36m', '66615469': '\033[5;36m'}   # id对应颜色
sk = socket.socket(type=socket.SOCK_DGRAM)
sk.bind(('192.168.12.21', 9000))
print('start')
dic2 = {}
while 1:
    msg, client_addr = sk.recvfrom(1024)
    a, b = msg.decode('utf-8').split(' : ')
    if not b:
        continue
    dic2[a] = client_addr
    color = dic.get(a)
    content = ((color + a + '说:' + b) + '\033[0m').encode('utf-8')
    for user, addr in dic2.items():
        print(user, addr)
        sk.sendto(content, addr)
sk.close()
