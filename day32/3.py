#!/usr/bin/env python
# -*- coding:utf-8 -*-


# 3、写函数，计算两个大文件的md5值是否一致（5分）
# import os
# import hashlib
#
#
# def func(path1, path2):
#     size1 = os.path.getsize(path1)
#     md51 = hashlib.md5()
#     size2 = os.path.getsize(path2)
#     md52 = hashlib.md5()
#     with open(path1, 'rb')as f:
#         while size1 > 1024:
#             cont = f.read(1024)
#             md51.update(cont)
#             size1 -= len(cont)
#         else:
#             cont = f.read(size1)
#             md51.update(cont)
#             size1 -= len(cont)
#     with open(path2, 'rb')as f:
#         while size2 > 1024:
#             cont = f.read(1024)
#             md52.update(cont)
#             size2 -= len(cont)
#         else:
#             cont = f.read(size2)
#             md52.update(cont)
#             size2 -= len(cont)
#     print( md51.hexdigest() == md52.hexdigest() )
#
# func(r'D:\homework\day02\day02笔记.md', r'D:\homework\day02\day02笔记.md')