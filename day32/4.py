#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 4、写装饰器，补全log中的内容，使用time模块实现下列结果：(5分)
# blablabla
# 2019-04-02 20:28:32 func被调用了
# 提示：
# func.__name__可以获得函数名
import time
# from datetime import datetime
#
#
# def log(func):
#     def inner(*args, **kwargs):
#         func()
#         s = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#         print(s, func.__name__, '被调用了')
#         return
#
#     return inner
#
#
# @log
# def func():
#     print('blablabla')
#
#
# func()
