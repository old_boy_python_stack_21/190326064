#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1、写函数，接收一个路径做参数，将这个路径下所有的.png文件名都修改成.jpg文件
# 1) 基础需求，这个文件下没有其他文件夹了（5分）
# 2）加分需求，这个文件下还有其他文件夹，并需要考虑所有子文件夹中的png文件（10分）
# import os
#
#
# def func(path):
#     for a, b, c in os.walk(path):
#         for j in c:
#             file = os.path.join(a,j)
#             if j.endswith('.png'):
#                 name = j.split('.')[0] + '.jpg'
#                 new = os.path.join(a,name)
#                 print(file, name)
#                 os.rename(file, new)
#
#
# func(r'D:\test')
