#!/usr/bin/env python
# -*- coding:utf-8 -*-


# 5.生成一个文件，这个文件中包含100行信息，每行随机为172.25.254.0/24网段的ip地址，然后读取文件，取出所有ip的最后一位十
# 进制中是三位数的ip地址。（5分）
# 例如从如下列表中，取出172.25.254.111和172.25.254.112：
# 172.25.254.11
# 172.25.254.12
# 172.25.254.111
# 172.25.254.112

# import random
#
# with open('5.txt', 'w', encoding='utf-8') as f:
#     for i in range(100):
#         s = random.randint(0, 255)
#         ip = '172.25.254.' + str(s)
#         f.write(ip+'\n')
#
# with open('5.txt', 'r', encoding='utf-8') as f:
#     for i in f:
#         i = i.strip()
#         if len(i.split('.')[-1]) == 3:
#             print(i)
