#!/usr/bin/env python
# -*- coding:utf-8 -*-


# 2、手写一个基于tcp的socket程序，客户端输入任意一个网址发送致server端，在server端访问网页，并将网页源码返回给客户端。
# 要求（避免粘包问题）(5分)
import struct
import socket
import requests

sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

conn, addr = sk.accept()
s = conn.recv(4)      # 接收长度
len_web = struct.unpack('i', s)[0]
web = conn.recv(len_web).decode('utf-8')    # 接收网页

msg = requests.get(web)     # 获取网页源码
len_msg = struct.pack('i', len(msg))
conn.send(len_msg)      # 发送长度
conn.send(msg.encode('utf-8'))      # 发送源码

conn.close()
sk.close()
