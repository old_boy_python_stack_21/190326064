#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 6.请用面向对象的知识完成图书管理程序（20分）
# 现在有图书、用户等内容需要描述,请完成以下功能：
# 1）启动程序直接进入登录（5分）
# 2）登录成功之后可以看到用户对应的操作（5分）
# 操作包括：查看所有图书列表，将图书添加到借阅列表，查看已经借阅的图书，归还图书，退出程序
# 3）将下面的图书列表使用pickle的方式写入books_info文件（与大程序逻辑无关，可写在另一个py文件里）（5分）
# 商品列表如下：
# 图书名，当前数量
# 小王子,3
# 流畅的python,10
# 正则指引,8
# 制茶学,3
# 4)完成2）中“对应操作”中的具体代码，使你的程序更流畅（5分）
import sys


class Books:
    def __init__(self):
        self.lst = []

    def check(self):
        with open(r'D:\homework\day32\books_info', 'r', encoding='utf-8')as f:
            for i in f:
                print(i.strip())
        self.borrow()

    def borrow(self):
        inp = input('请选择要借阅的图书:')
        self.lst.append(inp)
        print('已添加到借阅列表!')

    def borrowed(self):
        print('您借阅的图书有:')
        for i in self.lst:
            print(i)

    def huan(self):
        for i in range(len(self.lst)):
            print(i+1, i)
        inp = int(input('请选择要归还的图书序号:'))
        self.lst.pop(inp-1)

    def exit(self):
        sys.exit()


class User:
    def login(self):
        while 1:
            print('请选择功能:\n1.查看所有图书列表\n2.将图书添加到借阅列表\n3.查看已借阅图书\n4.归还图书\n5.退出程序')
            dic = {'1': 'check', '2': 'borrow', '3': 'borrowed', '4': 'huan', '5': 'exit'}
            cho = input('请选择:')
            if not dic.get(cho):
                print('输入有误!')
                continue
            b = Books()
            getattr(b, dic.get(cho))()

    def run(self):
        while 1:
            print('欢迎进入图书管理系统!')
            print('请先登陆!')
            user = input('用户名:')
            pwd = input('密码:')
            if not (user == 'alex' and pwd == '123'):
                print('账号或密码错误!请重试!')
                continue
            print('登陆成功!')
            self.login()


if __name__ == '__main__':
    User().run()
