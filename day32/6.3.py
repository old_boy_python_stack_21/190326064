#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 将下面的图书列表使用pickle的方式写入books_info文件
# 商品列表如下：
# 图书名，当前数量
# 小王子,3
# 流畅的python,10
# 正则指引,8
# 制茶学,3

import pickle

s = '''图书名，当前数量
小王子,3
流畅的python,10
正则指引,8
制茶学,3'''

f = open('books_info', mode='wb')
msg = pickle.dump(s, f)
f.close()
