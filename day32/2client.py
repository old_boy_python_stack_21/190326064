#!/usr/bin/env python
# -*- coding:utf-8 -*-

import struct
import socket

sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

web = input('网址:')
len_web = struct.pack('i', len(web))
sk.send(len_web)
sk.send(web.encode('utf-8'))

msg = sk.recv(4)
len_msg = struct.unpack('i', msg)[0]
res = sk.recv(len_msg).decode('utf-8')

print(res)
sk.close()
