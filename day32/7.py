#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 有⼀个数据结构如下所⽰，请编写⼀个函数从该结构数据中返回由指定的字段和对应的值组成的字典。如果指定字段不存在，则跳过
# 该字段。（10分）
data = {"time": "2016-08-05T13:13:05",
        "some_id": "ID1234",
        "grp1": {"fld1": 1,
                 "fld2": 2},
        "xxx2": {"fld3": 0,
                 "fld5": 0.4},
        "fld6": 11,
        "fld7": 7,
        "fld46": 8}

# fields:由”|”连接的以"fld"开头的字符串,如:fld2|fld3|fld7|fld19
# 提示：请考虑字典的嵌套层数不是固定的

new = {}


def func(dic):
    for k, v in dic.items():
        if k.startswith('fld'):
            new[k] = v
        if type(v) == dict:
            func(v)
    return new


print(func(data))
