# day32笔记

## 1.回顾

### 1.1操作系统

- 发展历史
  - 多道, 分时, 实时
- 操作系统负责什么
  - 调度进程先后执行的顺序,控制执行的时间等
  - 资源的分配

### 1.2进程

- 进程和程序的区别
  - 运行的程序就是一个进程
- 进程的调度: 由操作系统完成
- 三状态:
  - 就绪 (操作系统调用-><-时间片到) 运行 (io操作) 阻塞 (IO结束->) 就绪
  - 阻塞影响了程序运行的效率

### 1.3概念

- 同步 异步
- 阻塞非阻塞
- 并发并行
  - 并发 ：并发是指多个程序 公用一个cpu轮流使用
  - 并行 ：多个程序 多个cpu 一个cpu上运行一个程序

## 2.线程

### 2.1进程是计算机中最小的资源分配单位.

- 数据隔离

### 2.2进程

- 创建进程  时间开销大
- 销毁进程  时间开销大
- 进程之间切换  时间开销大

- 两个程序,分别要做两件事 : 起两个进程
- 一个程序,分别做多件事 : 启动多个进程完成,开销大

### 2.3线程

- 是进程中的一部分，每一个进程中至少有一个线程.
- 进程是计算机中最小的资源分配单位（进程是负责圈资源）
- 线程是计算机中能被CPU调度的最小单位 （线程是负责执行具体代码的）

- 开销
  - 线程的创建，也需要一些开销（一个存储局部变量的结构，记录状态）
  - 创建、销毁、切换开销远远小于进程

python中的线程比较特殊,所以进程也有可能被用到

- 进程 ：数据隔离 开销大 同时执行几段代码
- 线程 ：数据共享 开销小 同时执行几段代码

## 3.进程模块multiprocessing

- multi	multiple多元的
- processing	进程

from multiprocessing import Process    调用

pid  process id进程占用端口	ppid   parent process  id父进程占用端口

```python
import os
print('start')
print(os.getpid(),os.getppid(),'end')	# 第一个是脚本pid,第二个是pythonpid
```

```python
import os
import time
from multiprocessing import Process

def func():
    print('start',os.getpid())
    time.sleep(1)
    print('end',os.getpid())

if __name__ == '__main__':
    p = Process(target=func)
    p.start()   # 异步 调用开启进程的方法 但是并不等待这个进程真的开启
    print('main :',os.getpid())
```



- 操作系统创建进程的方式不同
- Windows操作系统执行开启进程的代码
  - 实际上新的子进程需要通过import父进程的代码来完成数据的导入工作
  - 所以有一些内容我们只希望在父进程中完成，就写在if __ name__ == '__ main__':下面.(不写则报错)
- ios linux操作系统创建进程 fork(不写不报错)

- 主进程和子进程之间的关系
  - 主进程没结束 ：等待子进程结束
  - 主进程负责回收子进程的资源
  - 如果子进程执行结束，父进程没有回收资源，那么这个子进程会变成一个僵尸进程
- 主进程的结束逻辑
  - 主进程的代码结束
  - 所有的子进程结束
  - 给子进程回收资源
  - 主进程结束
-  主进程怎么知道子进程结束了的呢？
  - 基于网络、文件

- join方法 : 阻塞，直到子进程结束就结束

  ```python
  import time
  from multiprocessing import Process
  def send_mail():
      time.sleep(3)
      print('发送了一封邮件')
  if __name__ == '__main__':
      p = Process(target=send_mail)
      p.start()   # 异步 非阻塞
      # time.sleep(5)
      print('join start')
      p.join()    # 同步 阻塞 直到p对应的进程结束之后才结束阻塞
      print('5000封邮件已发送完毕')
  ```

- 开启10个进程，给公司的5000个人发邮件，发送完邮件之后，打印一个消息“5000封邮件已发送完毕”

  ```python
  import time
  import random
  from multiprocessing import Process
  def send_mail(a):
      time.sleep(random.random())
      print('发送了一封邮件',a)
  
  if __name__ == '__main__':
      l = []
      for i in range(10):
          p = Process(target=send_mail,args=(i,))
          p.start()
          l.append(p)
      print(l)
      for p in l:p.join()
      # 阻塞 直到上面的十个进程都结束
      print('5000封邮件已发送完毕')
  ```



## 4.总结

### 1.开启一个进程

```python
函数名(参数1,参数2)
from multiprocessing import Process
p = Process(target=函数名,args=(参数1,参数2))
p.start()
```

### 2.父进程和子进程

### 3.父进程会等待着所有的子进程结束之后才结束

为了回收资源

### 4.进程开启的过程中Windows和Linux/ios之间的区别:

- 开启进程的过程需要放在if name = 'main' 下
  - Windows中相当于在子进程中把主进程文件从头到尾执行力一遍,除了放在if name == 'main'下的代码
  - linux中不执行代码,直接执行调用的func函数

### 5.join方法

- 把一个进程的结束事件封装成一个join方法

- 执行join方法的效果就是 阻塞直到这个子进程执行结束就结束阻塞.

- 在多个子进程中使用join

  ```python
  p_l= []
  for i in range(10):
      p = Process(target=函数名,args=(参数1,参数2))
      p.start()
      p_l.append(p)
  for p in p_l:p.join()
  ```

- 所有的子进程都结束之后要执行的代码写在这里

## 5.补充__ name__变量

```python
print([__name__])
if __name__ == '__main__':
    # 控制当这个py文件被当作脚本直接执行的时候，就执行这里面的代码
    # 当这个py文件被当作模块导入的时候，就不执行这里面的代码
    print('hello hello')
```

- __ name__ == '__ main__'
  - 执行的文件就是__ name__所在的文件
- __ name__ == '文件名'
  - __ name__所在的文件被导入执行的时候









