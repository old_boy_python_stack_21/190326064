# day48笔记

## 1.回顾

#### 1.基本数据类型和引用数据类型

```
基本数据类型:number,string,boolean,undefined,null
引用数据类型:Array,Object,Function
```

#### 2.条件判断和循环

```js
switch(name){
	case 'xxx':
		break;
    case 'xxx':
		break;
    default:
        break;
}
for(var i = 0; i < 10; i ++){
}
三元运算
1 > 3 ? '真的' : '假的';
```

#### 3.赋值运算符，逻辑运算符

```
&&  跟py的and
||   or
!    not

i++
==   比较的值的
===   比较的值和数据类型
```

#### 4.字符串的常用方法

```js
slice() 切片，有一个参数，从当前位置切到最后，两个参数，顾头不顾尾
substring()
substr() 如果有两个参数，第二个参数表示切字符串的个数

拼接字符串
concat() 返回新的字符串
+
es6的模板字符串
``  插入变量用${变量名}

//获取索引
indexOf()
lastIndexOf()
//获取字符
charAt()
//获取字符的ASCII码
charCodeAt()
//转大写
toUppercase()
//转小写
tolowercase()

typeof 校验当前变量的数据类型
trim() 清除左右的空格
```

#### 5.数组的常用方法

```
toString()
join('*')
concat()

//栈方法 后进先出
push()
pop()
//堆方法 先进先出
unshift()
shift()

splice(起始位置，删除的个数，添加的元素)； 对数组的增加，删除，替换
slice()
reverse()
sort()

//迭代方法
for
forEach() 仅能在数组对象中使用
在函数中arguments 这个对象是伪数组
```

#### 6.对象

```js
var obj = {
	  name:'mjj',
	  age:18
}
obj['name']
obj['fav'] = function(){}
obj.name
obj.fav = function(){}

function fn(name){
    var obj = {};
    obj[name] = 'mjj';
    return obj;
}
fn('age')

//遍历对象
for(var key in obj){
    obj[key]
}
```

#### 7.函数

```js
1.创建方法
（1）普通函数
   function fn(){
   }
   fn();
(2)函数表达式
  var fn = function(){}
(3) 自执行函数
 ;(function(){
    this指向 一定是指向window
 })()

全局作用域下，函数作用域，自执行函数都指向了window,函数作用域中this指向可以发生改变，可以使用call()或者apply()

var obj = {name：'mjj'};
function fn(){
    console.log(this.name);
}
fn();//是空值，因为函数内部this指向了window
fn.call(obj)//此时函数内部的this指向了obj

作用： 解决冗余性代码，为了封装

构造函数
new Object();
new Array();
new String();
new Number();

//使用构造函数来创建对象
function Point(x, y) {
  this.x = x;
  this.y = y;
}
Point.prototype.toString = function () {
  return '(' + this.x + ', ' + this.y + ')';
};
var p = new Point(1, 2);

//es6用class来创建对象
class Person{
    constructor(x,y){
        this.x = x;
        this.y = y
    }
    toString(){
    }
}
var p = new Person();
```

#### 8.日期对象

```
var date = new Date();
date.getDate();
date.getMonth();
date.getDay();
date.getHours();
date.getMinutes();
date.getSeconds()

date.toLocaleString() 2018/08/21 21:30:23
```

#### 9.Math数学对象

```
Math.ceil();向上取整 天花板函数
Math.floor();向下取整，地板函数
Math.round()；标准的四舍五入
随机数
Math.random(); 获取0到1之间的数
function random(min,max){
	return Math.floor(Math.random()*(max-min)) + min;
}
```

#### 10.数值和字符串转换

```js
1.数值转字符串
toString();
数字+空的字符串
2.字符串转数值
parseInt() 转整数
parseFloat() 转浮点型
Number()
```

```
var a = NaN
isNaN(a)
Infinity 无限大的
```

## 2.BOM

browser object model浏览器对象模型

### 1.系统对话框方法

- alert警告框,提示内容,只有确定
- confirm确认框,提示内容,有确定和取消两个操作,确定返回true,取消返回false
- prompt输入框,显示内容和输入框,确定和取消.
  - 可以设置第二个参数,为输入框内默认显示值

### 2.定时方法

- 一次性任务
  - 在3秒之后 执行相应的操作,可以做异步行为

```js
setTimeout(function(){
   console.log('111');
},3000);
console.log('2222');
```

- 周期性循环

```js
var num = 0;
var timer = null;
// 开启定时器
timer = window.setInterval(function(){
    num++;
    if(num === 10){
        // 清除定时器
        clearInterval(timer);
    }
    console.log(num);
},1000);
```

### 3.location对象的属性和方法

```js
console.log(window.location);
setInterval(function(){
   // 做测试
   // 局部刷新 ajax技术:在不重载页面的情况,对网页进行操作
   location.reload();
},2000)
```

## 3.DOM

document object model文档对象模型

### 1.获取节点对象的方式

#### 1.getElementById() 通过id获取方法

```js
var box = document.getElementById('box');
console.log(box);
console.dir(box);
```

#### 2.getElementsByTagName() 通过标签名来获取节点对象

```js
var box2 = document.getElementsByTagName('div');
console.log(box2);
var lis = document.getElementsByTagName('li');
for(var i = 0; i < lis.length; i++){
    // lis[i].className = 'active';
    lis[i].onclick = function(){
        // this指向了绑定onclick的那个对象
        // 排他思想
        for(var j = 0; j < lis.length; j++){
            lis[j].className = '';
        }
        this.className = 'active';
    }
}
```

#### 3.getElementsByClassName() 通过类名获取

```js
var lis2 = document.getElementsByClassName('active');
console.log(lis2);
var box = document.getElementById('box2');
console.log(box.children);
```

### 2.对样式操作

- box.style.(css属性)
- onmouseover 悬浮时
- onmouseout 鼠标移开时
- onclick 点击时

```js
<head>
    <style type="text/css">
		#box{
            width: 200px;
            height: 200px;
            background-color: red;
		}
    </style>
</head>
<body>
	<div id="box">mjj</div>
	<script type="text/javascript">
        // 1.获取事件源对象
        var box = document.getElementById('box');
        // 2.绑定事件
        box.onmouseover = function (){
            // 3.让标签的背景色变绿
            box.style.backgroundColor = 'green';
            box.style.fontSize = '30px';
        };
        box.onmouseout = function (){
            // 3.让标签的背景色变红
            box.style.backgroundColor = 'red';
            box.style.fontSize = '16px';
        };
        var isRed = true;
        box.onclick = function(){
            if(isRed){
                this.style.backgroundColor = 'green';
                isRed = false;
            }else{
                this.style.backgroundColor = 'red';
                isRed = true;
            }
        }
	</script>
</body>
```

### 3.对属性设置

#### 1.getAttribute() 方法

- 接收一个参数——你打算查询的属性的名字

#### 2.setAttribute() 方法

- 对属性节点的值做出修改,第一个参数为属性名，第二个参数为属性值

```js
<head>
    <style type="text/css">
        .a{
            color: red;
            font-size: 30px;
        }
    p.active{
        color: blue;
    }
    </style>
</head>
<body>
	<p title="我的班级" id="p1" class="a">21</p>
    <script type="text/javascript">

		// 如果是自定义的属性,要在文档上能看到,通过setAttribute设置属性
        var p1 = document.getElementById('p1');
        console.log(p1.getAttribute('title'));
        console.log(p1.getAttribute('class'));
        p1.setAttribute('class','abc');
        p1.setAttribute('adadad','1321313');

        console.log(p1.className);
        console.log(p1.title);
        console.log(p1.getAttribute('adadad'));//1321313
        p1.abc = 123; //能设置不能显示
        console.dir(p1);
        console.log(p1.getAttribute('abc'));//null
        p1.onclick = function(){
        	this.className = this.className +' active';
    	}
    </script>
</body>
```

### 4.事件

```js
onclick //点击后...
onmouseover() //鼠标悬浮时
onmouseout() //鼠标移开后
```

### 5.节点的创建添加和删除

#### 1.创建节点createElement() 

- 创建元素节点

#### 2.插入节点appendChild() 

- 在指定的节点的最后一个子节点之后添加一个新的子节点

#### 3.插入节点insertBefore()

- 在已有的子节点前插入一个新的子节点

#### 4.删除节点removeChild()

- 从子节点列表中删除某个节点

#### 5.替换元素节点replaceChild()

- 实现子节点(对象)的替换。返回被替换对象的引用

#### 6.创建文本节点createTextNode

- 创建新的文本节点，返回新创建的Text节点

```js
var ul = document.getElementById('box');
// 创建节点
var li1 = document.createElement('li');
var li2 = document.createElement('li');
// innerText 只设置文本 innerHTML可以设置格式
li1.innerText  = '123';
li2.innerHTML = '<a href="#">12345</a>';
// li1.setAttribute('class','active');
console.log(li1);
console.log(li2);
// li1.children[0].style.color = 'red';
// li1.children[0].style.fontSize = '20px';
ul.appendChild(li1);
// ul.appendChild(li2);
// li2.innerHTML = '第一个';
ul.insertBefore(li2,li1);//在li1前面插入li2
ul.removeChild(li2);//删除
```

### 6.遍历数据操作页面

```html
<head>
    <style>
        ul li p.name {
            color: red;
        }
    </style>
</head>
<body>
<ul id="box">
	/*等待插入内容*/
</ul>
<script type="text/javascript">
    var box = document.getElementById('box');
    //从前端获取后端的数据
    var data = [
        {id: 1, name: '小米8', subName: '你真好', price: 98},
        {id: 2, name: '小米6', subName: '你真好2', price: 948},
        {id: 3, name: '小米4', subName: '你真好3', price: 100},
        {id: 4, name: '小米2', subName: '你真好4', price: 928},
        {id: 5, name: '小米10', subName: '你真好5', price: 918}
    ];
    for (var i = 0; i < data.length; i++) {
        var li = document.createElement('li');
        li.innerHTML = `<p class="name">${data[i].name}</p>
				<p class="subName">${data[i].subName}</p>
				<span class="price">${data[i].price}元</span>`;
        box.appendChild(li);
    }
</script>
</body>
```

### 7.轮播图

```html
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
    <div id="box">
        <img src="images/1.jpg" alt="" id="imgBox">
    </div>
    <button id="prev">上一张</button>  //设置按键
    <button id="next">下一张</button>
    <script type="text/javascript">
        var imgBox = document.getElementById('imgBox');
        var next = document.getElementById('next');
        var prev = document.getElementById('prev');
        var num = 1;
        next.onclick = function (){
            nextImg();
        }; //点击next标签时执行函数
        function nextImg(){
            num++;
            if(num === 5){
                num = 1;
            }
            imgBox.src = `images/${num}.jpg`;
        }
        setInterval(nextImg,1000);//设置每1秒执行一次函数
    </script>
</body>
```







