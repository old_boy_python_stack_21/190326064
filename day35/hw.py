#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.整理进程部分的重点，线程部分重点
# 2.多线程实现一个并发的socket server
'''
import socket
from threading import Thread

sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()


def func(i):
    conn, addr = sk.accept()
    while 1:
        msg = conn.recv(1024)
        print(msg)
        conn.send(b'received\n', i)
        print(i)


for i in range(10):
    t = Thread(target=func, args=(i,))
    t.start()
'''

# 3.使用多线程 实现一个请求网页 并且把网页写到文件中
#   生产者消费者模型来完成
'''
import requests
from threading import Thread
from multiprocessing import Queue


def prod(q, url):
    resp = requests.get(url)
    q.put(resp.text)


def cons(q):
    while 1:
        msg = q.get()
        if not msg:
            break
        with open('msg', 'w', encoding='utf-8')as f:
            f.write(msg)


if __name__ == '__main__':
    q = Queue(3)
    tlst = []
    for i in range(10):
        url = 'http://itianti.sinaapp.com/index.php/mulcpu'
        t = Thread(target=prod, args=(q, url))
        t.start()
        tlst.append(t)
    cus = Thread(target=cons, args=(q, )).start()
    for t in tlst:t.join()
    q.put(None)
'''

# 4.整理默写的内容
# 5.ftp第五个需求 分配家目录的需求
# 6.使用线程池，随意访问n个网址，并把网页内容记录在文件中
'''
import requests
from concurrent.futures import ThreadPoolExecutor

url = ['https://www.baidu.com', 'https://www.douban.com', 'https://www.tencent.com']
def func(url):
    msg = requests.get(url)
    filename = url.split('//')[-1]
    with open(filename, mode='w', encoding='utf-8')as f:
        f.write(msg.text)


if __name__ == '__main__':
    t = ThreadPoolExecutor(3)
    for i in url:
        t.submit(func,i)
'''

# 7.请简述什么是死锁现象？如何产生？如何解决
'''
一个线程或进程有多把锁,陷入阻塞且无法结束的情况
同时有多把锁或互斥锁多次acquire会出现
只用一把锁,每次acquire后要release
'''

# 8.请说说你知道的并发编程中的哪些锁？各有什么特点？
'''
互斥锁:在同一个进程/线程中不能连续多次acquire , 开销小
递归锁:在同一个进程/线程中可以连续多次acquire , acquire几次release几次 , 开销大.
'''

# 9.cpython解释器下的线程是否数据安全？
'''
不安全
+= -= *= /=和多个线程对同一个文件进行写操作时
'''

# 10.在多进程中开启多线程。
'''
from threading import Thread
from multiprocessing import Process


def th(p, t):
    print(p, t)


def pr(p):
    for i in range(3):
        Thread(target=th, args=(p, i)).start()


if __name__ == '__main__':
    for i in range(3):
        Process(target=pr, args=(i,)).start()
'''

# 11.在进程池中开启线程池
'''
from concurrent.futures import ProcessPoolExecutor
from concurrent.futures import ThreadPoolExecutor


def th(i, j):
    print(i, j)


def pr(i):
    t = ThreadPoolExecutor(2)
    for j in range(3):
        t.submit(th, i, j)


if __name__ == '__main__':
    p = ProcessPoolExecutor(2)
    for i in range(3):
        p.submit(pr, i)
'''
