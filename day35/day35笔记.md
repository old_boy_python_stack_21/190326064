# day35笔记

## 1.回顾

- ==什么是生产者消费者模型(面试题)==

```python
1.把一个产生数据并且处理数据的过程解耦
2.让生产的数据的过程和处理数据的过程达到一个工作效率上的平衡
3.中间的容器，在多进程中我们使用队列或者可被join的队列，做到控制数据的量
	当数据过剩的时候，队列的大小会控制这生产者的行为
	当数据严重不足的时候，队列会控制消费者的行为
	并且我们还可以通过定期检查队列中元素的个数来调节生产者消费者的个数
```

- 什么是GIL锁:

```python
1.全局解释器锁
2.cpython解释器中的机制
3.导致了在同一个进程中多个线程不能同时利用多核 —— python的多线程只能是并发不能是并行
```



## 2.锁

- 线程中共享内存会产生数据不安全.

```python
a = 0
def add_f():
    global a
    for i in range(200000):	# 20w个线程
        a += 1
def sub_f():
    global a
    for i in range(200000):
        a -= 1
from threading import Thread
t1 = Thread(target=add_f)
t1.start()
t2 = Thread(target=sub_f)
t2.start()
t1.join()
t2.join()
print(a)	# 应该是0,但GIL锁会影响数据保存.
```

- 即便是线程 , 即便有GIL , 也会出现数据不安全的问题:
- 1.操作的是全局变量
- 2.做以下操作
  - += -= *= /+ 先计算再赋值才容易出现数据不安全的问题
  - 包括 lst[0] += 1  dic['key']-=1

- 加锁:

  - 加锁会影响程序的执行效率，但是保证了数据的安全.

  ```python
  a = 0
  def add(lk):
      global a
      for i in range(100000):
          with lk:
              a += 1
  def sup(lk):
      global a
      for i in range(100000):
          with lk:
              a -= 1
  from threading import Thread, Lock
  lk = Lock()
  t1 = Thread(target=add, args=(lk,))
  t1.start()
  t2 = Thread(target=sup, args=(lk,))
  t2.start()
  t1.join()
  t2.join()
  print(a)
  ```

- 互斥锁
  - 在同一个线程中,不能连续acquire多次

## 3.单例模式

```python
import time
from threading import Lock

class A:
    __instance = None
    lock = Lock()
    def __new__(cls, *args, **kwargs):
        with cls.lock:
            if not cls.__instance:
                time.sleep(0.1)
                cls.__instance = super().__new__(cls)
        return cls.__instance
    def __init__(self,name,age):
        self.name = name
        self.age = age

def func():
    a = A('alex', 84)
    print(a)

from threading import Thread
for i in range(10):
    t = Thread(target=func)
    t.start()
```



## 4.死锁现象

- 死锁现象是怎么发生的
  - 1.有多把锁,一把以上
  - 2.多把锁交替使用
- 怎么解决
  - 递归锁----将多把互斥锁变成一把递归锁
    - 快速解决问题
    - 效率差
    - 递归锁也会发生死锁现象(多把锁交替使用时)
  - 优化代码逻辑
    - 可以使用互斥锁,解决问题
    - 效率相对好
    - 解决问题的效率相对低
- 锁
  - 互斥锁
    - 在一个线程中连续多次acquire会死锁
  - 递归锁
    - 在一个线程中连续多次acquire不会死锁

```python
import time
from threading import Thread,Lock
noodle_lock = Lock()
fork_lock = Lock()
def eat1(name,noodle_lock,fork_lock):
    noodle_lock.acquire()
    print('%s抢到面了'%name)
    fork_lock.acquire()
    print('%s抢到叉子了' % name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    fork_lock.release()
    print('%s放下叉子了' % name)
    noodle_lock.release()
    print('%s放下面了' % name)

def eat2(name,noodle_lock,fork_lock):
    fork_lock.acquire()
    print('%s抢到叉子了' % name)
    noodle_lock.acquire()
    print('%s抢到面了'%name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    noodle_lock.release()
    print('%s放下面了' % name)
    fork_lock.release()
    print('%s放下叉子了' % name)

lst = ['alex','wusir','taibai','yuan']
Thread(target=eat1,args=(lst[0],noodle_lock,fork_lock)).start()
Thread(target=eat2,args=(lst[1],noodle_lock,fork_lock)).start()
Thread(target=eat1,args=(lst[2],noodle_lock,fork_lock)).start()
Thread(target=eat2,args=(lst[3],noodle_lock,fork_lock)).start()
# 一个人抢到叉子,另一个人抢到面,则会进入死锁状态.
# 用递归锁解决.
```



## 5.递归锁RLock

- 优点:在同一个线程中,可以连续acquire多次不会被锁住.
- 缺点:占用了更多资源

```python
import time
from threading import RLock,Thread
# noodle_lock = RLock()
# fork_lock = RLock()
noodle_lock = fork_lock = RLock()
print(noodle_lock,fork_lock)
def eat1(name,noodle_lock,fork_lock):	# 多次acquire多次release
    noodle_lock.acquire()
    print('%s抢到面了'%name)
    fork_lock.acquire()
    print('%s抢到叉子了' % name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    fork_lock.release()
    print('%s放下叉子了' % name)
    noodle_lock.release()
    print('%s放下面了' % name)

def eat2(name,noodle_lock,fork_lock):
    fork_lock.acquire()
    print('%s抢到叉子了' % name)
    noodle_lock.acquire()
    print('%s抢到面了'%name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    noodle_lock.release()
    print('%s放下面了' % name)
    fork_lock.release()
    print('%s放下叉子了' % name)

lst = ['alex','wusir','taibai','yuan']
Thread(target=eat1,args=(lst[0],noodle_lock,fork_lock)).start()
Thread(target=eat2,args=(lst[1],noodle_lock,fork_lock)).start()
Thread(target=eat1,args=(lst[2],noodle_lock,fork_lock)).start()
Thread(target=eat2,args=(lst[3],noodle_lock,fork_lock)).start()
```



## 6.互斥锁解决死锁问题

```python
import time
from threading import Lock,Thread
lock = Lock()
def eat1(name):		# 一次acquire一次release
    lock.acquire()
    print('%s抢到面了'%name)
    print('%s抢到叉子了' % name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    print('%s放下叉子了' % name)
    print('%s放下面了' % name)
    lock.release()

def eat2(name):
    lock.acquire()
    print('%s抢到叉子了' % name)
    print('%s抢到面了'%name)
    print('%s吃了一口面'%name)
    time.sleep(0.1)
    print('%s放下面了' % name)
    print('%s放下叉子了' % name)
    lock.release()

lst = ['alex','wusir','taibai','yuan']
Thread(target=eat1,args=(lst[0],)).start()
Thread(target=eat2,args=(lst[1],)).start()
Thread(target=eat1,args=(lst[2],)).start()
Thread(target=eat2,args=(lst[3],)).start()
```

## 7.队列

- 线程之间的通信 线程安全

- Queue  先进先出队列

  ```python
  from queue import Queue  # 先进先出队列
  q = Queue(5)
  q.put(0)
  q.put(1)
  q.put(2)
  q.put(3)
  q.put(4)
  print('444444')
  print(q.get())
  print(q.get())
  print(q.get())
  print(q.get())
  print(q.get())	# 0,1,2,3,4
  ```

- LifoQueue    后进先出队列

  ```python
  from queue import LifoQueue  # 后进先出队列
  lfq = LifoQueue(4)
  lfq.put(1)
  lfq.put(2)
  lfq.put(3)
  print(lfq.get())
  print(lfq.get())
  print(lfq.get())	# 3,2,1
  ```

- 优先级队列

  - 自动的排序
  - 抢票用户级别
  - 告警级别

  ```python
  from queue import PriorityQueue
  pq = PriorityQueue()
  pq.put((10,'alex'))
  pq.put((6,'wusir'))
  pq.put((20,'yuan'))
  print(pq.get())
  print(pq.get())
  print(pq.get())		# (6,'wusir'),(10,'alex'),(20,'yuan')
  ```

## 8.池

- 池 : 预先的开启固定个数的进程数，当任务来临的时候，直接提交给已经开好的进程,让这个进程去执行就可以了,节省了进程(线程的开启 关闭 切换都需要时间),并且减轻了操作系统调度的负担.

### 8.1进程池(from concurrent.futures import ProcessPoolExecutor)

- .submit(函数,参数)   提交
- .shutdown()    关闭池  阻塞直到所有的任务执行完毕

```python
import os
import time
from concurrent.futures import ProcessPoolExecutor

def func():
    print('start', os.getpid())
    time.sleep(1)
    print('end', os.getpid())

if __name__ == '__main__':
    pp = ProcessPoolExecutor(5)
    for i in range(20):
        pp.submit(func)     # 提交
    pp.shutdown()   # 关闭池之后就不能继续提交任务，并且会阻塞，直到已经提交的任务完成
    print('done')
```

- 任务的参数  +  返回值
  - ret.result()获取返回值

```python
import os
import time
from concurrent.futures import ProcessPoolExecutor

def func(i):
    print('start', os.getpid())
    time.sleep(1)
    print('end', os.getpid())
    return i**2

if __name__ == '__main__':
    p = ProcessPoolExecutor(5)      # 创建5个进程池
    lst = []
    for i in range(15):
        s = p.submit(func, i)
        lst.append(s)
    for i in lst:
        print(i.result())           # .result输出函数返回值
    print('done', os.getpid())
```

- 进程池的开销大,一个池中的任务个数限制了我们程序的并发个数



### 8.2线程池(from concurrent.futures import ThreadPoolExecutor)

```python
import os
import time
from concurrent.futures import ThreadPoolExecutor

def func(i):
    print('start', os.getpid())
    time.sleep(1)
    print('end', os.getpid())
    return i**2

t = ThreadPoolExecutor(10)
lst = []
for i in range(10):
    res = t.submit(func, i)
    lst.append(res)
t.shutdown()
for i in lst:
    print(i.result())
print('Done')
```

或者使用map函数

- tp.map(func,iterable) 迭代获取iterable中的内容，作为func的参数，让子线程来执行对应的任务

```python
from concurrent.futures import ThreadPoolExecutor
def func(i):
    print('start', os.getpid())
    time.sleep(random.randint(1,3))
    print('end', os.getpid())
    return '%s * %s'%(i,os.getpid())
tp = ThreadPoolExecutor(20)
ret = tp.map(func,range(20))
for i in ret:
    print(i)
ret_l = []
for i in range(10):
    ret = tp.submit(func,i)
    ret_l.append(ret)
tp.shutdown()
print('main')
```



### 8.3回调函数

- ret.add_done_callback(函数)    给当前任务绑定回调函数 , 先执行完的执行()里的函数.并且ret的结果会作为参数返回给绑定的函数

```python
import requests
from concurrent.futures import ThreadPoolExecutor
def get_page(url):
    res = requests.get(url)
    return {'url':url,'content':res.text}
def parserpage(ret):
    dic = ret.result()
    print(dic['url'])
tp = ThreadPoolExecutor(5)
url_lst = [
    'http://www.baidu.com',   # 3
    'http://www.cnblogs.com', # 1
    'http://www.douban.com',  # 1
    'http://www.tencent.com',
    'http://www.cnblogs.com/Eva-J/articles/8306047.html',
    'http://www.cnblogs.com/Eva-J/articles/7206498.html',
]
ret_l = []
for url in url_lst:
    ret = tp.submit(get_page,url)
    ret_l.append(ret)
    ret.add_done_callback(parserpage)	# 回调函数
```













