# day10笔记

## 一.参数

### 1.1基本知识

- 不限数量,不限数据类型

```python
def func(a1,a2):
    代码块
func(1,2)
```

### 1.2位置传参

- 按位置挨个代入

``` python
def func(a1,a2):
    代码块
func(1,2)	#a1=1,a2=2
```

### 1.3关键字传参

- 位置传参在前,关键字在后

``` python
def func(a,b,c):
    代码块
func(1,2,c=3)	#c=3是关键字参数
```

### 1.4默认参数

``` python
def func(a,b=2,c=3):
    代码块
func(1)	#b默认等于2,c=3,传入其他值则修改.
```

### 1.5万能参数

#### 1.5.1 *args

- 接收多个位置参数,并将参数转换成元组.

```python
def func(*args):
    print(args)
#不带*调用函数
func(1,2,3,4,5)	
#可传入多个值,所有数据类型,输出是元组

#带*调用函数
func(*(1,2,3))
func(*[1,2,3])
#两者都输出元组
```

- 只能用位置传参.

```python
def func(*args):
    print(args)
# func(1)
# func(1,2)
func(1,2) # args=(1, 2)
func((11,22,33,44,55)) # args=((11,22,33,44,55),)
func(*(11,22,33,44,55)) # args=(11,22,33,44,55)
```

#### 1.5.2 **kwargs

- 接收多个关键字参数,将参数转化为字典.

```python
def func(**kwargs):
    print(kwargs)
    
#不带**调用函数
func(k1=1,k2=2)	
#kwargs={'k1':1,'k2':2}

#带**调用函数
func(**{'k1':'v2','k2':'v2'}) 
#kwargs={'k1':'v2','k2':'v2'}
```

- 只能用关键字传参.

#### 1.5.3两者结合

```python
def func(*args,**kwargs):
    print(args,kwargs)

func(1,2,3,4,5,k1=2,k5=9,k19=999)
#args=(1,2,3,4,5) kwargs={'k1':2,'k5':9,'k19':999}

func(*[1,2,3],k1=2,k5=9,k19=999)
#args=(1,2,3) kwargs={'k1':2,'k5':9,'k19':999}

func(*[1,2,3],**{'k1':1,'k2':3})
#args=(1,2,3) kwargs={'k1':1,'k2':3}

func(111,222,*[1,2,3],k11='alex',**{'k1':1,'k2':3})
#args=(111,222,1,2,3) kwargs={'k11':'alex','k1':1,'k2':3}
```

### 1.6重点

#### 1.6.1定义函数三种方法:

```python
def func1(a1,a2):
    pass 

def func2(a1,a2=None):
    pass 

def func3(*args,**kwargs):
    pass 
```

#### 1.6.2调用函数

位置参数 > 关键字参数

## 二.作用域

### 2.1基本知识

- 在python中,整个py文件是全局作用域,函数内部是局部作用域.

```python
a = 1
def s1():	
    x1 = 666
    print(x1)	#3.输出666
    print(a)	#4.局部没有a则去父级找,输出1
    print(b)	#5.局部没有a则去父级找,输出2

b = 2
print(a)	#1.输出1
s1()		#2.调用s1()
a = 88888	#6.a被重新赋值
def s2():
    print(a,b)	#8.输出88888,2
    s1()	#9.调用s1,输出666,88888,2

s2()		#7.调用s2()
```

### 2.2一个函数有一个作用域.

```python
def func():
    x = 9
    print(x)	#2.输出9
func()		#1.调用func()
print(x)	#3.全局没有x报错
```

### 2.3作用域中查找数据规则

- 优先在局部查找,找不到去父级找, 找不到再去父级找,直到全局,全局没有则报错.

```python
x = 10			#1.全局变量x=10
def func():
    x = 8		#3.局部变量x=8
    print(x)	#4.输出8
    def x1():	
        print(x)	#6.x1()局部没有x去父级找,x=8输出8
    x1()		#5.调用x1()
    x = 9		#7.x重新赋值为9
    x1()		#8.调用x1()	9.输出9
    x = 10		#10.x重新赋值为10
    print(x)	#11.输出10
func()			#2.调用func()
```

- 只能子级去父级找,不能为父级的变量赋值.能修改可变数据类型.

```python
name = 'oldboy'
def func():
    name = 'alex'	#2.在自己作用域再创建一个这样的值。
    print(name)		#3.输出alex
func()			#1.调用func()
print(name)		#4.输出oldboy
```

```python
name = [1,2,43]
def func():
    name.append(999)	#2.name是列表,可变
    print(name)		#3.输出[1,2,43,999]
func()				#1.调用func()
print(name)			#4.输出[1,2,43,999]
```

### 2.4 global和nonlocal.

- global使用全局变量.

```python
name = ["老男孩",'alex']
def func():
    global name		#2.调用全局变量name
    name = '我'		#3.name被重新赋值
func()				#1.调用func()
print(name)			#4.输出我
```

```python
name = "老男孩"
def func():
    name = 'alex'		#2.局部name=alex
    def inner():
        global name		#4.调用全局变量name
        name = 999		#5.name被重新赋值
    inner()				#3.调用inner()
    print(name)			#6.输出局部变量name:alex
func()					#1.调用func()
print(name)				#7.输出999
```

- nonlocal使用父级变量.

```python
name = "老男孩"
def func():
    name = 'alex'		#2.局部变量
    def inner():
        nonlocal name	#4.找到上一级的name
        name = 999		#5.将父级name重新赋值
    inner()				#3.调用inner()
    print(name)			#6.输出999
func()					#1.调用func()
print(name)				#7.输出老男孩
```

### 2.5补充

全局变量必须全部大写.