#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import json
import socket


def up():
    sk.send('1'.encode('utf-8'))
    file_path = input('>>>')  # 输入文件路径
    if file_path.upper() == 'Q':
        sk.send(b'Q')
        return
    if os.path.isabs(file_path):  # 判断文件路径是否为绝对路径
        filename = os.path.basename(file_path)  # 返回文件名
        filesize = os.path.getsize(file_path)  # 返回文件大小
        dic = {'filename': filename, 'filesize': filesize}  # 存入字典
        str_dic = json.dumps(dic)  # 转为str格式
        sk.send(str_dic.encode('utf-8'))
        with open(file_path, mode='rb') as f:  # 打开文件,模式为读取字节
            content = f.read()
            sk.send(content)

    print('上传完成!')


def download():
    sk.send(b'2')
    print('3333')
    msg = sk.recv(1024).decode('utf-8')
    article = json.loads(msg)
    for i in range(len(article)):
        print('序号:', i+1, '文件名:', article[i])
    user = int(input('请选择序号:'))
    sk.send(article[user-1].encode('utf-8'))
    strdic = sk.recv(1024).decode('utf-8')
    dic = json.loads(strdic)
    content = sk.recv(dic['filesize'])
    with open(dic['filename'], 'wb') as f:
        f.write(content)
    print('下载完成!')


def run():
    dic = {'1': up, '2': download}
    while 1:
        user = input('1.上传文件\n2.下载文件\n请选择(Q/q退出):')
        if user.upper() == 'Q':
            break
        choice = dic.get(user)
        if not choice:
            print('输入有误!')
            continue
        dic[user]()
    print('任务完成程序结束!')
    return


def regis():
    while 1:
        username = input('用户名:')
        pwd = input('密码:')
        if username == 'alex' and pwd == '123':
            print('登陆成功!')
            run()
            return
        else:
            print('账号或密码错误!')


sk = socket.socket()
sk.connect(('127.0.0.1', 9000))
if __name__ == '__main__':
    regis()
    sk.close()
