# day29笔记

## 1.回顾

### 1.tcp协议

- 可靠,慢,全双工,数据的长度大
- 三次握手 : syn  ack
- 四次挥手 : fin  ack
- 区别 : 三次握手把一个回复和请求连接的两条信息合并成一条了
- 挥手不能合并 : 由于一方断开连接后可能另一方还有数据没有传递完,所以不能立即断开.

### 2.udp协议

- 不可靠,快,数据的长度小

## 2.编码

### 1.计算机上的存储 / 网络上数据的传输  --二进制

1位   bit比特

10101100   8位1个字节

### 2.send

str-encode(编码) -> bytes

### 3.recv

bytes-decode(编码) -> str

## 3.struct模块

- ret = struct.pack('i', 1000)
  - 把int转换为bytes
- struct.unpack('i', ret)
  - 把ret转换为int

## 4.黏包现象

- 定义 : 指发送方发送的若干包数据到接收方接收时粘成一包，从接收缓冲区看，后一包数据的头紧接着前一包数据的尾。
- 解决该现象 : 需要让发送端在发送数据前，把自己将要发送的字节流总大小让接收端知晓,用struct模块.

- server端

```python
import time
import struct
import socket

sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

conn, _ = sk.accept()   # 等待连接
byte_len = conn.recv(4)     # 接收四个字节的文件长度
size = struct.unpack('i', byte_len)[0]  # 解包4个字节,第0项是文件大小
msg1 = conn.recv(size)  # 接收文件大小的字节
print(msg1)

time.sleep(0.1)
byte_len = conn.recv(4)     # 再次接收文件长度
size = struct.unpack('i', byte_len)[0]
msg2 = conn.recv(size)
print(msg2)
conn.close()
sk.close()
# 发送4个字节的文件的信息
# json --》文件的大小+文件名。。。。
# 发送文件
```

- client端

```python
import struct
import socket

sk = socket.socket()
sk.connect(('127.0.0.1',9000))

msg = b'hello'
byte_len = struct.pack('i',len(msg))    # 打包msg长度
sk.send(byte_len)   #  发送长度
sk.send(msg)        #  发送msg
msg = b'world'
byte_len = struct.pack('i',len(msg))
sk.send(byte_len)
sk.send(msg)

sk.close()
```

## 5.作业

### 1.tcp协议实现聊天基础需求

- 1.server和client端连接之后，能知道对面这个人是哪一个好友  qq号
- 2.不同好友的聊天颜色不同

server端

```python
import json
import socket


def chat(conn):     # 聊天模块
    while True:
        msg = conn.recv(1024).decode('utf-8')       # 接收消息,msg字典的json形式
        dic_msg = json.loads(msg)
        uid = dic_msg['id']                         # 获取msg的id账号
        if dic_msg['msg'].upper() == 'Q':           # 如果对方输入的msg的msg项为Q则退出
            print('%s已经下线' % color_dic[uid]['name'])
            break
        print('%s%s : %s\033[0m' % (color_dic[uid]['color'], color_dic[uid]['name'], dic_msg['msg']))   # color字典的id项的color项
        inp = input('>>>')
        conn.send(inp.encode('utf-8'))  # 发送内容
        if inp.upper() == 'Q':          # Q退出
            print('您已经断开和%s的聊天' % color_dic[uid]['name'])
            break


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()
color_dic = {
    '12345': {'color': '\033[32m', 'name': 'alex'},
    '12346': {'color': '\033[31m', 'name': 'wusir'},
    '12347': {'color': '\033[33m', 'name': 'yuan'},
}       # 各个账号颜色
while True:
    conn, _ = sk.accept()       # 等待接入  conn是连接的数据
    chat(conn)                  # 调用chat聊天模块
    conn.close()                # 关闭连接
sk.close()
```

client端

```python
import socket
import json

sk = socket.socket()

id = '12346'                                # 登录的id账号
sk.connect(('127.0.0.1', 9000))             # 连接到服务器
while True:
    inp = input('>>>')
    dic = {'msg': inp, 'id': id}            # 把内容和id账号放入字典
    str_dic = json.dumps(dic)               # 把字典变成str格式
    sk.send(str_dic.encode('utf-8'))        # 发送
    if inp.upper() == 'Q':
        print('您已经断开和server的聊天')
        break
    msg = sk.recv(1024).decode('utf-8')     # 接收服务器的消息
    if msg.upper() == 'Q':
        break
    print(msg)
sk.close()
```

### 2.tcp协议实现用户登陆

- 加入hashlib密文存储密码

server端

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
import socket
import hashlib


def get_md5(username, password):
    md5 = hashlib.md5(username.encode('utf-8'))     # 加的盐使用 用户名
    md5.update(password.encode('utf-8'))            # 把密码转为md5
    return md5.hexdigest()


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

conn, _ = sk.accept()
str_dic = conn.recv(1024).decode('utf-8')
dic = json.loads(str_dic)                           # 接收用户字典信息
with open('userinfo', encoding='utf-8') as f:       # 打开用户信息文件
    for line in f:
        user, passwd = line.strip().split('|')
        if user == dic['usr'] and passwd == get_md5(dic['usr'], dic['pwd']):    # 判断账号密码相同
            res_dic = {'opt': 'login', 'result': True}
            break
    else:
        res_dic = {'opt': 'login', 'result': False}
sdic = json.dumps(res_dic)          # 把登录结果转为str
conn.send(sdic.encode('utf-8'))     # 发送
conn.close()
sk.close()
```

client端

```python
import json
import socket

usr = input('username : ')
pwd = input('password : ')
dic = {'usr': usr, 'pwd': pwd}
str_dic = json.dumps(dic)       # 把字典转为str
sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

sk.send(str_dic.encode('utf-8'))        # 发送账号密码
ret = sk.recv(1024).decode('utf-8')     # 接收客户端的返回内容
ret_dic = json.loads(ret)               # str转换为字典
if ret_dic['result']:   # 字典的result项为True
    print('登陆成功')
sk.close()
```

### 3.tcp协议完成文件上传

- 基础的需求 ：两台机器能从一台发送给另一台就可以了
- 进阶的需求 ：考虑大文件（选做）

server端

```python
import json
import socket

sk = socket.socket()
sk.bind(('192.168.12.87', 9000))
sk.listen()

conn, addr = sk.accept()
print(addr)
str_dic = conn.recv(1024).decode('utf-8')       # 接收str字典
dic = json.loads(str_dic)                       # 转换为字典
content = conn.recv(dic['filesize'])            # 接收字典中的文件大小项
with open(dic['filename'], mode='wb') as f:     # 创建字典中文件名的项,模式为写字节
    f.write(content)
conn.close()
sk.close()
```

client端

```python
import os
import json
import socket

sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

file_path = input('>>>')                    # 输入文件路径
if os.path.isabs(file_path):                # 判断文件路径是否为绝对路径
    filename = os.path.basename(file_path)      # 返回文件名
    filesize = os.path.getsize(file_path)       # 返回文件大小
    dic = {'filename': filename, 'filesize': filesize}      # 存入字典
    str_dic = json.dumps(dic)                   # 转为str格式
    sk.send(str_dic.encode('utf-8'))
    with open(file_path, mode='rb') as f:       # 打开文件,模式为读取字节
        content = f.read()
        sk.send(content)
sk.close()
```









