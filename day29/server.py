#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.文件的上传、下载
# 2.结合用户认证
# 要求：
# 1.tcp协议粘包的问题，使用struct模块解决粘包问题
# 2.传递的所有的信息都尽量是json格式
# 3.再server端用上反射

import os
import json
import time
import socket


def up():
    str_dic = conn.recv(1024).decode('utf-8')  # 接收str字典
    if str_dic == 'Q':
        return
    dic = json.loads(str_dic)  # 转换为字典
    print(dic)
    content = conn.recv(dic['filesize'])  # 接收字典中的文件大小项
    filebase = os.path.join(r'D:\homework\day29\file', dic['filename'])
    with open(filebase, mode='wb') as f:  # 创建字典中文件名的项,模式为写字节
        f.write(content)


def download():
    a = os.listdir(r'D:\homework\day29\file')       # 服务器上的文件的列表
    stra = json.dumps(a)        # 字符串列表
    conn.send(stra.encode('utf-8'))         # 发给client
    filename = conn.recv(1024).decode('utf-8')      # 接收文件名
    file = os.path.join(r'D:\homework\day29\file', filename)        # 获取文件路径
    filesize = os.path.getsize(file)
    dic = {'filename': filename, 'filesize': filesize}
    strdic = json.dumps(dic)
    conn.send(strdic.encode('utf-8'))
    with open(file, mode='rb') as f:       # 打开文件,模式为读取字节
        content = f.read()

    conn.send(content)
    print('下载完成!')


def run(con):
    dic = {'1': up, '2': download}
    while 1:
        choice = con.recv(1024).decode('utf-8')
        print(choice)
        func = dic.get(choice)
        if choice.upper() == 'Q':
            break
        if not func:
            print('输入有误!')
            time.sleep(1)
            continue
        func()
        print('任务完成!')
        break


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()
while 1:
    conn, addr = sk.accept()
    run(conn)
    conn.close()


sk.close()
