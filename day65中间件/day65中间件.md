# day65中间件

博客:<https://www.cnblogs.com/maple-shaw/articles/9333824.html>

## 1.回顾

Django - web框架 , socket服务端

### 1.http协议

- 请求的格式

  ```
  “请求方式 URL路径 协议版本HTTP/1.1\r\n
  k1:v1\r\n
  k2:v2\r\n
  \r\n
  请求体（请求的数据）”
  ```

- 响应的格式

  ```
  “HTTP/1.1 状态码 状态描述\r\n
  k1:v1\r\n
  k1v2\r\n
  \r\n
  响应体（HTML页面）”
  ```

- 请求头信息
  - contentType：  text/html    application/json 
  - set-cookie   cookie  
  - Location  
- MVC   MTV (model,templates,views)

### 2.路由    

- url  和 函数的对应关系
  - url（正则表达式，视图，{数据} ，name(url别名)）

- 分组和命名分组

- 路由分发  include

- url的命名和反向解析

- namespace  命名空间

  

- 视图   CBV (class)   FBV (function)

- request (10个属性和方法)

- response (4个 Jsonresponse)

### 3.ORM

### 4.模板

### 5.cookie session

### 6.ajax

### 7.form



## 2.今日内容

### 1.中间件

- 中间件是处理django的请求和响应的框架级别的钩子，本质上就是一个类。

- 用于在全局范围内改变Django的输入和输出。每个中间件组件都负责做一些特定的功能 , 但是由于其影响的是全局，所以需要谨慎使用

- 中间件可以定义五个方法:
  - process_request(self,request)
  - process_view(self, request, view_func, view_args, view_kwargs)
  - process_template_response(self,request,response)
  - process_exception(self, request, exception)
  - process_response(self, request, response)
  - 以上方法的返回值可以是None或一个HttpResponse对象，如果是None，则继续按照django定义的规则向后继续执行，如果是HttpResponse对象，则直接将该对象返回给用户.

- 4个特征：  执行时间、执行顺序、参数、返回值

- 自定义中间件

  ```python
  from django.utils.deprecation import MiddlewareMixin
  
  class MD1(MiddlewareMixin):
      def process_request(self, request):
          print("MD1里面的 process_request")
      def process_response(self, request, response):
          print("MD1里面的 process_response")
          return response
  ```

#### 1.process_request(self,request)

- 执行时间：视图函数之前

- 参数：request   —— 》 和视图函数中是同一个request对象

- 执行顺序：按照注册的顺序  顺序执行

- 注册

  - 在settings.py的MIDDLEWARE配置项中注册上述两个自定义中间件

  ```python
  MIDDLEWARE = [
      ...
      'middlewares.MD1',  # 自定义中间件MD1
      'middlewares.MD2'  # 自定义中间件MD2
  ]
  ```

- 返回值：

  - None ： 正常流程
  - HttpResponse： 后面的中间的process_request、视图函数都不执行，直接执行当前中间件中的process_response方法，倒序执行之前的中间件process_response方法。

#### 2.process_response(self, request, response)

- 执行时间：视图函数之后
- 参数：
  - request   —— 》 和视图函数中是同一个request对象
  - response   ——》  返回给浏览器响应对象
- 执行顺序：按照注册的顺序  倒序执行
- 返回值：
  - HttpResponse：必须返回response对象

#### 3.process_view

- process_view(self, request, view_func, view_args, view_kwargs)
- 执行时间：视图函数之前，process_request之后
- 参数：
  - request   —— 》 和视图函数中是同一个request对象
  - view_func  ——》 视图函数
  - view_args   ——》 视图函数的位置参数
  - view_kwargs  ——》 视图函数的关键字参数
- 执行顺序：按照注册的顺序  顺序执行
- 返回值：
  - None ： 正常流程
  - HttpResponse： 后面的中间的process_view、视图函数都不执行，直接执行最后一个中间件中的process_response方法，倒叙执行之前的中间中process_response方法。

#### 4.process_exception(self, request, exception)

- 执行时间（触发条件）：视图层面有错时才执行
- 参数：
  - request   —— 》 和视图函数中是同一个request对象
  - exception   ——》 错误对象
- 执行顺序：按照注册的顺序  倒序执行
- 返回值：
  - None ： 交给下一个中间件去处理异常，都没有处理交由django处理异常
  - HttpResponse： 后面的中间的process_exception不执行，直接执行最后一个中间件中的process_response方法，倒序执行之前的中间中process_response方法。
- 如果视图函数中无异常，process_exception方法不执行。

#### 5.process_template_response(self,request,response)

- 执行时间（触发条件）：在视图函数执行完成后立即执行，但是它有一个前提条件，那就是视图函数返回的对象有一个render()方法（或者表明该对象是一个TemplateResponse对象或等价方法） , 视图返回的是一个templateResponse对象
- 参数：
  - request   —— 》 和视图函数中是同一个request对象
  - response   ——》  templateResponse对象
- 执行顺序：按照注册的顺序  倒叙执行
- 返回值：
  - HttpResponse：必须返回response对象

### django请求的生命周期

![](D:\homework\day65中间件\1561783661582.png)









