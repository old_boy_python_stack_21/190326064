#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.shortcuts import redirect, reverse
from django.utils.deprecation import MiddlewareMixin
from app01 import models


class AuthMiddleware(MiddlewareMixin):
    def process_request(self, request):
        # 没登录
        if request.path_info in [reverse('login')]:
            return
        if not request.session.get('is_login'):
            return redirect(reverse('login'))
        # 登录成功
        obj = models.UserProfile.objects.filter(pk=request.session.get('user_id')).first()
        if obj:
            request.user_obj = obj  # request.user源码中包含
