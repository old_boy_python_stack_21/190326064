from app01 import models
from django.views import View
from django.db.models import Q
from django.conf import settings
from django.db import transaction
from utils.pagination import Pagination
from app01.forms import AddForm, ConsultRecordForm, EnrollmentForm
from django.shortcuts import render, redirect, reverse, HttpResponse

users = models.Customer.objects.all()


def index(request):
    return render(request, 'index.html')


# CBV
class CustomerList(View):
    def get(self, request, *args, **kwargs):

        # query = request.GET.get('query', '')
        # q = Q(qq__contains=query) | Q(phone__contains=query)
        q = self.search(['qq', 'phone'])
        if request.path_info == reverse('user_list'):
            all_customer = models.Customer.objects.filter(q, consultant__isnull=True)
            cititle = '客户中心'
            zhuan = '分配'
        else:
            all_customer = models.Customer.objects.filter(q, consultant=request.user_obj)
            cititle = '我的客户'
            zhuan = '移除'
        page = Pagination(request.GET.get('page', 1), len(all_customer), request.GET.copy(), 5)
        return render(request, 'consultant/user_list.html',
                      {'users': all_customer[page.start:page.end], 'page_html': page.page_html, 'cititle': cititle,
                       'zhuan': zhuan})

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action')
        if not hasattr(self, action):
            return HttpResponse('非法操作')
        ret = getattr(self, action)()
        if ret:
            return ret
        return self.get(request, *args, **kwargs)

    def multi_apply(self):
        pk = self.request.POST.getlist('pk')
        if models.Customer.objects.filter(consultant=self.request.user_obj).count() + len(
                pk) > settings.MAX_CUSTOMER_NUM:
            return HttpResponse('超出上限')
        try:
            with transaction.atomic():
                queryset = models.Customer.objects.filter(pk__in=pk, consultant=None).select_for_update()
                if len(pk) == queryset.count():
                    queryset.update(consultant=self.request.user_obj)
                else:
                    return HttpResponse('手速太慢,客户没啦!')
        except Exception as e:
            print(e)

    def multi_pub(self):
        pk = self.request.POST.getlist('pk')
        models.Customer.objects.filter(pk__in=pk).update(consultant=None)

    def search(self, field_list):
        query = self.request.GET.get('query', '')
        q = Q()
        q.connector = 'OR'
        for field in field_list:
            q.children.append(Q(('{}__contains'.format(field), query)))
        return q


# FBV
# def user_list(request):
#     if request.path_info == reverse('user_list'):
#         all_customer = models.Customer.objects.filter(consultant__isnull=True)
#         cititle = '客户中心'
#         zhuan = '分配'
#     else:
#         all_customer = models.Customer.objects.filter(consultant_id=request.session.get('user_id'))
#         cititle = '我的客户'
#         zhuan = '移除'
#     page = Pagination(request.GET.get('page', 1), len(all_customer), 5)
#     return render(request, 'user_list.html',
#                   {'users': all_customer[page.start:page.end], 'page_html': page.page_html, 'cititle': cititle,
#                    'zhuan': zhuan})


def add_customer(request):
    form_obj = AddForm()
    if request.method == 'POST':
        form_obj = AddForm(request.POST)
        if form_obj.is_valid():
            form_obj.save()
            return redirect(reverse('user_list'))
    return render(request, 'consultant/add_customer.html', {'form_obj': form_obj})


# def edit_customer(request, pk):
#     obj = models.Customer.objects.filter(pk=pk).first()
#     form_obj = AddForm(instance=obj)
#     if request.method == 'POST':
#         form_obj = AddForm(data=request.POST, instance=obj)
#         if form_obj.is_valid():
#             form_obj.save()
#             return redirect(reverse('user_list'))
#     return render(request, 'edit_customer.html', {'form_obj': form_obj})


def customer_change(request, pk=None):
    obj = models.Customer.objects.filter(pk=pk).first()
    form_obj = AddForm(instance=obj)
    if request.method == 'POST':
        form_obj = AddForm(data=request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect(reverse('user_list'))
    title = '编辑客户' if pk else '新增客户'
    return render(request, 'consultant/edit_customer.html', {'form_obj': form_obj, 'title': title})


def zhuan(request):
    return redirect(reverse('my_customer'))


def consult_change(request, pk=None, customer_id=None):
    obj = models.ConsultRecord.objects.filter(pk=pk).first()
    models.ConsultRecord(customer_id=customer_id, consultant=request.user_obj)
    form_obj = ConsultRecordForm(request, customer_id, instance=obj)
    if request.method == 'POST':
        form_obj = ConsultRecordForm(request, customer_id, request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            return redirect(reverse('consult_list'))
    title = '编辑跟进' if pk else '新增跟进'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


class ConsultList(View):

    def get(self, request, customer_id=0, *args, **kwargs):
        cititle = '跟进记录'
        if not customer_id:
            # 当前登录的销售的
            all_consult = models.ConsultRecord.objects.filter(consultant=request.user_obj, delete_status=False)
        else:
            # 一个客户的
            all_consult = models.ConsultRecord.objects.filter(customer_id=customer_id, delete_status=False)
        page = Pagination(request.GET.get('page', 1), len(all_consult), request.GET.copy(), 5)
        return render(request, 'consultant/consult_list.html',
                      {'users': all_consult.order_by('-date')[page.start:page.end], 'page_html': page.page_html,
                       'cititle': cititle, 'customer_id': customer_id})

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action')
        if not hasattr(self, action):
            return HttpResponse('非法操作')
        getattr(self, action)()
        return self.get(request, *args, **kwargs)


class EnrollmentList(View):

    def get(self, request, *args, customer_id=None, **kwargs):
        if not customer_id:
            # 展示一个销售填写报名记录
            all_enrollment = models.Enrollment.objects.filter(customer__in=request.user_obj.customers.all(),
                                                              delete_status=False)
        else:
            # 某一个客户的报名记录
            all_enrollment = models.Enrollment.objects.filter(customer_id=customer_id, delete_status=False)
        return render(request, 'consultant/enrollment_list.html', {'all_enrollment': all_enrollment.order_by('-enrolled_date'), })


def enrollment_change(request, pk=None, customer_id=None):
    obj = models.Enrollment(customer_id=customer_id) if customer_id else models.Enrollment.objects.filter(pk=pk).first()
    form_obj = EnrollmentForm(instance=obj)
    if request.method == 'POST':
        form_obj = EnrollmentForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            return redirect(reverse('enrollment_list'))

    title = '编辑报名表' if pk else '新增报名表'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})
