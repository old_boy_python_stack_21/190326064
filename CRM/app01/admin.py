from django.contrib import admin
from app01 import models

admin.site.register(models.Customer)
admin.site.register(models.UserProfile)
admin.site.register(models.Department)
admin.site.register(models.Campus)
admin.site.register(models.ClassList)
admin.site.register(models.ConsultRecord)
admin.site.register(models.Enrollment)
admin.site.register(models.CourseRecord)
