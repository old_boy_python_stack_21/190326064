#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.请使用面向对象实现栈（后进先出）
"""
class Account:
    def __init__(self):
        self.lst = []

    def func(self, x):
        self.lst.append(x)

    def pop(self):
        self.lst.pop()


obj = Account()
obj.func('a')
print(obj.lst)
obj.func('b')
print(obj.lst)
obj.pop()
print(obj.lst)
obj.pop()
print(obj.lst)
"""

# 2.请使用面向对象实现队列（先进先出）
'''
class Account:
    def __init__(self):
        self.lst = []

    def func(self, x):
        self.lst.append(x)

    def pop(self):
        self.lst.pop(0)


obj = Account()
obj.func('a')
print(obj.lst)
obj.func('b')
print(obj.lst)
obj.pop()
print(obj.lst)
obj.pop()
print(obj.lst)
'''

# 3.如何实现一个可迭代对象？
'''
a = '12345'
b = a.__iter__()
print(b.__next__())
'''

# 4.看代码写结果
'''
class Foo(object):

    def __init__(self):
        self.name = '武沛齐'
        self.age = 100


obj = Foo()
setattr(Foo, 'email', 'wupeiqi@xx.com')

v1 = getattr(obj, 'email')
v2 = getattr(Foo, 'email')

print(v1, v2)
# wupeiqi@xx.com wupeiqi@xx.com
'''

# 5.请补充代码（提：循环的列表过程中如果删除列表元素，会影响后续的循环，推荐：可以尝试从后向前找）
'''
li = ['李杰', '女神', '李杰', '金鑫', '李杰', '李杰', '李杰', '李杰', '李杰', '武沛齐', '李杰', '李杰']

name = input('请输入要删除的姓氏：')  # 如输入“李”，则删除所有姓李的人。
# 方法一,正向删除:
"""
while 1:
    s = True
    for i in li:
        if i.startswith(name):
            s = li.remove(i)
    if s:
        break

print(li)
"""
# 方法二,反向删除:
"""
for i in li[::-1]:
    if i.startswith(name):
        li.remove(i)
print(li)
"""
'''

# 6.有如下字典，请删除指定数据。
'''
class User(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age


info = [User('武沛齐', 19), User('李杰', 73), User('景女神', 16)]

name = input('请输入要删除的用户姓名：')
# 请补充代码将指定用户对象再info列表中删除。
for i in info:
    obj = i
    if name == obj.name:
        info.remove(i)
for j in info:
    print(j.name)
'''

# 7.补充代码实现：校园管理系统。
'''
class User(object):
    def __init__(self, name, email, age):
        self.name = name
        self.email = email
        self.age = age

    def __str__(self):
        return self.name


class School(object):
    """学校"""

    def __init__(self):
        # 员工字典，格式为：{"销售部": [用户对象,用户对象,] }
        self.user_dict = {}

    def invite(self, department, user_object):
        """
        招聘，到用户信息之后，将用户按照指定结构添加到 user_dict结构中。
        :param department: 部门名称，字符串类型。
        :param user_object: 用户对象，包含用户信息。
        :return:
        """
        if not self.user_dict.get(department):
            self.user_dict[department] = [user_object, ]
            print('录入成功!')
            print(self.user_dict)
            return
        self.user_dict.get(department)
        self.user_dict[department].append(user_object)
        print('录入成功!')
        print(self.user_dict)
        return

    def dimission(self, username, department=None):
        """
        离职，讲用户在员工字典中移除。
        :param username: 员工姓名
        :param department: 部门名称，如果未指定部门名称，则遍历找到所有员工信息，并将在员工字典中移除。
        :return:
        """
        if not department:
            status = False
            for i in self.user_dict.values():
                for j in i[::-1]:
                    if username == j.name:
                        status = True
                        i.remove(j)
                        print('删除成功!')
                        print(self.user_dict)
                        return
            if not status:
                print('找不到该姓名!')
                return

        if not self.user_dict.get(department):
            print('该部门不存在,请重新输入!')
            return
        yuangong = self.user_dict[department]
        yuangong().remove(username)
        print('删除成功!')
        print(self.user_dict)

    def run(self):
        """
        主程序
        :return:
        """
        while 1:
            print('1.招聘 2.离职')
            a = input('请选择功能(N/n退出):')
            if a.upper() == 'N':
                return
            if not a.isdigit():
                print('请输入数字!')
                continue
            if a == '1':
                bumen = input('部门名称:')
                name = input('姓名:')
                email = input('邮箱:')
                age = input('年龄:')
                ss = User(name, email, age)
                self.invite(bumen, ss)
            elif a == '2':
                us = input('请输入员工姓名:')
                bumen2 = input('部门名称(有无皆可):')
                self.dimission(us, bumen2)
            else:
                print('输入有误,请重新输入!')


if __name__ == '__main__':
    obj = School()
    obj.run()
'''

# 8.请编写网站实现如下功能。
'''
需求：

实现 MIDDLEWARE_CLASSES 中的所有类，并约束每个类中必须有process方法。

用户访问时，使用importlib和反射 让 MIDDLEWARE_CLASSES 中的每个类对 login、logout、index 方法的返回值进行包装，最终让用户看到包装后的结果。
如：

用户访问 : http://127.0.0.1:8000/login/ ，
页面显示： 【csrf】【auth】【session】 登录 【session】 【auth】 【csrf】

用户访问 : http://127.0.0.1:8000/index/ ，
页面显示： 【csrf】【auth】【session】 登录 【session】 【auth】 【csrf】

即：每个类都是对view中返回返回值的内容进行包装。
'''

MIDDLEWARE_CLASSES = [
    'utils.session.SessionMiddleware',
    'utils.auth.AuthMiddleware',
    'utils.csrf.CrsfMiddleware',
]

from wsgiref.simple_server import make_server
import importlib


class yueshu:
    def process(self, a1):
        pass


class View(object):
    def login(self):
        return '登陆'

    def logout(self):
        return '等处'

    def index(self):
        return '首页'


def func(environ, start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
    obj = View()
    method_name = environ.get('PATH_INFO').strip('/')
    if not hasattr(obj, method_name):
        return ["123".encode("utf-8"), ]
    response = getattr(obj, method_name)()
    for path in MIDDLEWARE_CLASSES:
        module_path, class_name = path.rsplit('.', maxsplit=1)
        module_object = importlib.import_module(module_path)  # from utils import redis
        cls = getattr(module_object, class_name)
        obj = cls()
        response = obj.process(response)
    return [response.encode("utf-8")]


server = make_server('127.0.0.1', 8000, func)
server.serve_forever()
