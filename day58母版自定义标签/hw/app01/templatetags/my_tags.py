#!/usr/bin/env python
# -*- coding:utf-8 -*-

from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.simple_tag
def sqr_list(value):
    s = ''
    for i in range(1, value + 1):
        s += '<li><a href="#">{}的平方是{}</a></li>'.format(i, i * i)
    return mark_safe(s)


a = {'aa': [1, 2, 3, 4, 5]}

for i in a:
    print(i)
