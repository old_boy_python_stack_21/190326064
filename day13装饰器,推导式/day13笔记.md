# day13笔记

## 一.装饰器

### 1.定义

在不改变原函数内部代码的基础上，在函数执行之前和之后自动执行某个功能。

```python
def func(arg):
    def inner():
        arg()
    return inner

def f1():
    print(123)

v1 = func(f1)		# 调用func,f1给arg,返回inner给v1
v1()			# 调用inner,f1()输出123
```

```python
def func(arg):
    def inner():
        return arg()
    return inner

def f1():
    print(123)
    return 666

v1 = func(f1)	# 调用func,f1给arg,返回inner给v1
result = v1() # 执行inner,arg()=f1()输出123,返回666给inner的return,返回给result.
print(result) # 666
```

```python
def func(arg):
    def inner():
        v = arg()
        return v 
    return inner 

# 第一步：执行func函数并将下面的函数参数传递，相当于：func(index)
# 第二步：将func的返回值重新赋值给下面的函数名。 index = func(index)
@func 
def index():
    print(123)
    return 666

print(index)
```

```python
# 计算函数执行时间
import time

def wrapper(func):
    def inner():
        start_time = time.time()
        v = func()
        end_time = time.time()
        print(end_time-start_time)
        return v
    return inner

@wrapper
def func1():
    time.sleep(2)
    print(123)
@wrapper
def func2():
    time.sleep(1)
    print(123)

def func3():
    time.sleep(1.5)
    print(123)

func1()
```

### 2.目的

在不改变原函数的基础上,再函数执行前后自定义功能.

### 3.编写装饰器和应用

```python
def x(func):
    def y():
        # 前
        ret = func()
        # 后
        return ret 
   	return y 

# 装饰器的应用
@x
def index():
    return 10

@x
def manage():
    pass

# 执行函数，自动触发装饰器了
v = index()
print(v)
```

### 4.应用场景

想要为函数扩展功能时,可以选择用装饰器.

### 5.装饰器格式

```python
def 外层函数(参数): 
    def 内层函数(*args,**kwargs):
        return 参数(*args,**kwargs)
    return 内层函数
```

### 6.装饰器应用格式

```python
@外层函数
def index():
    pass

index()
```

## 二.推导式

### 1.列表推导式

目的:方便生成一个列表.

格式:

```python
v1 = [i for i in 可迭代对象 ]
v2 = [i for i in 可迭代对象 if 条件 ]
```

```python
# 面试题
def num():
    return [lambda x:i*x for i in range(4)]
# num() -> [函数,函数,函数,函数]
print([ m(2) for m in num() ]) # [6,6,6,6]
```

### 2.集合推导式

和列表推导式一致,()变为{}

```python
v1 = { i for i in 'alex' }
```

### 3.字典推导式

```python
v1 = { 'k'+str(i):i for i in range(10) }
```

## 三.其他

```python
import time
v = time.time()	# 距1970年到现在的秒数
print(v)
time.sleep(2)	# 等待两秒执行
```



