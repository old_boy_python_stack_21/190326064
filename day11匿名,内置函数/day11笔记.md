# day11笔记

## 一.函数高级

### 1.函数名作变量

内存地址赋值

```python
def func():
	print(123)
v1 = func	# func内存地址给v1
func()
v1()		#v1()也执行func()
```

数据类型中包含函数地址

```python
def func():
    print(123)			# 4.输出123,return默认返回None

func_list = [func, func, func]	# 3.列表中三项都指向func
# func_list[0]()
# func_list[1]()
# func_list[2]()
for item in func_list:
    v = item()			# 1.func_list中的值赋给v
    print(v)			# 2.输出
```

```python
def func():
    print(123)

def bar():
    print(666)

info = {'k1': func, 'k2': bar}

info['k1']()			# 1.调用func,输出123
info['k2']()			# 2.调用bar,输出666
```

### 2.函数作为参数传递

```python
def func(arg):
    print(arg)		# 2.输出1
    
func(1)				# 1.调用func
func([1,2,3,4])		# 3.调用func,输出[1,2,3,4]

def show():
    return 999
func(show)		# 4.调用func,传入show,输出show内存地址
```

```python
def func(arg):
    v1 = arg()		# 2.v1 = show(),调用show
    print(v1)		# 4.输出show()的返回值None
    
def show():
    print(666)		# 3.输出666
    
func(show)			# 1.调用func,传入show
```

```python
def func(arg):
    v1 = arg()			# 2.v1 = show(),输出666
    print(v1)			# 3.输出show()的返回值None
    
def show():
    print(666)
    
result = func(show)		# 1.func传入show	4.res=None
print(result)			# 5.输出func的返回值None
```

### 3.调用多个函数

```python
def func():
    print('花费查询')
def bar():
    print('语音沟通')
def base():
    print('xxx')
def show():
    print('xxx')
def test():
    print('xxx')
info = {
    'f1': func,
    'f2': bar,
    'f3':base,
    'f4':show,
    'f5':test
}
choice = input('请选择要选择功能：')
function_name = info.get(choice)	# 根据输入的键选择info中的值
if function_name:				# 存在则执行,不存在为None
    function_name()				# 执行:键()
else:
    print('输入错误')
```

## 二.lambda表达式

用于表示简单的函数。

```python
def func(a1,a2):
    return a1 + 100

func = lambda a1,a2: a1+100		#接收输入的值为a1和a2,:后面为return返回值
```

## 三.内置函数

### 1.自定义函数

### 2.内置函数

- 输入输出:

  print()	input()

- 数学运算:

  sum()	max()	min()	abs()	float()	divmod()

- 强制转换:

  int()	str()	list()	tuple()	dict()	set()	bool()

- 其他:

  len()	open()	range()	id()	type()

### 3.进制转换

- bin	二进制

  bin()	将十进制转化为二进制

- oct	八进制

  oct()	将十进制转化为八进制

- hex	十六进制

  hex()	将十进制转化为十六进制

- int	十进制

  int()	将其他进制转化为十进制
  
  
  