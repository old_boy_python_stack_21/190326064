#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.列举 str、list、dict、set 中的常用方法（每种至少5个），并标注是否有返回值。
'''
str:upper()大写   有
lower()小写   有
isdigit()判断是否是数字   有
replace()替换     有
strip()去空格  有
split()切割   有

list:append()追加     无
insert()插入      无
pop()删除     有
remove()删除      无
extend()延长      无

dict:keys()取所有键   有
values()取所有值      有
items()取所有键值对     有
update()更新      无
get()取键对应的值     有

set:add()添加     无
discard()删除    无
intersection()交集    有
union()合集    有
difference()差集    有
symmetric_difference()对称差集      有
'''

# 2.列举你了解的常见内置函数 【面试题】
'''
数学相关:
sum()求和     max()最大值      min()最小值      divmod()整除和余数    float()浮点   abs()绝对值
输入输出:
input()输入     print()输出
强制转换:
int()整型     bool()布尔值     list()列表    tuple()元组   set()集合     str()字符串
其他:
len()长度     range()范围      open()打开文件    id()查看内存地址     type()查看数据类型
'''

# 3.看代码分析结果
'''
def func(arg):
    return arg.replace('苍老师', '***')             # 4.返回"Alex的女朋友***和大家都是好朋友"给result

def run():                                          # 2.run()
    msg = "Alex的女朋友苍老师和大家都是好朋友"
    result = func(msg)                              # 3.调用func("Alex的女朋友苍老师和大家都是好朋友")
    print(result)                                   # 5.输出Alex的女朋友***和大家都是好朋友

run()                                               # 1.调用run()
'''

# 4.看代码分析结果
'''
def func(arg):
    return arg.replace('苍老师', '***')                # 3."Alex的女朋友***和大家都是好朋友"

def run():
    msg = "Alex的女朋友苍老师和大家都是好朋友"
    result = func(msg)                                 # 2.调用func("Alex的女朋友苍老师和大家都是好朋友")
    print(result)                                      # 4.输出Alex的女朋友苍老师和大家都是好朋友

data = run()                                           # 1.调用run()
print(data)                                            # 5.输出None
'''

# 5.看代码分析结果
'''
DATA_LIST = []

def func(arg):
    return DATA_LIST.insert(0, arg)     # 2.返回None

data = func('绕不死你')                 # 1.调用func()
print(data)                             # 3.None
print(DATA_LIST)                        # 4.DATA_LIST = ['绕不死你']
'''

# 6.看代码分析结果
'''
def func():
    print('你好呀')
    return '好你妹呀'

func_list = [func, func, func]      # 1.func为内存地址,没有调用

for item in func_list:              # 2.循环列表
    val = item()                    # 3.val = func()
    print(val)                      # 4.输出func的返回值

# 输出三次 你好呀\n好你妹呀
'''

# 7.看代码分析结果
'''
def func():
    print('你好呀')
    return '好你妹呀'


func_list = [func, func, func]      # 1.func为内存地址,没有调用

for i in range(len(func_list)):     # 2.循环range(3),i=0,1,2
    val = func_list[i]()            # 3.func_list[i]都执行func
    print(val)                      # 4.输出func()返回值

# 输出三次 你好呀\n好你妹呀
'''

# 8.看代码写结果
'''
tips = "啦啦啦啦"
def func():
    print(tips)
    return '好你妹呀'
func_list = [func, func, func]      # 1.func为内存地址,没有调用
tips = '你好不好'                    # 2.tips被重新赋值
for i in range(len(func_list)):     # 3.循环3次
    val = func_list[i]()            # 4.执行func()
    print(val)                      # 5.输出func()返回值

# 输出三次 你好不好\n好你妹呀
'''

# 9.看代码写结果
'''
def func():
    return '烧饼'           # 4.返回 烧饼
def bar():
    return '豆饼'           # 4.返回 豆饼
def base(a1, a2):
    return a1() + a2()      # 2.返回func() + bar()
result = base(func, bar)    # 1.调用base()    3.res=func() + bar()    5.res=烧饼豆饼
print(result)               # 6.输出 烧饼豆饼
'''

# 10.看代码写结果
'''
def func():
    return '烧饼'
def bar():
    return '豆饼'
def base(a1, a2):
    return a1 + a2
result = base(func(), bar())    # 1.调用base(),执行里面的func和bar
print(result)                   # 2.烧饼豆饼
'''

# 11.看代码写结果
'''
v1 = lambda :100
print(v1())         # 100

v2 = lambda vals: max(vals) + min(vals)
print(v2([11,22,33,44,55]))             # 66

v3 = lambda vals: '大' if max(vals)>5 else '小'
print(v3([1,2,3,4]))                            # 小
'''

# 12.看代码写结果
'''
def func():
    num = 10
    v4 = [lambda :num+10,lambda :num+100,lambda :num+100,]  # 2.三个函数
    for item in v4:                                         # 3.循环v4的三个函数
        print(item())                                       # 4.输出20,110,110
func()                                                      # 1.调用func
'''

# 13.看代码写结果
'''
for item in range(10):
    print(item) # 0-9

print(item)     # 9
'''

# 14.看代码写结果
'''
def func():
    for item in range(10):
        pass
    print(item)
func()              # 9
'''

# 15.看代码写结果
'''
item = '老男孩'
def func():
    item = 'alex'
    def inner():
        print(item)
    for item in range(10):          # 9
        pass
    inner()
func()
'''

# 16.看代码写结果【新浪微博面试题】
'''
def func():
    for num in range(10):                                       # num = 9
        pass
    v4 = [lambda :num+10,lambda :num+100,lambda :num+100,]
    result1 = v4[1]()                                           # res1=109
    result2 = v4[2]()                                           # res1=109
    print(result1,result2)
func()
'''

# 17.通过代码实现如下转换
# 二进制转换成十进制：
'''
v = '0b1111011'
print(int(v,base=2))
'''
# 十进制转换成二进制：
'''
v = 18
print(bin(v))
'''
# 八进制转换成十进制：
'''
v = '011'
print(int(v,base=8))
'''
# 十进制转换成八进制：
'''
v = 30
print(oct(v))
'''
# 十六进制转换成十进制：
'''
v = '0x12'
print(int(v,base=16))
'''
# 十进制转换成十六进制：
'''
v = 87
print(hex(v))
'''

# 18.请编写一个函数实现将IP地址转换成一个整数。【面试题】
# 如 10.3.9.12 转换规则为二进制：
#         10            00001010
#          3            00000011
#          9            00001001
#         12            00001100
# 再将以上二进制拼接起来计算十进制结果：00001010 00000011 00001001 00001100 = ？
'''
ip = '10.3.9.12'
ad_lst = ip.split('.')
s = ''
for i in ad_lst:
    s += bin(int(i))[2:].rjust(8,'0')
print(int(s,base=2))
'''

