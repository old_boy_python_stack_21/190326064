# day30笔记

## 1.回顾

- 黏包现象
  - 发生在发送端
    - 两个数据发送间隔短+数据长度小,tcp优化机制将两条信息作为一条发送出去,为了减少tcp协议中的'确认收到'的网络延迟时间
  - 发生在接收端
    - tcp协议中所传输的数据无边界,来不及接收的多条数据会在接收方的内核的缓存端粘在一起
  - 本质:接收信息的边界不清晰

- 解决黏包问题
  - 自定义协议1
    - 首先发送报头,长度4字节,内容是即将发送的报文的字节长度
    - struct模块pack能把所有的数字转换为4字节
    - 然后发送报文
  - 自定义协议2
    - 专门用来做文件发送的协议
    - 先发送报头字典的字节长度,再发送字典(包含文件名和大小),再发送文件内容
- tcp特点
  - 面向连接的,流式的,可靠的,慢的,全双工通信
  - 邮件  文件  http   web
- udp特点
  - 面向数据包的,无连接的,不可靠的,快的,能完成1对1,1对多,多对1,多对多的高效通讯协议
  - 即时聊天工具  视频的在线观看
- 三次握手
  - accept接受过程中等待客户端的连接
  - connect客户端发起一个syn链接请求
    - 如果收到server端响应ack的同时还会再收到一个由server端发来的syc链接请求
    - client端进行回复ack之后,就建立起了一个tcp协议的链接.
  - 三次握手的过程在代码中是由accept和connect共同完成的,具体的细节在socket中没有体现出来.
- 四次挥手
  - server和client端对应的在代码中都有close方法.
  - 每一端发起的close操作都是一次fin的断开请求,得到'断开确认ack'之后就可以结束一端的数据发送.
  - 如果两端都发起close,那么就是两次请求和两次回复,一共是四次操作
  - 可以结束 两端的数据发送,表示链接断开.

## 2.非阻塞io模型

- server端

```python
import socket

sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.setblocking(False)       # 设置非阻塞
sk.listen()

conn_l = []  # 已连接的列表
del_l = []  # 要断开的连接的列表
while True:
    try:
        conn, addr = sk.accept()  # 阻塞，直到有一个客户端来连我 , 由于非阻塞没有连接直接进入except
        print(conn)
        conn_l.append(conn)     # 把连接信息添加到列表
    except BlockingIOError:
        for c in conn_l:    # 循环已连接列表
            try:
                msg = c.recv(1024).decode('utf-8')      # 接收client发来的消息,没收到则进入except
                if not msg:     # 如果为None,表示client不再传信息
                    del_l.append(c)     # 把连接添加到要删除的列表
                    continue        # 继续接收
                print('-->', [msg])     # 如果收到则打印收到内容
                c.send(msg.upper().encode('utf-8'))     # 将收到内容变大写发送给client.
            except BlockingIOError:
                pass
        for c in del_l:     # 循环要删除的列表
            conn_l.remove(c)    # 从已连接的列表中删除
        del_l.clear()       # 清空要删除的列表
sk.close()
```

- client端

```python
import time
import socket

sk = socket.socket()
sk.connect(('127.0.0.1', 9000))
for i in range(30):
    sk.send(b'123')     # 发送30次
    msg = sk.recv(1024)     # 接收server信息
    print(msg)
    time.sleep(0.2)
sk.close()
```



## 3.验证客户端的合法性1

- server端

```python
import os
import hashlib
import socket


def get_md5(secret_key, randseq):
    md5 = hashlib.md5(secret_key)
    md5.update(randseq)     # 生成的随机序列加密md5
    res = md5.hexdigest()
    return res


def chat(conn):
    while True:
        msg = conn.recv(1024).decode('utf-8')
        print(msg)
        conn.send(msg.upper().encode('utf-8'))


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

secret_key = b'alexsb'      # 密钥 , 盐
while True:
    conn, addr = sk.accept()
    randseq = os.urandom(32)        # 生成随机32位字节
    conn.send(randseq)              # 发送给client
    md5code = get_md5(secret_key, randseq)      # 把密钥和随机序列传入md5加密函数
    ret = conn.recv(32).decode('utf-8')     # 接收client返回的md5
    print(ret)
    if ret == md5code:      # 判断与server端是否一致
        print('是合法的客户端')
        chat(conn)          # 一致则执行聊天函数
    else:
        print('不是合法的客户端')
        conn.close()        # 断开连接
sk.close()
```

- client端

```python
import hashlib
import socket
import time


def get_md5(secret_key, randseq):
    md5 = hashlib.md5(secret_key)
    md5.update(randseq)
    res = md5.hexdigest()
    return res


def chat(sk):
    while True:
        sk.send(b'hello')
        msg = sk.recv(1024).decode('utf-8')
        print(msg)
        time.sleep(0.5)


sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

secret_key = b'alexsb'
randseq = sk.recv(32)       # 接收server发来的随机序列
md5code = get_md5(secret_key, randseq)      # 加密为md5

sk.send(md5code.encode('utf-8'))    # 发送给server,一致则执行chat函数,不一致结束进程.
chat(sk)

sk.close()
```

## 4.验证客户端的合法性2

- server端

```python
import os
import hmac
import socket


def get_md5(secret_key, randseq):
    h = hmac.new(secret_key, randseq)   # hmac.new(盐,随机序列)
    res = h.digest()        # 返回加密的数据
    return res


def chat(conn):
    while True:
        msg = conn.recv(1024).decode('utf-8')
        print(msg)
        conn.send(msg.upper().encode('utf-8'))


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

secret_key = b'alexsb'      # 密钥, 盐
while True:
    conn, addr = sk.accept()
    randseq = os.urandom(32)    # 生成随机32位字节
    conn.send(randseq)      # 随机序列发给client
    hmaccode = get_md5(secret_key, randseq)     # 把随机序列加密计算
    ret = conn.recv(16)     # 接收client发来的数据
    if ret == hmaccode:     # 判断是否一致
        print('是合法的客户端')
        chat(conn)
    else:
        print('不是合法的客户端')
        conn.close()
sk.close()
```

- client端

```python
import socket
import time
import hmac


def get_md5(secret_key, randseq):
    h = hmac.new(secret_key, randseq)
    res = h.digest()
    return res


def chat(sk):
    while True:
        sk.send(b'hello')
        msg = sk.recv(1024).decode('utf-8')
        print(msg)
        time.sleep(0.5)


sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

secret_key = b'alexsb'
randseq = sk.recv(32)       # 接收32位随机序列
hmaccode = get_md5(secret_key, randseq)     # 加密

sk.send(hmaccode)       # 发送给server
chat(sk)        # 一致则调用聊天模块

sk.close()
```

## 5.socketserver模块

- server端

```python
import socketserver


class Myserver(socketserver.BaseRequestHandler):
    def handle(self):  # 自动触发了handle方法，并且self.request == conn
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('1'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('2'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('3'.encode('utf-8'))


server = socketserver.ThreadingTCPServer(('127.0.0.1', 9000), Myserver)
server.serve_forever()
```

- client端

```python
import socket
import time

sk = socket.socket()
sk.connect(('127.0.0.1', 9000))
for i in range(3):
    sk.send(b'hello,yuan')
    msg = sk.recv(1024)
    print(msg)
    time.sleep(3)

sk.close()
```



