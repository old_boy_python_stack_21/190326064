#!/usr/bin/env python
# -*- coding:utf-8 -*-
import socketserver


class Myserver(socketserver.BaseRequestHandler):
    def handle(self):  # 自动触发了handle方法，并且self.request == conn
        while 1:
            a = self.request.recv(1024)
            print(a)


server = socketserver.ThreadingTCPServer(('127.0.0.1', 9000), Myserver)
server.serve_forever()