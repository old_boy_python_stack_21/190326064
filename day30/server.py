#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import socket
import hashlib


def md5():
    md5 = hashlib.md5()
    md5.update()
    res = md5.hexdigest()
    return res


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

secret = b'alexsb'

conn, addr = sk.accept()
randstr = os.urandom(16)
conn.send(randstr)
