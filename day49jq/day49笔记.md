# day49笔记

网络link地址:<script src="http://code.jquery.com/jquery-latest.js"></script>

## 1.回顾

### 1.列出至少5个以上数组的常用方法，并说明含义

- push,pop,shift,unshift,concat

### 2.列出Math常用方法，并说明含义

- ceil向上取整,floor向下取整,random随机,round四舍五入

### 3.函数对象中，可以通过哪两个方法改变函数内部this的指向

```js
function fn(){
    console.log(this);//this指向了window
}
fn.call(obj);
fn.apply(obj)
```
### 4.javascript的基本数据类型和引用数据类型有哪些？

	基本数据类型：number,string,boolean,undefined,null
	引用数据类型：Array,Object,Function,Date
### 5.DOM

- 文档对象模型

### 6.获取节点对象三种方式

- 通过id  document.getElementById()
- 通过标签  .getElementsByTagName()
- 通过类名  .getElementsByClassName()

### 7.如何设置节点对象的样式,属性,类?

- 设置样式 obj.style
- 设置属性 obj.setAttribute(name,value);
  		obj.getAttribute(name);
    		obj.className
    		obj.title

### 8.节点对象的创建,添加.删除分别用什么方法?

- var op =  document.createElement('p');
- box.appendChild(op);
- box.insertBefore(newNode,oldNode)
- box.removeChild(op);

### 9.列出js中的事件

- onclick 鼠标单击事件
- onmouseover 鼠标经过事件
- onmouseout 鼠标移开事件
- onchange 文本框内容改变事件
- onsubmit
- onload 网页加载事件
- onfocus 光标聚焦事件
- onblur 光标失焦事件

### 10.定时方法有哪两个

- setTimeout(callback,毫秒) 一次性任务，延迟操作，异步
- setInterval(callback,毫秒) 周期性循环任务  动画 css  transtion tranform

### 11.设置值的操作

- innerText 只设置文本
- innerHTML 即设置文本，又渲染标签
- 针对与表单控件
  inputText.value = '123';

## 2.jquery

### 1.简介

```js
jQuery是一个快速，小巧，功能丰富的JavaScript库。它通过易于使用的API在大量浏览器中运行，使得HTML文档遍历和操作，事件处理，动画和Ajax变得更加简单。通过多功能性和可扩展性的结合，jQuery改变了数百万人编写JavaScript的方式。

操作: 获取节点元素对象，属性操作，样式操作，类名，节点的创建，删除，添加，替换
jquery核心：write less,do more
```

#### 1.1 jquery对象转换js对象

- $('button')[0]

#### 1.2 js对象转换jquery对象

- $(js对象)

### 2.jquery选择器

- 基础选择器

- 高级选择器

- 属性选择器

- 基本过滤选择器

  ```js
  :eq() 选择第一个 索引从0开始
  :first 获取第一个
  :last 获取最后一个
  :odd 获取奇数
  :even 获取偶数
  ```

- 过滤的方法

  ```js
  .eq() 选择一个 索引从0开始
  .children() 获取亲儿子
  .find() 获取的后代
  .parent() 获取父级对象
  .siblings() 获取除它之外的兄弟元素
  ```

### 3.动画

- 普通动画
  - show()  hide()  toggle()
- 卷帘门动画
  - slideDown()  slideUp()  slideToggle()
- 淡入淡出效果
  - fadeIn()  fadeOut()  fadeToggle()
- 自定义动画
  - .animate({params},speed,callback)

### 4.样式操作

- 通过调用.css()方法
- 如果传入一个参数，看一下这个参数如果是一个字符串表示获取值，如果是对象，表示设置多少属性值，如果是两个参数，设置单个属性值

### 5.类操作

- addClass()  removeClass() 
- toggleClass() 对设置或移除被选元素的一个或多个类进行切换。

### 6.对属性操作

- attr(name,value); 设置属性
- removeAttr(name);  删除属性

## 3.事件

```html
<form action="">
    <p class="name">
        <label for="username">用户名:</label>
        <input type="text" id="username" onfocus="message();" onblur="message2();">
        <!--onfocus光标聚焦时执行函数,onblur光标失焦时执行函数-->
    </p>
    <p class="pwd">
        <label for="pwd">密码:</label>
        <input type="password" id="pwd" placeholder="请输入密码">
        <!--placeholder输入之前显示内容-->
    </p>
    <input type="submit" value="注册">
</form>
```

## 4.bootstrap

- 简洁、直观、强悍的前端开发框架

- 导航栏,使用时copy修改即可.

```html
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">小猿圈</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container-fluid">
    呵呵呵呵呵
</div>
</body>
```

## 5.jQuery的使用

- window.jQuery=Window.$=jQuery
- $(内容) 把内容转换为jQuery对象, .css设置样式.
- 先导入:<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>

```js
<div id="box" class="box">
    <p class="active">mjj1</p>
	<p class="active">mjj2</p>
	<input type="text">
</div>
<!-- 1.导入模块 -->
<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

    // console.dir($);
    // 选择器 基础选择器
    console.log($('.box'));//jquery对象 伪数组
    // jquery对象转换js节点对象
    console.log($('#box')[0]);
    // js节点对象转换jq对象
    var box = document.getElementById('box');
    console.log($(box),box);
    console.log($('#box>p'));			
	console.log($('input[type=submit]'));

	$('#box .active').click(function(){
    // 样式操作
    console.log(this);
    // this.style
    // 设置单个样式属性
    // $(this).css('color','red');
    // $(this).css('fontSize','20px');
    // 设置多个属性
    $(this).css({
        'color':'red',
        "font-size":40
    })
    console.log($(this).css('color'));
})
</script>
```

## 6.基本过滤选择器+筛选选择器

- :eq(索引)  得到索引项
- :gt(a)  得到所有索引值大于a的项
- :lt(a)  得到所有索引值小于a的项
- :odd  得到序号为奇数的元素
- :even  得到序号为偶数的元素
- :first  得到第一个元素
- :last  得到最后一个元素

- .find('a')  选择所有a项,()里可以写后代选择器
- .children()  只选择直接子元素
- .siblings()  查找所有兄弟元素,不包括自己
- .parent()  查找直接父元素
- .eq(index)  查找指定元素的第index个元素

```js
<body>
    <ul>
        <li class="a">
            <a href="#">mjj</a>
        </li>
        <li class="b">wusir</li>
        <li class="c">alex</li>
        <li class="d">sb</li>
    </ul>
<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    /* 	console.log($('ul li:eq(1)'));
			console.log($('ul li:lt(1)'));
			console.log($('ul li:first'));
			console.log($('ul li:last')); */
    
    // 筛选方法
    // 即选择儿子又选择孙子....
    console.log($('ul').find('li a'));
// 只选择亲儿子
console.log($('ul').children());

console.log($('a').parent().parent().parent().parent().parent().parent());

//补充：获取文档,body和html
console.log(document);
console.log(document.body);
console.log(document.documentElement);
</script>
</body>
```

## 7.选项卡

- js实现

```html
<head>
	<meta charset="utf-8">
	<title></title>
	<style type="text/css">
		button.active{
			color: red;
		} /*选中时的属性*/
		p{
			display: none;
		} /*未选中时p标签隐藏*/
		p.active{
			display: block;
		} /*选中时p标签显示*/
	</style>
</head>
<body>
	<button class="active">热门</button>
	<button>电视影音</button>
	<button>电脑</button>
	<button>家具</button>
	<p class="active">热门</p>
	<p>电视影音</p>
	<p>电脑</p>
	<p>家具</p>
	<script type="text/javascript">
		// 1.获取标签
		var btns = document.getElementsByTagName('button');
		var ops = document.getElementsByTagName('p');

		//2.给每个标签绑定点击事件
		//方法1
		for(var i = 0;i < btns.length; i++){
			btns[i].index = i;
			btns[i].onclick = function (){
				for(var j = 0; j < btns.length; j++){
					btns[j].className = '';
					ops[j].className = '';
				}
				//改变button的样式
				this.className = 'active';
				//改变p标签的样式
				ops[this.index].className = 'active';
			}
		}
		//方法2  let声明的变量只在当前作用域有效
		// for(let i = 0;i < btns.length; i++){
		// 	btns[i].onclick = function (){
		// 		for(var j = 0; j < btns.length; j++){
		// 			btns[j].className = '';
		// 			ops[j].className = '';
		// 		}
		// 		//改变button的样式
		// 		this.className = 'active';
		// 		//改变p标签的样式
		// 		ops[i].className = 'active';
		// 	}
		// }
	</script>
</body>
```

- jQuery实现

```html
<script type="text/javascript">
    $('button').click(function(){
        // 链式编程
        //第二个按钮 索引1
        //console.log($(this).addClass('pppp'));//也会执行操作
        $(this).addClass('active').siblings('button').removeClass('active');
        // 获取当前点击的元素的索引
        // console.log($(this).index());
        $('p').eq($(this).index()).addClass('active').siblings('p').removeClass('active');
    })
</script>
```



## 8.动画

- show() 显示
- hide() 隐藏
- .text() 获取当前文本
- toggle() 切换,开关,一般与stop连用
- stop() 停止
- slideDown(时间) 卷帘门下降效果
- slideUp(时间) 卷帘门上升效果
- fadeIn(时间) 淡入
- fadeOut(时间) 淡出
- animate({a对象},时间,回调函数) 自定义动画,var a对象={自定义格式}

```html
<head>
    <meta charset="utf-8">
    <title></title>
    <style>
        #box {
            width: 200px;
            height: 200px;
            background-color: red;
            display: none;
        }
    </style>
</head>
<body>
    <button id="btn">显示</button>
    <div id="box"></div>
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $('#btn').click(function () {
            //第一种
            // if ($(this).text() === '显示') {
            //     $('#box').show(1000, function () {
            //         $('#btn').text('隐藏');//写在里面会执行完动画再修改文字
            //     });
            // } else {
            //     $('#box').hide(1000, function () {
            //         $('#btn').text('显示');
            //     });
            // }

            //第二种
            // $('#box').stop().toggle(2000);

            // $('#box').slideDown(2000);
            // $('#box').fadeIn(2000);
        })
    </script>
</body>
```

## 9.自定义动画

- animate 不支持背景色修改,需要插件

```html
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <style>
        div {
            position: absolute;
            left: 20px;
            top: 30px;
            width: 100px;
            height: 100px;
            background-color: green;
        }
    </style>
    <script src="js/jquery.js"></script>
    <script>
        jQuery(function () {
            $("button").click(function () {
                var json = {"width": 500, "height": 500, "left": 300, "top": 300, "border-radius": 100};
                var json2 = {
                    "width": 100,
                    "height": 100,
                    "left": 100,
                    "top": 100,
                    "border-radius": 100,
                    "background-color": "red"
                };
                //自定义动画
                $("div").animate(json, 1000, function () {
                    $("div").animate(json2, 1000, function () {
                        alert("动画执行完毕！");
                    });
                });

            })
        })
    </script>
</head>
<body>
<button>自定义动画</button>
<div></div>
</body>
```

