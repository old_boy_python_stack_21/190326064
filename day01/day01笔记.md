# day01笔记

## 1.计算机基础

### 1.操作系统

Windows:xp/7/8/10

Linux:Ubuntu/CentOS/RedHat

Unix:MacOS

### 2.安装解释器

1.官网下载解释器

2.安装

3.添加环境变量:

​	此电脑,高级系统设置,高级,环境变量

### 3.编码

1.ASCII:英文,8bit,1byte

2.Unicode:万国码,32bit,4byte,浪费资源

3.UTF-8:简化万国码,英文8bit,欧洲16bit,中文24bit

## 2.数据类型

1.字符串:str,' '," ",''' '''

2.整形:int

3.布尔值:bool

## 3.变量命名规范

1.由数字,字母,下划线组成.

2.不能由数字开头.

3.不能是关键字.

## 4.输入

xxx = input('xxx')

得到的内容永远是str

Python2: raw_input()

Python3: input()

## 5.注释

1.单行注释:#

2.多行注释:""" """/''' '''

## 6.条件判断

if 条件1:

​	代码块1

elif 条件2:

​	代码块2

...

else:

​	代码块n