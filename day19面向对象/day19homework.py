#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.简述编写类和执行类中方法的流程。
"""
编写:
class Person:
    def __init__(self, send):
        self.name = name
    def name(self):
        pass
    def age(self):
        pass

执行:
a = Person()
a.name()
a.age()
"""

# 2.简述面向对象三大特性?
'''
封装 / 继承 / 多态
'''

# 3.将以下函数改成类的方式并调用 :
'''
def func(a1):   
    print(a1) 
'''

'''
class Fun:
    def func(self,a1):
        print(a1)


a = Fun()
a.func(1)
'''

# 4.面向对象中的self指的是什么?
'''
指调用该函数的对象
'''

# 5.以下代码体现面向对象的什么特点?
"""
class Person:
    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender


obj = Person('武沛齐', 18, '男')
# 封装
"""

# 6.以下代码体现面向对象的什么特点?
'''
class Message:
    def email(self):
        """
        发送邮件
        :return:
        """
        pass

    def msg(self):
        """
        发送短信
        :return:
        """
        pass

    def wechat(self):
        """
        发送微信
        :return:
        """
        pass

# 封装
'''

# 7.看代码写结果
'''
class Foo:
    def func(self):
        print('foo.func')


obj = Foo()
result = obj.func()
print(result)
# foo.func  None
'''

# 8.定义个类，其中有计算周长和面积的方法(圆的半径通过参数传递到初始化方法)。
'''
class Func:
    def __init__(self, r):
        self.r = r

    def zhouchang(self):
        return 2*3.14*self.r

    def mianji(self):
        return 3.14*self.r**2


a = Func(1)
print(a.zhouchang(), a.mianji())
'''

# 9.面向对象中为什么要有继承?
'''
把相同的属性和方法归到基类中,不需要再次编写相同的代码.
'''

# 10.Python继承时，查找成员的顺序遵循什么规则?
'''
从左到右查找.
'''

# 11.看代码写结果
'''
class Base1:
    def f1(self):
        print('base1.f1')

    def f2(self):
        print('base1.f2')

    def f3(self):
        print('base1.f3')
        self.f1()


class Base2:
    def f1(self):
        print('base2.f1')


class Foo(Base1, Base2):
    def f0(self):
        print('foo.f0')
        self.f3()


obj = Foo()
obj.f0()
# foo.f0    base1.f3    base1.f1
'''

# 12.看代码写结果:
'''
class Base:
    def f1(self):
        print('base.f1')

    def f3(self):
        self.f1()
        print('base.f3')


class Foo(Base):
    def f1(self):
        print('foo.f1')

    def f2(self):
        print('foo.f2')
        self.f3()


obj = Foo()
obj.f2()
# foo.f2    foo.f1  base.f3
'''

# 13.补充代码实现
"""
 需求
1. while循环提示用户输入:用户名、密码、邮箱(判断是否有@)
2. 为每个用户创建 个对象，并添加到列表中。
3. 当列表中的添加 3个对象后，跳出循环并以此循环打印所有用户的姓名和邮箱
"""

"""
class Func:
    def __init__(self, user, pwd, email):
        self.user = user
        self.pwd = pwd
        self.email = email
        s = '用户:%s,邮箱:%s' % (self.user, self.email)


user_list = []
while True:
    user = input("请输入用户名:")
    pwd = input("请输入密码:")
    email = input("请输入邮箱:")
    if '@' not in email:
        print('请输入正确的邮箱!')
        continue
    a = Func(user, pwd, email)
    user_list.append(a)
    if len(user_list) == 3:
        break

for i in user_list:
    print(i.user, i.email)
"""


# 14.补充代码:实现用户注册和登录。


class User:
    def __init__(self, name, pwd):
        self.name = name
        self.pwd = pwd


class Account:
    def __init__(self):
        # 用户列表，数据格式：[user对象，user对象，user对象]
        self.user_list = []

    def login(self):
        """
        用户登录，输入用户名和密码然后去self.user_list中校验用户合法性
        :return:
        """
        while 1:
            print('***登陆***')
            username = input('请输入帐号:')
            password = input('请输入密码:')
            status = False
            for i in range(len(self.user_list)):
                if username == self.user_list[i].name and password == self.user_list[i].pwd:
                    status = True
                    break
            if status:
                print('登陆成功!')
                return
            print('用户名或密码错误!请重新输入!')

    def register(self):
        """
        用户注册，没注册一个用户就创建一个user对象，然后添加到self.user_list中，表示注册成功。
        :return:
        """
        print('***注册***')
        username = input('请输入账号:')
        pwd = input('请输入密码:')
        user = User(username, pwd)
        self.user_list.append(user)
        print('注册成功!')

    def run(self):
        """
        主程序
        :return:
        """
        while 1:
            dic = {'1': self.register, '2': self.login}
            print('''
1.注册
2.登陆''')
            a = input('请选择(N/n退出):')
            if a.upper() == 'N':
                return
            if dic.get(a) == None:
                print('输入有误!请重新输入!')
                continue
            dic.get(a)()


if __name__ == '__main__':
    obj = Account()
    obj.run()
