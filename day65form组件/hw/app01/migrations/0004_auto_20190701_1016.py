# -*- coding: utf-8 -*-
# Generated by Django 1.11.21 on 2019-07-01 02:16
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0003_auto_20190701_1013'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='author',
            name='books',
        ),
        migrations.RemoveField(
            model_name='authorbook',
            name='author',
        ),
        migrations.RemoveField(
            model_name='authorbook',
            name='book',
        ),
        migrations.DeleteModel(
            name='Author',
        ),
        migrations.DeleteModel(
            name='AuthorBook',
        ),
        migrations.DeleteModel(
            name='Book',
        ),
    ]
