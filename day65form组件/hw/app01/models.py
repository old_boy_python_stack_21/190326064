from django.db import models


class User(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=16)
    pwd = models.CharField(max_length=16)
    phone = models.BigIntegerField()

