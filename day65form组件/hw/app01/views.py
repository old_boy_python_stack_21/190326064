from app01 import models
from django import forms
from django.core.validators import RegexValidator, ValidationError
from django.shortcuts import render, redirect, HttpResponse


class User(forms.Form):
    username = forms.CharField(
        max_length=16,
        min_length=8,
        label='请输入用户名',
        required=True,
        error_messages={
            'required': '此项为必填项',
            'min_length': '用户名长度不能小于8位',
            'max_length': '用户名长度不能大于16位',
        }
    )
    pwd = forms.CharField(
        label='请输入密码',
        widget=forms.PasswordInput,
        error_messages={
            'required': '此项为必填项', })
    re_pwd = forms.CharField(
        label='确认密码',
        widget=forms.PasswordInput,
        error_messages={
            'required': '此项为必填项', })
    phone = forms.CharField(
        validators=[RegexValidator(r'^1[3-9]\d{9}$', '手机号格式不正确')],
        error_messages={
            'required': '此项为必填项', }
    )

    def clean_username(self):
        v = self.cleaned_data.get('username')
        if 'sb' in v:
            raise ValidationError('不符合社会主义核心价值观')
        else:
            return v

    def clean(self):
        pwd = self.cleaned_data.get('pwd')
        re_pwd = self.cleaned_data.get('re_pwd')
        if pwd == re_pwd:
            return self.cleaned_data
        else:
            self.add_error('re_pwd', '两次密码不一致!')
            raise ValidationError('两次密码不一致')

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })


def reg(request):
    form_obj = User()
    if request.method == 'POST':
        form_obj = User(request.POST)
        if form_obj.is_valid():
            print(form_obj.cleaned_data)  # 通过校验的数据
            return HttpResponse('注册成功')
    return render(request, 'reg.html', {'form_obj': form_obj})
