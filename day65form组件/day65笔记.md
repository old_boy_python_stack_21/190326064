# day65笔记

## 1.回顾

### 1.json

- 一种数据交换的格式
- python
  - 支持的数据类型：字符串、数字、列表、字典、布尔值、None
  - 转化
    - 序列化   josn.dumps(python的数据)      josn.dump(python的数据,f)    
    - 反序列化   josn.loads(json的字符串)      josn.load(json的字符串,f)
- js
  - 支持的数据类型：字符串、数字、数组、对象、布尔值、null
  - 转化
    - 序列化：JSON.stringfy(js的数据类型)
    - 反序列化：JSON.parse(json的字符串)

```
from django.http.response import JsonResponse
```

### 2.发请求的方式

- 1.在地址栏种输入地址   GET
- 2.a标签    GET
- 3.form表单   GET/POST

```
1. method 请求方式,action 地址
2. 标签要有name属性   有的需要有value
3. 要有一个input  type=‘submit’  或者button
```

- 4.ajax
  - 使用js发送请求。

  - 特点：异步  局部刷新 传输的数据量小

  - 使用jq发ajax 

    - 导入jq

    ```javascript
    $.ajax({
    	url：发送的地址，
    	type:请求方式，
    	data:{数据},   
    	successs: funcation(res){
        	res  ——>响应体
    	}
    })
    ```

### 3.上传文件

```javascript
var formobj = new FormData()
formobj.append(key, $("#f1")[0].files[0] )
$.ajax({
		url：发送的地址，
		type:请求方式，
		processData:false;    // 不处理编码方式
		contentType:false;    //  不处理请求头
		data: formobj ,   
		successs: funcation(res){    res  ——>响应体
		}
})
```

### 4.ajax通过django的csrf校验 (三种方法)

- 1.给data添加csrfmiddlewaretoken的键值对

```html
dada:{
	'csrfmiddlewaretoken':隐藏的input的值 或者 cookie中csrftoken的值
}
```

- 2.添加请求头

```html
hearders:{
	'x-csrftoken':  隐藏的input的值   cookie中csrftoken的值
}
```

- 3.文件导入
  - 见day64



## 2.今日内容

博客:https://www.cnblogs.com/maple-shaw/articles/9537309.html

### 1.from组件

#### form组件的功能

1. 生产input标签
2. 对提交的数据可以进行校验
3. 提供错误提示

#### 定义form组件

- views

```python
from django import forms

class RegForm(forms.Form):
    username = forms.CharField() 
    pwd = forms.CharField() 
    # 默认生成的表单为=前面定义的username/pwd,需要改变时(label='内容')
    # 密码使用密文(widget=forms.PasswoedInput)
```

#### 使用

- 视图views

```python
def reg(request):
    form_obj = RegForm() # 实例化定义的类名
    # form_obj = RegForm(request.POST) # 接收提交的数据
    # form_obj.is_valid():  # 对数据进行校验
    # print(form_obj.cleaned_data)  # 通过校验的数据

	return render(request, 'reg2.html', {'form_obj': form_obj})
```

- 模板templates

```Django
{{ form_obj.as_p }}    ——》   生成一个个P标签  input  label
{{ form_obj.errors }}    ——》   form表单中所有字段的错误
{{ form_obj.username }}     ——》 一个字段对应的input框
{{ form_obj.username.label }}    ——》  该字段的中午提示
{{ form_obj.username.id_for_label }}    ——》 该字段input框的id
{{ form_obj.username.errors }}   ——》 该字段的所有的错误
{{ form_obj.username.errors.0 }}   ——》 该字段的第一个错误的错误
{#form标签中加上novalidate不使用默认提示#}
```

#### 常用字段

- 都要继承Field

```
CharField    字节
IntegerField	整数
FloatField	浮点
DecimalField	小数
ChoiceField(choice=((1, '男'), (2, '女')))	单选,默认下拉选择
MultipleChoiceField		多选
```

#### 字段参数

```python
required=True,              # 是否允许为空
widget=None,                # HTML插件
	widget=forms.widgets.PasswordInput	# 密文
	widget=forms.RadioSelect()	# 单选设置radio格式
label=None,                 # 用于生成Label标签或显示内容
initial=None,               # input框内初始值
error_messages={'required': '不能为空', 'invalid': '格式错误'},        # 提示错误信息,默认None
validators=[],              # 自定义验证规则,把当前参数传给[]内验证
disabled=False,             # 是否可以编辑
```

#### 验证

##### 1.内置校验

```
required=True	必填参数
min_length	最短长度
max_length	最长长度
```

##### 2.自定义校验器

1. 写函数

```python
from django.core.exceptions import ValidationError

def checkname(value):
    # 通过校验规则 不做任何操作
    # 不通过校验规则   抛出异常
    if 'alex' in value:
        raise ValidationError('不符合社会主义核心价值观')

# 需要校验的字段中写入如下
validators=[checkname]
```

2.使用内置的校验器

```python
from django.core.validators import RegexValidator

# 需要校验的字段中写入如下
validators=[RegexValidator(r'正则','错误提示')]
```

##### 3.钩子函数：

- 写了就会执行

1. 局部钩子

   对当前字段校验

   ```python
   def clean_字段名(self):
       # 局部钩子
       # 通过校验规则  必须返回当前字段的值
       # 不通过校验规则   抛出异常
       raise ValidationError('不符合社会主义核心价值观')
   ```

2. 全局钩子

   对所有字段校验

   ```python
   def clean(self):
       # 全局钩子
       # 通过校验规则  必须返回所有字段的值
       # 不通过校验规则   抛出异常   '__all__'
       pass
   ```

#### is_valid的流程：

- 1.执行full_clean()的方法：
  - 定义错误字典
  - 定义存放清洗过数据的字典
- 2.执行_clean_fields方法：
  - 循环所有的字段
  - 获取当前的值
  - 对值进行校验 （ 内置校验规则   自定义校验器）
  - 1.通过校验
    - self.cleaned_data[name] = value 
    - 如果有局部钩子，执行进行校验：
    - 通过校验重新赋值——》 self.cleaned_data[name] = value 
    - 不通过校验——》     self._errors  添加当前字段的错误 并且 self.cleaned_data中当前字段的值删除掉
  - 2.没有通过校验
    - self._errors  添加当前字段的错误
- 3.执行全局钩子clean方法





