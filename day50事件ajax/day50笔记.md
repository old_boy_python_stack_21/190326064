# day50笔记

## 1.回顾

### 1.jQuery对象和JS节点对象如何转换？

```js
jq -> js
	$('a')[0]
js -> jq
	$(js节点对象)
```

### 2.jQuery的基础选择器和高级选择器有哪些?

```html
<div id='box' class='box'>
    <p>
        mjj
    </p>
</div>
```

```
$('#box') 
$('.box')
$('div')
$('div p')
$('div>p')
$('div,p')
$('div.box')
```

### 3.请使用属性选择器选中元素

```html
<input type='submit' value='提交'>

$('input[type=submit]')
```

### 4.说明以下筛选选择器的含义

```js
$('ul li:eq(1)'); 选中第二个元素，索引从0开始
$('ul li:first'); 选中第一个元素
$('ul li:last'); 选中最后一个元素
$('ul li:odd'); 选奇数
$('ul li:even'); 选偶数
```

### 5.说明以下筛选方法的含义

```js
$('ul li').eq(0) 选中第一个元素，索引从0开始
$('ul li').find('p') 选中所有的后代p
$('ul li').chilren() 选中亲儿子
$('ul li').parent() 选中亲爹
$('ul li').siblings() 选中兄弟，除它之外，通常用制作网页的选项卡
```

#### 6.设置样式使用JQ的哪个方法？并分别说明传入到方法参数不同，它的作用是什么？

```js
css('color');//获取属性
css({'color'：'red'});既可以设置一个属性，又可以设置多个属性
css('color','yellow');设置一个属性
```

#### 7.给元素设置类名和移除类名用哪个方法？

```js
addClass('active abc rtt');//添加类
removeClass('active abc');//移除类
toggleClass('active') //开关式的切换类名
```

#### 8.说明以下JQ的动画方法的含义？

```js
show(2000，callback);//普通动画显示
hide();//普通动画隐藏
toggle();//普通动画切换
slieDown() //卷帘门效果
slideUp()
slideToggle()
fadeIn() //淡入淡出效果
fadeOut()
fadeToggle()
animate({样式属性}，2000，callback);//自定义动画效果  非常重要
stop();//停止
```

#### 9.获取当前元素的索引用哪个方法？

```js
$(this).index()
```

#### 10.对值的操作用哪些方法？

```js
text('hello world');//给元素赋值文本
text();//获取当前的文本
html('<p>hello world</p>');//传入标签或文本
html();//即获取标签又获取文本
val();//只针对于表单控件
```



## 2.对属性的操作

- $(function(){内容})  文档加载完之后调用
- attr()  修改标签中属性
  - 获取值:attr('属性');
  - 添加单个值:attr('属性','值');
  - 添加多个值:attr({'属性':值,'属性':值});

```html
console.log($('p').attr('id'));
$('p').attr('title','mjj');
$('p').attr({'a':1,'b':2});

<p id="box">mjj</p>
```

- removeAttr()  移除属性,多个之间加空格
  - removeAttr('属性 属性');

```js
$('p').removeAttr('title id a');
```

- prop()  修改当前节点对象属性 单选操作

```js
console.log($('input[type=radio]').eq(0).prop('checked'));
```

- removeProp() 移除节点对象属性



## 3.对文档的操作

- append / appendTo 后置追加
  - 父.append(子)
    - 子可以是内容/标签/js节点对象
    - jq对象则会移动到最后
  - 子.appendTo(父)
    - 内容同append
    - 后面可以直接.css('格式')

```js
$('#box').append('mjj');
$('内容').appendTo('#box').css('color','yellow');
```

- prepend() / prependTo()  前置追加
  - 父.prepend(子)
  - 子.prependTo(父)
  - 内容同append

```js
$('#box').prepend('<p>hello world</p>');
$('内容').prependTo('#box')
```

- before / insertbefore 之前插入元素
  - 兄弟元素.before(要插入的兄弟元素);
  - 要插入的兄弟元素.insertBefore(兄弟元素);

```js
$('h2').before('mjj2');
$('<h3>女神</h3>').insertBefore('h2');
```

- after / insertAfter  之后插入元素
  - 同before
- appendTo / prependTo / insertBefore子元素要求标签,因为是新建内容
- replaceWith / replaceAll  替换
  - $('被替换').replaceWith('替换成的内容');

```js
$('h3').replaceWith('000000');
```

- remove()  既删除节点，也删除事件
  - 事件执行一次就删除

```js
$('#btn').click(function(){
    alert(1);
    $(this).remove(); //执行后删除
    console.log($(this).remove()); //返回删除的对象
    $('#box').after($(this).remove()); //在原内容的位置添加删除的元素,但不能执行
})
```

- detach()  仅删除节点,不删除事件

```js
$('#btn').click(function(){
    alert(1);
    $(this).detach();
    $('#box').after($(this).detach());
})

html(' ')
```

- empty() / html('')  清空元素

```js
$('#box').html(' ');
$('#box').empty();
```



## 4.事件

### 4.1鼠标事件

- mouseover 鼠标移入(穿过父级和子级元素都会调用)
- mouseout 鼠标移出
- mouseenter 鼠标移入(穿过父级和子级不会调用)
- mouseleave 鼠标移出
- focus 鼠标聚焦
- blur 鼠标失焦

```js
默认聚焦
<input type="text">
$('input[type=text]').focus();
```

- keydown 键盘按下时的操作
  - keyCode键盘按键编码
  - 回车13  删除8

```js
$('input[type=text]').keydown(function(e){
    console.log(e.keyCode);
    switch (e.keyCode){
        case 8://按下的keycode=8时清空内容
            $(this).val(' ')
            break;
        default:
            break;}
});
```

### 4.2表单提交事件

- 默认事件提交会刷新网页
- 阻止默认事件方法

```html
html中:
<a href="javascript:void(0);">百度一下</a>
简写:
<a href="javascript:;">百度一下</a>

jq中:
e.preventDefault();
```



## 5.ajax

- 执行一个异步的http请求,用于前后端交互

```js
$.ajax({
    url:'https://api.apeland.cn/api/banner/', //地址
    methods:'get', //方法
    success:function(res){ //成功执行操作
        console.log(res);
        if(res.code === 0){
            var cover = res.data[0].cover;
            var name = res.data[0].name;
            console.log(cover);
            $('#box').append(`<img src=${cover} alt=${name}>`)
        }
    },
    error:function(err){ //失败执行操作
        console.log(err);
    }
})
```



## 6.jQuery插件

- www.jq22.com

## 7.video,mp3

```html
<audio src="路径.mp3" controls=""></audio>
```


