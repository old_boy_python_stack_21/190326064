# day46笔记

## 1.回顾

### 1.浮动的现象

- 脱标,贴边,收缩,文字环绕
- 浮动带来的问题:不计算浮动元素高度,撑不起父盒子的高度

### 2.清除浮动问题的方式

- 1.给父盒子添加固定高度

- 2.内墙法

  - 给最后一个浮动元素添加一个空的块级标签，设置该标签的属性为clear:both;

- 3.伪元素清除

  - 给父元素添加类

    ```css
    .clearfix::after{
     	content:'',
     	display:block;
     	clear:both
     }
    ```

- 4.overflow:hidden   BFC区域

### 3.overflow:hidden和overflow:scroll属性的作用？

- hidden 超出部分隐藏
- scroll 滚动条

### 4.定位有哪几种？

- position : static静态 relative相对 absolute绝对 fixed固定

### 5.相对定位的元素有哪些特征？它的参考点是谁？

- 1.给一个标准文档流下的盒子单纯的设置相对定位，与普通的盒子没有任何区别
- 2.top|bottom|left|right
- 以原来的位置为参考点,应用：1.微调元素 2.做“子绝父相”

### 6.绝对定位的元素由哪些特征？它的参考点？

- 脱标,压盖
- 参考点：是否有定位（相对定位，绝对定位，固定定位）的祖先盒子进行定位，如果没有定位的祖先盒子，以body为参考点. 重要： “子绝父相”


### 7.阐述一下，“子绝父相”布局方案的好处

- 要浮动一起浮动，有浮动清除浮动，浮动带来的好处：实现元素并排



## 2.固定定位

- 1.脱标
- 2.固定不变
- 3.提高层级
- 参考点 : 浏览器左上角

```css
position: fixed;
top: 0;
left: 0;
```

## 3.z-index

- 只适用于定位的元素，z-index:auto;
- 取值为整数，数值越大，它的层级越高 , 显示在上层
- 如果元素设置了定位，没有设置z-index，那么谁写在最后面的，表示谁的层级越高。(与标签的结构有关系)
- 从父现象。通常布局方案我们采用子绝父相，比较的是父元素的z-index值，哪个父元素的z-index值越大，表示子元素的层级越高。

## 4.background背景

- 背景图backgroud-image: url('地址');
- 重复backgroud-repeat: no-repeat;
- 位置backgroud-position: px px;
  - 可以设置center center居中

## 5.圆角

- 圆角 border-radius:半径px;
- 50%为圆形,大于50也是圆形.

## 6.阴影

- box-shadow: 水平距离 垂直距离 模糊程度 阴影颜色 inset(内发光,默认外发光);
- 过渡 transition:all 时间 ;

## 7.行内和块的水平和垂直居中

- 行
  - p标签 : line-height=height ; text-align:center;
  - table标签 : 默认垂直居中,text-align: center;
  - 把display转为table-cell; vertical-align:middle ; text-align:center;

- 块

  - 子绝父相 , 子代设置

  ```css
  maigin:auto;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  ```

  - display:table-cell
    - 父代display:table-cell; vertical-align:middle ; text-align:center;
    - 子代设宽高则display:inline-block , 不设则行内
  - position - 随父盒子改变居中
    - 子绝父相
    - 子代设置 left:50%(父盒子的50%); top:50%(父盒子的50%); margin-left:-50px(回挪子盒子宽的一半); maigin-top:-50px(回挪子盒子高的一半)

## 8.大背景图的设置

- background: url("图片") no-repeat 左右位置 上下位置;

```
width: 100%;
height: px;
background: url("1.png") no-repeat center top;
```









