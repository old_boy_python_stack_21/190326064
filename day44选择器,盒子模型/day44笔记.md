# day44笔记

## 1.回顾

### 1.div和span标签在网页中的作用

- div:将网站分割成独立的逻辑区域  division 分割
- span: 小区域标签,在不影响文本正常显示的情况下，单独设置对应的样式

### 2.css基础选择器和高级选择器有哪些？

选择器的作用：选中标签

- 基础选择器
  - id选择器 特定属性的元素（标签）
  - 类选择器 class = 'active'   .active 类是可以重复，并且可以设置多个
  - 标签选择器

- 高级选择器
  - 后代    子子孙孙
  - 子代   只包括儿子
  - 组合  html,body,div,p,ul....
  - 交集 span.active

### 3.盒子模型的属性有哪些？并说明属性的含义，画出盒子模型图

```html
width:内容的宽度
height:内容的高度
border:边框
padding:内边距
margin: 外边距
```

### 4.如何让文本垂直和水平居中？

- 让行高等于盒模型的高度实现垂直居中
- 使用text-align:center；实现文本水平居中

### 5.如何清除a标签的下划线？

- text-decoration: none;

  - none;无线
  - underline:下划线
  - overline:上划线
  - line-through:删除线

### 6.如何重置网页样式

- reset.css

```css
html,body,p,ul,ol{
    margin: 0;
    padding: 0;
}
/*通配符选择器 */
*{
    margin: 0;
    padding: 0;
}
a{
    text-decoration: none;
}
```

### 7.如何清除input和textarea标签的默认边框和外线

```css
input,textarea{
    border: none;
    outline: none;
}
```

### 8.在css中哪些属性是可以继承下来的

```
color,text-xxx,font-xxx,line-height,letter-spacing,word-spacing
```

### 9.如何正确比较css中的权重

- 如果选中了标签 : 数选择器的数量  id  class 标签 谁大优先级越高 如果一样大，后面优先级越大
- 如果没有选中标签 : 当前属性是被继承下来，他们的权重为0，与选中的标签没有可比性
- 都是继承来的，谁描述的近，就显示谁的属性（就近（选中的标签越近）原则），如果描述的一样近，继续数选择器的数量。
- !important 它设置当前属性的权重为无限大，大不过行内样式的优先级

### 10.块级标签和行内标签

- 块级标签

  - 1.可以设置高度,如果不设置宽度,默认是父标签的100%的宽度.
  - 2.独占一行

  ```html
  p,div,ul,ol,li,h1~h6,table,tr,form
  ```

- 行内标签

  - 1.不可以设置宽高
  - 2.在一行内显示

  ```html
  a,span,b,strong,em,i
  ```

- 行内块标签

  - 1.可以设置宽高
  - 2.在一行内显示

  ```html
  input,img
  ```

  

## 2.今日内容

### 1.伪类选择器

- 对于a标签,如果想设置a标签的样式,要作用于a标签上,对于继承性来说,a标签不起作用.

- 爱恨准则LoVe HAte

  ```html
  a:link{}       没有被访问时的属性
  a:visited{}    被访问过的属性
  a:hover{}      指向标签时的属性
  a:active{}     点住时的属性
  ```


### 2.属性选择器

- input[type=输入的类型]
- text文本  password密码  radio单选  checkbox多选  file选择文件

```css
input[type='text']{
    background-color: red;
}
input[type='checkbox']{
}
input[type='submit']{
}
```

### 3.伪元素选择器

- p:first-letter{设置第一个字格式}
- p:before{content:'';}  在p前面加内容
- p:after{content:'';}  在p后面加内容

```css
p::first-letter{
    color: red;
    font-size: 20px;
    font-weight: bold;
}
p::before{
    content:'@';
}
/*解决浮动布局常用的一种方法*/
p::after{
    /*通过伪元素添加的内容为行内元素*/
    content:'$';
}
```

### 4.常用格式化排版

#### 1.字体属性

- 字体

  - body{font-family:'字体1,字体2...';} 从字体中从左往右找

- 字体大小

  - font-size:12px
  - px像素  绝对单位
  - em  相对当前盒子模型的font-size单位
  - rem  相对根元素(html)比例

  ```css
  # em
  .box{
      font-size: 20px;
      /*1em = 20px*/
      width: 20em;
      height: 20em;
      background-color: red;
  }
  # rem
  html{
      font-size: 40px;
  }
  .box{
      /*1rem=40px  1px=0.025rem */
      font-size: 12px;
      width: 20rem;
      height: 20rem;
      background-color: red;
  }
  ```

- 字体样式

  - normal 默认字体
  - italic  斜体

  ```css
  font-style: italic;
  ```

- 字体粗细

  - 400  普通
  - 700  加粗

  ```css
  font-weight: 700;
  ```

#### 2.文本属性

- 文本修饰

  - none  无修饰
  - underline  下划线
  - overline  上划线
  - line-through  删除线

  ```css
  text-decoration: underline;
  ```

- 文本缩进

  ```css
  text-indent:(px,e,rem);
  ```

- 间距

  - letter-spacing  汉字间距
  - word-spacing  单词间距

  ```css
  letter-spacing:(px,e,rem);
  word-spacing:(px,e,rem);
  ```

### 5.盒子模型

margin:在水平方向上不会出现问题，垂直方向上会出现塌陷问题

#### border 边框

- 三要素:粗细 线性样式 颜色
- 4个参数,按顺时针排列.
- style
  - solid  实线
  - dotted  圆点线
  - dashed  虚线
  - double  双线

```css
border-width: 4px 10px 5px 20px;
border-style: solid dotted dashed double;
border-color: black purple red blue;

# 综合用法
border-top: 1px solid #000;
	  顶部  粗细 样式  颜色
```

- 利用border创建三角形

```css
border-top: 15px solid yellow;
border-left: 15px solid transparent;
border-right: 15px solid transparent;
```

#### margin 外边距

- 标签:nth-child(数字)   选择第(数字)个标签
- 水平方向外边距

```html
<head>
    <style>
        span:nth-child(1){
            background-color: red;
            margin-right: 20px;
        }
         span:nth-child(2){
             background-color: green;
             margin-left: 100px;
        }
    </style>
</head>
<body>
    <span>mjj</span><span>wusir</span>
</body>
```

- 竖直方向外边距
- 垂直方向上会出现外边距合并现象，塌陷。以设置的最大的magrin距离为基准.

```html
<head>
    <style>
        div{
            width: 200px;
            height: 200px;
        }
        #box1{
            background-color: red;
            margin-bottom: 30px;
        }
        #box2{
            background-color: black;
            margin-top: 100px
        }
        #box3{
            background-color: yellowgreen;
        }
        #box3 p{
            background-color: goldenrod;
            margin-top: 30px;
        }
    </style>
</head>
<body>
    <div id="box1"></div>
    <div id="box2"></div>
<div id="box3">
    <p>mjj</p>
</div>
</body>
```



#### 布局-浮动

- float浮动
- 文字环绕图片

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        img{
            width: 400px;
        }
        .box{
            float: left;
        }
        p{
            border: 1px solid red;
        }
    </style>
</head>
<body>
	<div class="box">
	<img src="https://i1.mifile.cn/a4/xmad_15590487108559_JOpcA.jpg" alt="">
	</div>
	<p>央视网消息（新闻联播）：中共中央总书记、国家主席习近平28日上午在北京人民大会堂亲切会见出席第九届世界华侨华人社团联谊大会和中华海外联谊会五届一次理事大会的全体代表，代表党中央、国务院向大家表示热烈欢迎和衷心祝贺，向世界各地华侨华人致以诚挚问候。</p>
</body>
```

- 盒子浮动

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        div{
            width: 200px;
            height: 200px;
        }
        .father{
            width: 1200px;
            height: 200px;
        }
        .box1{
            background-color: red;
            float: left;
        }
        .box2{
            width: 500px;
            background-color: black;
            float: left;
        }
    </style>
</head>
<body>
    <div class="father">
        <div class="box1"></div>
        <div class="box2"></div>
    </div>
</body>
```

- 小米顶部栏
- container
- rgba(0,0,0,0-1)  前三位rgb,最后参数为透明度

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        *{
            padding: 0;
            margin: 0;
        }
        #top{
            width: 100%;
            height: 40px;
            background-color: #333;
        }
        .container{
            width: 1226px;
            height: 40px;
            /*margin-right: auto;*/
            /*margin-left: auto;*/
            margin: 0 auto; # 居中
            background-color: red;
        }
        #top .top_l{
            width: 300px;
            height: 40px;
            background-color: greenyellow;
            float: left;
        }
         #top .shop{
            width: 100px;
            height: 40px;
            background-color: blueviolet;
             float: right;
        }
         #top .login{
            width: 130px;
            height: 40px;
            background-color: rgba(0,0,0,.5);
             float: right;
        }

    </style>
</head>
<body>
    <div id="top">
        <div class="container">
            <div class="top_l">导航</div>
            <div class="shop">购物车</div>
            <div class="login">登陆注册</div>
        </div>
    </div>
</body>
```