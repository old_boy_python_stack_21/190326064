#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import sys
import time
import socket
base_path = os.path.dirname(os.path.dirname(__file__))
sys.path.append(base_path)
from src.account import account


def run():
    while 1:
        print('******欢迎使用本网盘!******\n1.登陆\n2.注册')
        msg = input('请选择(Q/q退出)')
        if msg.upper() == 'Q':
            return
        elif msg == '1':
            account.login(sk)
        elif msg == '2':
            account.register(sk)
        else:
            print('输入有误,请重新输入!')
            time.sleep(2)


if __name__ == '__main__':
    sk = socket.socket()
    sk.connect(('127.0.0.1', 8000))
    run()
    sk.close()
