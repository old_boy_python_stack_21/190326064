#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import sys
import time
from src.md5 import md5
from src.sendrecv import sendrecv
from conf import settings
from src.file import updown

USERNAME = None
DIC2 = {'user': None, }


class Account:

    def register(self, sk):
        while 1:
            user = input('请输入用户名:')
            pwd = input('请输入密码:')
            dic = {'user': user, 'pwd': md5.md5(user, pwd)}
            if hasattr(sendrecv, 'my_send'):
                getattr(sendrecv, 'my_send')(sk, dic)
            if hasattr(sendrecv, 'my_recv'):
                res_dic = getattr(sendrecv, 'recv_msg')(sk)
                if res_dic['result']:
                    print('注册成功')
                    path = os.path.join(settings.PATH, dic['user'])
                    os.mkdir(path)
                    break
                else:
                    print('用户名已存在')

    def login(self, sk):
        while 1:
            print('******请登录******')
            username = input('用户名:')
            password = input('密码:')
            dic = {'status': 'login', 'username': username, 'pwd': md5.md5(username, password)}
            sendrecv.my_send(sk, dic)
            dic = sendrecv.my_recv(sk)
            if not dic['flag']:
                print('登陆失败!')
                continue
            print('登陆成功!')
            global USERNAME
            USERNAME = username
            time.sleep(1)
            break
        while 1:
            print('您已使用%sKB,共%sKB' % (round(int(dic['size']) / 1024, 1), round(int(dic['total']) / 1024, 3)))
            global DIC2
            DIC2['user'] = username
            user = input('1.上传\n2.下载\n请选择(Q/q退出):')
            if user.upper() == 'Q':
                sendrecv.my_send(sk, '886')
                break
            if user == '1':
                DIC2['option'] = 'up'
            elif user == '2':
                DIC2['option'] = 'down'
            choice = DIC2.get('option')
            if not choice:
                print('请重新输入!')
                continue
            sendrecv.my_send(sk, DIC2)
            getattr(updown, DIC2['option'])(sk)


account = Account()
