#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys


class Jindu:
    def jindu(self, done, total):
        rate = done / total
        rate_num = int(rate * 100)
        if rate_num == 100:
            r = '\r%s>%d%%\n' % ('=' * rate_num, rate_num,)
        else:
            r = '\r%s>%d%%' % ('=' * rate_num, rate_num,)
        sys.stdout.write(r)
        sys.stdout.flush


jindu = Jindu()
