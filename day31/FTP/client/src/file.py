#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import time
import hashlib
from src.sendrecv import sendrecv
from src.jindu import jindu
from src import account


class Updown:

    def up(self, sk):  # 上传
        while 1:
            path = input('请输入要上传的文件路径(Q/q返回):').strip()
            if path.upper() == 'Q':
                sendrecv.my_send(sk, 'False')
                return
            sendrecv.my_send(sk, 'True')
            flag = os.path.isfile(path)
            if not flag:
                print('输入错误,请重新输入!')
                continue
            filename = os.path.basename(path)
            filesize = os.path.getsize(path)
            dic3 = {'user': account.USERNAME, 'filename': filename, 'filesize': filesize}
            sendrecv.my_send(sk, dic3)
            msg = sendrecv.my_recv(sk)
            if not msg:
                print('文件大小超出范围,请重新选择!')
                continue
            md5 = hashlib.md5()
            a = 0
            b = filesize
            with open(path, mode='rb')as f:
                while filesize > 0:
                    cont = f.read(1024)
                    md5.update(cont)
                    sk.send(cont)
                    filesize -= len(cont)
                    a += len(cont)
                    jindu.jindu(a, b)
            print('上传成功!')
            md5file = sendrecv.my_recv(sk)
            if md5file == md5.hexdigest():
                sendrecv.my_send(sk, True)
                print('文件一致.')
            else:
                print('文件不一致,请重试!')
                sendrecv.my_send(sk, False)
                continue
            cho = input('Y继续上传,N返回上级:')
            if cho.upper() == 'Y':
                sendrecv.my_send(sk, True)
                continue
            elif cho.upper() == 'N':
                sendrecv.my_send(sk, False)
                return

    def down(self, sk):  # 下载
        while 1:
            while 1:
                lst = sendrecv.my_recv(sk)      # 接收目录
                for i in range(len(lst)):
                    print(i + 1, lst[i])
                cho = input('请选择序号(Q/q退出):')
                if cho.upper() == 'Q':
                    return
                if not cho.isdigit():
                    print('输入有误,请重新输入!')
                    continue
                cho = int(cho)
                dic = {'cho': lst[cho - 1]}
                sendrecv.my_send(sk, dic)      # 发送序号
                choice_format = sendrecv.my_recv(sk)
                if choice_format == '目录':   # 如果是目录继续循环
                    continue
                else:       # 如果是文件退出循环
                    break
            filedic = sendrecv.my_recv(sk)      # 接收文件属性
            md5file = hashlib.md5()
            a = 0
            b = filedic['filesize']
            filepath = os.path.join(r'D:\homework\day31\FTP\client\bin', filedic['filename'])
            with open(filepath, mode='wb')as f:
                while filedic['filesize'] > 1024:
                    content = sk.recv(1024)
                    f.write(content)
                    md5file.update(content)
                    filedic['filesize'] -= len(content)
                    a += len(content)
                    jindu.jindu(a, b)
                else:
                    while filedic['filesize'] > 0:
                        content = sk.recv(filedic['filesize'])
                        f.write(content)
                        md5file.update(content)
                        filedic['filesize'] -= len(content)
                        a += len(content)
                        jindu.jindu(a, b)
            md5 = sendrecv.my_recv(sk)
            if md5file.hexdigest() == md5:
                sendrecv.my_send(sk, True)
                print('文件一致.')
            else:
                sendrecv.my_send(sk, False)
                print('文件不一致,请重试!')
                continue
            print('下载成功!')
            time.sleep(1)
            return


updown = Updown()
