#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
import struct


class Sendrecv:

    def my_send(self, sk, content):
        str_dic = json.dumps(content)
        len_dic = struct.pack('i', len(str_dic))
        sk.send(len_dic)
        sk.send(str_dic.encode('utf-8'))

    def my_recv(self, sk):
        a = sk.recv(4)
        len_dic = struct.unpack('i', a)[0]
        b = sk.recv(len_dic).decode('utf-8')
        content = json.loads(b)
        return content


sendrecv = Sendrecv()
