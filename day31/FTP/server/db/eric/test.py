import requests
from concurrent.futures import ThreadPoolExecutor

url_lst = [
    'http://www.baidu.com',   # 3
    'http://www.cnblogs.com', # 1
    'http://www.douban.com',  # 1
    'http://www.tencent.com',
    'http://www.cnblogs.com/Eva-J/articles/8306047.html',
    'http://www.cnblogs.com/Eva-J/articles/7206498.html',
]

def get(url):
    msg = requests.get(url)
    return {'url':url, 'content':msg}

def pro(s):
    dic = s.result()
    print(dic['url'])

t = ThreadPoolExecutor(5)
ret = []
for url in url_lst:
    s = t.submit(get, url)
    ret.append(s)
    s.add_done_callback(pro)
