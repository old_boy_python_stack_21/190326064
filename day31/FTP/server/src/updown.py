#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import sys
import hashlib
base_path = os.path.dirname(os.path.dirname(__file__))
sys.path.append(base_path)
from src.sendrecv import sendrecv
from bin.server import myserver


class Updown:
    def up(self, user):  # 上传
        dic = sendrecv.my_recv(myserver.request)
        filebase = os.path.join(r'D:\homework\day31\FTP\db\\' + dic['user'], dic['filename'])
        print(filebase)
        md5file = hashlib.md5()
        with open(filebase, mode='wb') as f:  # 创建字典中文件名的项,模式为写字节
            while dic['filesize'] > 1024:
                content = myserver.request.recv(1024)
                f.write(content)
                md5file.update(content)
                dic['filesize'] -= len(content)
            else:
                while dic['filesize'] > 0:
                    content = myserver.request.recv(dic['filesize'])
                    f.write(content)
                    md5file.update(content)
                    dic['filesize'] -= len(content)
        sendrecv.my_send(myserver.request, md5file.hexdigest())
        return

    def down(self, user):  # 下载
        path = os.path.join(r'D:\homework\day31\FTP\db', user)
        a = os.listdir(path)
        sendrecv.my_send(myserver.request, a)
        dic = sendrecv.my_recv(myserver.request)
        file = os.path.join(path, dic['cho'])
        filename = os.path.basename(file)
        filesize = os.path.getsize(file)
        md5 = hashlib.md5()
        dic2 = {'filename': filename, 'filesize': filesize}
        sendrecv.my_send(myserver.request, dic2)
        with open(file, mode='rb')as f:
            while filesize > 0:
                cont = f.read(1024)
                md5.update(cont)
                myserver.request.send(cont)
                filesize -= len(cont)
        sendrecv.my_send(myserver.request, md5.hexdigest())


updown = Updown()
