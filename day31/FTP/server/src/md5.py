#!/usr/bin/env python
# -*- coding:utf-8 -*-
import hashlib


class Md5:
    def md5user(self, username, pwd):
        md5 = hashlib.md5(username.encode('utf-8'))
        md5.update(pwd.encode('utf-8'))
        return md5.hexdigest()


md5 = Md5()
