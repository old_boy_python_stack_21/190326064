#!/usr/bin/env python
# -*- coding:utf-8 -*-
import json
import struct


class Sendrecv:
    def my_send(self, arg, dic):
        bdic = json.dumps(dic).encode('utf-8')
        len_dic = struct.pack('i', len(bdic))
        arg.send(len_dic)
        arg.send(bdic)

    def my_recv(self, arg):
        userinfo = arg.recv(4)
        len_dic = struct.unpack('i', userinfo)[0]
        str_dic = arg.recv(len_dic).decode('utf-8')
        return json.loads(str_dic)


sendrecv = Sendrecv()
