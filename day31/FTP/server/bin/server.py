#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
import json
import struct
import hashlib
import socketserver

base_path = os.path.dirname(os.path.dirname(__file__))
sys.path.append(base_path)
from src.sendrecv import sendrecv


class Myserver(socketserver.BaseRequestHandler):

    def up(self, dic):  # 上传
        while 1:
            msg = sendrecv.my_recv(self.request)
            if msg == 'False':
                return
            filedic = sendrecv.my_recv(self.request)
            print(filedic)  # true
            if int(dic['size']) + int(filedic['filesize']) > int(dic['total']):
                sendrecv.my_send(self.request, False)
                continue
            sendrecv.my_send(self.request, True)
            filebase = os.path.join('D:\homework\day31\FTP\server\db\\' + filedic['user'], filedic['filename'])
            md5file = hashlib.md5()
            with open(filebase, mode='wb') as f:  # 创建字典中文件名的项,模式为写字节
                while filedic['filesize'] > 1024:
                    content = self.request.recv(1024)
                    f.write(content)
                    md5file.update(content)
                    filedic['filesize'] -= len(content)
                else:
                    while filedic['filesize'] > 0:
                        content = self.request.recv(filedic['filesize'])
                        f.write(content)
                        md5file.update(content)
                        filedic['filesize'] -= len(content)
            sendrecv.my_send(self.request, md5file.hexdigest())
            msg = sendrecv.my_recv(self.request)
            if not msg:
                print('md5不一致,重新上传.')
                continue
            msg = sendrecv.my_recv(self.request)
            if msg:
                print('继续上传.')
                continue
            return

    def down(self, user):  # 下载
        filepath = os.path.join(r'D:\homework\day31\FTP\server\db', user['username'])
        while 1:
            files = os.listdir(filepath)
            sendrecv.my_send(self.request, files)   # 发送目录
            dic = sendrecv.my_recv(self.request)    # 接收dic={'cho':}
            cho = os.path.join(filepath, dic['cho'])    # 拼接path和序号的路径
            if os.path.isdir(cho):   # 如果是目录,继续发送
                filepath = cho      # 把新目录给path
                sendrecv.my_send(self.request, '目录')
                continue
            sendrecv.my_send(self.request, '文件')
            file = os.path.join(filepath, dic['cho'])   # 拼接文件目录
            filename = os.path.basename(file)       # 获取文件名
            filesize = os.path.getsize(file)        # 获取文件大小
            md5 = hashlib.md5()         # md5文件一致性
            dic2 = {'filename': filename, 'filesize': filesize}
            sendrecv.my_send(self.request, dic2)
            with open(file, mode='rb')as f:
                while filesize > 0:
                    cont = f.read(1024)
                    md5.update(cont)
                    self.request.send(cont)
                    filesize -= len(cont)
            sendrecv.my_send(self.request, md5.hexdigest())
            msg = sendrecv.my_recv(self.request)
            if not msg:
                filepath = os.path.join(r'D:\homework\day31\FTP\server\db', user['username'])
                continue
            else:
                return

    def handle(self):
        while 1:
            dic = sendrecv.my_recv(self.request)  # {'username': username, 'pwd': password}
            userinfo_path = r'D:\homework\day31\FTP\server\db\userinfo'
            flag = False
            with open(userinfo_path, 'r', encoding='utf-8')as f:
                for line in f:
                    a, b, total = line.strip().split('|')
                    if dic['username'] == a and dic['pwd'] == b:
                        flag = True
                        break
            dic['flag'] = flag
            if flag:        # 计算已使用大小
                size = 0
                ss = os.path.join(r'D:\homework\day31\FTP\server\db', a)
                s = os.walk(ss)
                for i in s:
                    for j in i[-1]:
                        size += os.path.getsize(os.path.join(i[0], j))
                dic['size'] = size
                dic['total'] = total
            sendrecv.my_send(self.request, dic)
            if flag:
                print('%s上线了.' % dic['username'])
                break
        while 1:
            dic2 = sendrecv.my_recv(self.request)
            if dic2 == '886':
                print('%s已下线.' % dic['username'])
                break
            func = getattr(self, dic2['option'])
            func(dic)


if __name__ == '__main__':
    sk = socketserver.ThreadingTCPServer(('127.0.0.1', 8000), Myserver)
    sk.serve_forever()
