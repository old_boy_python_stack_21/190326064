#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.为函数写一个装饰器，在函数执行之后输入 after
"""
def wrapper(arg):
    def inner(*args):
        arg()
        print('after')
    return inner

@wrapper
def func():
    print(123)

func()
"""

# 2.为函数写一个装饰器，把函数的返回值 +100 然后再返回。
'''
def wrapper(arg):
    def inner(*args):
        v = arg() + 100
        return v
    return inner

@wrapper
def func():
    return 7

result = func()
print(result)
'''

# 3.为函数写一个装饰器，根据参数不同做不同操作。
#
# flag为True，则 让原函数执行后返回值加100，并返回。
# flag为False，则 让原函数执行后返回值减100，并返回。
'''
def x(flag):
    def inner(arg):
        def class3():
            if flag:
                return arg()+100
            return arg()-100
        return class3
    return inner


@x(True)
def f1():
    return 11

@x(False)
def f2():
    return 22

r1 = f1()
r2 = f2()
print(r1,r2)
'''

# 4.写一个脚本，接收两个参数。
# 第一个参数：文件
# 第二个参数：内容
# 请将第二个参数中的内容写入到 文件（第一个参数）中。
# 执行脚本： python test.py oldboy.txt 你好
'''
import sys

a = sys.argv
with open(a[1], mode='w+', encoding='utf-8') as f:
    f.write(a[2])
'''

# 5.递归的最大次数是多少？
'''
1000
'''

# 6.看代码写结果
'''
print("你\n好")   # 你 换行 好
print("你\\n好")  # 你\n好
print(r"你\n好")  # 你\n好
'''

# 7.写函数实现，查看一个路径下所有的文件【所有】。
'''
import os

v = os.walk(r'D:\homework\day14')
for a,b,c in v:
    for i in c:
        path = os.path.join(a,i)
        print(path)
'''

# 8.写代码,请根据path找到code目录下所有的文件【单层】，并打印出来。
'''
import os


v = os.listdir("..\day14")
for i in v:
    print(i)
'''

# 9.1斐波那契数列
'''
lst = [1,1,]
count = 2
while 1:
    a = lst[-1] + lst[-2]
    if a < 4000000:
        lst.append(a)
        count += 1
    else:
        break

print(lst,count)
'''

# 9.2
'''
dic_a = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'f': 'hello'}
dic_b = {'b': 3, 'd': 5, 'e': 7, 'm': 9, 'k': 'world'}
for i in dic_b:
    if i in dic_a:
        dic_a[i] = dic_a[i] + dic_b[i]
    else:
        dic_a[i] = dic_b[i]

print(dic_a)
'''

# 10.
'''
[10,'a']
[123]
[10,'a']
'''

# 11.1  A,B,C

# 11.2
'''
tupleA = ('a', 'b', 'c', 'd', 'e')
tupleB = (1, 2, 3, 4, 5)
dic = {}
for i in range(len(tupleA)):
    dic[tupleA[i]] = tupleB[i]
print(dic)
'''

# 11.3
'''
import sys
print(sys.argv)
print(len(sys.argv))
print(sys.argv[0])
'''

# 11.4
'''
ip = '192.168.0.100'
lst = [int(i) for i in ip.split('.')]
print(lst)
'''

# 11.5
'''
Alist = ['a', 'b', 'c']
s = ','.join(Alist)
print(s)
'''

# 11.6
'''
a = StrA[-2:]
b = StrA[1:3]
'''

# 11.7
# Alist = [1, 2, 3, 1, 3, 1, 2, 1, 3]
# 方法一
'''
a = Alist[:3]
print(a)
'''
# 方法二
'''
b = set(Alist)
print(list(b))
'''

# 11.8
'''
import os
def func(path):
    for a,b,c in os.walk(path):
        for i in c:
            ret = os.path.join(a,i)
            print(ret)

func(r'D:\homework')
'''

# 11.9

'''
for a in range(1, 10000):
    lst = []
    for b in range(1, a):
        if a % b == 0:
            lst.append(b)
    if sum(lst) == a:
        print(a)
'''


# 11.10进程池

# 11.11

list1 = [1, 2, 3, 4, 5]
list2 = [5, 4, 3, 2, 1]

# 11.12
"""
with open('a.txt','r',encoding='utf-8') as f:
    for line in f:
        print(line)
"""
