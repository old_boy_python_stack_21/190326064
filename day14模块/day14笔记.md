# day14笔记

## 一.装饰器

### 1.基本格式

```python
def x(func):
    def inner():
        return func()
    return inner 

@x
def index():
	pas
```

### 2.关于参数

```python
def x1(func):
    def inner(*args,**kwargs):			# 无敌,接收任意参数
        return func(*args,**kwargs)
    return inner 

@x1
def f1():
    pass

@x1
def f2(a1):
    pass
@x1
def f3(a1,a2):
    pass 
```

### 3.关于返回值

```python
def x1(func):
    def inner(*args,**kwargs):
        data = func(*args,**kwargs)		# 执行f1,输出123,返回666给data
        return data
    return inner 

@x1
def f1():			# 装饰f1
    print(123)
    return 666
v1 = f1()			# 返回data给v1
print(v1)			# 输出666
```

```python
def x1(func):
    def inner(*args,**kwargs):
        data = func(*args,**kwargs)		# 执行f1,输出123,返回666给data,但没返回
    return inner 		# 返回None

@x1
def f1():
    print(123)
    return 666

v1 = f1()
print(v1)
```

### 4.关于前后

```python
def x1(func):
    def inner(*args,**kwargs):
        print('调用原函数之前')		# 1.
        data = func(*args,**kwargs) 	# 2.执行原函数,输出123,返回None给data
        print('调用员函数之后')		# 3.
        return data
    return inner 

@x1
def index():
    print(123)
    
index()
```

### 5.带参数的装饰器

```python
# 1.装饰器无参数
# 第一步：执行 ret = xxx(index)
# 第二步：将返回值赋值给 index = ret 
@xxx
def index():
    pass

# 2.装饰器带参数
# 第一步：执行 v1 = uuu(9)
# 第二步：ret = v1(index)
# 第三步：index = ret 
@uuu(9)
def index():
    pass
# 如下
def uuu(counter):
    def v1(func):
        def inner(*args,**kwargs):
            ret = func(*args,**kwargs) # 执行原函数并获取返回值
            return ret
        return inner 
	return v1 

@uuu(9)
def index():
    pass
```

```python
def x(counter): 
    print('x函数')
    def wrapper(func):
        print('wrapper函数')
        def inner(*args,**kwargs):
            if counter:
                return 123
            return func(*args,**kwargs)
        return inner
    return wrapper

@x(True)	# 处理第一种情况
def fun990():
    pass

@x(False)	# 处理第二种情况
def func10():
    pass
```

## 二.模块 import xxx

### 1.sys

- sys.getrefcount , 获取一个值的应用计数

```python
a = [11,22,33]	# 1
b = a	# 2 
print(sys.getrefcount(a)) # 3
```

- sys.getrecursionlimit , python默认支持的递归数量
- sys.stdout.write --> print (进度,百分比)

```python
import time
for i in range(1,101):
    msg = "%s%%\r" %i
    print(msg,end='')
    time.sleep(0.05)
```

```python
import os

# 1. 读取文件大小（字节）
file_size = os.stat('filename').st_size

# 2.一点一点的读取文件
read_size = 0
with open('filename',mode='rb') as f1,open('a.mp4',mode='wb') as f2:
    while read_size < file_size:
        chunk = f1.read(1024) # 每次最多去读取1024字节
        f2.write(chunk)
        read_size += len(chunk)
        val = int(read_size / file_size * 100)
        print('%s%%\r' %val ,end='')
```

- sys.argv

```python
"""
让用户执行脚本传入要删除的文件路径，在内部帮助用将目录删除。
C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py
"""
import sys

# 获取用户执行脚本时，传入的参数。
# C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
# sys.argv = [D:/code/s21day14/7.模块传参.py, D:/test]
path = sys.argv[1]

# 删除目录
import shutil
shutil.rmtree(path)
```

### 2.os

和操作系统相关的数据。

- os.path.exists(path) ， 如果path存在，返回True；如果path不存在，返回False

- os.stat('filename').st_size ， 获取文件大小

- os.path.abspath() ， 获取一个文件的绝对路径

  ```python
  path = 'filename' # D:\code\s21day14\filemname
  
  import os
  v1 = os.path.abspath(path)
  print(v1)
  ```

- os.path.dirname ，获取路径的上级目录

  ```python
  import os
  v = r"D:\code\s21day14\filename"
  
  print(os.path.dirname(v))
  ```

- os.path.join ，路径的拼接

  ```python
  import os
  path = "D:\code\s21day14" # user/index/inx/fasd/
  v = 'n.txt'
  
  result = os.path.join(path,v)
  print(result)
  result = os.path.join(path,'n1','n2','n3')
  print(result)
  ```

- os.listdir ， 查看一个目录下所有的文件【第一层】

  ```python
  import os
  
  result = os.listdir(r'D:\code\s21day14')
  for path in result:
      print(path)
  ```

- os.walk ， 查看一个目录下所有的文件【所有层】

```python
import os

result = os.walk(r'D:\code\s21day14')
for a,b,c in result:
    # a,正在查看的目录 b,此目录下的文件夹  c,此目录下的文件
    for item in c:
        path = os.path.join(a,item)
        print(path)
```

- 补充转义

```python
v1 = r"D:\code\s21day14\n1.mp4"  (推荐)
print(v1)

v2 = "D:\\code\\s21day14\\n1.mp4"
print(v2)
```

### 3.shutil

```python
import shutil
shutil.rmtree(path)
```

