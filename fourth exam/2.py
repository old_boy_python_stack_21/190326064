#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 2.请使用多线程实现生产者消费者模型
import time
import random
from queue import Queue
from threading import Thread


def pro(q, name, food):
    for i in range(10):
        time.sleep(random.random())
        fd = '%s%s' % (food, i)
        q.put(fd)
        print('%s生产了一个%s' % (name, food))


def cus(q, name):
    while True:
        food = q.get()
        if not food: break
        time.sleep(random.randint(1, 3))
        print('%s吃了%s' % (name, food))


def s(ccount, pcount):
    q = Queue(3)
    for i in range(ccount):
        Thread(target=cus, args=(q, 'alex')).start()
    p_l = []
    for i in range(pcount):
        p1 = Thread(target=pro, args=(q, 'wusir', '泔水'))
        p1.start()
        p_l.append(p1)
    for p in p_l: p.join()  # 等待所有子进程结束
    for i in range(ccount):  # 给每个消费者发送None,消费者接受到None时停止接受并退出.
        q.put(None)


s(1, 1)
