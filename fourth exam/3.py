#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 3、开启一个有20个线程的线程池，提交200个任务
# 1）要获取这200个任务的返回值（4分）
# 2）要等待这200个任务结束前后，在log文件中记录开始和结束事件（4分）
import time
from multiprocessing import RLock
from concurrent.futures import ThreadPoolExecutor


def func(i):
    with rl:
        with open('log', 'a', encoding='utf-8')as f:
            f.write(str(i) + ' ' + time.strftime('%Y-%m-%d %H:%M:%S') + '\n')
        time.sleep(0.01)
        with open('log', 'a', encoding='utf-8')as f:
            f.write(str(i) + ' ' + time.strftime('%Y-%m-%d %H:%M:%S') + '\n')
        return i


rl = RLock()
t = ThreadPoolExecutor(20)
lst = []
for i in range(200):
    res = t.submit(func, i)
    lst.append(res)
t.shutdown()
for i in lst:
    print(i.result())
print('Done')
