# 4、写程序说明死锁现象是如何产生的，并用文字说明原因，列举解决方案。

import time
from threading import Thread, Lock

nlock = Lock()
flock = Lock()


def eat1(name, nlock, flock):
    nlock.acquire()
    print('%s抢到面了' % name)
    flock.acquire()
    print('%s抢到叉子了' % name)
    print('%s吃了一口面' % name)
    time.sleep(0.1)
    flock.release()
    print('%s放下叉子了' % name)
    nlock.release()
    print('%s放下面了' % name)


def eat2(name, nlock, flock):
    flock.acquire()
    print('%s抢到叉子了' % name)
    nlock.acquire()
    print('%s抢到面了' % name)
    print('%s吃了一口面' % name)
    time.sleep(0.1)
    nlock.release()
    print('%s放下面了' % name)
    flock.release()
    print('%s放下叉子了' % name)


lst = ['alex', 'wusir', 'taibai', 'yuan']
Thread(target=eat1, args=(lst[0], nlock, flock)).start()
Thread(target=eat2, args=(lst[1], nlock, flock)).start()
Thread(target=eat1, args=(lst[2], nlock, flock)).start()
Thread(target=eat2, args=(lst[3], nlock, flock)).start()

'''
死锁现象:
1.有多把锁,一把以上
2.多把锁交替使用

解决方案:
1.将多把互斥锁变成一把递归锁
2.优化代码逻辑
'''
