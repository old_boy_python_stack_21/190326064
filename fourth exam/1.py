#!/usr/bin/env python
# -*- coding:utf-8 -*-
import gevent
import time
import requests
from gevent import monkey
monkey.patch_all()
url_lst = ['www.baidu.com', ]


def func(url):      # 请求网页
    msg = requests.get(url)
    time.sleep(0.01)
    file = url[url]+'.html'
    with open(file, 'w', encoding='utf-8') as f:
        f.write(msg.text)


lst = []
for url in url_lst:
    g = gevent.spawn(func, url)
    g.start()
    lst.append(g)
gevent.joinall(lst)
