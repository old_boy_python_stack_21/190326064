# 5、 请根据描述完成下列列题目
# 表结构
# 用户(user)表：用户id(user_id),用户名(user_name),密码(password)
# 订单(orders)表：订单编号(order_id)，下单时间(order_time),支付金额(payment)，用户id(uid)
# 商品(goods)表：商品编号(goods_id)，商品名称(gname),商品单价(price)，库存(g_num)
# 商品_订单表(goods_orders)：编号(id)，商品编号(g_id)，订单编号(o_id)，购买个数(buy_num)
# ddl语句：（4分）
# 1）创建表，并根据描述给表中的字段设置合理的数据类型，并设计合理的表与表之间关系；
"""
用户表:
create table user(user_id int primary key auto_increment,user_name char(16) not null unique,password char(16) not null);
订单表:
create table orders(order_id int primary key,order_time datetime,payment float(99,2) not null,uid int not null,
foreign key(uid) references user(user_id) on delete cascade on update cascade);
商品表:
create table goods(goods_id int primary key,gname char(16) not null,price float(99,2) not null,g_num int not null);
商品_订单表:
create table goods_orders(id int primary key,g_id int not null,o_id int not null,buy_num int not null,
foreign key(g_id) references goods(goods_id) on delete cascade on update cascade,
foreign key(o_id) references orders(order_id) on delete cascade on update cascade
);
"""

# dml语句 :（14分）
# 1）使用python，读文件，并通过pymysql模块将文本内容写入数据库（4分）
'''
# user表
# import pymysql
# conn = pymysql.connect(host='127.0.0.1', user='root', password="123",database='exam')
# cur = conn.cursor()
# with open('C:\Users\建雄\Desktop\机试\机试\第5题\user', mode='r', encoding='utf-8')as f:
#     for line in f:
#         a,b,c = line.strip().split(',')
#         a,b,c = "'"+a+"'","'"+b+"'","'"+c+"'"
#         sql = 'insert into user values(%s,%s,%s)' % (a,b,c)
#         cur.execute(sql)
#         print(sql)
# conn.commit()
# conn.close()


# orders表
# import pymysql
# conn = pymysql.connect(host='127.0.0.1', user='root', password="123",database='exam')
# cur = conn.cursor()
# with open(r'C:\Users\建雄\Desktop\机试\机试\第5题\orders', mode='r', encoding='utf-8')as f:
#     for line in f:
#         a,b,c,d = line.strip().split(',')
#         a,b,c,d = a,"'"+b+"'",c,d
#         sql = 'insert into orders values(%s,%s,%s,%s)' % (a,b,c,d)
#         cur.execute(sql)
#         print(sql)
# conn.commit()
# conn.close()

# goods表
# import pymysql
# conn = pymysql.connect(host='127.0.0.1', user='root', password="123",database='exam')
# cur = conn.cursor()
# with open(r'C:\Users\建雄\Desktop\机试\机试\第5题\goods', mode='r', encoding='utf-8')as f:
#     for line in f:
#         a,b,c,d = line.strip().split(',')
#         a,b,c,d = a,"'"+b+"'",c,d
#         sql = 'insert into goods values(%s,%s,%s,%s)' % (a,b,c,d)
#         cur.execute(sql)
#         print(sql)
# conn.commit()
# conn.close()

# goods_orders表
# import pymysql
# conn = pymysql.connect(host='127.0.0.1', user='root', password="123",database='exam')
# cur = conn.cursor()
# with open(r'C:\Users\建雄\Desktop\机试\机试\第5题\goods_orders', mode='r', encoding='utf-8')as f:
#     for line in f:
#         a,b,c,d = line.strip().split(',')
#         sql = 'insert into goods_orders values(%s,%s,%s,%s)' % (a,b,c,d)
#         cur.execute(sql)
#         print(sql)
# conn.commit()
# conn.close()
'''

# 2）求出价格最贵的商品id（2分）
# select goods_id from goods order by price desc limit 1;

# 3）求’taibai’购买的所有订单，并按照订单总额从高到低排序（2分）
# select order_id,payment from orders where uid in (select user_id from user where user_name='taibai') order by payment
# desc;

# 4）求出销量最高的商品名（2分）
# select gname from goods inner join (select*from goods_orders)o on o.g_id=goods.goods_id group by goods.goods_id order
# by sum(buy_num)desc limit 1;

# 5）求出每个月下单的用户人数（执行select month(time) from orders来学习month的用法，并完成本题）（2分）
# select count(*) from orders group by month(order_time);

# 6）求出本月各商品的销售总金额和个数，按照个数排序（2分）
''' select gname,price*ss,ss from goods inner join (select g_id,sum(buy_num) ss from goods_orders group by g_id)o on
o.g_id=goods.goods_id order by ss;'''
