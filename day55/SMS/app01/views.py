from django.shortcuts import render, redirect, HttpResponse
from app01 import models


# 主页
def index(request):
    all_stu = models.Stu.objects.all()
    all_cls = models.Clas.objects.all()
    all_tea = models.Teacher.objects.all()
    return render(request, 'index.html', {'all_stu': all_stu, 'all_cls': all_cls, 'all_tea':all_tea})


# 新增班级
def add_cls(request):
    error = ''
    if request.method == 'POST':
        cls_name = request.POST.get('cls_name')
        if models.Clas.objects.filter(cls_name=cls_name):
            error = '班级名称已经存在了,回家去吧'
        if not cls_name:
            error = '你输的什么东西?'
        if not error:
            models.Clas.objects.create(cls_name=cls_name)
            return redirect('/index/')
    return render(request, 'add_cls.html', {'error': error})


# 删除班级
def del_cls(request):
    pk = request.GET.get('id')
    obj = models.Clas.objects.filter(pk=pk)
    if not obj:
        return HttpResponse('你想删还没有呢!')
    obj.delete()
    return redirect('/index/')


# 编辑班级
def edit_cls(request):
    error = ''
    pk = request.GET.get('id')
    obj_list = models.Clas.objects.filter(pk=pk)
    if not obj_list:
        return HttpResponse('没有!不让你编辑!')
    obj = obj_list[0]
    if request.method == 'POST':
        cls_name = request.POST.get('cls_name')
        if models.Clas.objects.filter(cls_name=cls_name):
            error = '你修改的名称被人取了呢!'
        if obj.name == cls_name:
            error = '你改了啥呀?重改!'
        if not cls_name:
            error = '写点东西吧,别空着呀!'
        if not error:
            obj.name = cls_name
            obj.save()
            return redirect('/index/')

    return render(request, 'edit_cls.html', {'obj': obj, 'error': error})


# 增加学生
def add_stu(request):
    if request.method == 'POST':
        sname = request.POST.get('stu_name')
        gender = request.POST.get('gender')
        sid_id = request.POST.get('sid_id')
        models.Stu.objects.create(sname=sname, gender=gender, sid_id=sid_id)
        return redirect('/index/')

    all_cls = models.Clas.objects.all()
    return render(request, 'add_stu.html', {'all_cls': all_cls})


# 删除学生
def del_stu(request):
    pk = request.GET.get('id')
    qq = models.Stu.objects.filter(pk=pk).delete()
    return redirect('/index/')


# 编辑学生
def edit_stu(request):
    pk = request.GET.get('id')
    stu_obj = models.Stu.objects.get(pk=pk)
    if request.method == 'POST':
        stu_name = request.POST.get('stu_name')
        gender = request.POST.get('gender')
        stu_cls = request.POST.get('sid_id')
        print(3, stu_cls)
        stu_obj.sname = stu_name
        stu_obj.gender = gender
        stu_obj.sid_id = models.Stu.objects.get(pk=stu_cls)
        stu_obj.save()
        return redirect('/index/')
    all_stu = models.Clas.objects.all()
    return render(request, 'edit_stu.html', {'stu_obj': stu_obj, 'all_stu': all_stu})


# 增加老师
def add_tea(request):
    if request.method == 'POST':
        tname = request.POST.get('tname')
        clss = request.POST.getlist('cls')
        aa = models.Teacher.objects.create(tname=tname)
        aa.t_cls.set(clss)
        return redirect('/index/')

    all_tea = models.Clas.objects.all()
    return render(request, 'add_tea.html', {'all_tea': all_tea})


# 编辑老师
def edit_tea(request):
    pk = request.GET.get('id')
    tea_obj = models.Teacher.objects.get(pk=pk)
    if request.method == 'POST':
        tname = request.POST.get('tname')
        clss = request.POST.getlist('clss')
        tea_obj.tname = tname
        tea_obj.save()
        tea_obj.t_cls.set(clss)
        return redirect('/index/')
    all_cls = models.Clas.objects.all()
    return render(request, 'edit_tea.html', {'tea_obj': tea_obj, 'all_cls': all_cls})


# 删除老师
def del_tea(request):
    pk = request.GET.get('id')
    models.Teacher.objects.filter(pk=pk).delete()
    return redirect('/index/')
