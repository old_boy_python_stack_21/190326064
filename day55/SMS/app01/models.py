from django.db import models


class Clas(models.Model):
    id = models.AutoField(primary_key=True)
    cls_name = models.CharField(max_length=32)

    def __str__(self):
        return self.cls_name


class Stu(models.Model):
    sid = models.ForeignKey('Clas', on_delete=models.CASCADE)
    sname = models.CharField(max_length=32)
    gender = models.CharField(max_length=32)


class Teacher(models.Model):
    tname = models.CharField(max_length=32,null=False)
    t_cls = models.ManyToManyField('Clas')
