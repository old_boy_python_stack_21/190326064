# day64笔记

## 1.回顾

### 1.cookie

#### 定义

- 保存在浏览器上的一组组键值对。

#### 为什么有cookie？

- http协议是无状态的。每次http请求都是对立的，相互之间没有关联。用cookie保存状态。

#### 特性

1. 由服务器让浏览器进行设置的。
2. 浏览器保存在浏览器本地上。
3. 下次访问时会自动携带相应的cookie

#### django中操作cookie

- 设置	本质:set-cookie ： ‘key:value’
  - ret =  HttpResponse('xxxxx')
  - ret.set_cookie(key ,  value , max_age=1000,path='/')
  - domain  secure   httponly 
  - ret.set_signed_cookie(key ,  value ,  salt='s21'   )
- 获取	本质 cookie ： ‘...’
  - request.COOKIES   []   get()
  - request.get_signed_cookie(key,salt='s21',default='xx')

- 删除     set-cookie ： ‘key:"" ,max-age=0’
  - ret =  HttpResponse('xxxxx')
  - ret.delete_cookie(key)

### 2.session

#### 定义

- 保存在服务器上的一组组键值对,必须依赖cookie

#### 为什么要有session？

1. cookie保存在浏览器本地，不安全
2. cookie的大小受到限制

#### django中操作session

- 设置：

  - request.session[key] = value

- 获取

  - request.session[key]       request.session.get(key)

- 删除
  - del request.session[key]     request.session.pop(key)

  - request.session.delete()   #  删除session的数据   不删除cookie

  - request.session.flush()      #  删除session的数据   删除cookie

- 其他
  - request.session.session_key	拿到当前用户的session key
  - request.session.set_expiry(value)	设置超时时间,不设置则默认2周,设置0时浏览器关闭时失效,还可以设置一个时间
  - request.session.clear_expiried()	清除过期的session

- 配置
  - 存放session的位置 : 数据库的一张表db , 缓存cache , 文件file , 加密cookie

    ```python
    from django.conf import global_settings# session全局设置
    from django.contrib.sessions.backends import db
    ```

  - 每次请求都保存session数据

    - SESSION_SAVE_EVERY_REQUEST = False



## 2.今日内容

### 1.csrf装饰器

- 导入csrf作为装饰器

```python
from django.views.decorators.csrf import csrf_exempt,csrf_protect,ensure_csrf_cookie
from django.utils.decorators import method_decorator

# csrf_exempt   某个视图不需要进行csrf校验,加在dispatch方法上或class上写入name='dispatch'
class Login(View):
	@method_decorator(csrf_exempt)
	def dispatch(self,request,*args,**kwargs):
		ret = super().dispatch(request, *args, **kwargs)
	return ret
# 或者
@method_decorator(csrf_exempt,name='dispatch')
class Login(View):
	...

# csrf_protect  某个视图需要进行csrf校验,settings中的csrf中间件打开,表单中加入{% csrf_token %},装饰器加在post上.
@method_decorator(csrf_protect)
def post(self, request, *args, **kwargs):
	...

# ensure_csrf_cookie  确保生成csrf的cookie,加在get上.注释掉settings中间件时也能生成csrftoken.
@method_decorator(ensure_csrf_cookie)
def get(self, request, *args, **kwargs):
	...
```

### 2.csrf功能

- 1.csrf中间件中执行process_request：

  - 从cookie中获取到csrftoken的值

  - csrftoken的值放入到 request.META

- 2.执行process_view

  - 1.查询视图函数是否使用csrf_exempt装饰器，使用了就不进行csrf的校验

  - 2.判断请求方式：

    - 如果是GET', 'HEAD', 'OPTIONS', 'TRACE' 不进行csrf校验

    - 其他的请求方式（post，put）, 进行csrf校验：

      - 1.获取cookie中csrftoken的值

      - 2.获取csrfmiddlewaretoken的值
      - 能获取到  ——》 request_csrf_token
      - 获取不到   ——》 获取请求头中X-csrftoken的值 ——》request_csrf_token
      - 比较上述request_csrf_token和cookie中csrftoken的值，比较成功接收请求，比较不成功拒绝请求。

### 3.ajax

- 博客:<https://www.cnblogs.com/maple-shaw/articles/9524153.html>
- AJAX(Asynchronous Javascript And XML) , 异步的Javascript和XML(不知xml)
- JSON
  - python中用json模块把各种格式序列化成json格式的字符串在网络上传输.
  - js中
    - JSON.parse()	将字符串转为js对象
    - JSON.stringify()	将js转换为json字符串

#### 1.发请求的方式 (同步)

1. 地址栏输入地址  GET
2. form表单  GET/POST  
3. a标签   GET

#### 2.ajax 

- 就是使用js的技术发请求和接收响应的。
- 特点：
  - 异步 / 局部刷新 / 传输的数据量少
- data中传输列表时注意
  - request.POST.get('键)
  - 1.把列表JSON.stringify(列表)转为json格式 , 接收时也需要解析(json.loads(内容))
  - 2.不用json时,服务器取到的键会跟上[] , 值为列表最后一个 , 取列表全部需要request.POST.getlist('键')

#### 3.jqery发ajax请求

- 发送表单时button设置type='button' , 或者去掉form表单
- 两数字相加
  - html

```html
<input type="text" name="i1">+
<input type="text" name="i2">=
<input type="text" name="i3">
<button id="b1">计算</button>

<script>
$.ajax({
    url: '/calc/',  // 发请求到这个地址
    type: 'post',	// 请求方式
    data: {
        a: $("[name='i1']").val(),
        b: $("[name='i2']").val(),
    },
    success: function (res) {		// 响应成功走success
        $("[name='i3']").val(res)
    },
    error:function (error) {		// 响应失败走error
        console.log(error)
    }
})
</script>
```

- views

```python
def calc(request):
    a = request.POST.get('a')
    b = request.POST.get('b')
    c = int(a) + int(b)
    return HttpResponse(c)
```



#### 4.上传文件：

- html

```javascript
<input type="file" id="f1">
<button id="b1">上传</button>

$('#b1').click(function () {
	var  formobj =  new FormData();

	formobj.append('file',document.getElementById('f1').files[0]);
	// formobj.append('file',$('#f1')[0].files[0]);   以上二种方法
	formobj.append('name','alex');

    $.ajax({
        url: '/upload/',
        type: 'post',
        data:formobj ,
        processData:false,  // 格式
        contentType:false,
        success: function (res) {
            $("[name='i3']").val(res)
        },
    })
})
```



#### 5.ajax通过csrf的校验 (三种方法)

- 前提条件：确保有csrftoken的cookie
- 在页面中使用{% csrf_token %}
- 加装饰器 ensure_csrf_cookie
- 导入
  - from django.views.decorators.csrf import csrf_exempt,csrf_protect,  ensure_csrf_cookie

##### 1.给data中添加csrfmiddlewaretoken的值

```javascript
data: {
    'csrfmiddlewaretoken':$('[name="csrfmiddlewaretoken"]').val(),
    a: $("[name='i1']").val(),
    b: $("[name='i2']").val(),
},
```

##### 2.加请求头

```html
headers:{
    'x-csrftoken':$('[name="csrfmiddlewaretoken"]').val(),
},
```

##### 3.使用文件

- 把如下内容写入js文件,在html中导入该文件,原理同方法2

```java
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
  beforeSend: function (xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader("X-CSRFToken", csrftoken);
    }
  }
});
```

