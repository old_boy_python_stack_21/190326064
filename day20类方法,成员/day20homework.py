#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.简述面向对象三大特性并用代码表示。
"""
封装:
class Account:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def func(self):
        pass

继承:
class A:
    pass

class B(A):
    pass

多态:
class A:
    def func(self, arg):
        v = arg[-1]
"""

# 2.什么是鸭子模型？
'''
都只有一个例如self.send参数的方法称为鸭子模型.
'''

# 3.列举面向对象中的类成员和对象成员。
'''
类成员:
实例变量、类变量、静态方法、普通方法、类方法、属性
对象成员:
对象变量.
'''

# 4. @classmethod和@staticmethod的区别?
'''
classmethod多了一个参数cls,输出自己类的名称不需要传值.
'''

# 5.Python中双下滑 __ 有什么作用？
'''
成员修饰符:把该数据类型(方法/变量/参数)转换为私有.
'''

# 6.看代码写结果
'''
class Base:
    x = 1


obj = Base()
print(obj.x)    # 1
obj.y = 123
print(obj.y)    # 123
obj.x = 123
print(obj.x)    # 123
print(Base.x)   # 1
'''

# 7.看代码写结果
'''
class Parent:
    x = 1


class Child1(Parent):
    pass


class Child2(Parent):
    pass


print(Parent.x, Child1.x, Child2.x)     # 1 1 1
Child2.x = 2
print(Parent.x, Child1.x, Child2.x)     # 1 1 2
Child1.x = 3
print(Parent.x, Child1.x, Child2.x)     # 1 3 2
'''

# 8.看代码写结果
'''
class Foo(object):
    n1 = '武沛齐'
    n2 = '金老板'

    def __init__(self):
        self.n1 = '女神'


obj = Foo()
print(obj.n1)   # 女神
print(obj.n2)   # 金老板
'''

# 9.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):
    n1 = '武沛齐'

    def __init__(self, name):
        self.n2 = name


obj = Foo('太白')
print(obj.n1)   # 武沛齐
print(obj.n2)   # 太白

print(Foo.n1)   # 武沛齐
print(Foo.n2)   # 报错
'''

# 10.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):
    a1 = 1
    __a2 = 2

    def __init__(self, num):
        self.num = num
        self.__salary = 1000

    def show_data(self):
        print(self.num + self.a1)


obj = Foo(666)

print(obj.num)  # 666
print(obj.a1)   # 1
print(obj.__salary)     # 私有,报错
print(obj.__a2)     # 私有,报错
print(Foo.a1)       # 1
print(Foo.__a2)     # 私有,报错
'''

# 11.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):
    a1 = 1

    def __init__(self, num):
        self.num = num

    def show_data(self):
        print(self.num + self.a1)


obj1 = Foo(666)
obj2 = Foo(999)
print(obj1.num)     # 666
print(obj1.a1)      # 1

obj1.num = 18
obj1.a1 = 99

print(obj1.num)     # 18
print(obj1.a1)      # 99

print(obj2.a1)      # 1
print(obj2.num)     # 999
print(Foo.a1)       # 1
print(obj1.a1)      # 99
'''

# 12.看代码写结果，注意返回值。
'''
class Foo(object):

    def f1(self):
        return 999

    def f2(self):
        v = self.f1()   # v = 999
        print('f2')
        return v

    def f3(self):
        print('f3')
        return self.f2()

    def run(self):
        result = self.f3()
        print(result)

obj = Foo()
v1 = obj.run()  # f3    f2  999
print(v1)       # None
'''

# 13.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):

    def f1(self):
        print('f1')

    @staticmethod
    def f2():
        print('f2')

obj = Foo()
obj.f1()    # f1
obj.f2()    # f2

Foo.f1()    # 报错
Foo.f2()    # f2
'''

# 14.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):

    def f1(self):
        print('f1')

    @classmethod
    def f2(cls):
        print('f2')

obj = Foo()
obj.f1()    # f1
obj.f2()    # f2

Foo.f1()    # 报错
Foo.f2()    # f2
'''

# 15.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):

    def f1(self):
        print('f1')
        self.f2()
        self.f3()

    @classmethod
    def f2(cls):
          print('f2')

    @staticmethod
    def f3():
          print('f3')

obj = Foo()
obj.f1()    # f1    f2  f3
'''

# 16.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Base(object):
    @classmethod
    def f2(cls):
          print('f2')

    @staticmethod
    def f3():
          print('f3')

class Foo(object):
    def f1(self):
        print('f1')
        self.f2()
        self.f3()

obj = Foo()
obj.f1()        # f1    然后报错
'''

# 17.看代码写结果
'''
class Foo(object):
    def __init__(self, num):
        self.num = num

v1 = [Foo for i in range(10)]
v2 = [Foo(5) for i in range(10)]
v3 = [Foo(i) for i in range(10)]

print(v1)       # [十个Foo函数]
print(v2)       # [十个不同的Foo函数地址]
print(v3)       # [十个不同的Foo函数地址]
'''

# 18.看代码写结果
'''
class StarkConfig(object):

    def __init__(self, num):
        self.num = num

    def changelist(self, request):
        print(self.num, request)


config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    print(item.num)     # 1 2   3
'''

# 19.看代码写结果
'''
class StarkConfig(object):

    def __init__(self, num):
        self.num = num

    def changelist(self, request):
        print(self.num, request)


config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    item.changelist(666)        # 1 666     2 666   3 666
'''

# 20.看代码写结果
'''
class Department(object):
    def __init__(self, title):
        self.title = title

class Person(object):
    def __init__(self, name, age, depart):
        self.name = name
        self.age = age
        self.depart = depart


d1 = Department('人事部')
d2 = Department('销售部')

p1 = Person('武沛齐', 18, d1)
p2 = Person('alex', 18, d1)
p3 = Person('安安', 19, d2)

print(p1.name)      # 武沛齐
print(p2.age)       # 18
print(p3.depart)    # d2地址
print(p3.depart.title)      # 销售部
'''


# 21.看代码写结果
'''
class Department(object):
    def __init__(self, title):
        self.title = title

class Person(object):
    def __init__(self, name, age, depart):
        self.name = name
        self.age = age
        self.depart = depart

    def message(self):
        msg = "我是%s,年龄%s,属于%s" % (self.name, self.age, self.depart.title)
        print(msg)


d1 = Department('人事部')
d2 = Department('销售部')

p1 = Person('武沛齐', 18, d1)
p2 = Person('alex', 18, d1)
p1.message()        # 我是武沛齐,年龄18,属于人事部
p2.message()        # 我是alex,年龄18,属于人事部
'''

# 22.编写类完成以下的嵌套关系
"""
角色：学校、课程、班级
要求：
1. 创建北京、上海、深圳三所学校。
2. 创建课程
    北京有三种课程：Linux、Python、Go
    上海有两种课程：Linux、Python
    深圳有一种课程：Python
3. 创建班级(班级包含：班级名称、开班时间、结课时间、班级人数)
    北京Python开设：21期、22期
    北京Linux开设：2期、3期
    北京Go开设：1期、2期
    上海Python开设：1期、2期
    上海Linux开设：2期
    深圳Python开设：1期、2期
"""
class School:
    def __init__(self, add):
        self.add = add

class Lesson:
    def __init__(self, school_obj, les):
        self.school = school_obj
        self.les = les

class Grade:
    def __init__(self, lesson_obj, *args):
        self.lesson = lesson_obj
        self.qi = args


s1 = School('北京')
s2 = School('上海')
s3 = School('深圳')

l1 = Lesson(s1, ['Linux', 'Python', 'Go'])
l2 = Lesson(s2, ['Linux', 'Python'])
l3 = Lesson(s3, ['Python'])

g1 = Grade(l1.les, '21期', '22期','1.1','2.2','80')
g2 = Grade(l2.les, '2期', '3期','1.1','2.2','80')
g3 = Grade(l3.les, '1期', '2期','1.1','2.2','80')

print(s1.add, l1.les[1], g1.qi)
print(s1.add, l1.les[0], g1.qi)
print(s1.add, l1.les[2], g1.qi)

