# day36笔记

## 1.回顾

### 1.锁 --- 都能维护线程间的数据安全

- 互斥锁 : 不能再一个线程中连续acquire,开销小
- 递归锁 : 可以连续在一个线程中acquire多次,acquire几次就release几次,开销大
- 死锁现象:
  - 在某一些线程中出现陷入阻塞并且永远无法结束阻塞的情况就是死锁现象.
  - 出现死锁:
    - 多把锁交替使用
    - 互斥锁在一个线程中连续acquire
  - 避免死锁
    - 在一个线程中只有一把锁,并且每一次acquire之后都要release.
  - 数据不安全的现象: += -= *= /=和多个线程对同一个文件进行写操作.

### 2.队列

- 先进先出 Queue
- 后进先出 LifoQueue
- 优先级 PriorityQueue

### 3.池

#### 3.1开线程/进程还是池?

- 如果只是开启一个子线程做一件事,就可以单独开线程
- 有大量的任务等待程序去做,要达到一定的并发数,开启进程池
- 根据程序的io操作判断用不用池:
  - 以下情况不用: socket的server端,大量阻塞io,recv,recvfrom,socketserver
  - 以下用:爬虫

#### 3.2.回调函数

- 执行完子线程任务之后直接调用对应的回调函数
- 爬取网页 需要等待数据传输和网络上的响应高IO的 -- 子线程
- 分析网页 没有什么IO操作 -- 这个操作没必要在子线程完成，交给回调函数

#### 3.3. ThreadPoolExecutor常用方法

- tp = ThreadPoolExecutor(cpu*5)

- obj = submit (需要在子线程执行的函数名,参数)
- obj
  - 1.获取返回值 obj.result() 是一个阻塞方法
  - 2.绑定回调函数 obj.add_done_callback(子线程执行完毕之后要执行的代码对应的函数)
- ret = map(需要在子线程执行的函数名,iterable)
  - 1.迭代ret,总是能得到所有的返回值
- shutdown
  - tp.shutdown()  阻塞整个池

### 4.进程池 和线程池都有锁

- 所有在线程中能工作的基本都不能在进程中工作.
- 在进程中能够使用的基本在线程中也能使用

### 5.多线程起多进程

```python
import os
from multiprocessing import Process
from threading import Thread

def tfunc():
    print('线程', os.getpid())
def pfunc():
    print('进程-->',os.getpid())
    for i in range(10):
        Thread(target=tfunc).start()    # 进程函数中开线程

if __name__ == '__main__':
    Process(target=pfunc).start()       # 开进程
```



## 2.重点

### 2.1操作系统

- 1.计算机中所有的资源都是由操作系统分配的
- 2.操作系统调度任务：时间分片、多道机制
- 3.CPU的利用率是我们努力的指标

### 2.2并发

#### 2.2.1进程

- 开销大 数据隔离 资源分配单位 cpython下可以利用多核
- 三状态 : 就绪 运行 阻塞
- multiprocessing模块
  - Process-开启进程
  - Lock-互斥锁
    - 为什么在进程中加锁----因为进程操作文件也会发生数据不安全
  - Queue-队列  IPC机制（Pipe,redis,memcache,rabbitmq,kafka）
    - 生产者消费者模型
  - Manager-提供数据共享机制

#### 2.2.2线程

- 开销小 数据隔离 cpu调度单位  cpython下不能利用多核
- GIL锁
  - 全局解释器锁
  - cpython解释器提供的
  - 导致了一个进程中多个线程同时只有一个线程能访问CPU --- 多线程不能利用多核.
- threading
  - Thread类,能开启线程start，等待线程结束join
  - Lock-互斥锁,不能在一个线程中连续acquire，效率相对高
  - Rlock-递归锁,可以在一个线程中连续acquire，效率相对低
- 线程队列 queue模块
  - Queue先进先出
  - LifoQueue后进先出
  - PriorityQueue优先级队列

#### 2.2.3池

- 导入:from concurrent.future import ThreadPoolExecutor/ProcessPoolExecutor
- 实例化 tp = ThreadPoolExecutor(num),pp = ProcessPoolExecutor(num)
- 提交任务到池中  返回一个对象 obj=tp.submit(func,arg1,arg2...)
  - 使用这个对象获取返回值 obj.result()
  - 回调函数 obj.add_done_callback(回调函数)
- 阻塞等待池中的任务都结束  tp.shutdown()



## 3.协程

- 协程 : 用户级别的,由我们自己写的python代码来控制切换的 , 是操作系统不可见的
- 在一个线程下的多个任务之间来回切换,那么每一个任务都是一个协程.
- 在Cpython解释器下 (协程和线程都不能利用多核,都是在一个CPU上轮流执行.)
  - 由于多线程本身就不能利用多核,所以即便是开启了多个线程也只能轮流在一个CPU上执行.
  - 协程如果把所有任务的IO操作都规避掉,只剩下需要使用CPU的操作,就意味着协程就可以做到提高CPU利用率的效果.
- 线程和协程
  - 线程  切换需要操作系统,开销大,操作系统不可控,给操作系统的压力大
    - 操作系统对IO操作的感知更加灵敏
  - 协程  切换需要python代码,开销小,用户操作可控,完全不会增加操作系统的压力
    - 用户级别能够对IO操作的感知比较低

## 4.切换问题和greenlet模块

- 两种切换方式:
  - 原生python完成  yield  asyncio
  - C语言完成的python模块

```python
import time
from  greenlet import greenlet

def eat():
    print('wusir is eating')
    time.sleep(0.5)
    g2.switch()         # 执行g2
    print('wusir finished eat')

def sleep():
    print('小马哥 is sleeping')
    time.sleep(0.5)
    print('小马哥 finished sleep')
    g1.switch()         # 继续g1刚才的位置执行

g1 = greenlet(eat)
g2 = greenlet(sleep)
g1.switch()     # 打开开关
```



## 5.gevent模块

- .spawn(函数) 创建协程任务
- from gevent import monkey
  - monkey.patch_all()	让gevent识别所有io操作(默认不识别)
- join()   阻塞 直到任务完成

```python
import time
import gevent
from gevent import monkey
monkey.patch_all()	# 规避所有io操作
def eat():
    print('wusir is eating')
    time.sleep(1)       # 遇到io自动切换
    print('wusir finished eat')

def sleep():
    print('小马哥 is sleeping')
    time.sleep(1)       # 遇到io自动切换
    print('小马哥 finished sleep')

g1 = gevent.spawn(eat)  # 创造一个协程任务
g2 = gevent.spawn(sleep)  # 创造一个协程任务
g1.join()   # 阻塞 直到g1任务完成为止
g2.join()   # 阻塞 直到g1任务完成为止
```

- gevent.joinall([g1,g2])	阻塞列表中的协程
- joinall([])   阻塞列表所有协程

```python
import time
import gevent
from gevent import monkey
monkey.patch_all()
def eat():
    print('wusir is eating')
    time.sleep(1)
    print('wusir finished eat')

def sleep():
    print('小马哥 is sleeping')
    time.sleep(1)
    print('小马哥 finished sleep')

# g1 = gevent.spawn(eat)  # 创造一个协程任务
# g3 = gevent.spawn(eat)  # 创造一个协程任务
# g2 = gevent.spawn(sleep)  # 创造一个协程任务
# gevent.joinall([g1,g2,g3])
g_l = []
for i in range(10):		# 起10个协程
    g = gevent.spawn(eat)
    g_l.append(g)	# 加到列表
gevent.joinall(g_l)
```

- .value    获取返回值

```python
import time
import gevent
from gevent import monkey
monkey.patch_all()
def eat():
    print('wusir is eating')
    time.sleep(1)
    print('wusir finished eat')
    return 'wusir***'

def sleep():
    print('小马哥 is sleeping')
    time.sleep(1)
    print('小马哥 finished sleep')
    return '小马哥666'

g1 = gevent.spawn(eat)
g2 = gevent.spawn(sleep)
gevent.joinall([g1,g2])
print(g1.value)		# wusir***
print(g2.value)		# 小马哥666
```



## 6.asyncio模块

- 是一个消息循环。我们从`asyncio`模块中直接获取一个`EventLoop`的引用，然后把需要执行的协程扔到`EventLoop`中执行，就实现了异步IO。

```python
import asyncio

async def hello():
    print("Hello world!")
    # 异步调用asyncio.sleep(1):
    await asyncio.sleep(1)
    print("Hello again!")

# 获取EventLoop:
loop = asyncio.get_event_loop()
# 执行coroutine
loop.run_until_complete(hello())
loop.close()
```

- 执行多个任务

```python
import asyncio

async def hello():
    print("Hello world!")
    await asyncio.sleep(1)
    print("Hello again!")
    return 'done'

loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.wait([hello(),hello()]))
loop.close()
```

- 获取单个返回值

```python
import asyncio

async def hello():
    print("Hello world!")
    await asyncio.sleep(1)
    print("Hello again!")
    return 'done'

loop = asyncio.get_event_loop()
task = loop.create_task([hello()])
loop.run_until_complete(task)
ret = task.result()
print(ret)
```

- 获取多个任务返回值

```python
import asyncio

async def hello(i):
    print("Hello world!")
    await asyncio.sleep(i)  # 睡i秒
    print("Hello again!")
    return 'done',i

loop = asyncio.get_event_loop()
task1 = loop.create_task(hello(2))
task2 = loop.create_task(hello(1))
task3 = loop.create_task(hello(3))
task_l = [task1,task2,task3]
tasks = asyncio.wait(task_l)
loop.run_until_complete(tasks)
for t in task_l:
    print(t.result())
```

- 按返回顺序获取返回值

```python
import asyncio

async def hello(i):
    print("Hello world!")
    await asyncio.sleep(i)
    print("Hello again!")
    return 'done',i


async def main():
    tasks = []
    for i in range(10):
        tasks.append(asyncio.ensure_future(hello(10-i)))
    for res in asyncio.as_completed(tasks):
        result = await res
        print(result)
loop = asyncio.get_event_loop()
loop.run_until_complete(main())
```

- asyncio使用协程完成http访问

```python
import asyncio

async def get_url():
    reader,writer = await asyncio.open_connection('www.baidu.com',80)
    writer.write(b'GET / HTTP/1.1\r\nHOST:www.baidu.com\r\nConnection:close\r\n\r\n')
    all_lines = []
    async for line in reader:
        data = line.decode()
        all_lines.append(data)
    html = '\n'.join(all_lines)
    return html

async def main():
    tasks = []
    for url in range(20):
        tasks.append(asyncio.ensure_future(get_url()))
    for res in asyncio.as_completed(tasks):
        result = await res
        print(result)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())  # 处理一个任务
    loop.run_until_complete(asyncio.wait([main()]))  # 处理多个任务

    task = loop.create_task(main())  # 使用create_task获取返回值
    loop.run_until_complete(task)
    loop.run_until_complete(asyncio.wait([task]))
```


