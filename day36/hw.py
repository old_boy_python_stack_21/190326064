#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.协程搞一个爬虫 'http://www.baidu.com'
'''
import gevent
import requests


def eat(url):
    msg = requests.get(url)
    print(msg.text)
    return 'Done'


url = 'https://www.baidu.com'
g1 = gevent.spawn(eat, url)
g1.join()
print(g1.value)
'''

# 线程部分：读程序
# 1、程序从flag a执行到falg b的时间大致是多少秒？
'''
import threading
import time
def _wait():
	time.sleep(60)
# flag a
t = threading.Thread(target=_wait,daemon = False)
t.start()
# flag b
'''
'''
daemon守护进程关闭,不随主程序停止而停止,执行60秒
'''

# 2、程序从flag a执行到falg b的时间大致是多少秒？
'''
import threading
import time
def _wait():
	time.sleep(60)
# flag a
t = threading.Thread(target=_wait,daemon = True)
t.start()
# flag b
'''
'''
daemon守护进程开启,随主程序停止而停止,执行0秒
'''

# 3、程序从flag a执行到falg b的时间大致是多少秒？
'''
import threading
import time
def _wait():
	time.sleep(6)
# flag a
t = threading.Thread(target=_wait,daemon = True)
t.start()
t.join()
# flag b
'''
'''
守护进程开启,t.join等待进程线程结束,60s
'''

# 4、读程序，请确认执行到最后number是否一定为0    # 1E7=10**7
'''
import threading

loop = int(1E6)

def _add(loop: int = 1):
    global number
    for _ in range(loop):
        number += 1

def _sub(loop: int = 1):
    global number
    for _ in range(loop):
        number -= 1

number = 0
ta = threading.Thread(target=_add, args=(loop,))
ts = threading.Thread(target=_sub, args=(loop,))
ta.start()
ts.start()
ta.join()
ts.join()
print(number)
'''
'''
不一定为0,多个线程对同一数据操作,数据不安全
'''

# 5、读程序，请确认执行到最后number是否一定为0
'''
import threading
loop = int(1E6)


def _add(loop: int = 1):
    global number
    for _ in range(loop):
        number += 1


def _sub(loop: int = 1):
    global number
    for _ in range(loop):
        number -= 1


number = 0
ta = threading.Thread(target=_add, args=(loop,))
ts = threading.Thread(target=_sub, args=(loop,))
ta.start()
ta.join()
ts.start()
ts.join()
print(number)
'''
'''
一定为0,单个线程执行结束开始执行下个线程,数据安全.
'''

# 7、读程序，请确认执行到最后number的长度是否一定为1     # 1E-8=10**(-8)
'''
import time
import threading

loop = int(1E7)


def _add(loop: int = 1):
    global numbers
    for _ in range(loop):
        numbers.append(0)


def _sub(loop: int = 1):
    global numbers
    while not numbers:
        time.sleep(1E-8)
    numbers.pop()


numbers = [0]
ta = threading.Thread(target=_add, args=(loop,))
ts = threading.Thread(target=_sub, args=(loop,))
ta.start()
ta.join()
ts.start()
ts.join()
print(len(numbers))
'''
'''
一定不为1,没有循环删除.
'''

# 8、读程序，请确认执行到最后number的长度是否一定为1
'''
import time
import threading

loop = int(1E7)


def _add(loop: int = 1):
    global numbers
    for _ in range(loop):
        numbers.append(0)


def _sub(loop: int = 1):
    global numbers
    while not numbers:
        time.sleep(1E-8)
    numbers.pop()


numbers = [0]
ta = threading.Thread(target=_add, args=(loop,))
ts = threading.Thread(target=_sub, args=(loop,))
ta.start()
ts.start()
ta.join()
ts.join()
print(len(numbers))
'''
'''
一定不为1,没有循环删除.
'''

# 协程
# 1、什么是协程？常用的协程模块有哪些？
'''
由用户程序控制,在一个线程下多个任务之间来回切换,每一个任务都是一个协程.
gevent模块,greenlet模块
'''

# 2、协程中的join是用来做什么用的？它是如何发挥作用的？
'''
阻塞,直到任务完成

'''

# 3、使用协程实现并发的tcp server端
'''
import gevent
import socket
from gevent import monkey
monkey.patch_all()

sk = socket.socket()
sk.bind(('127.0.0.1', 8080))
sk.listen()


def func(conn):
    while 1:
        msg = conn.recv(1024).decode('utf-8')
        print(msg)

glst = []
for i in range(3):
    conn, _ = sk.accept()
    g = gevent.spawn(func, conn)
    glst.append(g)
gevent.joinall(glst)
print('Done')
'''

# 4、在一个列表中有多个url，请使用协程访问所有url，将对应的网页内容写入文件保存
'''
import gevent
import requests

url_lst = ['https://www.baidu.com', 'https://www.douban.com', 'https://www.tencent.com']

def func(url):
    msg = requests.get(url)
    filename = url.split('//')[-1]
    with open(filename, 'w', encoding='utf-8')as f:
        f.write(msg.text)


glst = []
for url in url_lst:
    g = gevent.spawn(func, url)
    glst.append(g)
gevent.joinall(glst)
print('Done')
'''

# 综合
# 1、进程和线程的区别
'''
进程:开销大,数据隔离,资源分配单位,cpython下可以利用多核.
线程:开销小,数据共享,cpu调度的最小单位,cpython下不能利用多核.
'''

# 2、进程池、线程池的优势和特点
'''
定义一个池子,设置固定数量的进程/线程,调用时直接从池中取出来处理任务,处理完毕在放回池中,同时只能有固定数量的任务执行,
省去开启关闭进程/线程的时间.降低操作系统调度难度.
'''

# 3、线程和协程的异同?
'''
异:
1.线程切换由操作系统执行,协程切换由人为写代码执行.
2.线程开销大,协程开销小.
3.线程操作系统进行,不可控.协程人为代码进行,可控.
4.线程给操作系统压力大,协程不会增加操作系统压力.
同:
在Cpython解释器下,协程和线程都不能利用多核,都是在一个CPU上轮流执行.
'''

# 4、请简述一下互斥锁和递归锁的异同？
'''
异:
1.互斥锁开销小,递归锁开销大.
2.互斥锁不能连续多次acquire,递归锁可以.
同:
都能维护线程间的数据安全
'''

# 5、请列举一个python中数据安全的数据类型？
'''
列表和字典相对安全,一般需要对要操作的数据进行加锁处理.
'''

# 6、Python中如何使用线程池和进程池
'''
进程池:from concurrent.futures import ProcessPoolExecutor
p = ProcessPoolExecutor(进程数量)
p.submit(函数, 参数)
线程池:from concurrent.futures import ThreadPoolExecutor
t = ThreadPoolExecutor(线程数量)
t.submit(函数, 参数)
'''

# 7、简述 进程、线程、协程的区别 以及应用场景？
'''
进程:开销大,数据隔离,资源分配单位,cpython下可以利用多核.场景:允许多个任务同时运行.
线程:开销小,数据共享,cpu调度单位,cpython下不能利用多核.场景:允许单个任务分成不同的部分运行
协程:开销最小,人为代码操作任务的切换.场景:IO操作较多.
'''

# 8、什么是并行，什么是并发？
'''
并行:多个程序,多个cpu,一个cpu上运行一个程序
并发:指多个程序,公用一个cpu轮流使用
'''
