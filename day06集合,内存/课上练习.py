#!/usr/bin/env python
# -*- coding:utf-8 -*-

# li = [1,6,2,4,4,8,9]
# li.reverse()
# print(li)

# dic = {'k1':'v1','k2':'v2'}
# msg = input('请输入键值对:')
# k,v = msg.split(':')
# value = dic.get(k)        # value = dic[k]
# if value == v:
#     print('yes')
# else:
#     print('no')

# a = (1)
# b = (1)
# print(type(a))
# print(a is b)


v1 = [11,22,33]
v2 = v1
v1 = [1,2,3,4]
print(v2)


v1 = 'alex'
v2 = v1
v1 = 'oldboy'
print(v1,v2)

