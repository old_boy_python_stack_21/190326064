#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.列举你了解的字典中的功能（字典独有）。
'''
dic.keys() 获取所有key
dic.values() 获取所有value
dic.items() 获取所有键值对
dic.get(key) 得到key对应的value
dic.pop(key) 删除key对应的键值对,返回key对应的value
dic.update() 更新()内的内容到dic中.有则更新,无则添加.
'''

# 2.列举你了解的集合中的功能（集合独有）。
'''
.add() 添加
.discard(key) 删除
.update() 更新
.intersection() 交集
.union() 合集
.difference() 差集
.symmetric_difference() 对称差集
'''
# 3.列举你了解的可以转换为 布尔值且为False的值。
'''
0,'',[],(),{},set()
'''

# 4.请用代码实现
# info = {'name':'王刚蛋','hobby':'铁锤'}
# 4.1循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出。
'''
while 1:
    a = input('请输入键:')
    print(info.get(a))
'''
# 4.2循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出（如果key不存在，则获取默认“键不存在”，并输出）。
#   注意：无需考虑循环终止（写死循环即可）
'''
while 1:
    a = input('请输入键:')
    if a in list(info.keys()):
        print(info.get(a,'键不存在'))
    else:
        print('键不存在')
'''

# 5.请用代码验证 "name" 是否在字典的键中？
'''
info = {'name': '王刚蛋', 'hobby': '铁锤', 'age': '18', }
if 'name' in info.keys():
    print('yes')
else:
    print('no')
'''

# 6.请用代码验证 "alex" 是否在字典的值中？
'''
info = {'name': '王刚蛋', 'hobby': '铁锤', 'age': '18', }
if 'alex' in list(info.values()):
    print('yes')
else:
    print('no')
'''

# 7.有如下
# v1 = {'武沛齐','李杰','太白','景女神'}
# v2 = {'李杰','景女神'}
# 7.1请得到 v1 和 v2 的交集并输出
'''
v = v1.intersection(v2)
print(v)
'''
# 7.2请得到 v1 和 v2 的并集并输出
'''
v = v1.union(v2)
print(v)
'''
# 7.3请得到 v1 和 v2 的 差集并输出
'''
v = v1.difference(v2)
print(v)
'''
# 7.4请得到 v2 和 v1 的 差集并输出
'''
v = v2.difference(v1)
print(v)
'''

# 8.循环提示用户输入，并将输入内容追加到列表中（如果输入N或n则停止循环）
'''
lst = []
while 1:
    a = input('请输入:')
    if a.upper() == 'N':
        break
    lst.append(a)
print(lst)
'''

# 9.循环提示用户输入，并将输入内容添加到集合中（如果输入N或n则停止循环）
'''
a = set()
while 1:
    msg = input('请输入:')
    if msg.upper() == 'N':
        break
    a.add(msg)
print(a)
'''

# 10.写代码实现
# 循环提示用户输入，如果输入值在v1中存在，则追加到v2中，如果v1中不存在，则添加到v1中。（输入N或n则停止循环）
'''
v1 = {'alex', '武sir', '肖大'}
v2 = []
while 1:
    a = input('请输入:')
    if a.upper() == 'N':
        break
    elif a in v1:
        v2.append(a)
    else:
        v1.add(a)
print(v1,v2)
'''

# 11.判断以下值那个能做字典的key ？那个能做集合的元素？
# 1 字典的key 集合的元素
# -1 字典的key 集合的元素
# "" 字典的key 集合的元素
# None 字典的key 集合的元素
# [1,2] 不可哈希,都不行
# (1,) 字典的key 集合的元素
# {11,22,33,4} 不可哈希,都不行
# {'name':'wupeiq','age':18} 不可哈希,都不行

# 12.is 和 == 的区别？
'''
is 判断两者内存地址是否相同
== 判断两者的值是否相同
'''

# 13.type使用方式及作用？
'''
使用方式:type(内容) 
作用:输出()里内容的数据类型
'''

# 14.id的使用方式及作用？
'''
使用方式:id(内容)
作用:查看内容的内存地址
'''

# 15.看代码写结果并解释原因
'''
v1 = {'k1': 'v1', 'k2': [1, 2, 3]}
v2 = {'k1': 'v1', 'k2': [1, 2, 3]}

result1 = v1 == v2  # True
result2 = v1 is v2  # False
print(result1)
print(result2)

# v1==v2判断值是否相等,is判断内存地址是否相等
'''

# 16.看代码写结果并解释原因
'''
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = v1

result1 = v1 == v2  # True
result2 = v1 is v2  # True
print(result1)
print(result2)

# v1,v2指向同一地址
'''

# 17.看代码写结果并解释原因
'''
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = v1

v1['k1'] = 'wupeiqi'
print(v2)  # {'k1':'wupeiqi','k2':[1,2,3]}

# 修改原地址的值,指向同一地址的值都会改变.
'''

# 18.看代码写结果并解释原因
'''
v1 = '人生苦短，我用Python'
v2 = [1, 2, 3, 4, v1]
v1 = "人生苦短，用毛线Python"
print(v2)  # [1, 2, 3, 4, '人生苦短，我用Python']

# v2[-1]指向v1原地址,v1被赋予新值改变地址,不影响v2指向的原地址.
'''

# 19.看代码写结果并解释原因
'''
info = [1, 2, 3]
userinfo = {'account': info, 'num': info, 'money': info}
info.append(9)
print(userinfo)  # {'account': [1, 2, 3,9], 'num': [1, 2, 3,9], 'money': [1, 2, 3,9]}
info = "题怎么这么多"
print(userinfo)  # {'account': [1, 2, 3,9], 'num': [1, 2, 3,9], 'money': [1, 2, 3,9]}

# 第三行info改变原值指向info的都会受影响,第五行变量赋值不影响已经改变的userinfo。
'''

# 20.看代码写结果并解释原因
'''
info = [1, 2, 3]
userinfo = [info, info, info, info, info]
info[0] = '不仅多，还特么难呢'
print(info, userinfo)  # ['不仅多，还特么难呢', 2, 3] [['不仅多，还特么难呢', 2, 3], ['不仅多，还特么难呢', 2, 3], ['不仅
# 多，还特么难呢', 2, 3], ['不仅多，还特么难呢', 2, 3], ['不仅多，还特么难呢', 2, 3]]

# userinfo中都指向info,info的第0项改变会影响userinfo.
'''

# 21.看代码写结果并解释原因
'''
info = [1, 2, 3]
userinfo = [info, info, info, info, info]
userinfo[2][0] = '闭嘴'
print(info, userinfo)  # ['闭嘴', 2, 3] [['闭嘴', 2, 3], ['闭嘴', 2, 3], ['闭嘴', 2, 3], ['闭嘴', 2, 3], ['闭嘴', 2, 3]]

# userinfo第二项的第0项被赋值,也就是info[0]改变,影响info.
'''

# 22.看代码写结果并解释原因
'''
info = [1, 2, 3]
user_list = []
for item in range(10):
    user_list.append(info)  # user_list = [info*10]
info[1] = "是谁说Python好学的？"
print(user_list)  # user_list = [[1,"是谁说Python好学的？",3]*10]

# for循环user_list添加info十次,info[1]改变原值,user_list也改变.
'''

# 23.看代码写结果并解释原因
'''
data = {}
for i in range(10):
    data['user'] = i
print(data)  # {'user':9}

# for循环date添加key为user,i为value十次,字典中存在则替换,i最后一次循环是9.
'''

# 24.看代码写结果并解释原因
'''
data_list = []
data = {}
for i in range(10):
    data['user'] = i    # data = {'user':i}
    data_list.append(data)  # (data)指向{'user':i}
print(data_list)    # data = [{'user':9}*10]

# data.list.append(data)指向{'user':i},最后一次循环i=9,修改了data的内容,输出的内容受影响.
'''

# 25.看代码写结果并解释原因
'''
data_list = []
for i in range(10):
    data = {}  # 每次循环都是新的字典
    data['user'] = i
    data_list.append(data)
print(data_list)  # [{'user': 0},{'user': 1},...{'user': 9}]

# 每次循环都是新的字典,新地址,所以原来添加进去的data不受影响.
'''