# day06笔记

## 1.补充

### 1.列表

1.列表反转:.reverse()

2.列表排序:.sort()默认升序

​	sort(reverse=True)降序

### 2.字典

1.keys / values / items

2..get('key') 根据key得到value

3..pop('key') 根据key删除键值对,可以返回value , del 删除无返回值

4.update() 更新()内的内容到字典,有则替换,无则添加.

## 2.集合

### 1.特点

无序的,且不重复.

### 2.独有功能

.add 添加

.discard 删除

.update 更新

.intersection 交集

.union 合集

.difference 差集

.symmetric_difference 对称差集

### 3.公共功能

len() 长度

for循环

## 3.内存相关

### 1.示例

#### 1.内部修改

``` python
a = [1,2,3]
b = a
a.append(666)
print(b) # b = [1,2,3,666]
```

#### 2.重新赋值

```python
v1 = 'alex'
v2 = v1
v1 = 'oldboy'
print(v2) # v2 = alex v1 = oldboy
```

### 2.==和is的区别

==:数值相等相同

is : 内存地址相同



