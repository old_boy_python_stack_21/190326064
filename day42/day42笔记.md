# day42笔记

## 如何创建html文件

html : 超文本标记语言

### 1.标签

- 1.双闭合标签<html></html>
- 2.单闭合标签<meta charset="UTF-8">

### 2.head标签

- meta 基本网站元信息标签

  ```html
  <meta 元信息>
  ```

- title 网站的标题

  ```html
  <title>内容</title>
  ```

- link 连接css文件

  ```html
  <link rel='当前文档与被链接文档之间的关系(stylesheet等)' href='被链接的文件位置'>
  ```

- script 链接JavaScript文件

  ```html
  <script src="js/index.js">
  # 'index中设置var a = 2;alert(a);进网页前的弹出框'
  </script>
  ```

- style 内嵌样式

  ```html
  <style>
      body{backgroud-color:#666('颜色')}
  </style>
  ```

### 3.body标签

- h1-h6  标题标签 

  ```html
  <h1>内容</h1>
  ```

- p标签 段落标签

  ```html
  <p>内容</p>
  # &nbsp; 空格
  ```

- a  锚点 超链接标签

  - href 链接的网址
  - title 鼠标悬浮上的标题
  - style 行内样式
  - target 目标
    - 默认是_self 在当前页面中打开新链接
    - _blank 在新的空白页面打开新的链接

  ```html
  <h2(设置a的标题样式) style="background-color:背景色;">
  <a href='要跳转的网页' target='打开方式' style='text-decoration:none(设置无下划线);color:red(设置字体颜色);' title:'悬浮在内容上的提示'>显示的内容</a>
  </h2>
  
  <a href='#(空链接)/#addr)'>回到顶部</a>
  # 在要跳转的位置写入<h6 id="addr"></h6>
  ```

- 字体标签

  ```html
  <u>mjj</u>	# 下划线
  <strong>mjj</strong>	# 加粗
  <em>mjj</em>	# 斜体
  <i>mjj</i>	# 斜体
  ```

- img

  - src 链接的图片资源
  - title 标题
  - style 行内属性(每个标签都有)
  - alt 图片加载失败时显示的文本

  ```html
  <a href='要跳转的网页',title='悬浮在内容上的提示'>
  <img src="图片地址" alt="图片加载失败显示文本" width="200(按比例缩放,不写为原比例)">
  </a>
  
  <br>换行无间隙
  <hr>表示分割线
  ```

- ul 无序列表

  ```html
  <ul type='square(实心方点)/circle(空心圆点)/不写为实心圆点'>
      <li>第一项</li>
      <li>
      	<a href="网址">跳转</a>
      </li>
  </ul>
  ```

- ol 有序列表

  ```html
  <ol type='a(表自动排序abc)/1(123)/i(i ii iii)/A(ABC)/3(345)'>
      <li>第一项</li>
      <li>第二项</li>
  </ol>
  ```

- table 表格

  

  - | id   | name  | age  | sex  |
    | ---- | ----- | ---- | ---- |
    | 1    | peiqi | 19   | 女   |


  ```html
  <table border(边框)="1" cellspacing(单元格间距)="0" width(左右填充)="100%">
      <tr>	# 行
          <td>id</td>
          <td>name</td>
          <td>age</td>
          <td>sex</td>
      </tr>
      <tr>
          <td>1</td>
          <td>沛齐</td>
          <td>19</td>
          <td>女</td>
      </tr>
  </table>
  ```

- form 表单

  - input  单行输入框
    - type 控件的类型
      - text 文本输入框
      - password 密码框
      - radio 单选框
      - checkbox 多选框
      - submit  提交
      - datetime-local  选择时间
    - name
      - 名称 提交服务器的键值对的name
    - value
      - 值 提交服务器的键值对的value
    - checked
      - 设置默认为值,checked='checked',=后面写不写都可以.
  - select   name  multiple 多选框
    - option  value
    - selected  默认选中
  - textarea  多行输入框(右下角可拖拽)
    - name
    - value
    - cols  列
    - rows   行

  ```html
  <form action="域名" method="get/post等提交方式" enctype='multipart/form-data'(传文件使用)>
      <!--文本输入框-->
      <p id="username">
          <input type="text(文本)" name="name" value="(用户输入的内容)">
      </p>
      <p id="password">
           <input type="password(密码)" name="pwd">
      </p>
      <h4>单选框</h4>
      <p>
          <input type="radio" name="sex"(设置互斥效果) checked = 'checked'(默认)>男
          <input type="radio" name="sex"(设置互斥效果)>女
      </p>
      <h4>我的爱好：</h4>
      <p>
          <input type="checkbox(多选)" name="a" value="抽烟(在地址栏显示)"> 抽烟
          <input type="checkbox" name="b" value="喝酒"> 喝酒
          <input type="checkbox" name="c" value="烫头"> 汤头
      </p>
      <h4>下拉列表</h4>
      <p>
          <select name="fav" multiple(多选)>
              <option value="smoke">抽烟</option>
              <option value="drink" selected>喝酒</option>
              <option value="tangtou">烫头</option>
              <option value="tangtou">烫头</option>
              <option value="tangtou">烫头</option>
              <option value="tangtou">烫头</option>
              <option value="tangtou">烫头</option>
              <option value="tangtou">烫头</option>
              <option value="tangtou">烫头</option>
          </select>
      </p>
      <input type="datetime-local(选择时间)">
      <p>
          <textarea name="" id="" cols="30(列数)" rows="10(行数)">		</textarea>
      </p>
      <p>
          <input type="submit(提交)" value="登录">
      </p>
  </form>
  ```

- html特征:
  - 1.对换行不敏感
  - 2.空白折叠

