from django.contrib import admin
from rbac import models


class PermissionConfig(admin.ModelAdmin):
    list_display = ['id', 'url', 'title']
    list_editable = ['url', 'title']


admin.site.register(models.Role)
admin.site.register(models.User)
admin.site.register(models.Permission, PermissionConfig)
