#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.sys.path.append("/root/mods")的作用？
"""
增加导入模块的查找路径
"""

# 2.字符串如何进行反转？
'''
[::-1]
'''

# 3.不用中间变量交换a和b的值。
'''
a = 1
b = 2
a, b = b, a
print(a, b)
'''

# 4.*args和**kwargs这俩参数是什么意思？我们为什么要用它。
'''
*args接收多个位置参数,**kwargs接收多个关键字参数.
func(*args,**kwargs)能接收任意类型任意个参数.
'''

# 5.函数的参数传递是地址还是新值？
'''
地址
'''

# 6.看代码写结果：
'''
my_dict = {'a': 0, 'b': 1}


def func(d):
    d['a'] = 1
    return d


func(my_dict)
my_dict['c'] = 2
print(my_dict)      # {'a':1,'b':1,'c':2}
'''

# 7.什么是lambda表达式
'''
匿名函数,能一行表达一个简单的函数
lambda x : y
x为传入参数,':'后面为返回值
'''

# 8.range和xrang有什么不同？
'''
在py2里,range(x)立即生成列表,xrange(x)不立即创建,边循环边创建.
在py3里,range即为py2的xrange,没有xrange
'''

# 9."1,2,3" 如何变成 ['1','2','3',]
'''
s = "1,2,3"
a = s.split(',')
print(a)
'''

# 10.['1','2','3'] 如何变成 [1,2,3]
'''
lst = ['1','2','3']
new_lst = [int(i) for i in lst]
print(new_lst)
'''

# 11.def f(a,b=[]) 这种写法有什么陷阱？
'''
b默认值为一个空列表,列表创建在函数内存之外,易被其他改动.
'''

# 12.如何生成列表 [1,4,9,16,25,36,49,64,81,100] ，尽量用一行实现。
'''
lst = [i*i for i in range(1,11)]
print(lst)
'''

# 13.python一行print出1~100偶数的列表, (列表推导式, filter均可)
'''
print([i for i in range(1, 101) if i % 2 == 0])

print(list(filter(lambda i: i % 2 == 0, [i for i in range(1, 101)])))
'''

# 14.把下面函数改成lambda表达式形式.
#
# def func():
#     result = []
#     for i in range(10):
#         if i % 3 == 0:
#             result.append(i)
#     return result
'''
ret = lambda: [i for i in range(10) if i % 3 == 0]
print(ret())
'''

# 15.如何用Python删除一个文件？
'''
# 删除文件
import os
os.remove(path)

# 删除目录
import shutil
shutil.rmtree(path)
'''

# 16.如何对一个文件进行重命名？
'''
import os

os.rename(filename, newname)
'''

# 17.python如何生成随机数？
'''
import random

a = random.randint(1, 10)
print(a)
'''

# 18.从0-99这100个数中随机取出10个数，要求不能重复，可以自己设计数据结构。
'''
import random

ret = set()
while len(ret) < 10:
    a = random.randint(0,99)
    ret.add(a)
print(ret)
'''

# 19.用Python实现 9*9 乘法表 （两种方式）
# 方法一
'''
for a in range(1, 10):
    for b in range(1, a + 1):
        if a >= b:
            print('%s*%s=%s' % (b, a, a * b),end=' ')
    print()
'''

# 方法二
'''
a = 1
while a < 10:
    b = 1
    while b <= a:
        print('%s*%s=%s' % (b, a, b * a), end=' ')
        if a == b:
            print()
        b += 1
    a += 1
'''

# 20.请给出下面代码片段的输出并阐述涉及的python相关机制。
'''
def dict_updater(k, v, dic={}):
    dic[k] = v
    print(dic)


dict_updater('one', 1)  # one给k,1给v,输出dic={'one':1}
dict_updater('two', 2)  # two给k,2给v,输出dic={'one':1,'two':1}
dict_updater('three', 3, {})    # three给k,3给v,空字典给dic,输出dic={'three':3}
# 形参为可变类型(字典,元组)时会在创建参数的外部创建元素,下次使用还会使用同一个元素.
'''

# 21.写一个装饰器出来。
'''
def wrapper(arg):
    def inner(*args,**kwargs):
        return arg(*args,**kwargs)
    return inner
'''

# 22.用装饰器给一个方法增加打印的功能。
'''
def wrapper(arg):
    def inner(*args,**kwargs):
        return print(arg(*args,**kwargs))
    return inner

@wrapper
def func():
    a = 123

func()
'''

# 23.请写出log实现(主要功能时打印函数名)
'''
def log(arg):
    def inner():
        print('call', arg.__name__+'()')
        return arg()
    return inner

@log
def now():
    print("2013-12-25")

now()

# 输出
# call now()
# 2013-12-25
'''

# 24.向指定地址发送请求，将获取到的值写入到文件中。
'''
import requests  # 需要先安装requests模块：pip install requests
import json

response = requests.get('https://www.luffycity.com/api/v1/course_sub/category/list/')
print(response.text)
new = json.loads(response.text)
# 获取结构中的所有name字段，使用逗号链接起来，并写入到 catelog.txt 文件中。
# s  = [
#     {'id': 1, 'name': 'Python', 'hide': False, 'category': 1},
#     {'id': 2, 'name': 'Linux运维', 'hide': False, 'category': 4},
#     {'id': 4, 'name': 'Python进阶', 'hide': False, 'category': 1},
#     {'id': 7, 'name': '开发工具', 'hide': False, 'category': 1},
#     {'id': 9, 'name': 'Go语言', 'hide': False, 'category': 1},
#     {'id': 10, 'name': '机器学习', 'hide': False, 'category': 3},
#     {'id': 11, 'name': '技术生涯', 'hide': False, 'category': 1}
# ]
lst = []
with open('catelog.txt', 'w', encoding='utf-8') as f:
    for i in new['data']:
        lst.append(i['name'])
    ret = ','.join(lst)
    f.write(ret)
'''

# 25.请列举经常访问的技术网站和博客.
'''
github
csdn
cnblogs
'''

# 26.请列举最近在关注的技术
'''
Python模块的使用
'''

# 27.请列举你认为不错的技术书籍和最近在看的书（不限于技术）
'''
无
'''
