# day15笔记

## 一.补充

- 任意位置终止程序:

```python
import sys
sys.exit()
```

- py2和py3区别
  - 2:xrange,不会立即创建,而是在循环时,边循环边创建.

    range,立即创建列表.

  - 3.range,同py2的xrange.

## 二.模块基本知识

### 1.内置模块

```python
import sys
print(sys.argv)
```

### 2.第三方模块

下载安装才能使用

cmd中pip install xxx

### 3.自定义模块

创建xxx.py,在另外的文件中import xxx.

## 三.内置模块

### 1.os

os.rename(a,b)	a重命名为b

os.mkdir	创建目录

os.makedirs	创建目录和子目录

os.path.join	路径拼接

os.path.dirname(file)	查看文件存在的目录

os.path.abspath	绝对路径

os.path.exists	判断路径是否存在

os.stat(文件路径)	查看文件大小

os.listdir	查看一层文件

os.walk	查看所以文件

### 2.sys

sys.argv	获取脚本运行时的参数

sys.path	默认Python导入模块时,会按照sys.path中的路径挨个查找.

```python
import sys
sys.path.append('D:\\')
import oldboy
```

### 3.json

特殊的字符串.

只有:int / str / list / dict / 

最外层必须是列表或字典,如果包含字符串,必须是双引号"".

序列化:将Python的值转换为json格式的字符串.

反序列化:将json格式的字符串转换成Python的数据类型.