# day61笔记

## 1.回顾

### 1.urlconf

```python
from django.conf.urls import url
from . import views

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^home/$', views.home,{}, name='home'),
]
```

### 2.正则表达式

- r  ^   $  [0-9]  \d  \w   + ?   * {4} 

### 3.分组和命名分组

url上捕获参数  都是字符串

- 分组
  - 捕获的参数会按照位置传参传递给视图函数

```python
url(r'^blog/([0-9]{4})/(\d{2})$', views.blogs, name='blogs'),
```

- 命名分组
  - 捕获的参数会按照关键字传参传递给视图函数

```python
url(r'^blog/(?P<year>[0-9]{4})/(?P<month>\d{2})$', views.blogs, name='blogs'),
```

### 4.路由分发 include

```python
from django.conf.urls import url, include
from django.contrib import admin
from app01 import views

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls', namespace='app01')),
    url(r'^app02/', include('app02.urls', namespace='app02')),
]
```

### 5.传递参数

```python
  url(r'^home/(?P<year>\d{4})$', views.home,{'year':2019}, name='home'),
```

### 6.url的命名和反向解析

- 静态路由

  - 命名：

  ```python
  url(r'^home/$', views.home, name='home'),
  ```

  - 反向解析

    - 模板

      - {% url 'home' %}    ——》   '/app01/home/'

    - py文件

      from django.urls import reverse

      reverse('home')    ——》    '/app01/home/'

- 分组：

  - 命名：

  ```python
  url(r'^blog/([0-9]{4})/(\d{2})$', views.blogs, name='blogs'),
  ```

  - 反向解析
    - 模板

      - {% url 'blogs' '2019' '06' %}    ——》    '/app01/blog/2019/06'

    - py文件

      from django.urls import reverse

      reverse('blogs,'args=('2019','07'))    ——》    '/app01/blog/2019/07'

- 命名分组：

  - 命名：

  ```python
  url(r'^blog/(?P<year>[0-9]{4})/(?P<month>\d{2})$', views.blogs, name='blogs'),
  ```

  - 反向解析：
    - 模板

    {% url 'blogs' '2019' '06' %}    ——》    '/app01/blog/2019/06'     按照位置传参

    {% url 'blogs'  month='06' year='2019' %}    ——》    '/app01/blog/2019/06'    按照关键字传参 

  - py文件

    from django.urls import reverse

    reverse('blogs,'args=('2019','07'))    ——》    '/app01/blog/2019/07'    按照位置传参

    reverse('blogs',kwargs={'year':'2018','month':'08'})     ——》   '/app01/blog/2018/08'     按照关键字传参 

### 7.namespace

```python
url(r'^app01/', include('app01.urls', namespace='app01')),
```

{% url 'app01:home' %}    ——》   '/app01/home/'

reverse('app01:home')    ——》    '/app01/home/'



## 2.今日内容

https://www.cnblogs.com/maple-shaw/articles/9323320.html

https://www.cnblogs.com/maple-shaw/articles/9403501.html

```
ORM
对象关系映射（Object Relational Mapping，简称ORM）模式是一种为了解决面向对象与关系数据库存在的互不匹配的现象的技术。
简单的说，ORM是通过使用描述对象和数据库之间映射的元数据，将程序中的对象自动持久化到关系数据库中。
```

### 1.ORM的字段和参数

```python
AutoField    自增,必填参数primary_key=True，则成为数据库的主键。无该字段时，django自动创建.一个model不能有两个AutoField字段。
#BigAutoField
IntegerField  整数,-2147483648 ~ 2147483647
#SmallIntegerField小整数,PositiveSmallIntegerField正小整数,PositiveIntegerField正整数,BigIntegerField长整型
CharField    字符串,必须提供max_length
#EmailFieldemail字符串,IPAddressFieldIP地址字符串,URLFieldurl字符串,FileField文件,ImageField图片
BooleanField  布尔值
#NullBooleanField可以为空的布尔值
TextField    文本
日期时间:
DateTimeField:YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]
DateField:YYYY-MM-DD
TimeField:HH:MM[:ss[.uuuuuu]]
    auto_now_add=True    # 新增数据的时候会自动保存当前的时间
    auto_now=True        # 新增、修改数据的时候会自动保存当前的时间
DecimalField:十进制的小数
	max_digits       小数总长度   5
    decimal_places   小数位长度   2
FloatField:浮点型
BinaryField:二进制类型
```

- 自定义字段查看网页博客
- 参数：
  - null                数据库中字段是否可以为空

  - blank=True    form表单填写时可以为空

  - default           字段的默认值

  - db_index       创建索引

  - unique           唯一
  - db_column    数据库中字段的列名
  - primary_key  数据库中字段是否为主键
  - verbose_name        Admin中显示的字段名称
  - editable         Admin中是否可以编辑
  - help_text       Admin中该字段的提示信息
  - choices=((0, '女'), (1, '男')     Admin中可填写的内容和提示

### 2.表的参数

```python
class Meta:
    # 表名
    db_table = "person"
    # admin中显示的表名称
    verbose_name = '个人信息'	
    verbose_name_plural = '所有信息'
	# 联合索引 
    index_together = [
        ("name", "age"),  # 应为两个存在的字段
    ]
    # 联合唯一索引
    unique_together = (("name", "age"),)   # 应为两个存在的字段
```

### 3.ORM查询(13条)   必知必会13条

#### 在python脚本中调用Django环境

```python
import os

if __name__ == '__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "BMS.settings")
    import django
    django.setup()

    from app01 import models

    books = models.Book.objects.all()
    print(books)
```

#### 方法

```Django
<1> all():                 查询所有结果
<2> get(**kwargs):         返回与所给筛选条件相匹配的对象，返回结果有且只有一个，如果符合筛选条件的对象超过一个或者没有都会抛出错误。
<3> filter(**kwargs):      它包含了与所给筛选条件相匹配的对象
<4> exclude(**kwargs):     它包含了与所给筛选条件不匹配的对象
<5> values(*field):        返回一个ValueQuerySet——一个特殊的QuerySet，运行后得到的并不是一系列model的实例化对象，而是一个可迭代的字典序列
<6> values_list(*field):   它与values()非常相似，它返回的是一个元组序列，values返回的是一个字典序列
<7> order_by(*field):      对查询结果排序
<8> reverse():             对查询结果反向排序，请注意reverse()通常只能在具有已定义顺序的QuerySet上调用(在model类的Meta中指定ordering或调用order_by()方法)。
<9> distinct():            从返回结果中剔除重复纪录(如果你查询跨越多个表，可能在计算QuerySet时得到重复的结果。此时可以使用distinct()，注意只有在PostgreSQL中支持按字段去重。)
<10> count():              返回数据库中匹配查询(QuerySet)的对象数量。
<11> first():              返回第一条记录
<12> last():               返回最后一条记录
<13> exists():             如果QuerySet包含数据，就返回True，否则返回False
```

#### 返回值

- 返回queryset对象
  - all() / filter() / exclude() / order_by() / reverse() / distinct()
- 返回特殊的queryset
  - values() 返回一个可迭代的字典序列
  - values_list() 返回一个可迭代的元祖序列
- 返回具体对象
  - get() / first() / last()
- 返回布尔值
  - exists()
- 返回数字
  - count()

### 4.单表双下划线

```python
ret = models.Person.objects.filter(pk__gt=1)   # gt/greater than,大于
ret = models.Person.objects.filter(pk__lt=3)   # lt/less than,小于
ret = models.Person.objects.filter(pk__gte=1)   # gte/greater than equal,大于等于
ret = models.Person.objects.filter(pk__lte=3)   # lte/less than equal,小于等于

ret = models.Person.objects.filter(pk__range=[2,3])   # range,范围
ret = models.Person.objects.filter(pk__in=[1,3,10,100])   # in,成员判断

ret = models.Person.objects.filter(name__contains='A')	  # 包含大写A
ret = models.Person.objects.filter(name__icontains='A')   # 忽略大小写

ret = models.Person.objects.filter(name__startswith='a')  # 以什么开头
ret = models.Person.objects.filter(name__istartswith='A') # 忽略大小写

ret = models.Person.objects.filter(name__endswith='a')  # 以什么结尾
ret = models.Person.objects.filter(name__iendswith='I') # 忽略大小写

ret  = models.Person.objects.filter(birth__year='2019')
ret  = models.Person.objects.filter(birth__contains='2018-06-24')
ret  = models.Person.objects.filter(phone__isnull=False)
```

### 5.外键的操作

从有外键的表查询叫正向查询,从被关联表查询叫反向查询

#### 正向

- 对象查找

  - obj.关联字段.字段

  - (外键表的对象.外键字段名.关联表的字段名)

    ```python
    book_obj = models.Book.objects.first()  # 第一本书对象
    print(book_obj.publisher)  # 得到这本书关联的出版社对象
    print(book_obj.publisher.name)  # 得到出版社对象的名称
    ```

- 字段查找

  - 关联字段__字段

  - (外键字段__被关联表的字段='要查询的内容')

    ```python
    ret = models.Book.objects.filter(pub__name='老男孩出版社')
    ```

#### 反向

- 对象查找

  - obj.表名_set

  - (被关联表的对象.外键表名_set)

    - 外键表中可以写入related_name='新名',作为外键表名使用.

    ```python
    publisher_obj = models.Publisher.objects.first()  # 找到第一个出版社对象
    books = publisher_obj.book_set.all()  # 找到第一个出版社出版的所有书
    titles = books.values_list("title")  # 找到第一个出版社出版的所有书的书名
    ```

- 字段查找

  - 表名__字段

  - (外键表名__外键表中的字段='要查询的内容')

    - 外键表中可以写入related_name='新名',作为外键表名使用.

    ```python
    ret= models.Publisher.objects.filter(xxx__title='菊花怪大战MJJ')
    ```



