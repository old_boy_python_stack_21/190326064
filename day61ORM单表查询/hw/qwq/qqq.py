import os

if __name__ == '__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hw.settings")
    import django

    django.setup()

    from app01 import models
    from django.db.models import Max, Min, Avg, Sum, Count

    # 查找所有书名里包含金老板的书
    # ret = models.Book.objects.filter(title__contains='金老板')
    # select * from app01_book where title like'%金老板%';
    # 查找出版日期是2018年的书
    # ret = models.Book.objects.filter(publish_date__year=2018)
    # select * from app01_book where publish_date like'2018%';
    # 查找出版日期是2017年的书名
    # ret = models.Book.objects.filter(publish_date__year=2017)[0].title
    # select title from app01_book where publish_date like'2017%';
    # 查找价格大于10元的书
    # ret = models.Book.objects.filter(price__gt=10)
    # select * from app01_book where price>10;
    # 查找价格大于10元的书名和价格
    # obj = models.Book.objects.filter(price__gt=10)
    # for i in obj:
    #     ret = i.price,i.title
    #     print(ret)
    # select title,price from app01_book where price>10;
    # 查找memo字段是空的书
    # ret = models.Book.objects.filter(memo__isnull=True)
    # select * from app01_book where memo is null;
    # 查找在北京的出版社
    # ret = models.Publisher.objects.filter(city='北京')
    # select * from app01_publisher where city='北京';
    # 查找名字以沙河开头的出版社
    # ret = models.Publisher.objects.filter(name__startswith='沙河')
    # select * from app01_publisher where name like'沙河%';
    # 查找“沙河出版社”出版的所有书籍
    # obj = models.Publisher.objects.filter(name='沙河出版社')
    # for i in obj:
    #     book = i.book_set.all()
    #     for j in book:
    #         ret = j.title
    #         print(ret)
    # select title from app01_publisher aa inner join app01_book bb on aa.id=bb.publisher_id where name='沙河出版社';
    # 查找每个出版社出版价格最高的书籍价格
    # all_pub = models.Publisher.objects.all()
    # for pub in all_pub:
    #     print(pub.name)
    #     lst = []
    #     for i in models.Book.objects.all():
    #         # print(i)
    #         # print(lst)
    #         if i.publisher_id==pub.id:
    #             lst.append(i.price)
    #     if not lst:
    #         continue
    #     print(max(lst))
    # select price,name from app01_publisher aa inner join app01_book bb on aa.id=bb.publisher_id group by name having
    # max(price);
    # 查找每个出版社的名字以及出的书籍数量
    # all_pub = models.Publisher.objects.all()
    # for pub in all_pub:
    #     print(pub.name)
    #     count = 0
    #     for i in models.Book.objects.all():
    #         if i.publisher_id == pub.id:
    #             count += 1
    #     print(count)
    # select name,count(*) from app01_publisher aa inner join app01_book bb on aa.id=bb.publisher_id group by name;
    # 查找作者名字里面带“小”字的作者
    # ret = models.Author.objects.filter(name__contains='小')
    # select * from app01_author where name like'%小%';
    # 查找年龄大于30岁的作者
    # ret = models.Author.objects.filter(age__gt=30)
    # select * from app01_author where age>30;
    # 查找手机号是155开头的作者
    # ret = models.Author.objects.filter(phone__startswith='155')
    # select * from app01_author where phone like'155%';
    # 查找手机号是155开头的作者的姓名和年龄
    # obj = models.Author.objects.filter(phone__startswith='155')
    # for i in obj:
    #     print(i.name,i.age)
    # select name,age from app01_author where phone like'155%';
    # 查找每个作者写的价格最高的书籍价格
    # obj = models.Book.objects.values('author').annotate(max=Max('price'))
    # print(obj)
    # select name,max(price) from app01_author a inner join app01_book_author b on
    # a.id=b.author_id inner join app01_book d on book_id=d.id group by name;
    # 查找每个作者的姓名以及出的书籍数量
    # obj = models.Book.objects.values('author').annotate(count=Count('author'))
    # for i in obj:
    #     print(models.Author.objects.filter(pk=i['author'])[0].name,i['count'])
    # select name,count(*) from app01_author aa inner join app01_book_author bb on aa.id=bb.author_id group by name;
    # 查找书名是“跟金老板学开车”的书的出版社
    # obj = models.Book.objects.filter(title='跟金老板学开车')[0]
    # ret = models.Publisher.objects.filter(id=obj.id)
    # print(ret[0])
    # select name from app01_publisher aa inner join app01_book bb on aa.id=bb.publisher_id where title='跟金老板学开车';
    # 查找书名是“跟金老板学开车”的书的出版社所在的城市
    # obj = models.Book.objects.filter(title='跟金老板学开车')[0]
    # ret = models.Publisher.objects.filter(id=obj.id)
    # print(ret[0].city)
    # select city from app01_publisher aa inner join app01_book bb on aa.id=bb.publisher_id where title='跟金老板学开车';
    # 查找书名是“跟金老板学开车”的书的出版社的名称
    # obj = models.Book.objects.filter(title='跟金老板学开车')[0]
    # ret = models.Publisher.objects.filter(id=obj.id)
    # print(ret[0].name)
    # select name from app01_publisher aa inner join app01_book bb on aa.id=bb.publisher_id where title='跟金老板学开车';
    # 查找书名是“跟金老板学开车”的书的出版社出版的其他书籍的名字和价格
    # ret = models.Book.objects.filter(publisher=models.Publisher.objects.filter(book__title='跟金老板学开车')).values('title','price').exclude(title='跟金老板学开车')
    # print(ret)
    # select title,price from app01_book aa inner join app01_publisher bb on aa.publisher_id=bb.id where
    # publisher_id=(select publisher_id from app01_publisher aa inner join app01_book bb on aa.id=bb.publisher_id where
    # title='跟金老板学开车');
    # 查找书名是“跟金老板学开车”的书的所有作者
    # ret = models.Author.objects.filter(book__title='跟金老板学开车')
    # for i in ret:
    #     print(i.name)
    # select -------------------
    # 查找书名是“跟金老板学开车”的书的作者的年龄
    # ret = models.Author.objects.filter(book__title='跟金老板学开车')
    # for i in ret:
    #     print(i.age)
    # select -----------------------------
    # 查找书名是“跟金老板学开车”的书的作者的手机号码
    # ret = models.Author.objects.filter(book__title='跟金老板学开车')
    # for i in ret:
    #     print(i.phone)
    # select -----------------------------
    # 查找书名是“跟金老板学开车”的书的作者们的姓名以及出版的所有书籍名称和价钱
    # ret = models.Book.objects.filter(title='跟金老板学开车').values('author','author__book','author__book__price')
    # for i in ret:
    #     print(models.Author.objects.filter(pk=i['author'])[0].name,
    #           models.Book.objects.filter(pk=i['author__book'])[0].title,
    #           models.Book.objects.filter(pk=i['author__book'])[0].price,)
    # select -----------------------------
