#!/usr/bin/env python
# -*- coding:utf-8 -*-

import re

# 1、匹配一篇英文文章的标题 类似 The Voice Of China
'''
s = r'(?:[A-Z][a-z]+ ?)+'
ret = re.findall(s, 'The Voice Of China Of China')
print(ret)
'''

# 2、匹配一个网址
# 类似 https://www.baidu.com http://www.cnblogs.com
'''
s = r'https?://www\.[A-Za-z]+\.com'
ret = re.findall(s, 'https://www.baidu.com http://www.cnblogs.com')
print(ret)
'''

# 3、匹配年月日日期 类似 2018-12-06 2018/12/06 2018.12.06
'''
s = r'-?\d{1,4}(-|/|\.)((0[1-9])|(1[0-2]))\1((0[1-9])|(1[0-9])|(2[0-9])|(3[0-1]))'
ret = re.findall(s, '2018-12-06')
print(ret)
'''

# 4、匹配15位或者18位身份证号
'''
(\d{6}(19|20)\d\d(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[01])\d\d\d(\d|x))+|(\d{8}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[01])\d{3})
'''

# 5、从lianjia.html中匹配出标题，户型和面积，结果如下：
# [('金台路交通部部委楼南北大三居带客厅   单位自持物业', '3室1厅', '91.22平米'), ('西山枫林 高楼层南向两居 户型方正 采光好',
# '2室1厅', '94.14平米')]
'''
import re


pattern = '<div class="info clear">.*?data-sl="">(?P<title>.*?)</a>.*?<span class="divide">/</span>(?P<size>.*?)' \
          '<span class="divide">/</span>(?P<m2>.*?)<span class="divide">'
with open(r'D:\homework\day26\1', 'r', encoding='utf-8')as f:
    s = f.read()
    ret = re.findall(pattern, s, re.S)
    print(ret)
'''

# 6、1-2*((60-30+(-40/5)*(9-2*5/3+7/3*99/4*2998+10*568/14))-(-4*3)/(16-3*2))
# 从上面算式中匹配出最内层小括号以及小括号内的表达式
'''
\([^()]+\)
'''

# 7、从类似9-2*5/3+7/3*99/4*2998+10*568/14的表达式中匹配出乘法或除法
'''
\d+((\*|/)\d+)+
'''

# 8、通读博客，完成三级菜单
# http://www.cnblogs.com/Eva-J/articles/7205734.html
'''
menu = {
    '北京': {
        '海淀': {
            '五道口': {
                'soho': {},
                '网易': {},
                'google': {}
            },
            '中关村': {
                '爱奇艺': {},
                '汽车之家': {},
                'youku': {},
            },
            '上地': {
                '百度': {},
            },
        },
        '昌平': {
            '沙河': {
                '老男孩': {},
                '北航': {},
            },
            '天通苑': {},
            '回龙观': {},
        },
        '朝阳': {},
        '东城': {},
    },
    '上海': {
        '闵行': {
            "人民广场": {
                '炸鸡店': {}
            }
        },
        '闸北': {
            '火车战': {
                '携程': {}
            }
        },
        '浦东': {},
    },
    '山东': {},
}

lst = [menu]
while True:
    for i in lst[-1]:
        print(i)
    user = input('请选择:')
    if user.upper() == 'B':
        lst.pop()
    elif user.upper() == 'Q':
        break
    elif lst[-1].get(user):
        lst.append(lst[-1][user])
print('已结束')
'''

# 9、大作业：计算器
# 1)如何匹配最内层括号内的表达式
'''
\(([^()]+)\)
'''

# 2)如何匹配乘除法
'''
\d+((\*|/)\d+)+
'''

# 3)考虑整数和小数
'''
(\d+\.?\d+)|\d+
'''

# 4)写函数，计算‘2*3’,‘10/5’
'''
import re


def count(suanshi):
    a, b, c = re.split(r'([*/])', suanshi, )
    a = int(a)
    c = int(c)
    if b == '*':
        print(a*c)
    elif b == '/':
        print(a/c)


count('10/5')
'''

# 5)引用4)中写好的函数，完成'2*3/4'计算
'''
import re


def count(suanshi):
    try:
        while 1:
            x = re.search(r'\d+[*/]\d+', suanshi)
            a, b, c = re.split(r'([*/])', x.group())
            a = int(a)
            c = int(c)
            if b == '*':
                res = a * c
            elif b == '/':
                res = int(a / c)
            suanshi = suanshi.replace(x.group(), str(res), 1)
            print(suanshi)
    except Exception as e:
        print('ok')


count('2*3*46*3*43*43/43*43/4*53*3')
'''
