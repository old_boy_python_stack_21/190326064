# day26笔记

## 1.内容回顾

### 1.元字符

```
\d  所有的数字
\w  数字字母下划线
\s 空白（空格 换行符 制表符）
\D 非数字
\W 非数字字母下划线
\S 非空白
\t 制表符
\n 换行符
.  除换行符之外的任意字符
[] 匹配[]内所有的字符
[^ ] 不匹配[]内的所有字符
^   以。。。开始
$   以。。。结束
|   两种情况选一起，如果有规则重复，那么把长的放前面、
    或只负责把两个表达式分开，如果是在整个表达式中只对一部分内容进行或，需要分组
()  限定|的作用域
    限定一组正则的量词约束  (\d\w)?
```

### 2.量词

```
?	0个或1个
+	1个或多个
*	0个或多个
{n}		n个
{n,}	至少n个
{n,m}	至少n个,至多m个
```

### 3.贪婪匹配

- 默认在规则和量词范围内尽量多匹配

- 回溯算法

### 4.惰性匹配

- 尽量少的匹配
- .*?x    表示匹配任意的内容直到遇到x停止

## 2.转义符

```
正则表达式中的转义符在python的字符串中也刚好有转义的作用
但是正则表达式中的转义符和字符串中的转义符并没关系,且还容易有冲突,为了避免这种冲突,我们所有的正则都以在工具中的测试结果为结果,然后只需要在正则和待匹配的字符串外面都加 r 即可
```



## 3.正则模块(regex)

- findall  匹配字符串中所有符合规则的项,返回一个列表

  ```python
  import re
  ret = re.findall('\d+','alex83')
  print(ret)
  ```

- search  从头到尾从字符串中取出第一个符合条件的项,匹配到了，返回一个对象，用group取值,没匹配到，返回None，不能用group

  ```python
  ret = re.search('\d+','alex83')
  print(ret)
  if ret:
      print(ret.group())
  ```

- match  从头匹配字符串中取出从第一个字符开始是否符合规则,符合则返回对象,用group取值,不符合，就返回None

  ```python
  ret = re.match(r'\d','1alex83')
  print(ret.group())
  ```

- 进阶方法

  - 1.时间复杂度  效率  compile

    在同一个正则表达式重复使用多次的时候使用能够减少时间的开销

    ```python
    ret = re.compile('\d3')	#()里是正则表达式
    print(ret)	#compile后的正则
    r1 = ret.search('alex83')	
    print(r1)
    
    ret = re.compile('\d+')
    r3 = ret.finditer('taibai40')
    for i in r3:
        print(i.group())
    ```

    

  - 2.空间复杂度  内存占用率  finditer

    在查询的结果超过1个的情况下，能够有效的节省内存，降低空间复杂度，从而也降低了时间复杂度

    ```python
    ret = re.finditer(r'\d','safhl02urhefy023908'*20000000)  # ret是迭代器
    for i in ret:    # 迭代出来的每一项都是一个对象
        print(i.group())  # 通过group取值即可
    ```

  - 3.用户体验

- sub   替换

  ```python
  ret = re.sub('\d','D','alex83wusir74taibai',1) #第一个符合条件的替换
  print(ret)  # alexD3wusir74taiba
  ```

- subn    替换并显示数量

  ```python
  ret = re.subn('\d','D','alex83wusir74taibai') # 替换所有并显示数量
  print(ret)  #('alexDDwusirDDtaibai', 4)
  ```

- split   用符合条件的字符切割

  ```python
  ret = re.split('\d(\d)','alex83wusir74taibai')  # 默认自动保留分组中的内容
  print(ret)  # ['alex', '3', 'wusir', '4', 'taibai']
  ```

## 4.分组的概念和re模块

```python
s1 = '<h1>wahaha</h1>'
s2 = '<a>wahaha ya wahaha</a>'
# s1 取 h1  wahaha
# s2 取 a   wahaha ya wahaha
import re

ret = re.search('<(\w+)>(.*?)</\w+>',s1) #()里为各个组
print(ret)
print(ret.group(0))   # group参数默认为0 表示取整个正则匹配的结果
print(ret.group(1))   # 取第一个分组中的内容
print(ret.group(2))   # 取第二个分组中的内容

ret = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</\w+>',s1) # 给组命名?P<组名>
print(ret)
print(ret.group('tag'))   # 取tag分组中的内容
print(ret.group('cont'))   # 取cont分组中的内容
```

- 分组命名

  - (?P<名字>正则表达式)

  - 引用分组  (?P=组名)   这个组中的内容必须完全和之前已经存在的组匹配到的内容一模一样

    ```python
    s = '<h1>wahaha</h1>'
    ret = re.search('<(?P<tag>\w+)>.*?</(?P=tag)>', s)
    print(ret.group('tag')) # h1
    
    s1 = '<h1>wahaha</h1>'
    ret = re.search(r'<(\w+)>.*?</(\1)>', s1)  # 引用第一个分组
    print(ret.group(2) == ret.group(1)) # True
    ```

```python
ret = re.findall('\d(\d)','aa1alex83')
# findall遇到正则表达式中的分组，会优先显示分组()中的内容
print(ret)	# ['3']
```

- split   会保留分组中本来应该被切割掉的内容

- 分组和findall

  - 默认findall 优先显示分组内的内容

  - 取消分组优先显示 (?:正则)

    ```python
    ret = re.findall('\d+(?:\.\d+)?','1.234+2')
    print(ret)  #['1.234', '2']
    ```

- 例题:有时我们想匹配的内容包含在不想匹配的内容当中，这个时候只需要把不想匹配的先匹配出来，再通过手段去掉

  ```python
  import re
  ret=re.findall(r"\d+\.\d+|(\d+)","1-2*(60+(-40.35/5)-(-4*3))")
  print(ret)  #['1', '2', '60', '', '5', '4', '3']
  ret.remove('')
  print(ret)  #['1', '2', '60', '5', '4', '3']
  ```

## 5.爬虫实例

```python
import requests
import json
import re


def parser_page(par, content):
    res = par.finditer(content)
    for i in res:
        yield {'id': i.group('id'),
               'title': i.group('title'),
               'score': i.group('score'),
               'com_num': i.group('comment_num')}


def get_page(url):
    ret = requests.get(url)
    return ret.text


pattern = '<div class="item">.*?<em class="">(?P<id>\d+)</em>.*?<span class="title">(?P<title>.*?)</span>.*?' \
          '<span class="rating_num".*?>(?P<score>.*?)</span>.*?<span>(?P<comment_num>.*?)人评价</span>'
par = re.compile(pattern, flags=re.S)
num = 0
with open('movie_info', mode='w', encoding='utf-8') as f:
    for i in range(10):
        content = get_page('https://movie.douban.com/top250?start=%s&filter=' % num)
        g = parser_page(par, content)
        for dic in g:
            f.write('%s\n' % json.dumps(dic, ensure_ascii=False))
        num += 25
```

## 6.总结

- re模块
  - 转义符
  - python中的方法
    - findall
    - search
    - match
    - finditer
    - complie
    - split
    - sub
    - subn
  - python中的方法 + 正则表达式新内容
    - 分组 split findall
    - 分组命名
    - 引用分组
  - 爬虫例子
    - 生成器的send

## 7.三级菜单

- 递归做法

```python
def menu_func(menu):
    while True:
        for key in menu:
            print(key)
        inp = input('>>>').strip()
        if inp.upper() == 'Q': return 'q'
        if inp.upper() == 'B': return 'b'
        elif menu.get(inp):
            flag = menu_func(menu[inp])
            if flag == 'q': return 'q'
menu_func(menu)
print('ashkskfhjhflhs')
```

- 栈做法

```python
lst = [menu]
while lst:
    for key in lst[-1]:
        print(key)
    inp = input('Q/q退出,B/b返回上级>>>')
    if inp.upper() == 'Q':break
    elif inp.upper() == 'B':lst.pop()
    elif lst[-1].get(inp):
        lst.append(lst[-1][inp])
```

- 使用listdir求文件夹大小

```python
import os
lst = [r'D:\code']
size = 0
while lst:
    path = lst.pop()
    name_lst = os.listdir(path)
    for name in name_lst:
        full_path = os.path.join(path,name)
        if os.path.isdir(full_path):
            lst.append(full_path)
        elif os.path.isfile(full_path):
            size += os.path.getsize(full_path)
print(size)
```

