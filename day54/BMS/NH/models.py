from django.db import models


# Create your models here.

class User(models.Model):
    id = models.AutoField(primary_key=True)
    pub = models.CharField(max_length=32)               # 出版社名

    def __str__(self):
        return self.pub


class Book(models.Model):
    title = models.CharField(max_length=32)    # 图书名
    pub = models.ForeignKey('User', on_delete=models.CASCADE)
