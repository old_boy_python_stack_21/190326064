from django.shortcuts import render,redirect,HttpResponse
from NH import models


# 出版社列表
def pub_list(request):
    allpub = models.User.objects.all()
    return render(request, 'pub_list.html',{'allpub':allpub})


# 新增出版社
def add_pub(request):
    error = ''
    if request.method == 'POST':
        pub_name = request.POST.get('pub_name')
        if models.User.objects.filter(name=pub_name):
            error = '出版社名称已经存在了,回家去吧'
        if not pub_name:
            error = '你输的什么东西?'
        if not error:
            models.User.objects.create(name=pub_name)
            return redirect('/pub_list/')
    return render(request, 'add_pub.html', {'error': error})


# 删除出版社
def del_pub(request):
    pk = request.GET.get('id')
    obj = models.User.objects.filter(pk=pk)
    if not obj:
        return HttpResponse('你想删还没有呢!')
    obj.delete()
    return redirect('/pub_list/')


# 编辑出版社
def edit_pub(request):
    error = ''
    pk = request.GET.get('id')
    obj_list = models.User.objects.filter(pk=pk)
    if not obj_list:
        return HttpResponse('没有!不让你编辑!')
    obj = obj_list[0]
    if request.method == 'POST':
        pub_name = request.POST.get('pub_name')
        if models.User.objects.filter(name=pub_name):
            error = '你修改的名称被人取了呢!'
        if obj.name == pub_name:
            error = '你改了啥呀?重改!'
        if not pub_name:
            error = '写点东西吧,别空着呀!'
        if not error:
            obj.name = pub_name
            obj.save()
            return redirect('/pub_list/')

    return render(request, 'edit_pub.html', {'obj': obj, 'error': error})


# 书籍列表
def book_list(request):
    all_books = models.Book.objects.all()
    allpub = models.User.objects.all()
    return render(request, 'book_list.html', {'all_books': all_books,'allpub':allpub})


# 增加书籍
def add_book(request):
    if request.method == 'POST':
        book_name = request.POST.get('book_name')
        pub_id = request.POST.get('pub_id')
        models.Book.objects.create(title=book_name, pub_id=pub_id)
        return redirect('/book_list/')

    all_pub = models.User.objects.all()
    return render(request, 'add_book.html', {'all_pub':all_pub})


# 删除书籍
def del_book(request):
    pk = request.GET.get('id')
    models.Book.objects.filter(pk=pk).delete()
    return redirect('/book_list/')


# 编辑书籍
def edit_book(request):
    pk = request.GET.get('id')
    book_obj = models.Book.objects.get(pk=pk)
    if request.method == 'POSt':
        book_name = request.POST.get('book_name')
        pub_id = request.POST.get('pub_id')
        book_obj.title = book_name
        book_obj.pub = models.User.objects.get(pk=pub_id)
        book_obj.save()
        return redirect('/book_list/')
    all_pub = models.User.objects.all()
    return render(request, 'edit_book.html', {'book_obj': book_obj, 'all_pub': all_pub})

