# day43笔记

## 回顾

- 1.字体标签包含哪些:

  - h1-h6,b,strong,i,em

- 2.超链接标签a中的href属性有什么用:

  - 链接到一个新地址
  - 回到顶部
  - 跳转邮箱
  - 下载文件

- 3.img中标签src和alt属性作用

  - 链接图片的资源
  - 图片加载失败时显示的标题

- 4.如何创建一个简易的有边框线的表格

  ```html
  <table boeder='1' cellspacing=0>
      <th>
      	<td>id</td>
      	<td>name</td>
      </th>
  	<tr>
          <td>1</td>
          <td>mjj</td>
  	</tr>
  </table>
  ```

- 5.form标签中的action属性和method属性的作用

  - action:提交到服务器的地址,如果是空的字符串,它表示当前服务器地址
  - method:提交的方式,get和post.
    - get:明文不安全,地址栏只允许提交2kb的内容,提交的内容会在地址上显示.显示的方式http://....../index.html?name=value&name2=value2
    - post:密文提交安全,可以提交任意内容
    - 后期http协议的内容(重点)

- 6.在form标签中表单控件input中type类型有哪些？并分别说明他们代指的含义

  - type

    - text 单行文本输入框
    - password  密码输入框
    - radio 单选框
      - 互斥效果:给每个单选按钮设置相同的name属性
      - 如何默认选中:给单选按钮添加checked属性.
    - checkbox 多选框
      - 默认选中 添加checked属性
    - submit 提交按钮
    - file 上次文件
    - datetime 时间

  - 下拉列表

    - multiple 产生多选的效果

    ```html
    <select>
        <option name value selected>抽烟</option>
        <option name value selected>喝酒</option>
        <option name value selected>烫头</option>
    </select>
    ```

  - 多行文本输入框 textarea

    - cols 列
    - rows 行

- 7.表单控件中的name属性和value属性有什么意义:

  - name:提交到当前服务器的名称
  - value:提交到当前服务器的值
  - 以后给服务器来使用

- 8.思考

  ```html
  1.在一行内显示的标签有哪些?
  	b,strong,i,em,a,img,input,td
  2.独占一行的标签有哪些?
  	h1-h6,ul,ol,li,form,table,tr,p,div
  ```

## 今日内容

### style中设置格式的方法(加;)

- height 高度
- line-height 行高
- background-color 背景色
- text-align 位置(center)
- text-decoration : none设置字体无下划线
- color 设置字体颜色
- font-size 设置字体大小

### span标签

```html
<head>
    <style>
        .active{
            color: red;
        }
    </style>
</head>

https:// <span class="active">www.baidu.com</span> /s?wd=111
```

### div盒子标签

- 称为盒子标签,division : 分割
- 把网页分割成不同的独立的逻辑区域

```<a href='#'>   </a>```

```<span class='类名'>   </span>```

- 在head中设置格式
  - class设置 : 在style中 .类名{设置格式}
  - 设置所有相同标签 : style中 标签名{设置格式}
  - id设置 : style中 #id名{格式}

```html
<title>01 div盒子标签</title>
    <style type="text/css">
        #top{
            /*行高等于盒子的高度 实现垂直居中*/
            height: 40px;
            line-height: 40px;
            background-color: #333;
            /* text-align: center; */
        }
        a{
            text-decoration: none;
            color: #b0b0b0;
            font-size: 14px;
        }
        /*类选择器
        选择器作用：选中标签
        */
        .sep{
            color: #666;
        }
    </style>
```

- body中设置类名,id,相同格式需要相同名称

```html
<div id="top">
	<div class="top_l">
		<a href="#">小米商城</a>
		<span class="sep">|</span>
         <a href="#">金融</a>
         <span class="sep">|</span>
         <a href="#">危机</a>
	</div>
	<div class="shop"></div>
	<div class="user_login"></div>
</div>
<div id="nav">
	小米导航
</div>
<div id="content">
	小米的主体内容
</div>
<div id="footer">
	小米的脚
</div>
```

### label标签

- 它中的for属性跟表单控件的id属性有关联
- 点击label标签时,光标在相同id输入框中显示

```html
<form action="">
   <label for="username">用户名：</label>
   <input type="text" id="username">
   <label for="pwd">密码：</label>
   <input type="text" id="pwd">
</form>
```

### 模拟百度搜索

```html
<body>
<!--https://www.baidu.com/s?wd=111-->
<form action="https://www.baidu.com/s">	# 提交给这个服务器
    <input type="text" name="wd">	# 文本输入框,name是百度默认
    <input type="submit" value="百度一下">	# 提交
</form>
</body>
```

## CSS

css : 层叠样式表

### 1.css引入方式

- 在head中<style>内部写入

- 行内样式

  ```html
  <div style='color:red;'>mjj</div>
  ```

- 内嵌式

  ```html
  在head标签内部书写style
  <style>/*css代码*/</style>
  ```

- 外接式

  ```html
  <link href='css/index.css' rel='stylesheet'>
  ```

### 2.三种引入方式的优先级

- 行内样式>内嵌式和外接式
- 内嵌式和外接式看谁在后面,后面优先级高

### 3.css选择器

#### 基础选择器

- id选择器(#id名)

  - id唯一

  ```html
  #xxx
  ```

- 类选择器(.类名)

  - 可重复,归类,也可以设置多个

  ```html
  <head>
      <style>
          .box{
              width:200px;
              height:200px;
              background-color:yellow;
          }
          .active{
              border-radius: 200px;# 边框半径,圆
  		}
  		div{
              border:1px solid #000;# border边框solid实心
          }
          a{
              /*设置文本修饰*/
              text-decoration: none;
          }
          input{
              border:none;# 无边框
              outline:none;# 无外线
          }
          #user{
              border: 1px solid #555;
              height: 60px;
              font-size: 20px;
              padding: 0 10px;#第一个数字表示上下与边框的距离,第二个表示左右与边框距离.
          }# 自己设置输入边框
          #box{
              color: red;
          }# 给box内容设置字体颜色
  	</style>
  </head>
  <body>
      <div class='box active' id="box">内容</div>#(归类并单独设置)
      <hr>
      <div class='box'></div>
      <hr>
      <div class='box'></div>
      <a href="">百度一下</a>
  	<input type="text" id="user">
  </body>
  ```

  - 语法: .xxx

- 标签选择器(直接写标签名)

  ```html
  div{}
  p{}
  ul
  ol
  ...
  ```

#### 高级选择器

- 后代选择器

  ```html
  div p{
  	color: red;
  }
  ```

- 子代选择器

  ```html
  div>p{
  	color:red;
  }
  ```

- 组合选择器

  ```html
  div,p,body,html,ul,ol....{
  	padding: 0;
  	margin: 0;
  }
  ```

- 交集选择器

  ```html
  span{
  	color:red;
  }
  .active{
  	font-size: 20px;
  }
  span.active{
  	background-color: black;
  } # 交集部分
  ```

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        /*后代选择器,限制box后代格式*/
        #box p{
            color:red;
        }
        /*子代选择器,box>p,只限制p同级*/
        #box>p{
            color: yellow;
        }
        /*组合选择器,标签加逗号限制这些标签*/
        p,ul,ol,body{
            magin:0;
        }
        /*重置样式*/
        p,ul,ol,body{
            margin:0;
            padding: 0;
        }
        input,textarea{
            border:none;
            outline: 0;
        }
        span{
            color:red;
        }
        .active{
            font-size: 20px;
        }
        span.active{
            background-color: black;
        }
    </style>
</head>
<body>
    <div id="box">
        <div>
            <div>
                <div>
                    <p>
                        mjj
                    </p>
                </div>
            </div>
        </div>
        <p>
            wusir
        </p>
    </div>
    <ul>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
    </ul>
    <span class="active">女神</span>
</body>
</html>
```

#### 属性选择器

### 4.层叠性和权重

- 继承性:在css有某些属性是可以继承下来
  - color
  - text-xxx,(align等)
  - line-height
  - font-xxx (size等)

```html
<head>
    <style>
        body{
            font-size: 14px;
        }
        #box{
            height: 80px;
            color:red;
            text-align: center;
            line-height: 80px;
            background-color: yellow;
            font-size: 20px;
        }
        #box p{
            background-color: green;# transparent透明色
            font-size: 26px;
        }
    </style>
</head>
<body>
    <div id="box">
        <p class="active">
            mjj
        </p>
    </div>
    <a href="">WUSIR</a>
</body>
```

- 权重比较规则:

  - 继承来的属性权重为0
  - 标签权重 : 1
  - 类权重 : 10
  - id权重 : 100
  - 行内权重 : 1000

  ```html
  前提是选中了标签
  权重比较:
     1.数选择器数量： id 类 标签 谁大它的优先级越高，如果一样大，后面的会覆盖掉前面的属性
     2.选中的标签的属性优先级永远大于继承来的属性，它们没有可比性
     3.同是继承来的属性
       3.1 谁描述的近，谁的优先级越高
       3.1 描述的一样近，数选择器的数量
  ```

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        /*10*/
        .active{
            color: yellow;
        }
        /*100*/
        #active{
            color: green;
        }
        /*1*/
        p{
            color: red;
        }
        /*100*/
        #active{
            color: green;
        }
        /*!*1 0 1*!*/
        #box1 p{
            color: darkred;
        }
        /*#box1 .wrap2 .wrap3 p{*/
            /*color: yellow;*/
        /*}*/
        /*120*/
        #box1 .wrap2 .wrap3{
            color: greenyellow !important;
        }
        /*210*/
        #box1 #box2 .wrap3{
            color: gold;
        }

    </style>
</head>
<body>
    <div id="box1" class="wrap1">
        <div id="box2" class="wrap2">
            <div id="box3" class="wrap3">
                <p class="active" id="active">
                    wusir
                </p>
            </div>
        </div>
    </div>
<!--1.选中了标签
权重比较；
1.数选择器数量： id 类 标签  谁大它的优先级越高，如果一样大，后面的会覆盖掉前面的属性
2.选中的标签的属性优先级用于大于继承来的属性，它们是没有可比性
3.同是继承来的属性
    3.1 谁描述的近，谁的优先级越高
    3.1 描述的一样近，这个时候才回归到数选择器的数量
4.!important
大于所有权重,除行内.
-->
</body>
```



### 5.css的盒模型

- width 内容宽度

- height 内容高度

- padding 内边距,border到内容的距离

- border 边框

- margin 外边距

  ```html
  <style>
      #box{
          width: 260px;
          height: 260px;
          background-color: red;
          /*padding: 20px;*/
          /*2个值上下  左右*/
          /*padding: 0 20px ;*/
          /*3个值上  左右  下*/
          /*padding: 0 20px 30px;*/
          /*padding: 10px 20px 30px 40px;*/
          /*4个值上  上 右 左 下*/
          padding-top: 20px;
          border: 1px solid #000;
          margin-left: 30px;
      }
  </style>
  <body>
      <!--302*302*-->
      <div id="box" class="box">
          mjj
      </div>
  </body>
  ```

### HTML的嵌套关系

```html
<!--块级标签：1.独占一行 2.可以设置宽高，如果不设置宽，默认是父标签的100%宽度-->
display:block
<!--行内标签：1.在一行内显示 2.不可以设置宽高，如果不设置宽高，默认是字体的大小-->
display:inline
<!--行内块标签： 1.在一行内显示 2.可以设置宽高
在网页中：
行内转块和行内块是非常使用 
display:
	inline  行内
	inline-block 行内块
	block  块
嵌套关系：
1.块级标签可以嵌套块级和行内以及行内块
2.行内标签尽量不要嵌套块级
3.p标签不要嵌套div，也不要嵌套p
   可以嵌套a/img/表单控件-->
<style>
    a{
    width: 100px;
    height: 40px;
    background-color: red;
    display: inline-block;
    /*显示方式： block 显示块，inline 显示行内  inline-block 行内块*/
    text-align: center;
    /*文本排列方式：left center right*/
    line-height: 40px;
    /*行高：一行的高度。当行高=盒子模型的高度 实现垂直居中*/
    text-decoration: underline;
    /*文本修饰：none 无修饰，underline:下划线 line-through删除线*/
    <del>也是删除线</del>
    color: #333333;
    }
    input{
        width: 100px;
        height: 40px;
    }
    a{
        width: 200px;
        height: 250px;
    } # 设置a标签大小
    img{
        width: 200px;
        height: 250px;
    } # 设置图片大小
<style>

<del>aaaaa</del>
<div class="box">mjj</div>
<p class="box">wusir</p>
<a href="">百度一下</a>
<span>女神</span>
<input type="text">   # 可以设宽高
<input type="text">

<a href="">
    <img src="https://timgsa.baidu.com/.jpg" alt=""> # 有自己的宽高
</a>
<p>
    mjj
    <div>wusir</div>
</p>
```

