#!/usr/bin/env python
# -*- coding:utf-8 -*-

#  day13
# 2.# 2.请为 func 函数编写一个装饰器，添加上装饰器后可以实现：执行func时，先输入"before"，然后再执行func函数内部代码。

"""
def wrapper(arg):
    def inner():
        print('before')
        v = arg()
        return v
    return inner

@wrapper
def func():
    print('func')
    return

func()
"""

# 3.请为 func 函数编写一个装饰器，添加上装饰器后可以实现：执行func时，先执行func函数内部代码，再输出 "after"

"""
def wrapper(arg):
    def inner():
        v = arg()
        print('after')
        return v

    return inner


@wrapper
def func():
    print('func')
    return


func()
"""

# 4.请为以下所有函数编写一个装饰器，添加上装饰器后可以实现：执行func时，先执行func函数内部代码，再输出 "after"

"""
def di(arg):
    def inner(*args, **kwargs):
        v = arg(*args, **kwargs)
        print('after')
        return v
    return inner


@di
def func(a1):
    return a1 + "傻叉"


@di
def base(a1, a2):
    return a1 + a2 + '傻缺'


@di
def base2(a1, a2, a3, a4):
    return a1 + a2 + a3 + a4 + '傻蛋'


print(base('a', 'b'))
print(func("a"))
print(base2('a','b','c','d'))
"""

# 5.请为以下所有函数编写一个装饰器，添加上装饰器后可以实现：将被装饰的函数执行5次，讲每次执行函数的结果按照顺序放到列表
# 中，最终返回列表。

"""
import random


def decorate(count):
    def wrapper(arg):
        def inner(*args, **kwargs):
            lst = []
            for i in range(5):
                v = arg(*args, **kwargs)
                lst.append(v)
            return lst
        return inner
    return wrapper


@decorate(5)
def func():
    return random.randint(1, 4)


result = func()  # 执行5次，并将每次执行的结果追加到列表最终返回给result
print(result)
"""

# 6.请为以下函数编写一个装饰器，添加上装饰器后可以实现：执行 read_userinfo 函时，先检查文件路径是否存在，如果存在则执行
# 后，如果不存在则 输入文件路径不存在，并且不再执行read_userinfo函数体中的内容，再讲 content 变量赋值给None。
'''
import os


def decorate(arg):
    def inner(*args):
        path = args[0]
        if os.path.exists(path):
            v = arg()
            return v
        print('文件路径不存在!')
        return None
    return inner


@decorate
def read_userinfo(path):
    file_obj = open(path, mode='r', encoding='utf-8')
    data = file_obj.read()
    file_obj.close()
    return data


content = read_userinfo('D:\homework\\2.txt')
"""
温馨提示：如何查看一个路径是否存在？
import os
result = os.path.exists('路径地址')

# result为True，则表示路径存在。
# result为False，则表示路径不存在。
"""
'''

# 7.请为以下 user_list函数编写一个装饰器，校验用户是否已经登录，登录后可以访问，未登录则提示：请登录后再进行查看，然后
# 再给用户提示：系统管理平台【1.查看用户列表】【2.登录】并选择序号。此变量用于标记，用户是否经登录。
#    True,已登录。
#    False,未登录(默认)

'''
CURRENT_USER_STATUS = False


def decorate(arg):
    def inner():
        if CURRENT_USER_STATUS:
            return arg()
        print('请登录后再进行查看!')
        return 
    return inner


@decorate
def user_list():
    """查看用户列表"""
    for i in range(1, 100):
        temp = "ID:%s 用户名：老男孩-%s" % (i, i,)
        print(temp)


def login():
    """登录"""
    print('欢迎登录')
    while True:
        username = input('请输入用户名（输入N退出）：')
        if username == 'N':
            print('退出登录')
            return
        password = input('请输入密码：')
        if username == 'alex' and password == '123':
            global CURRENT_USER_STATUS
            CURRENT_USER_STATUS = True
            print('登录成功')
            return
        print('用户名或密码错误，请重新登录。')


def run():
    func_list = [user_list, login]
    while True:
        print("""系统管理平台
        1.查看用户列表；
        2.登录""")
        index = int(input('请选择：'))
        if index >= 0 and index < len(func_list) + 1:
            func_list[index - 1]()
        else:
            print('序号不存在，请重新选择。')


run()
'''

# 8.看代码写结果
"""
v = [lambda:x for x in range(10)]
print(v)        # [十个不同的lambda地址]
print(v[0])     # 一个内存地址
print(v[0]())   # 9
"""

# 9.看代码写结果
'''
v = [i for i in range(10,0,-1) if i > 5]
print(v)        # [10,9,8,7,6]
'''

# 10.看代码写结果
'''
data = [lambda x:x*i for i in range(10)]    # 新浪微博面试题
print(data)                                 # [十个不同的lambda地址]
print(data[0](2))                           # 18
print(data[0](2) == data[8](2))             # True
'''

# 11.请用列表推导式实现，踢出列表中的字符串，然后再将每个数字加100，最终生成一个新的列表保存。
'''
data_list = [11, 22, 33, "alex", 455, 'eirc']
new_data_list = [i+100 for i in data_list if type(i)==int]
print(new_data_list)
'''

# 12.请使用字典推导式实现，将如果列表构造成指定格式字典.
'''
data_list = [
    (1, 'alex', 19),
    (2, '老男', 84),
    (3, '老女', 73)
]
"""请使用推导式将data_list构造生如下格式：
info_list = {
    1: ('alex', 19),
    2: ('老男', 84),
    3: ('老女', 73)
}
"""
info_list = {i[0]: (i[1], i[2]) for i in data_list}
print(info_list)
'''

# day14
# 1.为函数写一个装饰器，在函数执行之后输入 after
"""
def wrapper(arg):
    def inner(*args):
        arg()
        print('after')
    return inner

@wrapper
def func():
    print(123)

func()
"""

# 2.为函数写一个装饰器，把函数的返回值 +100 然后再返回。
'''
def wrapper(arg):
    def inner(*args):
        v = arg()+100
        return v
    return inner

@wrapper
def func():
    return 7

result = func()
print(result)
'''

# 3.为函数写一个装饰器，根据参数不同做不同操作。
# flag为True，则 让原函数执行后返回值加100，并返回。
# flag为False，则 让原函数执行后返回值减100，并返回。
'''
def x(flag):
    def inner(arg):
        def class3():
            if flag:
                v = arg() + 100
                return v
            v = arg() - 100
            return v

        return class3

    return inner


@x(True)
def f1():
    return 11


@x(False)
def f2():
    return 22


r1 = f1()
r2 = f2()
print(r1, r2)
'''

# 4.写一个脚本，接收两个参数。
# 第一个参数：文件
# 第二个参数：内容
# 请将第二个参数中的内容写入到 文件（第一个参数）中。
# 执行脚本： python test.py oldboy.txt 你好
'''
import sys

a = sys.argv
with open(a[1],'w',encoding='utf-8') as f:
    f.write(a[2])
'''

# 5.递归的最大次数是多少？
'''
1000
'''

# 6.看代码写结果
'''
print("你\n好")   # 你 换行 好
print("你\\n好")  # 你\n好
print(r"你\n好")  # 你\n好
'''

# 7.写函数实现，查看一个路径下所有的文件【所有】。
'''
import os


def func(arg):
    v = os.walk(arg)
    for a, b, c in v:
        for i in c:
            s = os.path.join(a, i)
            print(s)
    return


func(r'D:\homework\day14')
'''

# # 8.写代码,请根据path找到code目录下所有的文件【单层】，并打印出来。
'''
import os

v = os.listdir('D:\homework\day14')
print(v, type(v))
'''

# 9.1斐波那契数列
'''
lst = [1,]
max = int(input('最大范围:'))
v = 1
while v <= max:
    lst.append(v)
    v = lst[-1] + lst[-2]
print(lst)
'''

# 9.2
'''
dic_a = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'f': 'hello'}
dic_b = {'b': 3, 'd': 5, 'e': 7, 'm': 9, 'k': 'world'}
for b in dic_b.keys():
    if b in dic_a.keys():
        dic_a[b] = dic_a[b] + dic_b[b]
    else:
        dic_a[b] = dic_b[b]

print(dic_a)
'''

# 10.
'''
[10,'a']
[123]
[10,'a']
'''

# 11.1  A,B,C

# 11.2
'''
tupleA = ('a', 'b', 'c', 'd', 'e')
tupleB = (1, 2, 3, 4, 5)
dic = {}
for i in range(len(tupleA)):
    dic[tupleA[i]] = tupleB[i]

print(dic)
'''

# 11.3
'''
import sys
print(sys.argv)
print(len(sys.argv))
print(sys.argv[0])
'''

# 11.4
'''
ip = '192.168.0.100'
lst = [int(i) for i in ip.split('.')]
print(lst)
'''

# 11.5
'''
Alist = ['a', 'b', 'c']
s = ','.join(Alist)
print(s)
'''

# 11.6
'''
a = StrA[-2:]
b = StrA[1:3]
'''

# 11.7
# Alist = [1, 2, 3, 1, 3, 1, 2, 1, 3]
# 方法一
'''
a = Alist[:3]
print(a)
'''
# 方法二
'''
b = set(Alist)
print(list(b))
'''

# 11.8
'''
import os
def func(arg):
    for a,b,c in os.walk(arg):
        for i in c:
            print(os.path.join(a, i))

func('D:\homework\day14')
'''

# 11.9
'''
lst = []
for a in range(1, 1000):
    c = 0
    for i in range(1, a):
        if a % i == 0:
            c += i
    if c == a:
        lst.append(a)
print(lst)
'''

# 11.12
"""
with open('a.txt', 'r', encoding='utf-8') as f:
    for line in f:
        print(line)
"""

# day15
# 1.sys.path.append("/root/mods")的作用？
"""
增加导入模块的查找路径
"""

# 2.字符串如何进行反转？
'''
[::-1]
'''

# 3.不用中间变量交换a和b的值。
'''
a = 1
b = 2
a, b = b, a
print(a, b)
'''

# 4.*args和**kwargs这俩参数是什么意思？我们为什么要用它。
'''
*args接收多个位置参数,**kwargs接收多个关键字参数.
func(*args,**kwargs)能接收任意类型任意个参数.
'''

# 5.函数的参数传递是地址还是新值？
'''
地址
'''

# 6.看代码写结果：
'''
my_dict = {'a': 0, 'b': 1}


def func(d):
    d['a'] = 1
    return d


func(my_dict)
my_dict['c'] = 2
print(my_dict)      # {'a':1,'b':1,'c':2}
'''

# 7.什么是lambda表达式
'''
匿名函数,能一行表达一个简单的函数
lambda x : y
x为传入参数,':'后面为返回值
'''

# 8.range和xrang有什么不同？
'''
在py2里,range(x)立即生成列表,xrange(x)不立即创建,边循环边创建.
在py3里,range即为py2的xrange,没有xrange
'''

# 9."1,2,3" 如何变成 ['1','2','3',]
'''
s = "1,2,3"
a = s.split(',')
print(a)
'''

# 10.['1','2','3'] 如何变成 [1,2,3]
'''
lst = ['1','2','3']
new_lst = [int(i) for i in lst]
print(new_lst)
'''

# 11.def f(a,b=[]) 这种写法有什么陷阱？
'''
b默认值为一个空列表,列表创建在函数内存之外,易被其他改动.
'''

# 12.如何生成列表 [1,4,9,16,25,36,49,64,81,100] ，尽量用一行实现。
'''
lst = [i**2 for i in range(1,11)]
print(lst)
'''

# 13.python一行print出1~100偶数的列表, (列表推导式, filter均可)
'''
print([i for i in range(1, 101) if i % 2 == 0])

print(list(filter(lambda i: i % 2 == 0, range(1, 101))))
'''

# 14.把下面函数改成lambda表达式形式.
#
# def func():
#     result = []
#     for i in range(10):
#         if i % 3 == 0:
#             result.append(i)
#     return result
'''
lambda: [i for i in range(10) if i % 3 ==0]

'''

# 15.如何用Python删除一个文件？
'''
# 删除文件
import os
os.remove(path)

# 删除目录
import shutil
shutil.rmtree(path)
'''

# 16.如何对一个文件进行重命名？
'''
import os

os.rename(filename, newname)
'''

# 17.python如何生成随机数？
'''
import random

a = random.randint(1, 10)
print(a)
'''

# 18.从0-99这100个数中随机取出10个数，要求不能重复，可以自己设计数据结构。
'''
import random
a = set()
while len(a) < 10:
    a.add(random.randint(0,99))

print(a)
'''

# 19.用Python实现 9*9 乘法表 （两种方式）
# 方法一
'''
for a in range(1, 10):
    for b in range(1, a + 1):
        if a >= b:
            print('%s*%s=%s ' % (b,a,a*b),end='')
    print()
'''
# 方法二
'''
a = 1
while a < 10:
    b = 1
    while b <= a:
        print('%s*%s=%s' % (b, a, b * a), end=' ')
        if a == b:
            print()
        b += 1
    a += 1
'''

# 20.请给出下面代码片段的输出并阐述涉及的python相关机制。
'''
def dict_updater(k, v, dic={}):
    dic[k] = v
    print(dic)


dict_updater('one', 1)  # one给k,1给v,输出dic={'one':1}
dict_updater('two', 2)  # two给k,2给v,输出dic={'one':1,'two':1}
dict_updater('three', 3, {})    # three给k,3给v,空字典给dic,输出dic={'three':3}
# 形参为可变类型(字典,元组)时会在创建参数的外部创建元素,下次使用还会使用同一个元素.
'''

# 21.写一个装饰器出来。
'''
def wrapper(arg):
    def inner(*args,**kwargs):
        return arg(*args,**kwargs)
    return inner
'''

# 22.用装饰器给一个方法增加打印的功能。
'''
def wrapper(arg):
    def inner(*args,**kwargs):
        return print(arg(*args,**kwargs))
    return inner

@wrapper
def func():
    a = 123

func()
'''

# 23.请写出log实现(主要功能时打印函数名)
'''
def log(arg):
    def inner():
        print('call', arg.__name__+'()')
        return arg()
    return inner

@log
def now():
    print("2013-12-25")

now()

# 输出
# call now()
# 2013-12-25
'''

# 24.向指定地址发送请求，将获取到的值写入到文件中。
'''
import requests

response = requests.get('https://www.baidu.com')
with open('24.txt','w',encoding='utf-8') as f:
    f.write(response.text)

'''

# 25.获取结构中的所有name字段，使用逗号链接起来，并写入到 catelog.txt 文件中。
'''
s  = [
    {'id': 1, 'name': 'Python', 'hide': False, 'category': 1},
    {'id': 2, 'name': 'Linux运维', 'hide': False, 'category': 4},
    {'id': 4, 'name': 'Python进阶', 'hide': False, 'category': 1},
    {'id': 7, 'name': '开发工具', 'hide': False, 'category': 1},
    {'id': 9, 'name': 'Go语言', 'hide': False, 'category': 1},
    {'id': 10, 'name': '机器学习', 'hide': False, 'category': 3},
    {'id': 11, 'name': '技术生涯', 'hide': False, 'category': 1}
]
lst = [i['name'] for i in s]
cont = ','.join(lst)
with open('catelog.txt','w',encoding='utf-8') as f:
    f.write(cont)
'''

# 25.请列举经常访问的技术网站和博客.
'''
github
csdn
cnblogs
'''

# 26.请列举最近在关注的技术
'''
Python模块的使用
'''

# day16
# 1.列举你常见的内置函数。
"""
强制转换:int() / str() / list() / dict() / tuple() / set() / bool()
数学相关:sum() / max() / min() / divmod() / float() / abs()
输入输出:input() / print()
其他:len() / open() / type() / id() / range()
"""

# 2.列举你常见的内置模块？
'''
json / getpass / os / sys / random / hashlib / copy / shutil / time
'''

# 3.json序列化时，如何保留中文？
'''
import json
a = '你好'
s = json.dumps(a,ensure_ascii=False)
print(s)
'''

# 4.程序设计：用户管理系统
# 功能：
# 1.用户注册，提示用户输入用户名和密码，然后获取当前注册时间，最后将用户名、密码、注册时间写入到文件。
# 2.用户登录，只有三次错误机会，一旦错误则冻结账户（下次启动也无法登录，提示：用户已经冻结）。
'''
from datetime import datetime
import os
USER_STATUS = {}


def register():
    while 1:
        print('***注册账号***')
        user = input('请输入账号:')
        with open('user.txt', 'r', encoding='utf-8') as f:
            user_exist = False
            for line in f:
                if line.split('----')[0] == user:
                    user_exist = True
                    break
            if user_exist:
                print('账号已存在!请重新输入!')
                continue
        pwd = input('请设置密码:')
        date = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
        with open('user.txt', 'a', encoding='utf-8') as f:
            f.write('%s----%s----%s----0\n' % (user, pwd, date))
        print('注册成功!')
        return


def login():
    while 1:
        print('***登陆帐号***')
        username = input('请输入账号:')
        if USER_STATUS.get(username) == None:
            USER_STATUS[username] = 0
        with open('user.txt', 'r', encoding='utf-8') as f:
            for line in f:
                if line.split('----')[0].strip() == username:
                    if line.split('----')[-1].strip() == '3':
                        print('账号已锁定!')
                        return
        password = input('请输入密码:')
        with open('user.txt', 'r', encoding='utf-8') as f:
            for line in f:
                if line.split('----')[0]==username and line.split('----')[1]==password:
                    print('登陆成功!')
                    USER_STATUS[username] = 0
                    return
        if USER_STATUS[username] <= 2:

            USER_STATUS[username] += 1
            print(USER_STATUS)
            if USER_STATUS[username] == 3:
                with open('user.txt', 'r', encoding='utf-8') as f1, open('user(改).txt', 'w', encoding='utf-8') as f2:
                    for line in f1:
                        if line.split('----')[0] == username:
                            new_line = line.replace('0', '3',-1)
                            f2.write(new_line)
                        else:
                            f2.write(line)
                os.remove('user.txt')
                os.rename('user(改).txt', 'user.txt')
                print('账号或密码错误3次,锁定账号!')
                return
            print('账号或密码错误!请重新输入!')


# register()
login()
'''


# 5.有如下文件，请通过分页的形式将数据展示出来。【文件非常小】
# 商品|价格
# 飞机|1000
# 大炮|2000
# 迫击炮|1000
# 手枪|123
# ...


def func():
    f = open('5-6商品列表.txt', 'r', encoding='utf-8')
    a = f.read()                            # 全部读到内存
    lst = a.split('\n')
    max_page, mowei = divmod(len(lst), 3)     # 最大页,最后一页条数(总条数,每页条数)
    if mowei > 0:
        max_page += 1
    while 1:
        user = input('要查看第几页(N/n退出):')
        if user.upper() == 'N':
            return
        if not user.isnumeric() or int(user) not in range(1, max_page+1):
            print('输入有误!请重新输入!')
            continue
        start = (int(user)-1)*3
        end = (int(user))*3
        data = lst[start+1:end+1]
        print(lst[0])
        for i in data:
            print(i.strip())
        if not (int(user)>max_page or int(user)<1):
            print('当前第%s页,共%s页.' % (user,max_page))

func()
