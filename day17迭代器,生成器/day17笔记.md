# day17笔记

## 一.补充

### 1.第三方模块安装

1.pip包管理工具: pip install 模块

2.源码安装: 

- 下载源码包 : 压缩文件.
- 解压压缩包
- 打开cmd,进入python安装目录下的lib\site-packages
- 执行python3 setup.py build
- 执行python3 setup.py install

3.自定义

- py文件
- 文件夹: 要创建 __ init__.py 文件

### 2.调用模块

- import
  - import 模块1	模块1.函数()
  - import 模块1.模块2.模块3     模块1.模块2.模块3.函数()
- from xx import xxx
  - from 模块.模块 import 函数     函数()
  - from 模块.模块 import 函数 as f     f()
  - from 模块.模块 import *     函数1()  函数2()
  - from 模块 import 模块     模块.函数()
  - from 模块 import 模块 as m     m.函数()
- 特殊情况
  - import 文件夹 加载 __ init__ .py
  - from 文件 import *

## 二.迭代器

### 1.while

while + 索引 + 计数器

### 2.迭代器

对某种对象(str / list / tuple / dict / set类创建的对象)- 可迭代对象中的元素进行逐一获取,表象:具有__ next__方法且每次调用都获取可迭代对象中的元素.

- 列表转换成迭代器

  ```python
  v1 = iter([11,22,33,44])
  v1 = [11,22,33,44].__iter__()
  # iter()和.__iter__()都是转换为迭代器
  ```

- 迭代器获取每个值需反复调用

  ```python
  v1 = [11,22,33,44]
  
  # 列表转换成迭代器
  v2 = iter(v1)
  result1 = v2.__next__()
  print(result1)			# 11
  result2 = v2.__next__()
  print(result2)			# 22
  result3 = v2.__next__()
  print(result3)			# 33
  result4 = v2.__next__()
  print(result4)			# 44
  result5 = v2.__next__()
  print(result5)			# 超出范围报错StopIteration
  """
  # v1 = "alex"
  # v2 = iter(v1)
  # while True:
  #     try:
  #         val = v2.__next__()
  #         print(val)
  #     except Exception as e:
  #         break
  
  try:
  	内容
  except Exception:
  	break
  报错结束进程
  """
  ```

- 报错表示迭代完毕.

- 判断一个对象是否是迭代器:dir(对象)查看内部是否有__ next__()方法.

### 3.for循环

```python
v1 = [11,22,33,44]
"""
内部会把v1转化为迭代器,反复执行__next__(),取完不报错
"""
for i in v1:
    print(i)
```

## 三.可迭代对象

- dir(对象)查看内部是否具有__ iter__()方法,且返回一个迭代器.

- 可以被for循环

## 四.生成器(函数的变异)

```python
# 生成器函数(内部包含yield)
def func():
    print('F1')
    yield 1
    print('F2')
    yield 2
    print('F3')
    yield 100
    print('F4')
# 函数内部代码不执行,返回一个生成器
v1 = func()
# 生成器可以被for循环，一旦开始循环那么函数内部代码就会开始执行。
for i in v1:
    print(i)
```

```python
def func():
    count = 1
    while True:
        yield count		# 循环一次返回yield停止,下次循环从下一行开始.
        count += 1

val = func()		# 获取生成器

for item in val:	# 开始循环生成器
    print(item)		# 1,2,3...
```

- 总结:如果函数中存在yield,该函数就是生成器函数,调用该生成器函数会返回生成器,生成器被for循环时,内部代码才会执行,每次返回yield的值,下次循环从yield下一行开始.

```python
def func():
    count = 1
    while True:
        yield count
        count += 1
        if count == 100:		# 符合条件即停止函数.
            return

val = func()
for item in val:				# 1,2...99
    print(item)
```

示例：读取redis文件

```python
def func():
    """
    分批去读取文件中的内容，将文件的内容返回给调用者。
    :return:
    """
    cursor = 0
    while True:
        f = open('db', 'r', encoding='utf-8')# 通过网络连接上redis
        # 代指   redis[0:10]
        f.seek(cursor)
        data_list =[]
        for i in range(10):
            line = f.readline()
            if not line:
                return
            data_list.append(line)
        cursor = f.tell()
        f.close()  # 关闭与redis的连接

        for row in data_list:
            yield row

for item in func():
    print(item)
```





