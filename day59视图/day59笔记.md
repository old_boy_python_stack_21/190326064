# day59笔记

## 1.回顾

### 1.for

```django
{% for i in list %}
	{{ i }} 
	{{ forloop }}
{% endfor %}
```

- forloop
  - counter   从1开始
  - counter0   从0开始
  - revcounter  到1结束
  - revcounter0  到0结束
- first   last  布尔值
- parentloop    {}
- for...empty

```django
{% for i in list %}
	{{ i }} 
	{{ forloop }}
{% empty %}
   空空如也
{% endfor %}
```

### 2.if

```django
{% if 条件 %}
	...
{% elif 条件 %}
	...
{% else %}
	...
{% endif %}
```

- 注意点：不支持连续判断  不支持算数运算  10>5>1   10 > 5 > 1  

### 3.with

```django
{% with 变量 as new %}
	{{ new }}
{% endwith %}
```

### 4.csrf_token

- 用于跨站请求伪造保护.
- {% csrf_token %}   放在form表单内   有一个隐藏的input标签   name=‘csrfmiddlewaretoken’  

### 5.母版和继承

- 母版：
  - 就是一个HTML页面，提取到多个页面的公共部分，页面中定义多个block块
- 继承：

1. {%  extends 'base.html' %}
2. 重写block块

- 注意点：

1. {%  extends   ‘base.html’ %}  写在第一行   上面不要有内容
2. {%  extends   ‘base.html’ %}  ‘base.html’ 加上引号   不然会当做是变量
3. 想显示的内容放在block块中
4. 多定义些block块  css   js 

### 6.组件

- 组件  一小段HTML代码段  nav.html 
- 使用：
  - {%  include  'nav.html'  %}

### 7.静态文件

- {% load static  %}
- {%  static ‘静态文件的相对路径’ %}   
- {%  get_static_prefix  %}      ——》   得到settings中的STATIC_URL  

### 8.自定义方法

- filter / simple_tag / inclusion_tag 

1. 在已注册的APP下创建templatetags的python包；

2. 在包内创建python文件  ——》  my_tags.py

3. 在py文件中写固定的内容：

   ```python 
   from django import template
   register = template.Library()
   ```

4. 写函数 + 装饰器

   ```python
   @register.filter(name='add_a')
   def add_xx(value,arg):
       return 'addd_xx'
   
   @register.simple_tag
   def join_str(*args,**kwargs):
       return  'xxxxx'
   
   @register.inslusion_tag('page.html')
   def page(num):
       return {'num':range(1,num+1)} #返回字典
   ```

5. 写模板

   ```django
   {% for i in num %}
   	{{ i }}
   {% endfor %}
   ```

6. 使用

   ```django
   {% load my_tags %} {#加载自定义标签内容#}
   {{ 'xxx'|add_xx:'a'  }}      {{ 'xxx'|add_a:'a'  }}  
   {% join_str 1 2 k1=3 k2=4  %}
   {% page 4 %}
   ```

   

## 2.今日内容

### 1.CBV和FBV

- FBV   function based  view    基于函数的视图

- CBV   class based view    基于类的视图

  定义CBV：

  ```python
  from django.views import View
  
  class AddPublisher(View):
      def get(self,request):
          """处理get请求"""
          return response
  
      def post(self,request):
          """处理post请求"""
          return response
  ```

    使用CBV：

  ```python
  url(r'^add_publisher/', views.AddPublisher.as_view()),
  ```

### 2.as_view的流程

1.项目启动 加载urls.py时，执行类.as_view()    ——》  得到view函数

2.请求到来的时候执行view函数：

1. 实例化当前类  ——》 self

   ​	self.request = request 

2. 执行self.dispatch(request, *args, **kwargs)

   1. 判断请求方式是否被允许：
      1. 允许    通过反射获取到对应请求方式的方法   ——》 handler
      2. 不允许   self.http_method_not_allowed  ——》handler
   2. 执行handler（request，*args,**kwargs）

    返回响应   —— 》 浏览器

### 3.视图加装饰器

- FBV  本身就是函数,直接加装饰器
- CBV
  - 类中的方法与独立函数不完全相同，因此不能直接将函数装饰器应用于类中的方法 ，我们需要先将其转换为方法装饰器。Django中提供了method_decorator装饰器用于将函数装饰器转换为方法装饰器。
  - 注意，请求过来后会先执行dispatch()这个方法，如果需要批量对具体的请求处理方法，如get，post等做一些操作的时候，这里我们可以手动改写dispatch方法，这个dispatch方法就和在FBV上加装饰器的效果一样.

```
from django.utils.decorators import method_decorator # 导入
```

1.加在方法上

```python
@method_decorator(timer)
def get(self, request, *args, **kwargs):
    """处理get请求"""
```

2.加在dispatch方法上

```python
#源码
@method_decorator(timer)
def dispatch(self, request, *args, **kwargs):
    # print('before')
    ret = super().dispatch(request, *args, **kwargs)
    # print('after')
    return ret

#views.py
@method_decorator(timer,name='dispatch')
class AddPublisher(View):
	...
```

3.加在类上

```python
@method_decorator(timer,name='post')
@method_decorator(timer,name='get')
#或
@method_decorator(timer,name='dispatch')
class AddPublisher(View):
    ...
```

- 区别：
  - 不使用method_decorator装饰
    - func   ——》 <function AddPublisher.get at 0x000001FC8C358598>
    - args  ——》 (<app01.views.AddPublisher object at 0x000001FC8C432C50>, <WSGIRequest: GET '/add_publisher/'>)
  - 使用method_decorator装饰之后,装饰器中第一个参数就是request对象：
    - func ——》 <function method_decorator.<locals>._dec.<locals>._wrapper.<locals>.bound_func at 0x0000015185F7C0D0>
    - args ——》 (<WSGIRequest: GET '/add_publisher/'>,)

### 4.request对象 

- 当一个页面被请求时，Django就会创建一个包含本次请求原信息的HttpRequest对象。Django会将这个对象自动传递给响应的视图函数，一般视图函数约定俗成地使用 request 参数承接这个对象。
- https://www.cnblogs.com/maple-shaw/articles/9285269.html

```python
# 属性
request.method   请求方式  GET/POST
request.GET    url上携带的参数
request.POST   POST请求提交的数据
request.path_info(同path)   URL的路径    不包含ip和端口  不包含参数
request.scheme	表示请求方案的字符串(http/https)
request.encoding	字符串,表示提交数据的编码方式(如果为None表示使用默认值utf-8)
request.body    请求体,byte类型
request.FILES   类似字典对象,包含上传文件的信息
#FILES中的每个键为<input type="file" name="" /> 中的name，值则为对应的数据,只有在请求的方法为POST,且提交的<form>带有enctype="multipart/form-data"的情况下才会包含数据,否则FILES将为一个空的类似于字典的对象。默认保存到manage.py同级目录下.
request.META    请求头,字典,包含所有的HTTP首部.
#除'CONTENT_LENGTH'和'CONTENT_TYPE'之外，请求中的任何'HTTP'首部转换为'META'的键时，都会将所有字母大写并将连接符替换为下划线最后加上'HTTP_'前缀。
request.COOKIES  字典,含所有cookie,键值都为字符串
request.session	 既可读又可写的类似于字典的对象，表示当前的会话。
request.user	表示当前登录的用户

# 方法
Request.is_secure()		如果请求时是安全的，则返回True；即请求通是过 HTTPS 发起的
request.get_full_path()   URL的路径,不包含ip和端口,包含参数
request.is_ajax()    判断是不是ajax请求
```

### 5.response对象

- HttpResponse默认Content-Type为text/html
- JsonResponse默认Content-Type为application/json,自动做反序列化,返回非字典时设置safe=False

```python
from django.shortcuts import render, redirect, HttpResponse

HttpResponse('字符串')    ——》  返回字符串
render(request,'模板的文件名',{k1:v1})   ——》 返回一个完整的TML页面
redirect('重定向的地址')    ——》 重定向   本质返回Location响应头：地址

from django.http.response import JsonResponse
JsonResponse({})
JsonResponse([],safe=False)
```








