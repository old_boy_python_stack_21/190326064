from django.shortcuts import render

# Create your views here.

def upload(request):
    if request.method == 'POST':
        file = request.FILES.get('upload')
        with open(file.name, 'wb')as f:
            for i in file.chunks():
                f.write(i)
    return render(request, 'index.html')
