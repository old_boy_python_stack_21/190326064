from django.shortcuts import render, redirect, HttpResponse
from app01 import models


def index(request):
    all_stu = models.Student.objects.all()
    all_cls = models.Classes.objects.all()
    all_tea = models.Teacher.objects.all()
    return render(request, 'index.html', {'all_stu': all_stu, 'all_cls': all_cls, 'all_tea': all_tea})


def add_stu(request):
    if request.method == 'POST':
        sname = request.POST.get('stu_name')
        score = request.POST.get('score')
        sid_id = request.POST.get('sid_id')
        models.Student.objects.create(sname=sname, score=score, cls_id=sid_id)
        return redirect('/index/')
    all_cls = models.Classes.objects.all()
    return render(request, 'add_stu.html', {'all_cls': all_cls})


def edit_stu(request):
    pk = request.GET.get('id')
    stu_obj = models.Student.objects.get(pk=pk)
    if request.method == 'POST':
        stu_name = request.POST.get('stu_name')
        score = request.POST.get('score')
        sid_id = request.POST.get('sid_id')
        stu_obj.sname = stu_name
        stu_obj.score = score
        stu_obj.cls_id = models.Classes.objects.get(pk=sid_id)
        stu_obj.save()
        return redirect('/index/')
    all_stu = models.Student.objects.all()
    all_cls = models.Classes.objects.all()
    return render(request, 'edit_stu.html', {'stu_obj': stu_obj, 'all_stu': all_stu, 'all_cls': all_cls})


def del_stu(request):
    pk = request.GET.get('id')
    models.Student.objects.filter(pk=pk).delete()
    return redirect('/index/')
