from django.db import models


class Classes(models.Model):
    cid = models.AutoField(primary_key=True)
    cname = models.CharField(max_length=16)


class Teacher(models.Model):
    tid = models.AutoField(primary_key=True)
    tname = models.CharField(max_length=16)
    cls = models.ForeignKey('Classes')


class Student(models.Model):
    sid = models.AutoField(primary_key=True)
    sname = models.CharField(max_length=16)
    score = models.IntegerField()
    cls = models.ForeignKey('Classes')
