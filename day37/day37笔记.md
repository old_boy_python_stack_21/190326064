# day37笔记

## 1.回顾

### 1.1协程

- 协程:多任务在一条线程上来回切换
- 为什么用协程:在一条线程上最大限度的提高CPU的使用率,在一个任务中遇到io的时候就切换到其他任务.
- 特点:
  - 开销很小,是用户级的(只能感知从用户级别能够感知的IO操作)
  - 不能利用多核,数据共享,数据安全.

### 1.2模块和用法

- gevent   基于greenlet切换

  - 先导入模块

  - 导入monkey,执行patch_all

  - 写一个函数当作协程要执行的任务

  - 协程对象 = gevent.spawn(函数名,参数)

  - 协程对象.join()或者gevent.joinall([协程对象,])

  - 分辨gevent是否识别的我们写的代码中的io操作的方法:

    ```python
    1.在patch_all之前打印一下涉及到io操作的函数地址
    2.在patch_all之后打印一下涉及到io操作的函数地址
    3.如果两地址一致,说明gevent没有识别这个io,如果不一致说明识别.
    ```

    

- asyncio   基于yield机制切换

  - async   标识一个协程函数

  - await   后面跟着一个asyncio模块提供的io操作的函数

  - loop   事件循环,负责在多个任务之间进行切换

  - 最简单的协程函数是如何完成的

    ```python
    import asyncio
    async def func():
        print('start')
        await asyncio.sleep(1)
        print('end')
    loop = asyncio.get_event_loop()
    # 单个任务
    loop.run_until_complete(func())
    # 多个任务
    wait_lst = asyncio.wait([func(),func()])
    loop.run_until_complete(wait_lst)
    ```

    

## 2.作业知识点补充

### 1.GIL锁

```python
1.全局解释器锁
2.由Cpython解释器提供的
3.导致一个进程中多个线程同一时刻只能有一个线程访问CPU
```



### 2.进程/线程中都需要用到锁的概念

- 互斥锁   在一个线程中不能连续多次acquire,效率高,产生死锁几率大.
- 递归锁   在一个线程中可以连续多次acquire,效率低,一把锁永远不死锁.

### 3.进程线程协程各自特点

- 进程   开销大,数据隔离,能利用多核,数据不安全,操作系统控制
- 线程   开销中,数据共享,cpython解释器下不能利用多核,数据不安全,操作系统控制
- 协程   开销小,数据共享,不能利用多核,数据安全,用户控制.
- 在哪些地方用到线程和协程:
  - 1.自己用线程,协程完成爬虫任务
  - 2.后来有丰富的爬虫框架
    - scrapy/beautyful soup/aiogttp
  - 3.web框架中的并发是如何实现的
    - 传统框架:Django多线程/flask 优先选用协程 其次使用线程
    - socket  server : 多线程
    - 异步框架 : tornado , sanic 底层都是协程

### 4.IPC进程之间的通信机制

- 内置的模块(基于文件):Pipe,Queue
- 第三方工具(基于网络):redis,rabbmq,kafka,memcache发挥的都是消息中间件的功能



## 3.mysql

### 3.1数据库

- 很多功能如果知识通过操作文件来改变数据非常繁琐
  - 程序员需要做很多事情
- 对于多台机器或者多个进程操作用一份数据
  - 程序员自己解决并发和安全问题比较麻烦
- 自己处理一些数据备份,容错的措施

### 3.2C/S架构   操作数据文件的一个管理软件

- 1.帮助我们解决并发问题
- 2.能够帮助我们用更简单更快速的方式完成数据的增删改查
- 3.能够给我们提供一些容错,高可用的机制
- 4.权限的认证

### 3.3数据库管理系统

- 专门用来管理数据文件,帮助用户更简洁的操作数据的软件
  - DBMS
  - 数据  data
  - 文件
  - 文件夹 ---- 数据库 database  db
  - 数据库管理员 ---- DBA

- 数据库管理系统
  - 关系型数据库
    - sql server
    - oracle    收费,比较严谨,安全性比较高
      - 国企  事业单位
      - 银行  金融行业
    - mysql    开源,
      - 小公司
      - 互联网公司
    - sqllite
  - 非关系型数据库(快,键值对形式存储,只能根据键找值)
    - redis
    - mongodb

### 3.4安装相关

- 安装路径
  - 不能有空格
  - 不能有中文
  - 不能带着有转义的特殊字符开头的文件夹名
- 安装之后发现配置有问题
  - 再修改配置往往不能生效
- 卸载之后重装
  - mysqld remove
  - 把所有的配置,环境变量修改到正确的样子
  - 重启计算机 - 清空注册表
  - 再重新安装

### 3.5mysql的CS架构

- mysqld install  安装数据库服务
- net start mysql  启动数据库的server端
  - 停止server  :  net stop mysql
- 客户端可以是python代码,也可以是一个程序
  - mysql.exe 是一个客户端
  - mysql -u用户名 -p密码

### 3.6mysql中的用户和权限

- 在安装数据库之后,有一个最高权限的用户root
- mysql -hserver端的ip -u用户名 -p密码
  - mysql -h192.168.12.87 -uroot -p123

- 我们的mysql客户端不仅可以连接本地的数据库,也可以连接网络上的某一个数据库的server端
- mysql>select user();
  - 查看当前用户是谁
- mysql>set password = password('密码');
  - 设置密码
- 创建用户
  - create user 's21'@'192.168.12.%' identified by '123';
  - mysql -us21 -p123 -hIP地址

- 授权
  - grant all所有/select只能查 on \*.* to 's21'@'192.168.12.%';
  - ```*.*```  第一个*表示所有库,第二个表示所有表
- 授权并创建用户
  - grand all on day37.* to 'alex'@'192.168.12.%' identified by '123';

- 查看文件夹
  - show databases;
- 创建文件夹
  - create databases 文件夹名;

### 3.7库,表,数据

- 创建库/表  DDL数据库定义语言
- 存数据,删除/修改数据,查看   DML数据库操纵语句
- grant/revoke  DCL控制权限

- 库
  - create database 数据库名;   创建库
  - show databases;   查看当前有多少哥数据库
  - select database();  查看当前使用的数据库
  - use 数据库名;   切换到这个数据库(文件夹)下
- 表
  - 查看当前文件夹中有多少张表
    - show tables;
  - 创建表
    - create table 表名(id int,name char(4));
  - 删除表
    - drop table 表名;
  - 查看表结构
    - desc 表名;
- 操作表中数据(DML)
  - 数据的增加
    - insert into 表名 values(1,'alex');
  - 数据的查看
    - select * from 表名;
  - 修改数据
    - update 表 set 字段名=值;   修改所有字段
    - update 表 set 字段名=值 where id=1;   修改指定id
  - 删除数据
    - delete from 表名;  全部删除
    - delete from 表名 where id=1;  删除指定id


