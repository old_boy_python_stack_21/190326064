#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 数据库练习
# 1.创建一个数据库，ftp
"""
create database ftp;
"""

# 2.创建一张表，userinfo表
# 存储三个字段：id,username,password
"""
create table userinfo(id int,username char(16),password char(16));
"""

# 3.向这张表中插入3条数据
# | 1 | alex | alex3714 |
# | 2 | wusir | 6666 |
# | 3 | yuan | 9999 |
'''
insert into userinfo values(1,'alex','alex3714');
insert into userinfo values(2,'wusir','6666');
insert into userinfo values(3,'yuan','9999');
'''

# 4.查看表中的所有数据
'''
select * from userinfo;
'''

# 5.修改wusir的密码,变成666
'''
update userinfo set password=666 where username='wusir';
'''

# 6.删除yuan这一行信息
'''
delete from userinfo where username='yuan'
'''

# python练习
# 1.有一个文件，这个文件中有20000行数据，开启一个线程池，为每100行创建一个任务，打印这100行数据。
'''
from threading import RLock
from concurrent.futures import ThreadPoolExecutor


def func(file, loc):
    with lock:
        with open(file, 'r', encoding='utf-8')as f:
            for i in range(loc):
                f.readline()
            msg = ''
            for i in range(100):
                msg += f.readline()
            return msg


lock = RLock()
tp = ThreadPoolExecutor(10)
loc = 0
lst = []
for i in range(200):
    t = tp.submit(func, r'D:\homework\day37\test.md', loc)
    lst.append(t)
    loc += 100
tp.shutdown()
for i in lst:
    print(i.result())
print('Done')
'''

# 2.写一个socket 的udp代码，client端发送一个时间格式，server端按照client的格式返回当前时间
# 例如 client发送'%Y-%m-%d'，server端返回'2019-5-20'
# server端
'''
import socket
from datetime import datetime

sk = socket.socket(type=socket.SOCK_DGRAM)
sk.bind(('127.0.0.1', 8080))
while 1:
    msg, add = sk.recvfrom(1024)
    res = datetime.now().strftime(msg.decode('utf-8')).encode('utf-8')
    sk.sendto(res, add)
sk.close()
'''

# client端
'''
import socket

sk = socket.socket(type=socket.SOCK_DGRAM)
msg = '%Y-%m-%d'.encode('utf-8')
sk.sendto(msg, ('127.0.0.1', 8080))
res = sk.recv(1024).decode('utf-8')
print(res)
sk.close()
'''

# 3.请总结进程、线程、协程的区别。和应用场景。
'''
进程:开销大,数据隔离,资源分配单位,能利用多核,数据不安全,操作系统控制.
线程:开销中,数据共享,Cpython解释器下不能利用多核,CPU调度的最小单位,数据不安全,操作系统控制.
协程:开销小,数据共享,不能利用多核,数据安全,由用户控制.本质也是线程.
'''
