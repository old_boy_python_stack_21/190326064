

# 1、查询男生、女生的人数；
"""
select gender,count(*) from student group by gender;
"""

# 2、查询姓“张”的学生名单；
"""
select * from student where sname like '张%';
"""

# 3、课程平均分从高到低显示
'''
select ,avg(num) as a from score group by course_id order by a desc;
'''

# 4、查询有课程成绩小于60分的同学的学号、姓名；
'''
select student.sid,sname from student inner join (select student_id from score where num<60)as o on student.sid = o.student_id
'''

# 5、查询至少有一门课是学号为1的同学所学课程的同学的学号和姓名；
'''
select student_id,student.sname from score inner join student on student.sid = score.student_id where score.course_id in (select course_id from score where student_id=1) group by student_id;
'''

# 6、查询出只选修了一门课程的全部学生的学号和姓名；
'''
select student.sid,sname from student inner join score on score.student_id = student.sid group by student_id having count(course_id)=1;
'''

# 7、查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分；
'''
select course_id,max(num),min(num) from score group by course_id;
'''

# 8、查询课程编号“2”的成绩比课程编号“1”课程低的所有同学的学号、姓名；
'''
课程2成绩和学号:select student_id,num from score where course_id=2;
课程1成绩和学号:select student_id,num from score where course_id=1;
结果:select sid,sname from student where sid in (select t2sid from (select student_id as t2sid,num from score where 
course_id=2) as t2 inner join (select student_id as t1sid,num from score where course_id=1)as t1 on t2.t2sid = t1.t1sid where t2.num>t1.num);
'''

# 9、查询“生物”课程比“物理”课程成绩高的所有学生的学号；
'''
生物id:select cid from course where cname='生物';
物理id:select cid from course where cname='物理';
select t1.student_id from (select num num1,student_id from score where course_id=1) as t1 inner join (select num num2,
student_id from score where course_id=2) as t2 on t1.student_id = t2.student_id where num1>num2;
'''

# 10、查询平均成绩大于60分的同学的学号和平均成绩;
'''
select sid,avg(num) from score group by student_id having avg(num)>60;
'''

# 11、查询所有同学的学号、姓名、选课数、总成绩；
'''
select student_id,sname,选课数,res from ((select student_id ,count(*) 选课数,sum(num) res from score group by student_id)
 as t inner join (select * from student)as o on o.sid=t.student_id);
'''

# 12、查询姓“李”的老师的个数；
'''
select count(*) from teacher where tname like '李%';
'''

# 13、查询没学过“张磊老师”课的同学的学号、姓名；
'''
序号:select tid from teacher where tname = '张磊老师';
select sid,sname from student as t inner join (select student_id,GROUP_CONCAT(course_id) ccid from score group by 
student_id having ccid not in (select cid from (select tid from teacher where tname = '张磊老师') t1 inner join course
 on t1.tid=course.teacher_id))as o on o.student_id=t.sid;
'''

# 14、查询学过“1”并且也学过编号“2”课程的同学的学号、姓名；*
'''
select sid,sname from student t inner join (select student_id,course_id cid from score where course_id = 1 or 
course_id=2 group by student_id having count(cid)=2)as o on o.student_id=t.sid;
'''

# 15、查询学过“李平老师”所教的所有课的同学的学号、姓名；
'''
李平教的课:select cid from(select tid from teacher where tname='李平老师') t1 inner join course on t1.tid=course.teacher_id;
select sid,sname from student where sid in (select student_id from(select cid from(select tid from teacher where 
tname='李平老师') t1 inner join course on t1.tid=course.teacher_id)t2 inner join score on t2.cid=score.course_id group 
by student_id having count(course_id)=2);
'''

# 1、查询没有学全所有课的同学的学号、姓名；
'''
select sid,sname from student inner join (select student_id stid,course_id from score) t1 on student.sid=t1.stid group by sid having count(sid)<4;
'''

# 2、查询和“002”号的同学学习的课程完全相同的其他同学学号和姓名；
'''
select student_id,sname from (
select student_id,sname,GROUP_CONCAT(course_id)as f from score t1 inner join (
select sid ss,sname from student) t2 on ss=t1.student_id group by ss
having f = (select course_id from score where student_id = 2));
'''

# 3、删除学习“叶平”老师课的SC表记录；
'''

'''

# 4、向SC表中插入一些记录，这些记录要求符合以下条件：①没有上过编号“002”课程的同学学号；②插入“002”号课程的平均成绩；
# 5、按平均成绩从低到高显示所有学生的“语文”、“数学”、“英语”三门的课程成绩，按如下形式显示： 学生ID,语文,数学,英语,
# 有效课程数,有效平均分；

# 6、查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分；
'''
select course_id,max(num),min(num) from score group by course_id;
'''

# 7、按各科平均成绩从低到高和及格率的百分数从高到低顺序；
'''
select student_id,avg(num) from score group by student_id order by avg(num) desc ;
'''

# 8、查询各科成绩前三名的记录:(不考虑成绩并列情况)
'''
select student_id,course_id,num from score where course_id=1 order by num desc limit 3;
select student_id,course_id,num from score where course_id=2 order by num desc limit 3;
select student_id,course_id,num from score where course_id=3 order by num desc limit 3;
select student_id,course_id,num from score where course_id=4 order by num desc limit 3;
'''

# 9、查询每门课程被选修的学生数；
# 10、查询同名同姓学生名单，并统计同名人数；
'''
select sname,count(*) from student group by sname;
'''

# 11、查询每门课程的平均成绩，结果按平均成绩升序排列，平均成绩相同时，按课程号降序排列；
'''
select course_id,avg(num) from score group by course_id order by avg(num) asc,course_id desc;
'''

# 12、查询平均成绩大于85的所有学生的学号、姓名和平均成绩；
'''
select student_id,avg(num) from score group by student_id having avg(num)>85;
'''

# 13、查询课程名称为“数学”，且分数低于60的学生姓名和分数；
# 14、查询课程编号为003且课程成绩在80分以上的学生的学号和姓名；
'''
select student_id,num from score where course_id=3 having num>80;
'''

# 15、求选了课程的学生人数
'''
select count(distinct student_id) from score;
'''

# 16、查询选修“杨艳”老师所授课程的学生中，成绩最高的学生姓名及其成绩；
'''
'''

# 17、查询各个课程及相应的选修人数；
'''
select student_id from score group by course_id;
'''

# 18、查询不同课程但成绩相同的学生的学号、课程号、学生成绩；
'''
select GROUP_CONCAT(student_id) t,GROUP_CONCAT(course_id),num from score group by num having count(t)>1;
'''

# 19、查询每门课程成绩最好的前两名；
# 20、检索至少选修两门课程的学生学号；
# 21、查询全部学生都选修的课程的课程号和课程名；
# 22、查询没学过“叶平”老师讲授的任一门课程的学生姓名；
# 23、查询两门以上不及格课程的同学的学号及其平均成绩；
# 24、检索“004”课程分数小于60，按分数降序排列的同学学号；
# 25、删除“002”同学的“001”课程的成绩；
