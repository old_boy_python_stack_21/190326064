# day41笔记

## 1.回顾

### 1.单表查询

- select (distinct)
- from
- where
  - 比较运算符 ```> < = != <> >= <=```
  - 逻辑运算符 and or not
  - 身份运算符 is is not 
  - 范围
    - 多选一 in (12,34,6) /  in ('技术','销售')
    - 数字的范围：between a and b
    - 字符串的范围
      - like   %  _
      - regexp 正则
- group by  分组
  - 聚合函数 count sum avg min max
- having 过滤组
  - 在groupby之后执行的
  - having中可以使用聚合函数
  - having 后面的条件要么是select的字段，要么是分组的字段
- order by
  - 排序 asc(默认升序) desc(降序)
- limit  (offset)
  - limit n
  - limit m,n == limit n offset m

### 2.多表查询

- 连表查询  更高效
  - 内连接  inner join
  - 外连接  left join / right join
- 子查询
  - 在查询一个结果的时候，依赖一个条件也是一个sql语句

## 2.索引原理

### 1.为什么要有索引

一般的应用系统，读写比例在10:1左右，而且插入操作和一般的更新操作很少出现性能问题，在生产环境中，我们遇到最多的，也是最容易出问题的，还是一些复杂的查询操作，因此对查询语句的优化显然是重中之重。说起加速查询，就不得不提到索引了。

### 2.什么是索引 - 目录

- 就是建立起在存储表阶段就有的一个存储结构,能在查询的时候加速
- 索引在MySQL中也叫是一种“键”，是存储引擎用于快速找到记录的一种数据结构。索引对于良好的性能非常关键，尤其是当表中的数据量越来越大时，索引对于性能的影响愈发重要。

### 3.索引的重要性

- 读写比例  10:1
- 读(查询)的速度至关重要

### 4.索引原理

- 通过不断地缩小想要获取数据的范围来筛选出最终想要的结果，同时把随机的事件变成顺序的事件，也就是说，有了这种索引机制，我们可以总是用同一种查找方式来锁定数据。
- block 磁盘预读原理
  - for line in f(慢)
  - 4096字节
  - 当一次IO时，不光把当前磁盘地址的数据，而是把相邻的数据也都读取到内存缓冲区内，因为局部预读性原理告诉我们，当计算机访问一个地址的数据的时候，与其相邻的数据也会很快被访问到。每一次IO读取的数据我们称之为一页(page)。具体一页有多大数据跟操作系统有关，一般为4k或8k，也就是我们读取一页内的数据时候，实际上才发生了一次IO
- 读硬盘的io操作的时间非常长,比CPU执行指令的时间长很多
- 尽量减少io次数才是读写数据的主要要解决的问题.

### 5.数据库的存储方式

- 新的数据结构 -- 树
- 平衡树 balance tree - b树
- 在b树的基础上进行了改良 - b+树
  - 1.分支节点和根节点都不再存储实际的数据了,让分支和根节点能存储更多的索引信息,就降低了树高度,所有的实际数据都存储在叶子节点中.
  - 2.在叶子节点之间加入了双向的链式结构(每个叶子节点都指向相邻的叶子节点地址),方便在范围查找时,只需要查找两个节点即可.
- mysql当中所有的b+树索引的高度都基本控制在3层.
  - 1.io操作的次数非常稳定
  - 2.有利于通过范围查询
- 什么会影响索引的效率 - 树的高度
  - 1.对哪一列创建索引,选择尽量短的列做索引
  - 2.对区分度高的列建索引,重复率超过了10%那么不适合创建索引

### 6.聚集索引和辅助索引

- 在innodb中,聚集索引和辅助索引并存
  - 聚集索引 - 主键,更快
    - 数据直接存储在树结构的叶子节点
  - 辅助索引 - 除了主键之外所有的索引都是辅助索引,稍慢
    - 数据不直接存储在树中
- 在myisam中,只有辅助索引,没有聚集索引

### 7.索引的种类

- primary key  主键 聚集索引  约束:非空+唯一
  - 联合主键
- unique 自带索引 辅助索引 约束:唯一
  - 联合唯一
- index 辅助索引   没有约束
  - 联合索引

### 8.创建删除索引

- 创建索引

  - 建表时设置索引

    ```mysql
    CREATE TABLE 表名 (
    	字段名1  数据类型 [完整性约束条件…],
    	字段名2  数据类型 [完整性约束条件…],
    	[UNIQUE | FULLTEXT | SPATIAL ]   INDEX | KEY
    	[索引名]  (字段名[(长度)]  [ASC |DESC]) 
    	);
    ```

  - 在已存在的表上创建

    - create index 索引名字 on 表(字段);
    - ALTER TABLE 表名 ADD INDEX 索引名 ;

- 删除索引

  - drop index 索引名 on 表名;

### 9.索引是如何发挥作用的

- select * from 表 where id=xxx;
  - 在id字段没有索引时效率低
  - 在id字段有索引之后效率高

### 10.聚集索引和其他索引的区别

```
聚集索引
1.纪录的索引顺序与物理顺序相同,因此更适合between and和order by操作
2.叶子结点直接对应数据,从中间级的索引页的索引行直接对应数据页
3.每张表只能创建一个聚集索引

非聚集索引
1.索引顺序和物理顺序无关
2.叶子结点不直接指向数据页
3.每张表可以有多个非聚集索引，需要更多磁盘和内容,多个索引会影响insert和update的速度
```

## 3.正确使用索引

- email作为条件
  - 不添加索引时肯定慢
  - 查询的字段不是索引字段也慢

- id作为条件时
  - 如果不加索引,速度慢
  - 加了索引速度快

### 1.索引不生效的原因

- 1.要查询的数据的范围大
  - ``` >  <   >=   <=   != ```
  - between   and
    - 慢:select * from 表 order by age limit 100000,5
    - 快:select * from 表 where id between 100000,100005
  - like
    - 结果的范围大,索引不生效
    - 如果abc%索引生效,%abc索引就不生效

- 2.如果一列内容的区分度不高,索引也不生效

  - name列

- 3.对两列内容进行条件查询

  - and  两端内容优先选择一个有索引的,并且树形结构更好的进行查询.
    - 两个条件都成立才能完成where条件，先完成范围小的缩小后面条件的压力

      ```mysql
      select * from s1 where id =1000000 and email = 'eva1000000@oldboy';
      ```
  - or  不会进行优化,只根据条件从左到右依次筛选
    - 条件中带有or的要想命中索引，这些条件中所有的列都是索引列

      ```sql
      select * from s1 where id =1000000 or email = 'eva1000000@oldboy';
      ```

- 4.注意事项

  ```
  - 避免使用select *
  - 使用count(*)
  - 创建表时尽量使用 char 代替 varchar
  - 表的字段顺序固定长度的字段优先
  - 组合索引代替多个单列索引（由于mysql中每次只能使用一个索引，所以经常使用多个条件查询时更适合使用组合索引）
  - 尽量使用短索引
  - 使用连接（JOIN）来代替子查询(Sub-Queries)
  - 连表时注意条件类型需一致
  - 索引散列值（重复少）不适合建索引，例：性别不适合
  ```

### 2.联合索引

- 指对表上的多个列合起来做一个索引。

- create index ind_mix on s1(id,name,email);
- select * from s1 where id =1000000 and email = 'eva1000000@oldboy';

- 在联合索引中如果使用了or条件索引就不能生效

- 最左前缀原则 ：在联合索引中，条件必须含有在创建索引的时候的第一个索引列

  - select * from s1 where id =1000000;    能命中索引

  - select * from s1 where email = 'eva1000000@oldboy';  不能命中索引

    ```sql
    (a,b,c,d)
    a,b		a,c		a	  a,d	  a,b,d	     a,c,d	  a,b,c,d
    ```

- 在整个条件中，从开始出现模糊匹配的那一刻，索引就失效了

  - select * from s1 where id >1000000 and email = 'eva1000001@oldboy';
  - select * from s1 where id =1000000 and email like 'eva%';

- 什么时候用联合索引

  - 只对a 对abc 条件进行索引 , 而不会对b 对c进行单列的索引
  - 第一个键相同的情况下，已经对第二个键进行了排序处理

### 3.单列索引

- 选择一个区分度高的列建立索引，条件中的列不要参与计算，条件的范围尽量小，使用and作为条件的连接符
- 使用or来连接多个条件
  - 在满足上述条件的基础上
  - 对or相关的所有列分别创建索引

### 4.覆盖索引

- 如果我们使用索引作为条件查询，查询完毕之后，不需要回表查，覆盖索引
- explain select id from s1 where id = 1000000;
- explain select count(id) from s1 where id > 1000000;
- 好处 : 辅助索引不包含整行记录的所有信息，故其大小要远小于聚集索引，因此可以减少大量的IO操作

### 5.合并索引

- 对两个字段分别创建索引，由于sql的条件让两个索引同时生效了，那么这个时候这两个索引就成为了合并索引

### 6.执行计划

- 如果你想在执行sql之前就知道sql语句的执行情况，那么可以使用执行计划
- explain sql   --> 并不会真正的执行sql，而是会给你列出一个执行计划

### 7.慢查询优化的步骤

```
0.先运行看看是否真的很慢，注意设置SQL_NO_CACHE
1.where条件单表查，锁定最小返回记录表。这句话的意思是把查询语句的where都应用到表中返回的记录数最小的表开始查起，单表每个字段分别查询，看哪个字段的区分度最高
2.explain查看执行计划，是否与1预期一致（从锁定记录较少的表开始查询）
3.order by limit 形式的sql语句让排序的表优先查
4.了解业务方使用场景
5.加索引时参照建索引的几大原则
6.观察结果，不符合预期继续从0分析
```

### 8.慢日志管理

- 慢日志
  - 执行时间>10
  - 未命中索引
  - 日志文件路径
- 配置
  - 内存
  - 配置文件

## 4.数据备份和事务

### 1.数据库的逻辑备份

- 语法
  - mysqldump -h服务器 -u用户名 -p密码 数据库名 > 备份文件.sql

```mysql
#单库备份
mysqldump -uroot -p123 db1 > db1.sql
mysqldump -uroot -p123 db1 table1 table2 > db1-table1-table2.sql

#多库备份
mysqldump -uroot -p123 --databases db1 db2 mysql db3 > db1_db2_mysql_db3.sql

#备份所有库
mysqldump -uroot -p123 --all-databases > all.sql
```

### 2.数据恢复

```mysql
#方法一：
[root@egon backup]# mysql -uroot -p123 < /backup/all.sql

#方法二：
mysql> use db1;
mysql> SET SQL_LOG_BIN=0;   #关闭二进制日志，只对当前session生效
mysql> source /root/db1.sql
```

### 3.事务和锁

```mysql
begin;  # 开启事务
select * from emp where id = 1 for update;  # 查询id值，for update添加行锁；
update emp set salary=10000 where id = 1; # 完成更新
commit; # 提交事务
rollback; # 事务回滚
```

## 5.sql注入

- 通过把sql命令插入到字符串中,达到欺骗服务器执行恶意的sql命令.
- 错误(--后加空格)

```Python
sql = "select * from userinfo where name = '%s' and password = '%s'"%(username,password)
```

- 正确

```Python
import pymysql

conn = pymysql.connect(host = '127.0.0.1',user = 'root',
                       password = '123',database='day41')
cur = conn.cursor()
username = input('user >>>')
password = input('passwd >>>')
sql = "select * from userinfo where name = %s and password = %s"
cur.execute(sql,(username,password))
print(cur.fetchone())
cur.close()
conn.close()
```









