#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.列举你常见的内置函数。
"""
强制转换:int() / str() / list() / dict() / tuple() / set() / bool()
数学相关:sum() / max() / min() / divmod() / float() / abs()
输入输出:input() / print()
其他:len() / open() / type() / id() / range()
"""

# 2.列举你常见的内置模块？
'''
json / getpass / os / sys / random / hashlib / copy / shutil / time
'''

# 3.json序列化时，如何保留中文？
'''
import json
a = '你好'
s = json.dumps(a,ensure_ascii=False)
print(s)
'''

# 4.程序设计：用户管理系统
# 功能：
# 1.用户注册，提示用户输入用户名和密码，然后获取当前注册时间，最后将用户名、密码、注册时间写入到文件。
# 2.用户登录，只有三次错误机会，一旦错误则冻结账户（下次启动也无法登录，提示：用户已经冻结）。

"""
from datetime import datetime
import os

USER = {}


def user():
    '''
    注册用户
    :return:
    '''
    username = input('注册用户名:')
    with open('4用户注册.txt', 'r', encoding='utf-8') as f:
        for line in f:
            if line.split('_')[0].strip() == username:
                return '帐户已存在!'
    pwd = input('密码:')
    sj = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    msg = '_'.join([username, pwd, sj, True])
    with open('4用户注册.txt', 'a', encoding='utf-8') as f:
        f.write(msg + '\n')
    return '注册成功!'


def login():
    '''
    登陆
    :return:
    '''
    while 1:
        username = input('用户名:')
        with open('4用户注册.txt', 'r', encoding='utf-8') as f2:
            status = False
            for line in f2:
                if line.split('_')[0] == username:
                    if line.split('_')[-1].strip() == 'False':
                        return '帐户已锁定!'
                    status = True
                    break

            if not status:
                return '用户不存在!'
        pwd = input('密码:')
        with open('4用户注册.txt', 'r', encoding='utf-8') as f:
            for line in f:
                if line.split('_')[0] == username and line.split('_')[1] == pwd:
                    USER[username] = 0
                    return '登陆成功'
        if USER.get(username) == None:
            USER[username] = 0
        if USER[username] < 3:
            USER[username] += 1
            if not USER[username] < 3:
                with open('4用户注册.txt', 'r', encoding='utf-8') as f1,open('4用户注册(改).txt', 'w', encoding='utf-8') as f2:
                    for line in f1:
                        if line.split('_')[0] == username:
                            new_line = line.replace('True', 'False')
                            f2.write(new_line)
                        else:
                            f2.write(line)
                os.remove('4用户注册.txt')
                os.rename('4用户注册(改).txt', '4用户注册.txt')
                return '输入错误超过3次,锁定账号!'
            print('登录失败请重试!')
print(user())
print(login())
"""

# 5.有如下文件，请通过分页的形式将数据展示出来。【文件非常小】
# 商品|价格
# 飞机|1000
# 大炮|2000
# 迫击炮|1000
# 手枪|123
# ...

'''
def func():
    f = open('5-6商品列表.txt', 'r', encoding='utf-8')
    a = f.read()                            # 全部读到内存
    lst = a.split('\n')
    max_page, mowei = divmod(len(lst), 3)     # 最大页,最后一页条数(总条数,每页条数)
    if mowei > 0:
        max_page += 1
    while 1:
        user = input('要查看第几页(N/n退出):')
        if user.upper() == 'N':
            return
        if not user.isnumeric() or int(user) not in range(1, max_page+1):
            print('输入有误!请重新输入!')
            continue
        start = (int(user)-1)*3
        end = (int(user))*3
        data = lst[start+1:end+1]
        print(lst[0])
        for i in data:
            print(i.strip())
        if not (int(user)>max_page or int(user)<1):
            print('当前第%s页,共%s页.' % (user,max_page))

func()
'''

# 6.有如下文件，请通过分页的形式将数据展示出来。【文件非常大】

# 商品|价格
# 飞机|1000
# 大炮|2000
# 迫击炮|1000
# 手枪|123

"""
def func():
    f = open('5-6商品列表.txt', 'r', encoding='utf-8')
    a = f.readlines()                       # 所有行组成一个列表
    print(a)
    max_page, mowei = divmod(len(a), 3)     # 最大页,最后一页条数(总条数,每页条数)
    if mowei > 0:
        max_page += 1
    while 1:
        user = input('要查看第几页(N/n退出):')
        if user.upper() == 'N':
            return
        if not user.isnumeric() or int(user) not in range(1, max_page+1):
            print('输入有误!请重新输入!')
            continue
        start = (int(user)-1)*3_
        end = (int(user))*3
        data = a[start+1:end+1]
        print(a[0].strip())
        for i in data:
            print(i.strip())
        if not (int(user)>max_page or int(user)<1):
            print('当前第%s页,共%s页.' % (user,max_page))

func()
"""

# 7.程序设计：购物车
# 有如下商品列表 GOODS_LIST，用户可以选择进行购买商品并加入到购物车 SHOPPING_CAR 中且可以选择要购买数量，购买完成之后将购买的所
# 有商品写入到文件中【文件格式为：年_月_日.txt】。
# 注意：重复购买同一件商品时，只更改购物车中的数量。
"""
SHOPPING_CAR = {}       # 购物车

GOODS_LIST = [
    {'id': 1, 'title': '飞机', 'price': 1000},
    {'id': 3, 'title': '大炮', 'price': 1000},
    {'id': 8, 'title': '迫击炮', 'price': 1000},
    {'id': 9, 'title': '手枪', 'price': 1000},
]       # 商品列表
from datetime import datetime


def shopping():
    while 1:
        for i in GOODS_LIST:
            print('序号:' + str(i['id']) + ' 商品:' + i['title'] + ' 价格:' + str(i['price']))
        user = input('请输入序号加入到购物车(N/n退出):')
        if user.upper() == 'N':
            return '退出'
        pand = False
        for j in GOODS_LIST:
            if int(user) == j['id']:
                pand = True
                if SHOPPING_CAR.get(j['title']) == None:
                    SHOPPING_CAR[j['title']] = 0
                break
        if not pand:
            print('输入错误!')
            continue
        while 1:
            count = input('请选择数量:')
            if not count.isdigit():
                print('请输入阿拉伯数字!')
                continue
            SHOPPING_CAR[j['title']] += int(count)
            date = datetime.now().strftime('%Y_%m_%d')
            with open("%s.txt" % date, 'w', encoding='utf-8') as f:
                f.write(str(SHOPPING_CAR))
                break

        print('已添加进购物车!')
        print(SHOPPING_CAR)


a = shopping()
print(a)
"""

# 8.程序设计：
"""
功能：
1.用户注册，提示用户输入用户名和密码，然后获取当前注册时间，最后将用户名、密码、注册时间写入到文件。
2.用户登录，只有三次错误机会，一旦错误则冻结账户（下次启动也无法登录，提示：用户已经冻结）。
3.商品浏览，分页显示商品（小文件）； 用户可以选择商品且可以选择数量然后加入购物车（在全局变量操作），
  不再购买之后，需要讲购物车信息写入到文件，文件要写入到指定目录：
    shopping_car(文件夹)
        - 用户名A(文件夹)
            2019-11-11-09-59.txt
            2019-11-12-11-56.txt
            2019-12-11-11-47.txt
        - 用户B(文件夹)
            2019-11-11-11-11.txt
            2019-11-12-11-15.txt
            2019-12-11-11-22.txt
  注意：重复购买同一件商品时，只更改购物车中的数量。
4.我的购物车，查看用户所有的购物车记录，即：找到shopping_car目录下当前用户所有的购买信息，并显示：
    2019-11-11-09-59
        飞机|1000|10个
        大炮|2000|3个
    2019-11-12-11-56.txt
        迫击炮|10000|10个
        手枪|123|3个

5.用户未登录的情况下，如果访问 商品流程 、我的购物车 时，提示登录之后才能访问，让用户先去选择登录（装饰器实现）。
"""

"""
import os
import json
from datetime import datetime

USER_STATUS = False
USER_COUNT = {}
SHOPPING_CAR = {}


def decorate(arg):

    def inner():
        if not USER_STATUS:
            print('请先登陆后再查看!')
            return
        arg()
        return

    return inner


def register():
    while 1:
        username = input('注册用户名:')
        with open('8login.txt', 'r', encoding='utf-8') as f:
            register1 = True
            for line in f:
                if line.split('----')[0].strip() == username:
                    register1 = False
                    print('帐户已存在!')
                    break
            if not register1:
                break
        pwd = input('密码:')
        sj = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        msg = '----'.join([username, pwd, sj, '0'])
        with open('8login.txt', 'a', encoding='utf-8') as f:
            f.write(msg + '\n')
        print('注册成功!')
        return


def login():
    while 1:
        username = input('用户名:')
        with open('8login.txt', 'r', encoding='utf-8') as f:
            status = False
            for line in f:
                if line.split('----')[0] == username:
                    if line.split('----')[-1].strip() == '3':
                        print('帐户已锁定!')
                        return
                    status = True
                    break

            if not status:
                print('用户不存在!')
                return
        pwd = input('密码:')
        with open('8login.txt', 'r', encoding='utf-8') as f:
            for line in f:
                if line.split('----')[0] == username and line.split('----')[1] == pwd:
                    USER_COUNT[username] = 0
                    print('登陆成功')
                    global USER_STATUS
                    USER_STATUS = username
                    return
        if USER_COUNT.get(username) == None:
            USER_COUNT[username] = 0
        if USER_COUNT[username] < 3:
            USER_COUNT[username] += 1
            if not USER_COUNT[username] < 3:
                with open('8login.txt', 'r', encoding='utf-8') as f1, \
                        open('8login(改).txt', 'w', encoding='utf-8') as f2:
                    for line in f1:
                        if line.split('----')[0] == username:
                            new_line = line.replace('0', '3')
                            f2.write(new_line)
                        else:
                            f2.write(line)
                os.remove('8login.txt')
                os.rename('8login(改).txt', '8login.txt')
                print('输入错误超过3次,锁定账号!')
                return
            print('登录失败请重试!')


@decorate
def goods():
    f = open('8商品列表', 'r', encoding='utf-8')
    a = f.read()
    lst = a.split('\n')
    max_page, end = divmod(len(lst), 3)  # 最大页,最后一页条数(总条数,每页条数)
    if end > 0:
        max_page += 1
    x, y, z = 0, 3, 1
    while 1:
        if y > len(lst):
            y = len(lst)
        for i in range(x, y):
            print(lst[i])
        print('当前第%s页,共%s页.' % (z, max_page))
        user = input('请输入序号加入到购物车,Y/y进入下一页,N/n退出:')
        if user.upper() == 'N':
            return '已退出'
        elif user.upper() == 'Y':
            x += 3
            y += 3
            z += 1
            if y == len(lst) + 3:
                x, y, z = 0, 3, 1
            continue
        elif user.isdigit():
            pand = False
            f = open('8商品列表', 'r', encoding='utf-8')
            for line in f:
                cut = line.split('|')
                if user == cut[0]:
                    pand = True
                    if SHOPPING_CAR.get(cut[1]) == None:
                        SHOPPING_CAR[cut[1]] = 0
                    break
            if not pand:
                print('输入错误!')
                continue
            while 1:
                count = input('请选择数量:')
                if not count.isdigit():
                    print('请输入阿拉伯数字!')
                    continue
                SHOPPING_CAR[cut[1]] += int(count)
                date = datetime.now().strftime('%Y-%m-%d-%H-%M')
                global USER_STATUS
                dir_path = os.path.exists('shopping_car\\%s' % USER_STATUS)
                if not dir_path:
                    os.mkdir('shopping_car\\%s' % USER_STATUS)
                with open("shopping_car\\%s\\%s.txt" % (USER_STATUS, date), 'w', encoding='utf-8') as f:
                    up = json.dumps(SHOPPING_CAR, ensure_ascii=False)
                    f.write(up)
                    break

            print('已添加进购物车!')
            print(SHOPPING_CAR)

        else:
            print('输入有误!')


@decorate
def shopping_car():
    if not os.path.exists('shopping_car\\%s' % USER_STATUS):
        print('您的购物车是空的,快去购买吧!')
        return
    content = os.walk('shopping_car\\%s' % USER_STATUS)
    for a, b, c in content:
        for x in c:
            print(x)
            f = open('shopping_car\\%s\\%s' % (USER_STATUS, x), 'r', encoding='utf-8')
            for line in f:
                a = json.loads(line)
                for item in a:
                    f = open('8商品列表', 'r', encoding='utf-8')
                    ls = f.read().split('\n')
                    for lm in ls:
                        if lm.split('|')[1] == item:
                            price = lm.split('|')[2]
                    print('    %s|%s|%s个' % (item,price , a[item]))

    return


def main():
    '''
    主页面
    :return:
    '''
    dic = {'1': register, '2': login, '3': goods, '4': shopping_car}
    while 1:
        print('''*****欢迎来到沙河商城!*****
1.注册帐户
2.登陆帐户
3.浏览商品
4.查看购物车''')
        a = input('请选择(N/n退出):')
        if a.upper() == 'N':
            return
        if dic.get(a) == None:
            print('输入有误!')
            continue
        dic.get(a)()


main()
"""

# 9.请使用第三方模块xlrd读取一个excel文件中的内容。
"""
import xlrd

# 打开Excel文件读取数据
workbook = xlrd.open_workbook('xlsx文件路径')

# 打印所有的sheet列出所有的sheet名字
print(workbook.sheet_names())

# 根据sheet索引或者名称获取sheet内容
Data_sheet = workbook.sheets()[0]
# Data_sheet = workbook.sheet_by_index(1)
# Data_sheet = workbook.sheet_by_name(u'Charts')

# 获取sheet名称、行数和列数
print(Data_sheet.name, Data_sheet.nrows, Data_sheet.ncols)

# 获取整行和整列的值（列表）
rows = Data_sheet.row_values(0)  # 获取第一行内容
cols = Data_sheet.col_values(1)  # 获取第二列内容
print(rows)
print(cols)

# 获取单元格内容的数据类型
# 相当于在一个二维矩阵中取值
# （row,col）-->(行,列)
cell_A1 = Data_sheet.cell(0, 0).value  # 第一行第一列坐标A1的单元格数据
# cell_C1 = Data_sheet.cell(0,2).value # 第一行第三列坐标C1的单元格数据

# cell_B1 = Data_sheet.row(0)[1].value # 第1行第2列
# cell_D2 = Data_sheet.col(3)[1].value # 第4列第2行

# 检查单元格的数据类型
# ctype的取值含义
# ctype : 0 empty,1 string, 2 number, 3 date, 4 boolean, 5 error
print(Data_sheet.cell(4, 0).ctype)

# 读取excel中单元格内容为日期的方式
date_value = xlrd.xldate_as_tuple(Data_sheet.cell_value(4, 0), workbook.datemode)

print(date_value)  # -->(2017, 9, 6, 0, 0, 0)

print('%d:%d:%d' % (date_value[3:]))  # 打印时间
print('%d/%02d/%02d' % (date_value[0:3])) 
"""


