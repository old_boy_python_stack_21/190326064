# day16笔记

## 一.补充

- 模块(类库)
- 可以是py文件,也可以是文件夹

### 1.常用内置模块

json / time / os / sys

### 2.自定义模块

可以把一个py文件或一个文件夹(包)当做一个模块,以方便于以后其他py文件的调用.

对于包的定义:

py2:文件夹中必须有__ init __.py文件

py3:不需要__ init __.py

推荐加上该文件.

### 3.模块的调用

- import 模块 模块.函数()		# 导入模块，加载此模块中所有的值到内存
- from 模块 import 函数 函数() 【as起别名 / *】
- from 模块 import 函数 as 别名 别名()

```python
# 调用模块文件夹下的jd下的f1函数.
import 模块.jd
lizhong.jd.f1()

from 模块 import jd
jd.f1()

from 模块.jd import f1
f1()
```

模块和要执行的py文件在同一目录且需要模块中的很多功能时,推荐用import 模块.

其他情况推荐:

1.from 模块 import 模块 模块.函数()

2.from 模块.模块 import 函数 函数()

### 4.内置模块

os / sys / time / hashlib / random / getpass / shutil / copy

json:

- json字典或列表中如有中文,序列化时想保留中文显示:

  ```python
  v = {'k1':'alex','k2':'李杰'}
  
  import json
  val = json.dumps(v,ensure_ascii=False)
  print(val)
  ```

- dumps

  ```python
  import json
  
  v = {'k1':'alex','k2':'李杰'}
  f = open('x.txt',mode='w',encoding='utf-8')
  val = json.dump(v,f,ensure_ascii=False)
  print(val)
  f.close()
  ```

- loads

  ```python
  import json
  
  f = open('x.txt',mode='r',encoding='utf-8')
  data = json.load(f)
  f.close()
  print(data,type(data))
  ```

## 二.json和pickle

### 1.json

优点:所有语言通用

缺点:只能序列化基本的数据类型 int / str /list / dict

### 2.pickle

优点:Python中所有的东西都能被序列化(除socket对象)

缺点:序列化的内容只有Python认识.

```python
import pickle

# #################### dumps/loads ######################
"""
v = {1,2,3,4}
val = pickle.dumps(v)
print(val)
data = pickle.loads(val)
print(data,type(data))
"""

"""
def f1():
    print('f1')

v1 = pickle.dumps(f1)
print(v1)
v2 = pickle.loads(v1)
v2()
"""

# #################### dump/load ######################
# v = {1,2,3,4}
# f = open('x.txt',mode='wb')
# val = pickle.dump(v,f)
# f.close()

# f = open('x.txt',mode='rb')
# data = pickle.load(f)
# f.close()
# print(data)
```

## 三.shutil模块

```python
import shutil

# 删除目录
# shutil.rmtree('test')

# 目录重命名
# shutil.move('test','ttt')

# 压缩文件
# shutil.make_archive('zzh','zip','D:\code\s21day16\lizhong')
#   			压缩成的文件名		格式 		把该目录压缩

# 解压文件
# shutil.unpack_archive('zzh.zip',extract_dir=r'D:\code\xxxxxx\xxxx',format='zip')
# 					要解压的文件		解压到的路径				压缩类型
```

## 四.time&datetime

UTC / GMT : 世界时间

本地时间 : 本地时区的时间.(北京时间东八区)

### 1.time模块

time.time()	时间戳,1970-1-1 00:00

time.sleep(10)	运行等待秒数

time.timezone	距0经度时区时间差,8hours

### 2.datetime模块

```python
import time
from datetime import datetime,timezone,timedelta

# ######################## 获取datetime格式时间 ##############################
"""
v1 = datetime.now() # 当前本地时间
print(v1)
tz = timezone(timedelta(hours=7)) # 当前东7区时间
v2 = datetime.now(tz)
print(v2)
v3 = datetime.utcnow() # 当前UTC时间
print(v3)
"""

# ######################## 把datetime格式转换成字符串 ##############################
# v1 = datetime.now()
# print(v1,type(v1))
# val = v1.strftime("%Y-%m-%d %H:%M:%S")
# print(val)

# ######################## 字符串转成datetime ##############################
# v1 = datetime.strptime('2011-11-11','%Y-%m-%d')
# print(v1,type(v1))

# ######################## datetime时间的加减 ##############################
# v1 = datetime.strptime('2011-11-11','%Y-%m-%d')
# v2 = v1 - timedelta(days=140)
# date = v2.strftime('%Y-%m-%d')
# print(date)

# ######################## 时间戳和datetime关系 ##############################
# ctime = time.time()
# print(ctime)
# v1 = datetime.fromtimestamp(ctime)
# print(v1)

# v1 = datetime.now()
# val = v1.timestamp()
# print(val)
```

### 3.异常处理

报错使用

```python
try:
    内容
except Exception as e:
    print('操作异常')
```







