#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.简述解释型语言和编译型语言的区别？
"""
1.解释型语言:Python,PHP,Ruby.
特点是一行一行的解释,一行一行的传输给计算机,报错行前面可以执行.
2.编译型语言:C,C++,Java,C#,Go.
特点是一次性把语言编译成文件,报错则不能编译,编译好的文件一定可以运行.
"""

# 2.列举你了解的Python的数据类型？
"""
1.int:整型,数字
2.str:字符串
3.bool:布尔值
4.list:列表
5.tuple:元组
"""

# 3.写代码，有如下列表，按照要求实现每一个功能。
# li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
# 3.1.计算列表的长度并输出
"""
print(len(li))
"""
# 3.2.请通过步长获取索引为偶数的所有值，并打印出获取后的列表
"""
li = li[::2]
print(li)
"""
# 3.3.列表中追加元素"seven",并输出添加后的列表
"""
li.append('seven')
print(li)
"""
# 3.4.请在列表的第1个位置插入元素"Tony",并输出添加后的列表
"""
li.insert(0,'Tony')
print(li)
"""
# 3.5.请修改列表第2个位置的元素为"Kelly",并输出修改后的列表
"""
li[1] = 'Kelly'
print(li)
"""
# 3.6.请将列表的第3个位置的值改成 "太白"，并输出修改后的列表
"""
li[2] = "太白"
print(li)
"""
# 3.7.请将列表 l2=[1,"a",3,4,"heart"] 的每一个元素追加到列表li中，并输出添加后的列表
"""
l2=[1,"a",3,4,"heart"]
for i in l2:
    li.append(i)
print(li)
"""
# 3.8.请将字符串 s = "qwert"的每一个元素添加到列表li中，一行代码实现，不允许循环添加。
"""
s = "qwert"
li2 = li + [s[0],s[1],s[2],s[3],s[4]]
print(li2)
"""
# 3.9.请删除列表中的元素"ritian",并输出添加后的列表
"""
li.remove('ritian')
print(li)
"""
# 3.10.请删除列表中的第2个元素，并输出删除元素后的列表
"""
li.pop(1)
print(li)
"""
# 3.11.请删除列表中的第2至第4个元素，并输出删除元素后的列表
"""
del li[1:4]
print(li)
"""

# 4.请用三种方法实现字符串反转 name = "小黑半夜三点在被窝玩愤怒的小鸟"（步长、while、for）
# name = "小黑半夜三点在被窝玩愤怒的小鸟"
# 方法一
"""
name1 = name[::-1]
print(name1)
"""
# 方法二
"""
count = 0
s = ''
while count < len(name):
    s += name[len(name)-count-1]
    count += 1
print(s)
"""
# 方法三
"""
s = ''
for i in range(len(name)):
    s += name[len(name)-i-1]
print(s)
"""

# 5.写代码，有如下列表，利用切片实现每一个功能
# li = [1, 3, 2, "a", 4, "b", 5,"c"]
# 5.1通过对li列表的切片形成新的列表 [1,3,2]
"""
li2 = li[:3]
print(li2)
"""
# 5.2通过对li列表的切片形成新的列表 ["a",4,"b"]
"""
li2 = li[3:6]
print(li2)
"""
# 5.3通过对li列表的切片形成新的列表 [1,2,4,5]
"""
li2 = li[::2]
print(li2)
"""
# 5.4通过对li列表的切片形成新的列表 [3,"a","b"]
"""
li2 = li[1:-2:2]
print(li2)
"""
# 5.5通过对li列表的切片形成新的列表 [3,"a","b","c"]
"""
li2 = li[1::2]
print(li2)
"""
# 5.6通过对li列表的切片形成新的列表 ["c"]
"""
li2 = []
li2.append(li[-1])
print(li2)
"""
# 5.7通过对li列表的切片形成新的列表 ["b","a",3]
"""
li2 = li[-3::-2]
print(li2)
"""

# 6.请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
"""
0 武沛齐
1 景女神
2 肖大侠
"""
# users = ["武沛齐","景女神","肖大侠"]
# 方法一
"""
count = 0
for i in users:
    print(count,i)
    count += 1
"""
# 方法二
"""
for i in range(len(users)):
    print(i,users[i])
"""

# 7.请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
"""
1 武沛齐
2 景女神
3 肖大侠
"""
# users = ["武沛齐","景女神","肖大侠"]
# 方法一
"""
count = 0
for i in users:
    count += 1
    print(count,i)
"""
# 方法二
"""
for i in range(len(users)):
    print(i+1,users[i])
"""

# 8.写代码，有如下列表，按照要求实现每一个功能。
# lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
# 8.1将列表lis中的"k"变成大写，并打印列表。
"""
lis[2] = 'K'
lis[3][2][0] = 'K1'
print(lis)
"""
# 8.2将列表中的数字3变成字符串"100"
"""
lis[1] = '100'
lis[3][2][1][1] = '100'
print(lis)
"""
# 8.3将列表中的字符串"tt"变成数字 101
"""
lis[3][2][1][0] = 101
print(lis)
"""
# 8.4在 "qwe"前面插入字符串："火车头"
"""
lis[3].insert(0,"火车头")
print(lis)
"""

# 9.写代码实现以下功能,如有变量 googs = ['汽车','飞机','火箭'] 提示用户可供选择的商品,用户输入索引后，将指定商品的内容
# 拼接打印，如：用户输入0，则打印 您选择的商品是汽车。
"""
googs = ['汽车','飞机','火箭']
for i in range(len(googs)):
    print(i,',',googs[i])
a = int(input('请输入序号:'))
print('您选择的商品是:',googs[a])
"""

# 10.请用代码实现:li = "alex",利用下划线将列表的每一个元素拼接成字符串"a_l_e_x"
"""
li = "alex"
a = '_'.join(li)
print(a)
"""

# 11.利用for循环和range找出 0 ~ 100 以内所有的偶数，并追加到一个列表。
"""
li = []
for i in range(101):
    if i % 2 == 0:
        li.append(i)
print(li)
"""

# 12.利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并追加到一个列表。
"""
li = []
for i in range(51):
    if i % 3 == 0:
        li.append(i)
print(li)
"""

# 13.利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并插入到列表的第0个索引位置，最终结果如下：[48,45,42...]
"""
li = []
for i in range(51):
    if i % 3 == 0:
        li.insert(0,i)
print(li)
"""

# 14.查找列表li中的元素，移除每个元素的空格，并找出以"a"开头，并添加到一个新列表中,最后循环打印这个新列表。
"""
li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", "  aqc"]
li2 = []
for i in li:
    s = i.strip()
    if s.startswith('a'):
        li2.append(s)
    print(li2)
"""

# 15.判断是否可以实现，如果可以请写代码实现。
li = ["alex",[11,22,(88,99,100,),33],"WuSir",("ritian", "barry",), "wenzhou"]
# 15.1请将 "WuSir" 修改成 "武沛齐"
"""
li[2] = '武沛齐'
print(li)
"""
# 15.2请将 ("ritian", "barry",) 修改为 ['日天','日地']
"""
li[3] = ['日天','日地']
print(li)
"""
# 15.3请将 88 修改为 87
"""
li[1][2] = (87,99,100)
print(li)
"""
# 15.4请将 "wenzhou" 删除，然后再在列表第0个索引位置插入 "文周"
"""
li.pop()
li.insert(0,"文周")
print(li)
"""
