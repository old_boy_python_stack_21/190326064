# day34笔记

## 1.回顾

- 进程是计算机中最小的资源分配单位
- 线程是计算机中最小的被CPU调度的单位

io操作:

- i input  向内存输入  input read recv recvftom accept connect close
- o output 从内存输出  print write send sendto accept connect close

start terminate join

- start terminate   异步 非阻塞
- join   同步阻塞

常见的ipc机制

- ipc机制 ：队列 管道
- 第三方工具（软件）提供给我们的IPC机制
  - redis
  - memcache
  - kafka
  - rabbitmq
  - 特点:并发,高可用,断电保存数据,解耦

## 2.生产者消费者模型

### 2.1什么是生产者消费者模式

生产者消费者模式是通过一个容器来解决生产者和消费者的强耦合问题。生产者和消费者彼此之间不直接通讯，而通过阻塞队列来进行通讯，所以生产者生产完数据之后不用等待消费者处理，直接扔给阻塞队列，消费者不找生产者要数据，而是直接从阻塞队列里取，阻塞队列就相当于一个缓冲区，平衡了生产者和消费者的处理能力。

### 2.2为什么要使用生产者和消费者模式

在线程世界里，生产者就是生产数据的线程，消费者就是消费数据的线程。在多线程开发当中，如果生产者处理速度很快，而消费者处理速度很慢，那么生产者就必须等待消费者处理完，才能继续生产数据。同样的道理，如果消费者的处理能力大于生产者，那么消费者就必须等待生产者。为了解决这个问题于是引入了生产者和消费者模式。

### 2.3解耦 (修改  复用  可读性)

- 把写在一起的大的功能分开成多个小的功能处理
- 登陆  注册

### 2.4队列

生产者和消费者之间的容器就是队列

```python
import time
import random
from multiprocessing import Process,Queue

def producer(q,name,food):	# 生产者
    for i in range(10):
        time.sleep(random.random())
        fd = '%s%s'%(food,i)
        q.put(fd)
        print('%s生产了一个%s'%(name,food))

def consumer(q,name):	# 消费者
    while True:
        food = q.get()
        if not food:break
        time.sleep(random.randint(1,3))
        print('%s吃了%s'%(name,food))


def cp(c_count,p_count):	# 设置消费者和生产者个数
    q = Queue(10)	# 队列(容器)长度
    for i in range(c_count):
        Process(target=consumer, args=(q, 'alex')).start()
    p_l = []
    for i in range(p_count):
        p1 = Process(target=producer, args=(q, 'wusir', '泔水'))
        p1.start()
        p_l.append(p1)
    for p in p_l:p.join()	# 等待所有子进程结束
    for i in range(c_count):	# 给每个消费者发送None,消费者接受到None时停止接受并退出.
        q.put(None)
if __name__ == '__main__':
    cp(2,3)
```

- 注意：结束信号None，不一定要由生产者发，主进程里同样可以发，但主进程需要等生产者结束后才应该发送该信号 , 但上述解决方式，在有多个生产者和多个消费者时，我们则需要用一个很low的方式去解决



## 3.生产者消费者实现爬虫

```python
from multiprocessing import Process, Queue
import requests

import re
import json

def producer(q, url):   # 设置生产者
    response = requests.get(url)
    q.put(response.text)

def consumer(q):    # 设置消费者
    while True:
        s = q.get()
        if not s: break
        com = re.compile(
            '<div class="item">.*?<div class="pic">.*?<em .*?>(?P<id>\d+).*?<span class="title">(?P<title>.*?)</span>'
            '.*?<span class="rating_num" .*?>(?P<rating_num>.*?)</span>.*?<span>(?P<comment_num>.*?)评价</span>', re.S)
        ret = com.finditer(s)
        for i in ret:
            print({
                "id": i.group("id"),
                "title": i.group("title"),
                "rating_num": i.group("rating_num"),
                "comment_num": i.group("comment_num")}
            )


if __name__ == '__main__':
    count = 0
    q = Queue(3)
    p_l = []
    for i in range(10):
        url = 'https://movie.douban.com/top250?start=%s&filter=' % count
        count += 25
        p = Process(target=producer, args=(q, url,)).start()
        p_l.append(p)
    p = Process(target=consumer, args=(q,)).start()
    for p in p_l: p.join()
    q.put(None)
```



## 4.joinablequeue

- 创建可连接的共享进程队列。这就像是一个Queue对象，但队列允许项目的使用者通知生产者项目已经被成功处理。通知进程是使用共享的信号和条件变量来实现的。 

- 除了与Queue对象相同的方法之外，还具有以下方法：

  - q.task_done() 
    使用者使用此方法发出信号，表示q.get()返回的项目已经被处理。如果调用此方法的次数大于从队列中删除的项目数量，将引发ValueError异常。

  - q.join() 
    生产者将使用此方法进行阻塞，直到队列中所有项目均被处理。阻塞将持续到为队列中的每个项目均调用q.task_done()方法为止。 
    下面的例子说明如何建立永远运行的进程，使用和处理队列上的项目。生产者将项目放入队列，并等待它们被处理。

```python
import time
import random
from multiprocessing import JoinableQueue,Process

def producer(q,name,food):  # 设置生产者
    for i in range(10):
        time.sleep(random.random())
        fd = '%s%s'%(food,i)
        q.put(fd)
        print('%s生产了一个%s'%(name,food))
    q.join()

def consumer(q,name):   # 设置消费者
    while True:
        food = q.get()
        time.sleep(random.random())
        print('%s吃了%s'%(name,food))
        q.task_done()

if __name__ == '__main__':
    jq = JoinableQueue()
    p = Process(target=producer, args=(jq, 'wusir', '泔水'))
    p.start()
    c = Process(target=consumer, args=(jq, 'alex'))
    c.daemon = True
    c.start()
    p.join()
```



## 5.进程之间的数据共享

- mulprocessing中有一个manager类, 封装了所有和进程相关的 数据共享 数据传递相关的数据类型 , 但是对于 字典 列表这一类的数据操作的时候会产生数据不安全 , 需要加锁解决问题，并且需要尽量少的使用这种方式.

```python
from multiprocessing import Manager,Process,Lock

def func(dic,lock):
    with lock:
        dic['count'] -= 1

if __name__ == '__main__':
    # m = Manager()
    with Manager() as m:
        l = Lock()
        dic = m.dict({'count':100})
        p_l = []
        for i in range(100):
            p = Process(target=func,args=(dic,l))
            p.start()
            p_l.append(p)
        for p in p_l:p.join()
        print(dic)
```

## 6.线程的理论

### 1.理论

- 线程  开销小  数据共享  是进程的一部分
- 进程  开销大  数据隔离  是一个资源分配单位

- cpython解释器   不能实现多线程利用多核

### 2.锁:GIL  全局解释器锁

- 保证了整个python程序中,只能有一个线程被CPU执行
- 原因 : cpython解释器中特殊的垃圾回收机制
- GIL锁导致了线程不能并行,可以并发.
- 所以使用锁线程并不影响高io型的操作,只会对高计算型的程序由效率上的影响,遇到高计算 : 多进程 + 多线程 ---分布式

- cpython  pypy有
- jpython  iron  python无

## 7.thread类

### 7.1threading模块

- multiprocessing 是完全仿照threading的类写的 , 二者在使用层面，有很大的相似性，因而不再详细介绍.

```python
import os
import time
from threading import Thread
def func():
	print('start son thread')
	time.sleep(1)
	print('end son thread',os.getpid())
# 启动线程 start
Thread(target=func).start()
print('start',os.getpid())
time.sleep(0.5)
print('end',os.getpid())
```

### 7.2开启多个子线程

```python
def func(i):
    print('start son thread',i)
    time.sleep(1)
    print('end son thread',i,os.getpid())
for i in range(10):
    Thread(target=func,args=(i,)).start()
print('main')
```

- 主线程什么时候结束？等待所有子线程结束之后才结束
- 主线程如果结束了，主进程也就结束了

### 7.3join方法

- 阻塞 直到子线程执行结束

```python
def func(i):
    print('start son thread',i)
    time.sleep(1)
    print('end son thread',i,os.getpid())
t_l = []
for i in range(10):
    t = Thread(target=func,args=(i,))
    t.start()
    t_l.append(t)
for t in t_l:t.join()
print('子线程执行完毕')
```

### 7.4使用面向对象的方式启动线程

```python
class MyThread(Thread):
    def __init__(self,i):
        self.i = i
        super().__init__()
    def run(self):
        print('start',self.i,self.ident)
        time.sleep(1)
        print('end',self.i)

for i in range(10):
    t = MyThread(i)
    t.start()
    print(t.ident)
```

### 7.5线程里的一些其他方法

- current_thread()  在哪个线程中被调用,就返回当前线程的对象
- enumerate()  返回当前活着的线程的对象列表(包括主线程)
- active_count()  返回当前活着的线程的个数(包括主线程)

```python
from threading import current_thread,enumerate,active_count
def func(i):
    t = current_thread()
    print('start son thread',i,t.ident)
    time.sleep(1)
    print('end son thread',i,os.getpid())

t = Thread(target=func,args=(1,))
t.start()
print(t.ident)
print(current_thread().ident)   # 水性杨花 在哪一个线程里，current_thread()得到的就是这个当前线程的信息
print(enumerate())
print(active_count())   # =====len(enumerate())
```

### 7.6terminate 结束进程

- 在线程中不能从主线程结束一个子线程

### 7.7测试进程和线程的效率差

```python
def func(a,b):
    c = a+b
import time
from multiprocessing import Process
from threading import Thread
if __name__ == '__main__':
    start = time.time()
    p_l = []
    for  i in range(500):
        p = Process(target=func,args=(i,i*2))
        p.start()
        p_l.append(p)
    for p in p_l:p.join()
    print('process :',time.time() - start)

    start = time.time()
    p_l = []
    for i in range(500):
        p = Thread(target=func, args=(i, i * 2))
        p.start()
        p_l.append(p)
    for p in p_l: p.join()
    print('thread :',time.time() - start)
```

### 7.8数据隔离还是共享？

```python
from threading import Thread
n = 100
def func():
    global n    # 不要在子线程里随便修改全局变量
    n-=1
t_l = []
for i in range(100):
    t = Thread(target=func)
    t_l.append(t)
    t.start()
for t in t_l:t.join()
print(n)
```

### 7.9守护线程daemon

```python
import time
from threading import Thread
def son1():
    while True:
        time.sleep(0.5)
        print('in son1')
def son2():
    for i in range(5):
        time.sleep(1)
        print('in son2')
t =Thread(target=son1)
t.daemon = True
t.start()
Thread(target=son2).start()
time.sleep(3)
```

- 守护线程一直等到所有的非守护线程都结束之后才结束 , 除了守护了主线程的代码之外也会守护子线程
- 结束顺序 ：非守护线程结束 -->主线程结束-->主进程结束-->守护线程结束
- 小绿本 ：p38 ：34题







