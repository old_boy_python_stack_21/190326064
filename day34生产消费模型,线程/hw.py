# 1.你了解生产者模型消费者模型么？如何实现？
"""
通过一个容器来解决生产者和消费者的强耦合问题.通过Queue队列实现.
"""

# 2.GIL锁是怎么回事?
'''
全局解释器锁,保证了整个python程序中,只能有一个线程被CPU执行
'''

# 3.请简述进程和线程的区别？
'''
进程:开销大 数据隔离 是一个资源分配单位
线程:开销小 数据共享 是进程的一部分
'''

# 4.多进程之间是否能实现数据共享？用哪个模块实现？
'''
能
multiprocessing模块中的manager
'''

# 5.请继续完成ftp作业：需求5.不同用户家目录不同，且只能访问自己的家目录

# 生成器练习题
# 1.读代码猜答案
# g1 = filter(lambda n: n % 2 == 0, range(10))  # [0,2,4,6,8]
# g2 = map(lambda n: n * 2, range(3))   # [0,2,4]
# for i in g1:
#     for j in g2:
#         print(i * j)      # 0,0,0,0,4,8,0,8,16,0,12,24,0,16,32
'''
0,0,0
'''

# 2.以下代码的输出是什么？请给出答案并解释。
# def multipliers():
#     return [lambda x: i * x for i in range(4)]
#
#
# print([m(2) for m in multipliers()])  # 0,2,4,6
'''
6,6,6,6
'''

# 请修改multipliers的定义来产生期望的结果。
'''
def multipliers():
    ret = (lambda x: i * x for i in range(4))
    return ret

print([m(2) for m in multipliers()])  # 0,2,4,6
'''
