Python学习笔记

## 第一章 计算机基础

### 1.1硬件

处理器,内存条,硬盘,显卡,网卡,主板...

### 1.2操作系统

Windows: xp/7/8/10

Linux: Ubuntu/CentOS/RedHat

Unix: MacOS

### 1.3解释器或编译器

解释器:Python/PHP/Ruby.按行处理,报错行之前都会运行,与计算机交流频繁.

编译器:Java/C/C++.把代码编译成文件,报错则不能编译,与计算机交流少.

### 1.4软件

人操作软件,软件是人与操作系统之间的桥梁,通过操作系统指挥硬件完成功能.

### 1.5进制

二进制:逢2进1.0,1,10,11...

八进制:逢8进1.0,1...6,7,10,...

十进制:逢10进1.0,1...,9,10,11...

十六进制:逢16进1.0,1,2...9,a,b,c,d,e,f,10...

## 第二章 Python入门

### 2.1环境安装

python官网下载,安装之后添加环境变量.

### 2.2编码

ASCII:英文数字和符号,8bit,1byte.

Unicode:万国码,可容纳所有语言,32bit,4byte,浪费资源.

UTF-8:简化万国码,英文8bit,欧洲16bit,中文24bit.

GBK:国标码,16bit,2byte.

GB2312,UTF-16.

### 2.3变量

变量命名规范:1.由数字,字幕,下划线组成.

2.不能由数字开头.

3.不能是关键字.

### 2.4python2和3的区别

- 字符串类型不同
  - 2:str型转换为unicode型
    3:byte型转换为str型
- 默认解释器编码
  - 2:ASCII
    3:UTF-8
- 输入
  - 2:raw_input()
    3:input()
- 输出:
  - 2:print ""
  - 3:print("")
- int
  - 2:int,有范围,超出则为long长整型
  - 3:int,无范围
- 除法
  - 2:没有小数,需要加一行代码
  - 3:有小数
- range和xrang
  - 2:range为立即创建列表,占内存.xrange为边循环边创建,省内存.
  - 3:range即为2中的xrange.
- 模块和包
  - 2:导入文件夹需要__ init__文件
  - 3:不需要
- 字典返回值
  - 2:列表
  - 3:迭代器
- map/filter返回值
  - 2:列表
  - 3:迭代器

### 2.5

### 2.6注释

单行注释:

``` python
# 注释内容
```

多行注释:

``` python
'''
注释内容
'''
```

### 2.7if语句

``` python
if 条件1:
    代码块1
elif 条件2:
    代码块2
...
else:
    代码块n
```

### 2.8while语句

``` python
while 条件:
    代码块1
else:
    代码块2
```

当条件不符合时执行else,符合条件时有break则退出while循环,continue则结束本次循环执行下次循环.

### 2.9运算符

```
算数运算:
+ / - / * / /
+= / -= / *= / /=

逻辑运算:
not > and > or
in / not in
```



## 第三章 数据类型

### 3.1整型int

无独有方法

py2中有int有范围,超出自动转为long长整型,py3中只有int

整除py2中无小数,可加一行代码实现

### 3.2布尔值bool

只有True和False.无独有方法.

0,'',[],{},(,),set(),None都可转化为False,其他为True.

### 3.3字符串str ''

| 方法          | 含义           | 备注                                       |
| ------------- | -------------- | ------------------------------------------ |
| .lower()      | 小写           |                                            |
| .upper()      | 大写           |                                            |
| .strip()      | 去空格         | lstrip()去左侧空格,rstrip()去右侧空格      |
| .split()      | 分割           | split('x'),用切割成一个列表,rsplit从右切割 |
| .isdigit()    | 判断是否是数字 |                                            |
| .capitalize() | 首字母大写     | 其余小写                                   |
| .count()      | 计数           | 计算()的内容出现次数                       |
| .encode()     | 编码           | (encoding='')''里为编码类型                |
| .endswith()   | 以...结尾      | ()里为内容                                 |
| .startswith() | 以...开头      | ()里为内容                                 |
| .find()       | 查找           | ()里为内容,找不到返回-1                    |
| .format()     | 格式化输出     | '{0}',fomat()                              |
| .join()       | 加入           | '_'.join('abc') a_b_c                      |
| .replace()    | 替换           | replace(a,b)把a换成b                       |

### 3.4列表list []

| 方法      | 含义 | 备注                        |
| --------- | ---- | --------------------------- |
| append()  | 追加 |                             |
| insert()  | 插入 | (0,a)第0个索引位置插入a     |
| clear()   | 清空 |                             |
| extend()  | 延长 | 把()的内容挨个追加到列表    |
| pop()     | 删除 | 删除索引项,返回删除的值     |
| remove()  | 删除 | 删除值,无值报错             |
| reverse() | 反转 |                             |
| sort()    | 排序 | 默认升序,(reverse=True)降序 |

### 3.5字典dict {}

{key:value,}

| 方法     | 含义         | 备注                        |
| -------- | ------------ | --------------------------- |
| clear()  | 清空         |                             |
| get()    | 取对应键的值 | get(key)=value              |
| items()  | 取所有键值对 | 伪列表                      |
| keys()   | 取所有键     | 伪列表                      |
| values() | 取所有值     | 伪列表                      |
| pop()    | 删除         |                             |
| update() | 更新         | a.update(b)把b的内容更新到a |

### 3.6元组tuple (,)

无独有方法,不可改变

### 3.7集合set() {,}

无重复内容

| 方法                   | 含义     | 备注                |
| ---------------------- | -------- | ------------------- |
| .add()                 | 添加     |                     |
| clear()                | 清空     |                     |
| difference()           | 差集     | a.difference(b)=a-b |
| discard()              | 删除     |                     |
| union()                | 合集     | a.union(b)=a+b      |
| intersection()         | 交集     |                     |
| symmetric_difference() | 对称差集 |                     |
| update()               | 更新     |                     |

### 3.8公共功能

| 方法    | 含义                  | 备注           |
| ------- | --------------------- | -------------- |
| len()   | 长度                  |                |
| 索引    | s[0] s的第0项         | 范围0-len(s)-1 |
| 切片    | s[0:2] s的前两个字符  | 顾头不顾尾     |
| 步长    | s[::2] 每两个取一个   | -1倒着取       |
| for循环 | for i in range(len()) | 都可以使用     |

### 3.9嵌套

小九九乘法表:

``` python
for a in range(1, 10):
    for b in range(1, a + 1):
        print('%s*%s=%s' % (b, a, a * b), end=' ')
        if a == b:
            print('')
```

## 第四章 文件操作

### 4.1文件基本操作

``` python
f = open('filename',mode='模式',encoding='编码类型')
f.write()	#写
f.read()	#读
f.flush()	#刷新
f.close()	#关闭文件
```

### 4.2打开模式

r 只读	只能读取(常用)

w 只写	写入前清空文件内容(常用)

a 追加	追加内容到文件末尾(常用)

r+ 读写 / w+ 写读 / a+ 追加读写

rb 读取二进制 / wb 写入二进制 / ab 追加二进制

r+b 读写二进制 / w+b 写读二进制 / a+b 追加读写二进制

### 4.3操作

read()	将文件全部读取到内存

read(1)	mode=r读取一个字符,mode=rb读取一个字节

write()	写入

seek()	调整光标位置

tell()	获取当前光标位置

flush()	强制将内存中的内容写入到文件

close()	关闭文件,with打开不用关闭

### 4.4文件的修改

``` python
with open('file1',mode='r',encoding='utf-8') as f1,
	open('file2',mode='w',encoding='utf-8') as f2:
#小文件
	data = f1.read()
    new_data = data.replace('内容1','内容2')
    f2.write(new_data)
#大文件
	for line in f1:
        new_line = line.replace('内容1','内容2')
        f2.write(new_line)
```

## 第五章 函数

### 5.1三元运算(三目运算)

v = 内容1 if 条件 else 内容2

符合条件输出内容1,否则输出内容2

### 5.2函数式编程

把N行代码定义成一个变量,后面直接调用变量即可.

可读性强,可重用性强.

### 5.3基本结构

三种方法:

``` python
def func1(a1,a2):
    pass 

def func2(a1,a2=None):
    pass 

def func3(*args,**kwargs):
    pass 
```

调用时位置参数在前 , 关键字参数在后.

### 5.4参数

- 不限数量,不限数据类型

- 位置传参,按位置挨个代入

  ```python
  def func(a1,a2):
      代码块
  func(1,2)	#a1=1,a2=2
  ```

- 关键字传参

  - 位置传参在前,关键字在后

  ```python
  def func(a,b,c):
      代码块
  func(1,2,c=3)	#c=3是关键字参数
  ```

- 默认参数

  ```python
  def func(a,b=2,c=3):
      代码块
  func(1)	#b默认等于2,c=3,传入其他值则修改.
  ```

- 万能参数

  - *args接收多个位置参数
  - **kwargs接收多个关键字参数

  ```python
  def func(*args,**kwargs):
      print(args,kwargs)
  ```

``` python
def 函数名(形式参数):
	代码块
函数名(实际参数)
```

### 5.5返回值

``` python
def 函数名():	#定义
	代码块
   	return x	#返回值x,不填默认None
print(函数名())	#调用函数,输出x/None
```

### 5.6作用域

- 在python中,整个py文件是全局作用域,函数内部是局部作用域.

  ```python
  a = 1
  def s1():
      x1 = 666
      print(x1)	#3.输出666
      print(a)	#4.局部没有a则去父级找,输出1
      print(b)	#5.局部没有a则去父级找,输出2
  
  b = 2
  print(a)	#1.输出1
  s1()		#2.调用s1()
  a = 88888	#6.a被重新赋值
  def s2():
      print(a,b)	#8.输出88888,2
      s1()	#9.调用s1,输出666,88888,2
  
  s2()		#7.调用s2()
  ```

- 一个函数有一个作用域.

  ```python
  def func():
      x = 9
      print(x)	#2.输出9
  func()		#1.调用func()
  print(x)	#3.全局没有x报错
  ```

- 优先在局部查找,找不到去父级找, 找不到再去父级找,直到全局,全局没有则报错.

  ```python
  x = 10			#1.全局变量x=10
  def func():
      x = 8		#3.局部变量x=8
      print(x)	#4.输出8
      def x1():	
          print(x)	#6.x1()局部没有x去父级找,x=8输出8
      x1()		#5.调用x1()
      x = 9		#7.x重新赋值为9
      x1()		#8.调用x1()	9.输出9
      x = 10		#10.x重新赋值为10
      print(x)	#11.输出10
  func()			#2.调用func()
  ```

- 只能子级去父级找,不能为父级的变量赋值.能修改可变数据类型.

  ```python
  name = 'oldboy'
  def func():
      name = 'alex'	#2.在自己作用域再创建一个这样的值。
      print(name)		#3.输出alex
  func()			#1.调用func()
  print(name)		#4.输出oldboy
  ```

  ```python
  name = [1,2,43]
  def func():
      name.append(999)	#2.name是列表,可变
      print(name)		#3.输出[1,2,43,999]
  func()				#1.调用func()
  print(name)			#4.输出[1,2,43,999]
  ```

- global使用全局变量.

  ```python
  name = ["老男孩",'alex']
  def func():
      global name		#2.调用全局变量name
      name = '我'		#3.name被重新赋值
  func()				#1.调用func()
  print(name)			#4.输出我
  ```

  ```python
  name = "老男孩"
  def func():
      name = 'alex'		#2.局部name=alex
      def inner():
          global name		#4.调用全局变量name
          name = 999		#5.name被重新赋值
      inner()				#3.调用inner()
      print(name)			#6.输出局部变量name:alex
  func()					#1.调用func()
  print(name)				#7.输出999
  ```

- nonlocal使用父级变量.

  ```python
  name = "老男孩"
  def func():
      name = 'alex'		#2.局部变量
      def inner():
          nonlocal name	#4.找到上一级的name
          name = 999		#5.将父级name重新赋值
      inner()				#3.调用inner()
      print(name)			#6.输出999
  func()					#1.调用func()
  print(name)				#7.输出老男孩
  ```

- 全局变量必须全部大写.

### 5.7函数高级

- 函数名作变量,内存地址赋值

  ```python
  def func():
  	print(123)
  v1 = func	# func内存地址给v1
  func()
  v1()		#v1()也执行func()
  ```

- 数据类型中包含函数地址

  ```python
  def func():
      print(123)			# 4.输出123,return默认返回None
  
  func_list = [func, func, func]	# 3.列表中三项都指向func
  # func_list[0]()
  # func_list[1]()
  # func_list[2]()
  for item in func_list:
      v = item()			# 1.func_list中的值赋给v
      print(v)			# 2.输出
  ```

  ```python
  def func():
      print(123)
  
  def bar():
      print(666)
  
  info = {'k1': func, 'k2': bar}
  
  info['k1']()			# 1.调用func,输出123
  info['k2']()			# 2.调用bar,输出666
  ```

- 函数作为参数传递

  ```python
  def func(arg):
      print(arg)		# 2.输出1
  
  func(1)				# 1.调用func
  func([1,2,3,4])		# 3.调用func,输出[1,2,3,4]
  
  def show():
      return 999
  func(show)		# 4.调用func,传入show,输出show内存地址
  ```

  ```python
  def func(arg):
      v1 = arg()		# 2.v1 = show(),调用show
      print(v1)		# 4.输出show()的返回值None
  
  def show():
      print(666)		# 3.输出666
  
  func(show)			# 1.调用func,传入show
  ```

  ```python
  def func(arg):
      v1 = arg()			# 2.v1 = show(),输出666
      print(v1)			# 3.输出show()的返回值None
  
  def show():
      print(666)
  
  result = func(show)		# 1.func传入show	4.res=None
  print(result)			# 5.输出func的返回值None
  ```

- 调用多个函数

  ```python
  def func():
      print('花费查询')
  def bar():
      print('语音沟通')
  def base():
      print('xxx')
  def show():
      print('xxx')
  def test():
      print('xxx')
  info = {
      'f1': func,
      'f2': bar,
      'f3':base,
      'f4':show,
      'f5':test
  }
  choice = input('请选择要选择功能：')
  function_name = info.get(choice)	# 根据输入的键选择info中的值
  if function_name:				# 存在则执行,不存在为None
      function_name()				# 执行:键()
  else:
      print('输入错误')
  ```

- 函数作返回值

  ```python
  def func():
      print(123)
  
  def bar():
      return func
  
  v = bar()			# 调用bar,v=func
  v()					# v()=func(),输出123
  ```

  ```python
  name = 'oldboy'
  def func():
      print(name)
  
  def bar():
      return func
  
  v = bar()			#调用bar,v=func
  v()					# v()=func(),输出oldboy.
  ```

  ```python
  def bar():
      def inner():
          print(123)
      return inner
  v = bar()			#调用bar,v=inner
  v()					#inner()输出123
  ```

  ```python
  name = 'oldboy'
  def bar():
      name = 'alex'
      def inner():
          print(name)
      return inner
  v = bar()			#调用bar,v=inner
  v()					#inner()输出alex
  ```

  ```python
  name = 'oldboy'
  def bar(name):
      def inner():
          print(name)
      return inner
  v1 = bar('alex') 	# 调用bar,v1=inner
  v2 = bar('eric') 	# 调用bar,v2=inner
  v1()	# inner()输出alex
  v2()	# inner()输出eric
  ```

### 5.8lambda表达式

- 用于表示简单的函数。

  ```python
  def func(a1,a2):
      return a1 + 100 
  
  func = lambda a1,a2: a1+100		#接收输入的值为a1和a2,:后面为return返回值
  ```

### 5.9内置函数

- 输入输出:

  print()	input()

- 数学运算:

  sum()	max()	min()	abs()	float()	divmod()

- 强制转换:

  int()	str()	list()	tuple()	dict()	set()	bool()

- 其他:

  len()	open()	range()	id()	type()

- 进制转换:

  bin()	oct()	hex()	int()

- 编码相关:

  chr()，将十进制数字转换成 unicode 编码中的对应字符串.

  ord()，根据字符在unicode编码中找到其对应的十进制.

  应用于生成随机验证码.

- 高级内置函数

  - map , 映射,循环让每个元素执行函数，将每个函数执行的结果保存到新的列表中，并返回。

    ```python
    v1 = [11,22,33,44]
    result = map(lambda x:x+100,v1)	# 第一个参数为执行的函数,第二个参数为可迭代元素.
    print(list(result)) # [111,122,133,144]
    ```

  - filter , 按条件筛选.

    ```python
    v1 = [11,22,33,'asd',44,'xf']
    
    # 一般做法
    def func(x):
        if type(x) == int:
            return True
        return False
    result = filter(func,v1)
    print(list(result))		# [11,22,33,44]
    
    # 简化做法
    result = filter(lambda x: True if type(x) == int else False ,v1)
    print(list(result))
    
    # 极简做法
    result = filter(lambda x: type(x) == int ,v1)
    print(list(result))
    ```

  - reduce , 对参数序列中元素进行累积.

    ```python
    import functools
    v1 = ['wo','hao','e']
    
    def func(x,y):
        return x+y
    result = functools.reduce(func,v1) 
    print(result)	# wohaoe
    
    result = functools.reduce(lambda x,y:x+y,v1)
    print(result)	# wohaoe
    ```

    

### 5.10闭包

- 为函数创建一块区域并为其维护自己数据，以后执行时方便调用。

  ```python
  def func(name):
      def inner():
          print(name)
  	return inner 
  
  v1 = func('alex')	# 调用func,v1=func
  v1()				# func()输出alex
  v2 = func('eric')	# 调用func,v2=func
  v2()				# func()输出eric
  ```


### 5.11自定义函数

- 函数式编程：增加代码的可读性和重用性。

- 生成器:

  ```python
  def func():
      yield 1
      yield 2
      yield 3
  
  v = func()  # 生成器
  # 循环v时或v.__next__() 时输出1,2,3。
  ```

- 列表推导式

  ```python
  v1 = [i for i in range(10)] # 立即循环创建所有元素。
  print(v1)
  ```

- 生成器推导式

  ```python
  v2 = (i for i in range(10)) # 创建了一个生成器，内部循环为执行。
  ```



## 第六章 模块

### 6.1md5加密字符串

- 普通加密

  ```python
  import hashlib		# 引入模块
  
  def get_md5(data):
      obj = hashlib.md5()		# 简单加密,易被破解
      obj.update(data.encode('utf-8'))
      result = obj.hexdigest()
      return result
  
  val = get_md5('123')	# md5加密'123'
  print(val)
  ```

- 加盐 , 高级加密

  ```python
  import hashlib
  
  def get_md5(data):
      obj = hashlib.md5("resdy54436jgfdsjdxff123ad".encode('utf-8'))
      obj.update(data.encode('utf-8'))
      result = obj.hexdigest()
      return result
  
  val = get_md5('123')
  print(val)
  ```

- 应用于用户注册登录

  ```python
  import hashlib
  USER_LIST = []		# 注册用户列表
  def get_md5(data):	# 加密函数
      obj = hashlib.md5("12:;idy54436jgfdsjdxff123ad".encode('utf-8'))
      obj.update(data.encode('utf-8'))
      result = obj.hexdigest()
      return result
  
  
  def register():		# 注册函数
      print('**************用户注册**************')
      while True:
          user = input('请输入用户名:')
          if user == 'N':
              return
          pwd = input('请输入密码:')
          temp = {'username':user,'password':get_md5(pwd)}
          USER_LIST.append(temp)
  
  def login():		# 登录函数
      print('**************用户登陆**************')
      user = input('请输入用户名:')
      pwd = input('请输入密码:')
  
      for item in USER_LIST:
          if item['username'] == user and item['password'] == get_md5(pwd):
              return True
  
  
  register()
  result = login()
  if result:
      print('登陆成功')
  else:
      print('登陆失败')
  ```

- 密码隐藏

  pycharm不管用,cmd中可用.

  ```python
  import getpass
  
  pwd = getpass.getpass('请输入密码：')
  if pwd == '123':
      print('输入正确')
  ```

  

### 6.2装饰器

- 定义:在不改变原函数内部代码的基础上，在函数执行之前和之后自动执行某个功能。

```python
def func(arg):
    def inner():
        arg()
    return inner

def f1():
    print(123)

v1 = func(f1)		# 调用func,f1给arg,返回inner给v1
v1()			# 调用inner,f1()输出123
```

```python
def func(arg):
    def inner():
        return arg()
    return inner

def f1():
    print(123)
    return 666

v1 = func(f1)	# 调用func,f1给arg,返回inner给v1
result = v1() # 执行inner,arg()=f1()输出123,返回666给inner的return,返回给result.
print(result) # 666
```

```python
def func(arg):
    def inner():
        v = arg()
        return v 
    return inner 

# 第一步：执行func函数并将下面的函数参数传递，相当于：func(index)
# 第二步：将func的返回值重新赋值给下面的函数名。 index = func(index)
@func 
def index():
    print(123)
    return 666

print(index)	# inner函数的地址
```

```python
# 计算函数执行时间
import time

def wrapper(func):
    def inner():
        start_time = time.time()
        v = func()
        end_time = time.time()
        print(end_time-start_time)
        return v
    return inner

@wrapper
def func1():
    time.sleep(2)
    print(123)
@wrapper
def func2():
    time.sleep(1)
    print(123)

def func3():
    time.sleep(1.5)
    print(123)

func1()
```

- 目的 : 在不改变原函数的基础上,再函数执行前后自定义功能.

- 编写装饰器和应用

  ```python
  def x(func):
      def y():
          # 前
          ret = func()
          # 后
          return ret 
  	return y 
  
  # 装饰器的应用
  @x
  def index():
      return 10
  
  @x
  def manage():
      pass
  
  # 执行函数，自动触发装饰器了
  v = index()
  print(v)	# 10
  ```

- 装饰器格式

  ```python
  def 外层函数(参数): 
      def 内层函数(*args,**kwargs):
          return 参数(*args,**kwargs)
      return 内层函数
  ```

- 装饰器应用格式

  ```python
  @外层函数
  def index():
      pass
  
  index()
  ```

- 带参数的装饰器

  ```python
  def x(counter): 
      print('x函数')
      def wrapper(func):
          print('wrapper函数')
          def inner(*args,**kwargs):
              if counter:
                  return 123
              return func(*args,**kwargs)
          return inner
      return wrapper
  
  @x(True)	# x(True)返回wrapper,@wrapper.	处理第一种情况
  def fun990():
      pass
  
  @x(False)	# 处理第二种情况
  def func10():
      pass
  ```

  

- 无敌装饰器

  ```python
  def x1(func):
      def inner(*args,**kwargs):			# 无敌,接收任意参数
          return func(*args,**kwargs)
      return inner 
  ```

  

### 6.2推导式

- 列表推导式 , 目的:方便生成一个列表.

  ```python
  v1 = [i for i in 可迭代对象 ]
  v2 = [i for i in 可迭代对象 if 条件 ]
  ```

- 集合推导式

  ```python
  v1 = { i for i in 'alex' }
  ```

- 字典推导式

  ```python
  v1 = { 'k'+str(i):i for i in range(10) }
  ```

  

### 6.3import和from x import y

- import
  - import 模块1	模块1.函数()
  - import 模块1.模块2.模块3     模块1.模块2.模块3.函数()
- from xx import xxx
  - from 模块.模块 import 函数     函数()
  - from 模块.模块 import 函数 as f     f()
  - from 模块.模块 import *     函数1()  函数2()
  - from 模块 import 模块     模块.函数()
  - from 模块 import 模块 as m     m.函数()
- 特殊情况
  - import 文件夹 加载 __ init__ .py
  - from 文件 import *

- 模块和要执行的py文件在同一目录且需要模块中的很多功能时,推荐用import 模块.

- 其他情况推荐:

  1.from 模块 import 模块 模块.函数()

  2.from 模块.模块 import 函数 函数()



### 6.4内置模块

#### 6.4.1sys

- sys.path	默认Python导入模块时,会按照sys.path中的路径挨个查找.

  ```python
  import sys
  sys.path.append('D:\\')
  import oldboy
  ```

- sys.getrefcount , 获取一个值的应用计数

  ```python
  a = [11,22,33]	# 1
  b = a	# 2
  print(sys.getrefcount(a)) # 3
  ```

- sys.getrecursionlimit , 可修改python默认支持的递归数量

- sys.stdout.write 相当于print,最后加上长度

- sys.argv  获取用户执行脚本时传入的参数 , 组成列表 , 第一项为脚本本身

- sys.exit()  任意位置终止程序

#### 6.4.2os

- os.rename(a,b)	a重命名为b
- os.mkdir	创建目录
- os.makedirs	创建目录和子目录
- os.stat(文件路径)	查看文件大小

- os.path.exists(path) ， 如果path存在，返回True；如果path不存在，返回False

- os.stat('filename').st_size ， 获取文件大小

- os.path.abspath() ， 获取一个文件的绝对路径

  ```python
  path = 'filename' # D:\code\s21day14\filemname
  
  import os
  v1 = os.path.abspath(path)
  print(v1)
  ```

- os.path.dirname ，获取路径的上级目录

  ```python
  import os
  v = r"D:\code\s21day14\filename"
  
  print(os.path.dirname(v))
  ```

- os.path.join ，路径的拼接

  ```python
  import os
  path = "D:\code\s21day14" # user/index/inx/fasd/
  v = 'n.txt'
  
  result = os.path.join(path,v)
  print(result)
  result = os.path.join(path,'n1','n2','n3')
  print(result)
  ```

- os.listdir ， 查看一个目录下所有的文件【第一层】

  ```python
  import os
  
  result = os.listdir(r'D:\code\s21day14')
  for path in result:
      print(path)
  ```

- os.walk ， 查看一个目录下所有的文件【所有层】

```python
import os

result = os.walk(r'D:\code\s21day14')
for a,b,c in result:
    # a,正在查看的目录 b,此目录下的文件夹  c,此目录下的文件
    for item in c:
        path = os.path.join(a,item)
        print(path)
```

- 补充转义

```python
v1 = r"D:\code\s21day14\n1.mp4"  (推荐)
print(v1)

v2 = "D:\\code\\s21day14\\n1.mp4"
print(v2)
```



#### 6.4.3json

- 特殊的字符串 , 只有:int / str / list / dict 

- 最外层必须是列表或字典,如果包含字符串,必须是双引号"".

- 序列化:将Python的值转换为json格式的字符串.

- 反序列化:将json格式的字符串转换成Python的数据类型.

- 优点:所有语言通用

  缺点:只能序列化基本的数据类型.

- json字典或列表中如有中文,序列化时想保留中文显示:

  ```python
  v = {'k1':'alex','k2':'李杰'}
  
  import json
  val = json.dumps(v,ensure_ascii=False)
  print(val)
  ```

- dumps

  ```python
  import json
  
  v = {'k1':'alex','k2':'李杰'}
  f = open('x.txt',mode='w',encoding='utf-8')
  val = json.dump(v,f,ensure_ascii=False)
  print(val)
  f.close()
  ```

- loads

  ```python
  import json
  
  f = open('x.txt',mode='r',encoding='utf-8')
  data = json.load(f)
  f.close()
  print(data,type(data))
  ```



#### 6.4.4time

UTC / GMT : 世界时间

本地时间 : 本地时区的时间(东八区)

- time.time()	时间戳,1970-1-1 00:00

- time.sleep(10)	运行等待秒数

- time.timezone	距0经度时区时间差 , 秒

#### 6.4.5hashlib

- md5加密

  ```python
  import hashlib
  
  md5 = hashlib.md5()		# ()里加盐
  md5.update('123'.encode('utf-8'))	# 加密字符串
  print(md5.hexdigest())
  ```

  

#### 6.4.6random

- random.randint()	随机数字

- shuffle	乱序

  ```python
  import random
  
  lst = [1, 5, 4, 6, 7, 9]
  a = random.shuffle(lst)
  print(lst)
  ```

- uniform	随机小数

  ```python
  import random
  
  a = random.uniform(1,3)
  print(a)	# 2.2647182479704737
  ```

- sample	可迭代中随机多个不重复

  ```python
  import random
  
  a = random.sample('12345', 4)
  print(a)	# ['1','2','5','4']
  ```

- choice	字符中随机取一个

  ```python
  import random
  
  a = random.choice('123456')
  print(a)	# '3'
  ```

#### 6.4.7getpass

- getpass.getpass()	密码不显示

#### 6.4.8shutil

```python
import shutil

# 删除目录
# shutil.rmtree('test')

# 目录重命名
# shutil.move('test','ttt')

# 压缩文件
# shutil.make_archive('zzh','zip','D:\code\s21day16\lizhong')
#   			压缩成的文件名		格式 		把该目录压缩

# 解压文件
# shutil.unpack_archive('zzh.zip',extract_dir=r'D:\code\xxxxxx\xxxx',format='zip')
# 					要解压的文件		解压到的路径				压缩类型
```

#### 6.4.9copy

- copy.copy()	浅拷贝,只拷贝第一层
- copy.deepcopy()  	深拷贝,拷贝全部层可变对象

#### 6.4.10pickle

- 优点:Python中所有的东西都能被序列化(除socket对象)

  缺点:序列化的内容只有Python认识.

#### 6.4.11datetime

```python
import time
from datetime import datetime,timezone,timedelta

# ######################## 获取datetime格式时间 ##############################
"""
v1 = datetime.now() # 当前本地时间
print(v1)
tz = timezone(timedelta(hours=7)) # 当前东7区时间
v2 = datetime.now(tz)
print(v2)
v3 = datetime.utcnow() # 当前UTC时间
print(v3)
"""

# ######################## 把datetime格式转换成字符串 ##############################
# v1 = datetime.now()
# print(v1,type(v1))
# val = v1.strftime("%Y-%m-%d %H:%M:%S")
# print(val)

# ######################## 字符串转成datetime ##############################
# v1 = datetime.strptime('2011-11-11','%Y-%m-%d')
# print(v1,type(v1))

# ######################## datetime时间的加减 ##############################
# v1 = datetime.strptime('2011-11-11','%Y-%m-%d')
# v2 = v1 - timedelta(days=140)
# date = v2.strftime('%Y-%m-%d')
# print(date)

# ######################## 时间戳和datetime关系 ##############################
# ctime = time.time()
# print(ctime)
# v1 = datetime.fromtimestamp(ctime)
# print(v1)

# v1 = datetime.now()
# val = v1.timestamp()
# print(val)
```

#### 6.4.12异常处理

报错时使用

```python
try:
    内容
except Exception:
    print('操作异常')
```

#### 6.4.13re模块

- findall   找到所有符合规则的项，并返回列表
- search   找到第一个符合规则的项，并返回一个对象
- match    从头开始匹配 找到第一个符合规则的项，并返回一个对象
- finditer 找到所有符合规则的项，并返回一个迭代器
- compile  预编译一个正则规则，节省多次使用同一个正则的编译时间
- sub      替换 默认替换所有，可以使用替换深度参数
- subn     替换 返回元组 第二项是替换次数
- split    根据正则规则切割，返回列表，默认不保留切掉的内容

### 6.5第三方模块

#### 6.5.1安装

1.pip包管理工具: pip install 模块

2.源码安装: 

- 下载源码包 : 压缩文件.
- 解压压缩包
- 打开cmd,进入python安装目录下的lib\site-packages
- 执行python3 setup.py build
- 执行python3 setup.py install

3.自定义

- py文件
- 文件夹: 要创建 __ init__.py 文件

#### 6.5.2调用模块

- import
  - import 模块1	模块1.函数()
  - import 模块1.模块2.模块3     模块1.模块2.模块3.函数()
- from xx import xxx
  - from 模块.模块 import 函数     函数()
  - from 模块.模块 import 函数 as f     f()
  - from 模块.模块 import *     函数1()  函数2()
  - from 模块 import 模块     模块.函数()
  - from 模块 import 模块 as m     m.函数()
- 特殊情况
  - import 文件夹 加载 __ init__ .py
  - from 文件 import *

### 6.6自定义模块

- 可以把一个py文件或一个文件夹(包)当做一个模块,以方便于以后其他py文件的调用.

- py2:文件夹中必须有__ init __.py文件

  py3:不需要__ init __.py

  推荐加上该文件.

## 第七章 面向对象

### 1.基本格式

```python
# ###### 定义类 ###### 
class 类名:
    def 方法名(self,name):
        print(name)
        return 123
    def 方法名(self,name):
        print(name)
        return 123
    def 方法名(self,name):
        print(name)
        return 123
# ###### 调用类中的方法 ###### 
# 1.创建该类的对象
obj = 类名()
# 2.通过对象调用方法
result = obj.方法名('alex')
print(result)
```

- 代码从上到下执行

  ```python
  class Foo:
      print('你')
      x = 1
      def func(sef):
          pass
      
      class Meta:
          print('好')
          y = 123
          def show(self):
              pass
  # 输出 你	好
  ```

### 2.对象的作用

- 存储一些值，方便以后自己使用。

```python
class File:
    def read(self):
        with open(self.xxxxx, mode='r', encoding='utf-8') as f:
            data = f.read()
        return data

    def write(self, content):
        with open(self.xxxxx, mode='a', encoding='utf-8') as f:
            f.write(content)

# # 实例化了一个File类的对象
obj1 = File()
# # 在对象中写了一个xxxxx = 'test.log'
obj1.xxxxx = "test.log"
# # 通过对象调用类中的read方法，read方法中的self就是obj。
# # obj1.read()
obj1.write('alex')


# 实例化了一个File类的对象
obj2 = File()
# 在对象中写了一个xxxxx = 'test.log'
obj2.xxxxx = "info.txt"
# 通过对象调用类中的read方法，read方法中的self就是obj。
# obj2.read()
obj2.write('alex')
```

```python
class Person:
    def show(self):
        temp = "我是%s,年龄：%s,性别：%s " %(self.name,self.age,self.gender,)
        print(temp)

p1 = Person()
p1.name = '李邵奇'
p1.age = 19
p1.gender = '男'
p1.show()

p2 = Person()
p2.name = '利奇航'
p2.age = 19
p2.gender = '男'
p2.show()
```

```python
class Person:
    def __init__(self,n,a,g): # 初始化方法（构造方法），给对象的内部做初始化。
        self.name = n
        self.age = a
        self.gender = g

    def show(self):
        temp = "我是%s,年龄：%s,性别：%s " % (self.name, self.age, self.gender,)
        print(temp)

# 类() 实例化对象，自动执行此类中的 __init__方法。
p1 = Person('李兆琪',19,'男')
p1.show()

p2 = Person('利奇航',19,'男')
p2.show()
```

总结：将数据封装到对象，方便使用。

### 3.归类和公共值

- 如果写代码时,函数比较多,比较乱:

  1.可以将函数归类,并放到同一个类中

  2.函数如果有一个反复使用的公共值,则可以放到对象中.(__ init__(self,xxx))

```python
class File:
    def __init__(self,path):
        self.file_path = path
    def read(self):
        print(self.file_path)
    def write(self,content):
        print(self.file_path)
    def delete(self):
        print(self.file_path)
    def update(self):
        print(self.file_path)

p1 = File('log.txt')
p1.read()

p2 = File('xxxxxx.txt')
p2.read()
```

```python
# 循环让用户输入：用户名/密码/邮箱。 输入完成后再进行数据打印。
# 1.
class Person:
    def __init__(self,user,pwd,email):
        self.username = user
        self.password = pwd
        self.email = email

USER_LIST = [对象(用户/密码/邮箱),对象(用户/密码/邮箱),对象(用户/密码/邮箱)]
while True:
    user = input('请输入用户名：')
    pwd = input('请输入密码：')
    email = input('请输入邮箱：')
    p = Person(user,pwd,email)
    USER_LIST.append(p)

for item in USER_LIST:
    temp = "我的名字：%s,密码：%s,邮箱%s" %(item.username,item.password,item.email,)
    print(temp)

# 2.
class Person:
    def __init__(self,user,pwd,email):
        self.username = user
        self.password = pwd
        self.email = email
        
    def info(self):
        return "我的名字：%s,密码：%s,邮箱%s" %(item.username,item.password,item.email,)
    
USER_LIST = [对象(用户/密码/邮箱),对象(用户/密码/邮箱),对象(用户/密码/邮箱)]
while True:
    user = input('请输入用户名：')
    pwd = input('请输入密码：')
    email = input('请输入邮箱：')
    p = Person(user,pwd,email)
    USER_LIST.append(p)

for item in USER_LIST:
    msg = item.info()
    print(msg)
```

### 4.游戏开发

```python
class Police:
    def __init__(self,name):
        self.name = name
        self.hp = 10000
    
    def tax(self):
        msg = "%s收了个税。" %(self.name,)
        print(msg)

lsq = Police('李邵奇')
zzh = Police('渣渣会')
tyg = Police('堂有光')


class Bandit:
    def __init__(self,nickname)
		self.nickname = nickname 
		self.hp = 1000
	
    def murder(self,name):
        msg = "%s去谋杀了%s" %(self.nickname, name,)


lcj = Bandit('二蛋')
lp = Bandit('二狗')
zsd = Bandit('狗蛋')

# 1. 二狗去谋杀渣渣会，二狗生命值-100; 渣渣会生命值减5000
lp.murder(zzh.name)
lp.hp = lp.hp - 100
zzh.hp = zzh.hp - 5000
# ... 
```

```python
class Police:
	def __init__(self,name)
		self.name = name
         self.hp = 10000
    def dao(self,other):
        msg = "%s个了%s一刀。" %(self.name,other.nickname)
        self.hp = self.hp - 10
        other.hp = other.hp - 50
        print(msg)
    def qiang(self):
        msg = "%s去战了个斗。" %(self.name,)
	def quan(self,other):
        msg = "%s个了%s一全。" %(self.name,other.nickname)
        self.hp = self.hp - 2
        other.hp = other.hp - 10
        print(msg)


class Bandit:
    def __init__(self,nickname)
		self.nickname = nickname
		self.hp = 1000
    def qiang(self,other):
        msg = "%s个了%s一全。" %(self.nickname,other.name)
        self.hp -= 20
        other.hp -= 500


lcj = Bandit('二蛋')
lsq = Police('李邵奇')
lsq.dao(lcj)
lsq.quan(lcj)
lcj.qiang(lsq)
```

### 5.继承

```python
# 父类(基类)
class Base:
    def f1(self):
        pass
# 子类（派生类）
class Foo(Base):
    def f2(self):
        pass

# 创建了一个字类的对象
obj = Foo()
# 执行对象.方法时，优先在自己的类中找，如果没有就是父类中找。
obj.f2()
obj.f1()

# 创建了一个父类的对象
obj = Base()
obj.f1()
```

```python
# 示例三
class Base:
    def f1(self):
        print('base.f1')
        
class Foo(Base):
    def f2(self):
        self.f1()
        print('foo.f2')
	def f1(self):
        print('foo.f1')
        
obj = Foo()
obj.f2()	# foo.f1	foo.f2

# 示例四
class Base:
    def f1(self):
        self.f2()
        print('base.f1')
	def f2(self):
        print('base.f2')
class Foo(Base):
    def f2(self):
        print('foo.f2')
        
obj = Foo()
obj.f1()	# foo.f2	base.f1

# 示例五
class TCPServer:
    pass
class ThreadingMixIn:
    pass
class ThreadingTCPServer(ThreadingMixIn, TCPServer): 
    pass

# 示例六
class BaseServer:
    def serve_forever(self, poll_interval=0.5):
        self._handle_request_noblock()
	def _handle_request_noblock(self):
        self.process_request(request, client_address)
        
	def process_request(self, request, client_address):
        pass
    
class TCPServer(BaseServer):
    pass

class ThreadingMixIn:
    def process_request(self, request, client_address):
        pass
    
class ThreadingTCPServer(ThreadingMixIn, TCPServer): 
    pass

obj = ThreadingTCPServer()
obj.serve_forever()		# 类可以有多个基类,从左往右挨个找.
```

注意事项：

- self 到底是谁.
- self 是哪个类创建的，就从此类开始找，自己没有就找父类(基类)。

### 6.多态和鸭子模型

```python
# Python
def func(arg):
    v = arg[-1] # arg.append(9)
    print(v)

# java
def func(str arg):
    v = arg[-1]
    print(v)
```

面试题：什么是鸭子模型。

```
对于一个函数而言，Python对于参数的类型不会限制，那么传入参数时就可以是各种类型，在函数中如果有例如：arg.send方法，那么就是对于传入类型的一个限制（类型必须有send方法）。
这就是鸭子模型，类似于上述的函数我们认为只要能呱呱叫的就是鸭子（只要有send方法，就是我们要想的类型）
```

### 7.使用面向对象的情况

- 函数(业务功能)比较多,可以使用面向对象来进行归类.
- 想要做数据封装(创建字典存储数据时,面向对象).
- 游戏示例:创建一些角色并且根据角色需要再创建人物.

### 8.迭代器

- 对某种(str / list / tuple / dict / set类创建的对象) 可迭代对象中的元素进行逐一获取,表象:具有__ next__方法且每次调用都获取可迭代对象中的元素.

- 列表转换成迭代器

  ```python
  v1 = iter([11,22,33,44])
  v1 = [11,22,33,44].__iter__()
  # iter()和.__iter__()都是转换为迭代器
  ```

- 迭代器获取每个值需反复调用

  ```python
  v1 = [11,22,33,44]
  
  # 列表转换成迭代器
  v2 = iter(v1)
  result1 = v2.__next__()
  print(result1)			# 11
  result2 = v2.__next__()
  print(result2)			# 22
  result3 = v2.__next__()
  print(result3)			# 33
  result4 = v2.__next__()
  print(result4)			# 44
  result5 = v2.__next__()
  print(result5)			# 超出范围报错StopIteration
  """
  # v1 = "alex"
  # v2 = iter(v1)
  # while True:
  #     try:
  #         val = v2.__next__()
  #         print(val)
  #     except Exception as e:
  #         break
  
  try:
  	内容
  except Exception:
  	break
  报错结束进程
  """
  ```

- 报错表示迭代完毕.

- 判断一个对象是否是迭代器:dir(对象)查看内部是否有__ next__()方法.

- for循环内部会把可迭代对象转化为迭代器,反复执行__ next__(),取完不报错

### 9.可迭代对象

- dir(对象)查看内部是否具有__ iter__()方法,且返回一个迭代器.

- 表象：可以被for循环对象就可以称为是可迭代对象.

  ```python
  class Foo:
      def __iter__(self):
          return iter([1,2,3,4])
  
  obj = Foo()
  
  class Foo:
      def __iter__(self):
          yield 1
          yield 2
          yield 3
  
  obj = Foo()
  ```

### 10.生成器

- 如果函数中存在yield,该函数就是生成器函数,调用该生成器函数会返回生成器,生成器被for循环时,内部代码才会执行,每次返回yield的值,下次循环从yield下一行开始.

```python
# 生成器函数(内部包含yield)
def func():
    print('F1')
    yield 1
    print('F2')
    yield 2
    print('F3')
    yield 100
    print('F4')
# 函数内部代码不执行,返回一个生成器
v1 = func()
# 生成器可以被for循环，一旦开始循环那么函数内部代码就会开始执行。
for i in v1:
    print(i)
```

```python
def func():
    count = 1
    while True:
        yield count		# 循环一次返回yield停止,下次循环从下一行开始.
        count += 1

val = func()		# 获取生成器

for item in val:	# 开始循环生成器
    print(item)		# 1,2,3...
```

```python
def func():
    count = 1
    while True:
        yield count
        count += 1
        if count == 100:		# 符合条件即停止函数.
            return

val = func()
for item in val:				# 1,2...99
    print(item)
```

- 读取redis文件

  ```python
  def func():
      """
      分批去读取文件中的内容，将文件的内容返回给调用者。
      :return:
      """
      cursor = 0
      while True:
          f = open('db', 'r', encoding='utf-8')# 通过网络连接上redis
          # 代指   redis[0:10]
          f.seek(cursor)
          data_list =[]
          for i in range(10):
              line = f.readline()
              if not line:
                  return
              data_list.append(line)
          cursor = f.tell()
          f.close()  # 关闭与redis的连接
  
          for row in data_list:
              yield row
  
  for item in func():
      print(item)
  ```

### 11.类成员

#### 11.1实例变量

```python
class Foo:
    def __init__(self,name):
        self.name = name
        
    def info(self):
        pass

obj1 = Foo('alex')
obj2 = Foo('eric')
```

#### 11.2类变量

- 定义 : 写在类的下一级和方法同一级.

  ```python
  class Foo:
      city = '北京'
      
      def __init__(self,name):
          self.name = name
  ```

- 访问:

  - 类.类变量名称
  - 对象 : 类变量名称

- 面试题

  ```python
  class Base:
      x = 1
  
  obj = Base()
  
  
  print(obj.x) # 先去对象中找，没有再去类中找。	1
  obj.y = 123  # 在对象中添加了一个y=123的变量。
  print(obj.y)	# 123
  obj.x = 123
  print(obj.x)	# 123
  print(Base.x)	# 1
  ```

```python
class Parent:
    x = 1
    
class Child1(Parent):
    pass

class Child2(Parent):
    pass

print(Parent.x,Child1.x,Child2.x) # 1 1 1
Child1.x = 2
print(Parent.x,Child1.x,Child2.x) # 1 2 1
Child2.x = 3
print(Parent.x,Child1.x,Child2.x) # 1 2 3
```

总结：找变量优先找自己，自己没有找 类 或 基类；修改或赋值只能在自己的内部设置。

#### 11.3方法（绑定方法/普通方法）

- 定义：至少有一个self参数

- 执行：先创建对象，由对象.方法()。

  ```python
  class Foo:
      def func(self,a,b):
          print(a,b)
  
  obj = Foo()
  obj.func(1,2)
  # ###########################
  class Foo:
      def __init__(self):
          self.name = 123
  
      def func(self, a, b):
          print(self.name, a, b)
  
  obj = Foo()
  obj.func(1, 2)
  ```

#### 11.4静态方法

- 定义：
  - @staticmethod装饰器
  - 参数无限制
- 执行：
  - 类.静态方法名 ()
  - 对象.静态方法() (不推荐)

```python
class Foo:
    def __init__(self):
        self.name = 123

    def func(self, a, b):
        print(self.name, a, b)

    @staticmethod
    def f1():
        print(123)

obj = Foo()
obj.func(1, 2)

Foo.f1()
obj.f1() # 不推荐
```

#### 11.5类方法

- 定义：
  - @classmethod装饰器
  - 至少有cls参数，当前类。
- 执行：
  - 类.类方法()
  - 对象.类方法() （不推荐）

```python
class Foo:
    def __init__(self):
        self.name = 123

    def func(self, a, b):
        print(self.name, a, b)

    @staticmethod
    def f1():
        print(123)

    @classmethod
    def f2(cls,a,b):
        print('cls是当前类',cls)
        print(a,b)

obj = Foo()
obj.func(1, 2)

Foo.f1()	# 123
Foo.f2(1,2)	# 类名称, 1, 2
```

- 面试题：

```python
# 问题： @classmethod和@staticmethod的区别？
"""
一个是类方法一个静态方法。
定义：
	类方法：用@classmethod做装饰器且至少有一个cls参数。
	静态方法：用staticmethod做装饰器且参数无限制。
调用：
	类.方法直接调用。
	对象.方法也可以调用。
"""
```

#### 11.6属性

- 定义：
  - @property装饰器
  - 只有一个self参数
- 执行：
  - 对象.方法 不用加括号。

```python
class Foo:

    @property
    def func(self):
        print(123)
        return 666

obj = Foo()
result = obj.func	# 123
print(result)	# 666
```

- 属性的应用 , 分页

```python
# 属性的应用

class Page:
    def __init__(self, total_count, current_page, per_page_count=10):
        self.total_count = total_count
        self.per_page_count = per_page_count
        self.current_page = current_page

    @property
    def start_index(self):
        return (self.current_page - 1) * self.per_page_count

    @property
    def end_index(self):
        return self.current_page * self.per_page_count

# 创建一个321项的列表
USER_LIST = []
for i in range(321):
    USER_LIST.append('alex-%s' % (i,))

# 请实现分页展示：
current_page = int(input('请输入要查看的页码：'))
p = Page(321, current_page)
data_list = USER_LIST[p.start_index:p.end_index]
for item in data_list:
    print(item)
```

### 12.成员修饰符

- 公有，所有地方都能访问到。
- 私有，只有自己可以访问到。

```python
class Foo:
    def __init__(self, name):
        self.__name = name

    def func(self):
        print(self.__name)


obj = Foo('alex')
# print(obj.__name)
obj.func()
```

```python
class Foo:
    __x = 1

    @staticmethod
    def func():
        print(Foo.__x)


# print(Foo.__x)
Foo.func()
```

```python
class Foo:

    def __fun(self):
        print('msg')

    def show(self):
        self.__fun()

obj = Foo()
# obj.__fun()
obj.show()
```

### 13.补充

```python
class Foo:
    def __init__(self,num):
        self.num = num


cls_list = []
for i in range(10):
    cls_list.append(Foo)
    
for i in range(len(cls_list)):
    obj = cls_list[i](i)
    print(obj.num)
```

```python
class Foo:
    def __init__(self,num):
        self.num = num

B = Foo
obj = B('alex')
```

```python
class Foo:
	def f1(self):
        print('f1')

    def f2(self):
        print('f2')

obj = Foo()

v = [ obj.f1,obj.f2 ]
for item in v:
    item()
```

```python
class Foo:
    def f1(self):
        print('f1')
    
    def f2(self):
        print('f2')
        
	def f3(self):
        v = [self.f1 , self.f2 ]
        for item in v:
            item()
            
obj = Foo()
obj.f3()
```

```python
class Account:

    def login(self):
        pass

    def register(self):
        pass

    def run(self):
        info = {'1':self.register, '2':self.login }
        choice = input('请选择:')
        method = info.get(choice)
        method()
```

```python
class Foo:
    pass

class Foo(object):
    pass

# 在python3中这俩的写法是一样，因为所有的类默认都会继承object类，全部都是新式类。

# 如果在python2中这样定义，则称其为：经典类
class Foo:
    pass 
# 如果在python2中这样定义，则称其为：新式类
class Foo(object):
    pass 

class Base(object):
    pass
class Bar(Base):
    pass
```

- 强制访问私有成员

```python
class Foo:
    def __init__(self,name):
        self.__x = name


obj = Foo('alex')

print(obj._Foo__x) # 强制访问私有实例变量
```

### 14.嵌套

- 函数：参数可以是任意类型。
- 字典：对象和类都可以做字典的key和value
- 继承的查找关系

```python
class StarkConfig(object):
    pass

class AdminSite(object):
    def __init__(self):
        self.data_list = []
        
    def register(self,arg):
        self.data_list.append(arg)
        
site = AdminSite()
obj = StarkConfig()
site.register(obj)
```

```python
class StarkConfig(object):
    def __init__(self,name,age):
        self.name = name
        self.age = age

class AdminSite(object):
    def __init__(self):
        self.data_list = []
        self.sk = None

    def set_sk(self,arg):
        self.sk = arg

site = AdminSite() # data_list = []  sk = StarkConfig
site.set_sk(StarkConfig)
site.sk('alex',19)
```

```python
class StackConfig(object):
    pass

class Foo(object):
    pass

class Base(object):
    pass

class AdminSite(object):
    def __init__(self):
        self._register = {}

    def registry(self,key,arg):
        self._register[key] = arg

site = AdminSite()
site.registry(1,StackConfig)
site.registry(2,StackConfig)
site.registry(3,StackConfig)
site.registry(4,Foo)
site.registry(5,Base)

for k,v in site._register.items():
    print(k,v() )
```

```python
class StackConfig(object):
    pass

class UserConfig(StackConfig):
    pass


class AdminSite(object):
    def __init__(self):
        self._register = {}

    def registry(self,key,arg=StackConfig):
        self._register[key] = arg

    def run(self):
        for key,value in self._register.items():
            obj = value()
            print(key,obj)
site = AdminSite()
site.registry(1)
site.registry(2,StackConfig)
site.registry(3,UserConfig)
site.run()
```

```python
class StackConfig(object):
    list_display = '李邵奇'

class UserConfig(StackConfig):
    list_display = '利奇航'


class AdminSite(object):
    def __init__(self):
        self._register = {}

    def registry(self,key,arg=StackConfig):
        self._register[key] = arg

    def run(self):
        for key,value in self._register.items():
            obj = value()
            print(key,obj.list_display)
site = AdminSite()
site.registry(1)
site.registry(2,StackConfig)
site.registry(3,UserConfig)
site.run()
```

```python
class StackConfig(object):
    list_display = '李邵奇'

    def changelist_view(self):
        print(self.list_display)

class UserConfig(StackConfig):
    list_display = '利奇航'

class AdminSite(object):
    def __init__(self):
        self._register = {}

    def registry(self,key,arg=StackConfig):
        self._register[key] = arg

    def run(self):
        for key,value in self._register.items():
            obj = value()
            obj.changelist_view()
site = AdminSite()
site.registry(1)
site.registry(2,StackConfig)
site.registry(3,UserConfig)
site.run()
```

### 15.特殊成员

- 就是为了能够快速实现执行某些方法而生。

#### 15.1__ init__	初始化

```python
class Foo:
    """
    类是干啥的。。。。
    """
    def __init__(self,a1):
        """
        初始化方法
        :param a1: 
        """
        self.a1 = a1
        
obj = Foo('alex')
```

#### 15.2.__ new__	创建空对象

```python
class Foo(object):
    def __init__(self):
        """
        用于给对象中赋值，初始化方法
        """
        self.x = 123
    def __new__(cls, *args, **kwargs):
        """
        用于创建空对象，构造方法
        :param args:
        :param kwargs:
        :return:
        """
        return object.__new__(cls)

obj = Foo()
```

#### 15.3.__ call__	对象后加()调用call方法

```python
class Foo(object):
    def __call__(self, *args, **kwargs):
        print('执行call方法')

# obj = Foo()
# obj()
Foo()()
```

```python
from wsgiref.simple_server import make_server

def func(environ,start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
    return ['你好'.encode("utf-8")  ]

class Foo(object):

    def __call__(self, environ,start_response):
        start_response("200 OK", [('Content-Type', 'text/html; charset=utf-8')])
        return ['你<h1 style="color:red;">不好</h1>'.encode("utf-8")]


# 作用：写一个网站，用户只要来方法，就自动找到第三个参数并执行。
server = make_server('127.0.0.1', 8000, Foo())
server.serve_forever()
```

#### 15.4.__ getitem__	__ setitem__	__ delitem__

```python
class Foo(object):

    def __setitem__(self, key, value):
        pass

    def __getitem__(self, item):
        return item + 'uuu'

    def __delitem__(self, key):
        pass


obj1 = Foo()
obj1['k1'] = 123  # 内部会自动调用 __setitem__方法
val = obj1['xxx']  # 内部会自动调用 __getitem__方法
print(val)
del obj1['ttt']  # 内部会自动调用 __delitem__ 方法
```

#### 15.5.__ str__

```python
class Foo(object):
    def __str__(self):
        """
        只有在打印对象时，会自动化调用此方法，并将其返回值在页面显示出来
        :return:
        """
        return 'asdfasudfasdfsad'

obj = Foo()
print(obj)
```

```python
class User(object):
    def __init__(self,name,email):
        self.name = name
        self.email = email
    def __str__(self):
        return "%s %s" %(self.name,self.email,)

user_list = [User('二狗','2g@qq.com'),User('二蛋','2d@qq.com'),User('狗蛋','xx@qq.com')]
for item in user_list:
    print(item)
```

#### 15.6.__ dict__	去对象中找到所有变量并将其转换为字典

```python
class Foo(object):
    def __init__(self,name,age,email):
        self.name = name
        self.age = age
        self.email = email

obj = Foo('alex',19,'xxxx@qq.com')
print(obj)
print(obj.name)
print(obj.age)
print(obj.email)
val = obj.__dict__ # 去对象中找到所有变量并将其转换为字典
print(val)
```

#### 15.7.上下文管理==面试题==

```python
class Foo(object):
    def __enter__(self):
        self.x = open('a.txt',mode='a',encoding='utf-8')
        return self.x
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.x.close()

with Foo() as ff:
    ff.write('alex')
    ff.write('alex')
    ff.write('alex')
    ff.write('alex')
```

```python
# class Context:
#     def __enter__(self):
#         print('进入')
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         print('推出')
#
#     def do_something(self):
#         print('内部执行')
#
# with Context() as ctx:
#     print('内部执行')
#     ctx.do_something()


class Foo(object):
    def do_something(self):
        print('内部执行')

class Context:
    def __enter__(self):
        print('进入')
        return Foo()

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('推出')

with Context() as ctx:
    print('内部执行')
    ctx.do_something()
```

#### 15.8.两个对象相加

```python
val = 5 + 8
print(val)

val = "alex" + "sb"
print(val)

class Foo(object):
    def __add__(self, other):
        return 123
    
obj1 = Foo()
obj2 = Foo()
val  = obj1 + obj2
print(val)
```

### 16.内置函数补充

#### 16.1.type，查看类型

```python
class Foo:
    pass

obj = Foo()

if type(obj) == Foo:
    print('obj是Foo类的对象')
```

#### 16.2.issubclass	谁是否是谁的父类或基类

```python
class Base:
    pass

class Base1(Base):
    pass

class Foo(Base1):
    pass

class Bar:
    pass

print(issubclass(Bar,Base))
print(issubclass(Foo,Base))
```

#### 16.3.isinstance	谁是否是谁的实例

```python
class Base(object):
    pass

class Foo(Base):
    pass

obj = Foo()

print(isinstance(obj,Foo))  # 判断obj是否是Foo类或其基类的实例（对象）
print(isinstance(obj,Base)) # 判断obj是否是Foo类或其基类的实例（对象）
```

### 17.super

```python
class Base(object):
    def func(self):
        print('base.func')
        return 123


class Foo(Base):
    def func(self):
        v1 = super().func()
        print('foo.func',v1)

obj = Foo()
obj.func()
# super().func() 去父类中找func方法并执行
```

```python
class Bar(object):
    def func(self):
        print('bar.func')
        return 123

class Base(Bar):
    pass

class Foo(Base):
    def func(self):
        v1 = super().func()
        print('foo.func',v1)

obj = Foo()
obj.func()
# super().func() 根据类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

```python
class Base(object): # Base -> object
    def func(self):
        super().func()
        print('base.func')

class Bar(object):
    def func(self):
        print('bar.func')

class Foo(Base,Bar): # Foo -> Base -> Bar
    pass

obj = Foo()
obj.func()

# super().func() 根据self对象所属类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

### 18.异常处理

#### 18.1.基本格式

```python
try:
    pass
except Exception as e:
    pass
```

```python
try:
    v = []
    v[11111] # IndexError
except ValueError as e:
    pass
except IndexError as e:
    pass
except Exception as e:
    print(e) # e是Exception类的对象，中有一个错误信息。
```

```python
try:
    int('asdf')
except Exception as e:
    print(e) # e是Exception类的对象，中有一个错误信息。
finally:
    print('最后无论对错都会执行')
    
# #################### 特殊情况 #########################
def func():
    try:
        # v = 1
        # return 123
        int('asdf')
    except Exception as e:
        print(e) # e是Exception类的对象，中有一个错误信息。
        return 123
    finally:
        print('最后')

func()
```

#### 18.2.主动触发异常

```python
try:
    int('123')
    raise Exception('阿萨大大是阿斯蒂') # 代码中主动抛出异常
except Exception as e:
    print(e)
```

```python
def func():
    result = True
    try:
        with open('x.log',mode='r',encoding='utf-8') as f:
            data = f.read()
        if 'alex' not in data:
            raise Exception()
    except Exception as e:
        result = False
    return result
```

#### 18.3.自定义异常

```python
class MyException(Exception):
    pass

try:
    raise MyException('asdf')
except MyException as e:
    print(e)
```

```python
class MyException(Exception):
    def __init__(self,message):
        super().__init__()
        self.message = message

try:
    raise MyException('asdf')
except MyException as e:
    print(e.message)
```

### 19.约束

```python
# 约束字类中必须写send方法，如果不写，则调用时候就报抛出 NotImplementedError 
class Interface(object):
    def send(self):
        raise NotImplementedError()

class Message(Interface):
    def send(self):
        print('发送短信')

class Email(Interface):
    def send(self):
        print('发送邮件')
```

```python
class Message(object):

    def msg(self):
        print('发短信')

	def email(self):
        print('邮件')

    def wechat(self):
        print('微信')

obj = Message()
obj.msg()
obj.email()
obj.wechat()
```

```python
class BaseMessage(object):
    def send(self,a1):
        raise NotImplementedError('字类中必须有send方法')

class Msg(BaseMessage):
    def send(self):
        pass

class Email(BaseMessage):
    def send(self):
        pass

class Wechat(BaseMessage):
    def send(self):
        pass

class DingDing(BaseMessage):
    def send(self):
        print('钉钉')
    
obj = Email()
obj.send()
```

### 20.反射

- 根据字符串的形式去某个对象中 操作 他的成员。
- getattr(对象,"字符串") 根据字符串的形式去某个对象中 获取 对象的成员。

```python
class Foo(object):
    def __init__(self,name):
        self.name = name
obj = Foo('alex')

# 获取变量
v1 = getattr(obj,'name')
# 获取方法
method_name = getattr(obj,'login')
method_name()
```

- hasattr(对象,'字符串') 根据字符串的形式去某个对象中判断是否有该成员。

```python
from wsgiref.simple_server import make_server

class View(object):
    def login(self):
        return '登陆'

    def logout(self):
        return '等处'

    def index(self):
        return '首页'

def func(environ,start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])

    obj = View()
    # 获取用户输入的URL
    method_name = environ.get('PATH_INFO').strip('/')
    if not hasattr(obj,method_name):
        return ["sdf".encode("utf-8"),]
    response = getattr(obj,method_name)()
    return [response.encode("utf-8")  ]

# 作用：写一个网站，用户只要来方法，就自动找到第三个参数并执行。
server = make_server('192.168.12.87', 8000, func)
server.serve_forever()
```

- setattr(对象,'变量','值') 根据字符串的形式去某个对象中设置成员。

```python
class Foo:
    pass

obj = Foo()
obj.k1 = 999
setattr(obj,'k1',123) # obj.k1 = 123

print(obj.k1)
```

- delattr(对象,'变量') 根据字符串的形式去某个对象中删除成员。

```python
class Foo:
    pass

obj = Foo()
obj.k1 = 999
delattr(obj,'k1')
print(obj.k1)
```

- python一切皆对象
  - py文件
  - 包
  - 类
  - 对象
- python一切皆对象，所以以后想要通过字符串的形式操作其内部成员都可以通过反射的机制实现。

### 21.模块:importlib

根据字符串的形式导入模块。

```python
模块 = importlib.import_module('utils.redis')
```



## 第八章 网络编程

### 8.1网络基础

#### 1.两个运行中的程序如何传递信息？

通过文件

#### 2.两台机器上的两个运行中的程序如何通信？

通过网络

#### 3.网络应用开发架构

C/S	client   server

B/S	browser   server

关系 : B/S是特殊的C/S架构

```python
# server端
import socket

sk = socket.socket()  # 买手机

sk.bind(('127.0.0.1', 9000))  # 绑定卡号
sk.listen()  # 开机

conn, addr = sk.accept()  # 等着接电话
conn.send(b'hello')
msg = conn.recv(1024)
print(msg)
conn.close()  # 挂电话
sk.close()  # 关机
```

```python
# client端
import socket
sk = socket.socket()
sk.connect(('127.0.0.1',9000))

msg = sk.recv(1024)
print(msg)
sk.send(b'byebye')

sk.close()
```

#### 4.名词

网卡 : 是一个实际存在在计算机中的硬件

mac地址 : 每一块网卡上都有一个全球唯一的mac地址

交换机 : 是连接多台机器并帮助通讯的物理设备，只认识mac地址

协议 : 两台物理设备之间对于要发送的内容，长度，顺序的一些约定

交换机实现的arp协议 : 通过ip地址获取一台机器的mac地址

网关ip : 一个局域网的网络出口，访问局域网之外的区域都需要经过路由器和网关

网段 : 指的是一个地址段 x.x.x.0  x.x.0.0  x.0.0.0

子网掩码 : 判断两台机器是否在同一个网段内的

```python
# 255.255.255.0 子网掩码
# 11111111.11111111.11111111.00000000

# 192.168.12.87		变成2进制
# 11000000.10101000.00001100.01010111	与子网掩码一一进行and比较
# 11111111.11111111.11111111.00000000
# 11000000.10101000.00001100.00000000   得出结果转为十进制192.168.12.0

# 192.168.12.7
# 11000000.10101000.00001100.00000111
# 11111111.11111111.11111111.00000000
# 11000000.10101000.00001100.00000000  192.168.12.0
```

ip地址能够确认一台机器

port 端口 : 0-65535   常用默认80

ip + port : 能确认一台机器上的一个应用

#### 5.ip地址

- ipv4协议   4位的点分十进制  32位2进制表示

  0.0.0.0 - 255.255.255.255

- ipv6协议 6位的冒分十六进制 128位2进制表示

  0:0:0:0:0:0-FFFF:FFFF:FFFF:FFFF:FFFF:FFFF

#### 6.公网和内网

- 公网ip

  每一个ip地址要想被所有人访问到，那么这个ip地址必须是你申请的

- 内网ip

  ```
  192.168.0.0 - 192.168.255.255
  172.16.0.0 - 172.31.255.255
  10.0.0.0 - 10.255.255.255
  ```

### 8.2编码

#### 1.计算机上的存储 / 网络上数据的传输  --二进制

1位   bit比特

10101100   8位1个字节

#### 2.send

str-encode(编码) -> bytes

#### 3.recv

bytes-decode(编码) -> str

### 8.3TCP协议

- 可靠  慢  全双工通信
- 建立连接的时候:三次握手  syn  ack
- 断开连接的时候:四次挥手  fin  ack
- 区别 : 三次握手把一个回复和请求连接的两条信息合并成一条了
- 挥手不能合并 : 由于一方断开连接后可能另一方还有数据没有传递完,所以不能立即断开.
- 在建立起连接之后
  - 发送的每一条信息都有回执
  - 为了保证数据的完整性,还有重传机制.
- 长连接:会一直占用双方的端口
- IO(input , output)操作,输入输出是相对内存来说的.
  - write  send
  - read  recv
- 应用场景
  - 文件的上传下载
- 能够传递的数据长度几乎没有限制

### 8.4UDP协议

- 无连接  速度快
- 可能会丢消息
- 能传递的数据长度是有限的,根据数据传递设备的设置有关.
- 应用场景
  - 即时通信类

### 8.5osi七层模型

- 应用层
  - 表示层
  - 会话层
- 传输层
- 网络层
- 数据链路层
- 物理层

```python
# osi五层协议     协议                  物理设备

# 5应用层      http https ftp smtp

#             python代码  hello

# 4传输层      tcp/udp协议 端口      四层交换机、四层路由器

# 3网络层      ipv4/ipv6协议         路由器、三层交换机

# 2数据链路层  mac地址  arp协议      网卡 、交换机

# 1物理层
```

### 8.6socket模块 (套接字)

- 工作在应用层和传输层之间的抽象层
  - 帮助我们完成了所有信息的组织和拼接
- socket对于程序员来说是网络操作的最底层

tcp协议

- server端

  ```python
  import socket
  
  
  sk = socket.socket()
  sk.bind(('127.0.0.1',9000))
  sk.listen()    # n 允许多少客户端等待
  print('*'*20)
  while True:   # outer
      conn,addr = sk.accept()    # 阻塞
      print(addr)
      while True:   # inner
          msg = conn.recv(1024).decode('utf-8')
          if msg.upper() == 'Q': break
          print(msg)
          inp = input('>>>')
          conn.send(inp.encode('utf-8'))
          if inp.upper() == 'Q':
              break
      conn.close()
  sk.close()
  ```

- client端

  ```python
  import socket
  
  
  sk = socket.socket()
  sk.connect(('127.0.0.1',9000))
  while True:
      inp = input('>>>')
      sk.send(inp.encode('utf-8'))
      if inp.upper() == 'Q':
          break
      msg = sk.recv(1024).decode('utf-8')
      if msg.upper() == 'Q': break
      print(msg)
  sk.close()
  ```

udp协议

- server端

  ```python
  import socket
  
  sk = socket.socket(type = socket.SOCK_DGRAM)
  sk.bind(('127.0.0.1',9000))
  while True:
      msg,client_addr = sk.recvfrom(1024)
      print(msg.decode('utf-8'))
      msg = input('>>>').encode('utf-8')
      sk.sendto(msg,client_addr)
  sk.close()
  ```

- client端

  ```python
  import socket
  
  sk = socket.socket(type=socket.SOCK_DGRAM)
  while True:
      inp = input('>>>').encode('utf-8')
      sk.sendto(inp,('127.0.0.1',9000))
      ret = sk.recv(1024)
      print(ret)
  sk.close()
  ```

### 8.7struct模块

- ret = struct.pack('i', 1000)
  - 把int转换为bytes
- struct.unpack('i', ret)
  - 把ret转换为int

### 8.8黏包现象

- 定义 : 指发送方发送的若干包数据到接收方接收时粘成一包，从接收缓冲区看，后一包数据的头紧接着前一包数据的尾。

- 发生在发送端

  - 两个数据发送间隔短+数据长度小,tcp优化机制将两条信息作为一条发送出去,为了减少tcp协议中的'确认收到'的网络延迟时间

- 发生在接收端

  - tcp协议中所传输的数据无边界,来不及接收的多条数据会在接收方的内核的缓存端粘在一起

- 本质:接收信息的边界不清晰

- 解决该现象 : 需要让发送端在发送数据前，把自己将要发送的字节流总大小让接收端知晓,用struct模块.

  - 自定义协议1
    - 首先发送报头,长度4字节,内容是即将发送的报文的字节长度
    - struct模块pack能把所有的数字转换为4字节
    - 然后发送报文

  - 自定义协议2
    - 专门用来做文件发送的协议
    - 先发送报头字典的字节长度,再发送字典(包含文件名和大小),再发送文件内容

- server端

```python
import time
import struct
import socket

sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

conn, _ = sk.accept()   # 等待连接
byte_len = conn.recv(4)     # 接收四个字节的文件长度
size = struct.unpack('i', byte_len)[0]  # 解包4个字节,第0项是文件大小
msg1 = conn.recv(size)  # 接收文件大小的字节
print(msg1)

time.sleep(0.1)
byte_len = conn.recv(4)     # 再次接收文件长度
size = struct.unpack('i', byte_len)[0]
msg2 = conn.recv(size)
print(msg2)
conn.close()
sk.close()
# 发送4个字节的文件的信息
# json --》文件的大小+文件名。。。。
# 发送文件
```

- client端

```python
import struct
import socket

sk = socket.socket()
sk.connect(('127.0.0.1',9000))

msg = b'hello'
byte_len = struct.pack('i',len(msg))    # 打包msg长度
sk.send(byte_len)   #  发送长度
sk.send(msg)        #  发送msg
msg = b'world'
byte_len = struct.pack('i',len(msg))
sk.send(byte_len)
sk.send(msg)

sk.close()
```

### 8.9tcp协议实现聊天基础需求

- 1.server和client端连接之后，能知道对面这个人是哪一个好友  qq号
- 2.不同好友的聊天颜色不同

server端

```python
import json
import socket


def chat(conn):     # 聊天模块
    while True:
        msg = conn.recv(1024).decode('utf-8')       # 接收消息,msg字典的json形式
        dic_msg = json.loads(msg)
        uid = dic_msg['id']                         # 获取msg的id账号
        if dic_msg['msg'].upper() == 'Q':           # 如果对方输入的msg的msg项为Q则退出
            print('%s已经下线' % color_dic[uid]['name'])
            break
        print('%s%s : %s\033[0m' % (color_dic[uid]['color'], color_dic[uid]['name'], dic_msg['msg']))   # color字典的id项的color项
        inp = input('>>>')
        conn.send(inp.encode('utf-8'))  # 发送内容
        if inp.upper() == 'Q':          # Q退出
            print('您已经断开和%s的聊天' % color_dic[uid]['name'])
            break


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()
color_dic = {
    '12345': {'color': '\033[32m', 'name': 'alex'},
    '12346': {'color': '\033[31m', 'name': 'wusir'},
    '12347': {'color': '\033[33m', 'name': 'yuan'},
}       # 各个账号颜色
while True:
    conn, _ = sk.accept()       # 等待接入  conn是连接的数据
    chat(conn)                  # 调用chat聊天模块
    conn.close()                # 关闭连接
sk.close()
```

client端

```python
import socket
import json

sk = socket.socket()

id = '12346'                                # 登录的id账号
sk.connect(('127.0.0.1', 9000))             # 连接到服务器
while True:
    inp = input('>>>')
    dic = {'msg': inp, 'id': id}            # 把内容和id账号放入字典
    str_dic = json.dumps(dic)               # 把字典变成str格式
    sk.send(str_dic.encode('utf-8'))        # 发送
    if inp.upper() == 'Q':
        print('您已经断开和server的聊天')
        break
    msg = sk.recv(1024).decode('utf-8')     # 接收服务器的消息
    if msg.upper() == 'Q':
        break
    print(msg)
sk.close()
```

### 8.10tcp协议实现用户登陆

- 加入hashlib密文存储密码

server端

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
import socket
import hashlib


def get_md5(username, password):
    md5 = hashlib.md5(username.encode('utf-8'))     # 加的盐使用 用户名
    md5.update(password.encode('utf-8'))            # 把密码转为md5
    return md5.hexdigest()


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

conn, _ = sk.accept()
str_dic = conn.recv(1024).decode('utf-8')
dic = json.loads(str_dic)                           # 接收用户字典信息
with open('userinfo', encoding='utf-8') as f:       # 打开用户信息文件
    for line in f:
        user, passwd = line.strip().split('|')
        if user == dic['usr'] and passwd == get_md5(dic['usr'], dic['pwd']):    # 判断账号密码相同
            res_dic = {'opt': 'login', 'result': True}
            break
    else:
        res_dic = {'opt': 'login', 'result': False}
sdic = json.dumps(res_dic)          # 把登录结果转为str
conn.send(sdic.encode('utf-8'))     # 发送
conn.close()
sk.close()
```

client端

```python
import json
import socket

usr = input('username : ')
pwd = input('password : ')
dic = {'usr': usr, 'pwd': pwd}
str_dic = json.dumps(dic)       # 把字典转为str
sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

sk.send(str_dic.encode('utf-8'))        # 发送账号密码
ret = sk.recv(1024).decode('utf-8')     # 接收客户端的返回内容
ret_dic = json.loads(ret)               # str转换为字典
if ret_dic['result']:   # 字典的result项为True
    print('登陆成功')
sk.close()
```

### 8.11tcp协议完成文件上传

- 基础的需求 ：两台机器能从一台发送给另一台就可以了
- 进阶的需求 ：考虑大文件（选做）

server端

```python
import json
import socket

sk = socket.socket()
sk.bind(('192.168.12.87', 9000))
sk.listen()

conn, addr = sk.accept()
print(addr)
str_dic = conn.recv(1024).decode('utf-8')       # 接收str字典
dic = json.loads(str_dic)                       # 转换为字典
content = conn.recv(dic['filesize'])            # 接收字典中的文件大小项
with open(dic['filename'], mode='wb') as f:     # 创建字典中文件名的项,模式为写字节
    f.write(content)
conn.close()
sk.close()
```

client端

```python
import os
import json
import socket

sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

file_path = input('>>>')                    # 输入文件路径
if os.path.isabs(file_path):                # 判断文件路径是否为绝对路径
    filename = os.path.basename(file_path)      # 返回文件名
    filesize = os.path.getsize(file_path)       # 返回文件大小
    dic = {'filename': filename, 'filesize': filesize}      # 存入字典
    str_dic = json.dumps(dic)                   # 转为str格式
    sk.send(str_dic.encode('utf-8'))
    with open(file_path, mode='rb') as f:       # 打开文件,模式为读取字节
        content = f.read()
        sk.send(content)
sk.close()
```

### 8.12非阻塞io模型

- server端

```python
import socket

sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.setblocking(False)       # 设置非阻塞
sk.listen()

conn_l = []  # 已连接的列表
del_l = []  # 要断开的连接的列表
while True:
    try:
        conn, addr = sk.accept()  # 阻塞，直到有一个客户端来连我 , 由于非阻塞没有连接直接进入except
        print(conn)
        conn_l.append(conn)     # 把连接信息添加到列表
    except BlockingIOError:
        for c in conn_l:    # 循环已连接列表
            try:
                msg = c.recv(1024).decode('utf-8')      # 接收client发来的消息,没收到则进入except
                if not msg:     # 如果为None,表示client不再传信息
                    del_l.append(c)     # 把连接添加到要删除的列表
                    continue        # 继续接收
                print('-->', [msg])     # 如果收到则打印收到内容
                c.send(msg.upper().encode('utf-8'))     # 将收到内容变大写发送给client.
            except BlockingIOError:
                pass
        for c in del_l:     # 循环要删除的列表
            conn_l.remove(c)    # 从已连接的列表中删除
        del_l.clear()       # 清空要删除的列表
sk.close()
```

- client端

```python
import time
import socket

sk = socket.socket()
sk.connect(('127.0.0.1', 9000))
for i in range(30):
    sk.send(b'123')     # 发送30次
    msg = sk.recv(1024)     # 接收server信息
    print(msg)
    time.sleep(0.2)
sk.close()
```

### 8.13.验证客户端的合法性1

- server端

```python
import os
import hashlib
import socket


def get_md5(secret_key, randseq):
    md5 = hashlib.md5(secret_key)
    md5.update(randseq)     # 生成的随机序列加密md5
    res = md5.hexdigest()
    return res


def chat(conn):
    while True:
        msg = conn.recv(1024).decode('utf-8')
        print(msg)
        conn.send(msg.upper().encode('utf-8'))


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

secret_key = b'alexsb'      # 密钥 , 盐
while True:
    conn, addr = sk.accept()
    randseq = os.urandom(32)        # 生成随机32位字节
    conn.send(randseq)              # 发送给client
    md5code = get_md5(secret_key, randseq)      # 把密钥和随机序列传入md5加密函数
    ret = conn.recv(32).decode('utf-8')     # 接收client返回的md5
    print(ret)
    if ret == md5code:      # 判断与server端是否一致
        print('是合法的客户端')
        chat(conn)          # 一致则执行聊天函数
    else:
        print('不是合法的客户端')
        conn.close()        # 断开连接
sk.close()
```

- client端

```python
import hashlib
import socket
import time


def get_md5(secret_key, randseq):
    md5 = hashlib.md5(secret_key)
    md5.update(randseq)
    res = md5.hexdigest()
    return res


def chat(sk):
    while True:
        sk.send(b'hello')
        msg = sk.recv(1024).decode('utf-8')
        print(msg)
        time.sleep(0.5)


sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

secret_key = b'alexsb'
randseq = sk.recv(32)       # 接收server发来的随机序列
md5code = get_md5(secret_key, randseq)      # 加密为md5

sk.send(md5code.encode('utf-8'))    # 发送给server,一致则执行chat函数,不一致结束进程.
chat(sk)

sk.close()
```

### 8.14.验证客户端的合法性2

- server端

```python
import os
import hmac
import socket


def get_md5(secret_key, randseq):
    h = hmac.new(secret_key, randseq)   # hmac.new(盐,随机序列)
    res = h.digest()        # 返回加密的数据
    return res


def chat(conn):
    while True:
        msg = conn.recv(1024).decode('utf-8')
        print(msg)
        conn.send(msg.upper().encode('utf-8'))


sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()

secret_key = b'alexsb'      # 密钥, 盐
while True:
    conn, addr = sk.accept()
    randseq = os.urandom(32)    # 生成随机32位字节
    conn.send(randseq)      # 随机序列发给client
    hmaccode = get_md5(secret_key, randseq)     # 把随机序列加密计算
    ret = conn.recv(16)     # 接收client发来的数据
    if ret == hmaccode:     # 判断是否一致
        print('是合法的客户端')
        chat(conn)
    else:
        print('不是合法的客户端')
        conn.close()
sk.close()
```

- client端

```python
import socket
import time
import hmac


def get_md5(secret_key, randseq):
    h = hmac.new(secret_key, randseq)
    res = h.digest()
    return res


def chat(sk):
    while True:
        sk.send(b'hello')
        msg = sk.recv(1024).decode('utf-8')
        print(msg)
        time.sleep(0.5)


sk = socket.socket()
sk.connect(('127.0.0.1', 9000))

secret_key = b'alexsb'
randseq = sk.recv(32)       # 接收32位随机序列
hmaccode = get_md5(secret_key, randseq)     # 加密

sk.send(hmaccode)       # 发送给server
chat(sk)        # 一致则调用聊天模块

sk.close()
```

### 8.15.socketserver模块

- server端

```python
import socketserver


class Myserver(socketserver.BaseRequestHandler):
    def handle(self):  # 自动触发了handle方法，并且self.request == conn
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('1'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('2'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('3'.encode('utf-8'))


server = socketserver.ThreadingTCPServer(('127.0.0.1', 9000), Myserver)
server.serve_forever()
```

- client端

```python
import socket
import time

sk = socket.socket()
sk.connect(('127.0.0.1', 9000))
for i in range(3):
    sk.send(b'hello,yuan')
    msg = sk.recv(1024)
    print(msg)
    time.sleep(3)

sk.close()
```



## 第九章 并发编程

### 9.1操作系统发展史

- 人机矛盾 : 手工操作,CPU利用率低
- 磁带存储 + 批处理
  - 降低数据的读取时间
  - 提高CPU的利用率
- 多道操作系统 -- 在一个任务遇到IO的时候主动让出CPU
  - 数据隔离
  - 时空复用
  - 能够在一个任务遇到io操作的时候主动把cpu让出来,给其他的任务使用
    - 切换要不要占用时间:要占用
    - 切换由操作系统完成.
- 分时操作系统 -- 给时间分片,让多个任务轮流使用cpu
  - 短作业优先算法
  - 先来先服务算法
  - 时间分片
    - cpu的轮转
    - 每一个程序分配一个时间片
  - 要切换,要占用时间
  - 反而降低了cpu的利用率
  - 提高了用户体验
- 分时操作系统 + 多道操作系统 + 实时操作系统
  - 多个程序一起在计算机中执行
  - 一个程序如果遇到IO操作,切出去让出CPU
  - 一个程序没有遇到IO,但是时间片到时了,切出去让出CPU
- 网络操作系统
- 分布式操作系统
- 操作系统负责什么
  - 调度进程先后执行的顺序,控制执行的时间等
  - 资源的分配

### 9.2进程

#### 9.2.1进程概念

运行中的程序,是计算机中最小的资源分配单位

数据隔离

- 创建进程  时间开销大
- 销毁进程  时间开销大
- 进程之间切换  时间开销大
- 两个程序,分别要做两件事 : 起两个进程
- 一个程序,分别做多件事 : 启动多个进程完成,开销大

#### 9.2.2程序和进程的区别

- 程序是一个文件
- 进程是这个文件被CPU运行起来了
- 进程的调度: 由操作系统完成
- 三状态:
  - 就绪 (操作系统调用-><-时间片到) 运行 (io操作) 阻塞 (IO结束->) 就绪
  - 阻塞影响了程序运行的效率

#### 9.2.3在操作系统中的唯一标识符:PID

#### 9.2.4操作系统调度进程的算法

- 短作业优先
- 先来先服务
- 时间片轮转
- 多级反馈算法

#### 9.2.5并行与并发

- 并行
  - 两个程序两个CPU,每个程序分别占用一个CPU自己执行自己的
  - 看起来同时执行,实际也是在每一个时间点上都在各自执行着.
- 并发
  - 两个程序一个CPU,每个程序交替的在一个CPU上执行
  - 看起来同时执行,实际仍然是串行.

#### 9.2.6同步和异步

- 同步  烧好水后吹头发
- 异步  同时进行

#### 9.2.7阻塞和非阻塞

- 阻塞  cpu不工作
- 非阻塞  cpu工作

#### 9.2.8同步阻塞和同步非阻塞

- 同步阻塞
  - conn.recv
  - socket阻塞的tcp协议的时候
- 同步非阻塞
  - func()  没有io操作
  - socket   非阻塞的tcp协议的时候
  - 调用函数(这个函数内部不存在io操作)

#### 9.2.9异步阻塞和异步非阻塞

- 异步阻塞
- 异步非阻塞
  - 把func扔到其他任务里去执行
  - 我本身的任务和func任务各自执行各自的   没有io操作

#### 9.2.10进程的三状态

- 就绪 : 当进程已分配到除CPU以外的所有必要的资源,只要获得处理机便可立即执行.
- 执行 : 当进程已获得处理机,其程序正在处理机上执行,此时的状态称为执行状态.
- 阻塞 : 正在执行的进程,由于等待某个事件发生而无法执行时,便放弃处理机而处于阻塞状态.

- 引起阻塞的事件可有多种,例如 : 等待io完成,申请缓冲区不能满足,等待信件等.

#### 9.2.11守护进程

```python
p = Process(target=函数,args=(参数,))
p.daemon = True
p.start()      # 把p子进程设置成了一个守护进程
```

- 守护进程是随着主进程的代码结束而结束的
  - 生产者消费者模型的时候
  - 和守护线程做对比的时候
- 所有的子进程都必须在主进程结束之前结束，由主进程来负责回收资源

#### 9.2.12Process的其他方法

```python
p.terminate()   # 结束进程,异步非阻塞
p.is_alive()	# 看进程是否还活着
p.join()  	# 同步阻塞
```



### 9.3线程

#### 9.3.1概念

- 是进程中的一部分，每一个进程中至少有一个线程.
- 进程是计算机中最小的资源分配单位（进程是负责圈资源）
- 线程是计算机中能被CPU调度的最小单位 （线程是负责执行具体代码的）
- 开销
  - 线程的创建，也需要一些开销（一个存储局部变量的结构，记录状态）
  - 创建、销毁、切换开销远远小于进程

python中的线程比较特殊,所以进程也有可能被用到

- 进程 ：数据隔离 开销大 同时执行几段代码
- 线程 ：数据共享 开销小 同时执行几段代码

#### 9.3.2进程模块multiprocessing

- multi	multiple多元的
- processing	进程

from multiprocessing import Process    调用

pid  process id进程占用端口	ppid   parent process  id父进程占用端口

```python
import os
print('start')
print(os.getpid(),os.getppid(),'end')	# 第一个是脚本pid,第二个是pythonpid
```

```python
import os
import time
from multiprocessing import Process

def func():
    print('start',os.getpid())
    time.sleep(1)
    print('end',os.getpid())

if __name__ == '__main__':
    p = Process(target=func)
    p.start()   # 异步 调用开启进程的方法 但是并不等待这个进程真的开启
    print('main :',os.getpid())
```



- 操作系统创建进程的方式不同

- Windows操作系统执行开启进程的代码

  - 实际上新的子进程需要通过import父进程的代码来完成数据的导入工作
  - 所以有一些内容我们只希望在父进程中完成，就写在if __ name__ == '__ main__':下面.(不写则报错)

- ios linux操作系统创建进程 fork(不写不报错)

- 主进程和子进程之间的关系

  - 主进程没结束 ：等待子进程结束
  - 主进程负责回收子进程的资源
  - 如果子进程执行结束，父进程没有回收资源，那么这个子进程会变成一个僵尸进程

- 主进程的结束逻辑

  - 主进程的代码结束
  - 所有的子进程结束
  - 给子进程回收资源
  - 主进程结束

- 主进程怎么知道子进程结束了的呢？

  - 基于网络、文件

- join方法 : 阻塞，直到子进程结束就结束

  ```python
  import time
  from multiprocessing import Process
  def send_mail():
      time.sleep(3)
      print('发送了一封邮件')
  if __name__ == '__main__':
      p = Process(target=send_mail)
      p.start()   # 异步 非阻塞
      # time.sleep(5)
      print('join start')
      p.join()    # 同步 阻塞 直到p对应的进程结束之后才结束阻塞
      print('5000封邮件已发送完毕')
  ```

- 开启10个进程，给公司的5000个人发邮件，发送完邮件之后，打印一个消息“5000封邮件已发送完毕”

  ```python
  import time
  import random
  from multiprocessing import Process
  def send_mail(a):
      time.sleep(random.random())
      print('发送了一封邮件',a)
  
  if __name__ == '__main__':
      l = []
      for i in range(10):
          p = Process(target=send_mail,args=(i,))
          p.start()
          l.append(p)
      print(l)
      for p in l:p.join()
      # 阻塞 直到上面的十个进程都结束
      print('5000封邮件已发送完毕')
  ```



### 9.4锁

- 都能维护线程间的数据安全

- 1.如果在一个并发的场景下，涉及到某部分内容
  - 是需要修改一些所有进程共享数据资源,需要加锁来维护数据的安全
- 2.在数据安全的基础上，才考虑效率问题
- 3.同步存在的意义 : 数据的安全性
- 在主进程中实例化 lock = Lock() , 把这把锁传递给子进程 , 在子进程中 对需要加锁的代码 进行 with lock：
  - with lock相当于lock.acquire()和lock.release()
- 在进程中需要加锁的场景
  - 共享的数据资源（文件、数据库）
  - 对资源进行修改、删除操作
- 加锁之后能够保证数据的安全性 但是也降低了程序的执行效率

#### 9.4.1死锁现象

- 死锁现象是怎么发生的
  - 1.有多把锁,一把以上
  - 2.多把锁交替使用
- 怎么解决
  - 递归锁----将多把互斥锁变成一把递归锁
    - 快速解决问题
    - 效率差
    - 递归锁也会发生死锁现象(多把锁交替使用时)
  - 优化代码逻辑
    - 可以使用互斥锁,解决问题
    - 效率相对好
    - 解决问题的效率相对低
- 数据不安全的现象: += -= *= /=和多个线程对同一个文件进行写操作.

#### 9.4.2互斥锁Lock

不能再一个线程中连续acquire,开销小

#### 9.4.3递归锁RLock

- 优点:在同一个线程中,可以连续acquire多次不会被锁住.
- 缺点:占用了更多资源

#### 9.4.4队列

- 线程之间的通信 线程安全
- Queue  先进先出队列
- LifoQueue    后进先出队列
- 优先级队列  PriorityQueue
  - 自动的排序
  - 抢票用户级别
  - 告警级别



### 9.5进程之间的通信

#### 9.5.1进程之间的数据隔离

```python
from multiprocessing import Process
n = 100
def func():
    global n
    n -= 1

if __name__ == '__main__':
    p_l = []
    for i in range(10):
        p = Process(target=func)    # 创建多个进程,每个进程在自己的作用域n-=1
        p.start()
        p_l.append(p)
    for p in p_l:p.join()
    print(n)    # 输出的n是全局变量的n
```

#### 9.5.2通信

进程之间的通信 - IPC(inter process communication)

- 先进先出

  ```python
  from multiprocessing import Queue,Process
  def func(exp,q):
      ret = eval(exp)
      q.put({ret,2,3})	# 一个一个放入队列
      q.put(ret*2)
      q.put(ret*4)
  
  if __name__ == '__main__':
      q = Queue()
      Process(target=func,args=('1+2+3',q)).start()
      print(q.get())	# 一个个取出
      print(q.get())
      print(q.get())	# 没有则阻塞
  ```

Queue基于  数据安全

pipe 管道(不安全的) = 文件家族的socket pickle

队列 = 管道 + 锁

```python
from multiprocessing import Pipe
pip = Pipe()
pip.send()
pip.recv()
```

```python
import queue

from multiprocessing import Queue
q = Queue(5)    # 设置队列长度
q.put(1)     # 放入数据
q.put(2)
q.put(3)
q.put(4)
q.put(5)   # 当队列为满的时候用put向队列中放数据 队列会阻塞
print('5555555')
try:
    q.put_nowait(6)  # 当队列为满的时候用put_nowait向队列中放数据 会报错并且会丢失数据
except queue.Full:
    pass
print('6666666')

print(q.get())
print(q.get())
print(q.get())   # 在队列为空的时候会发生阻塞
print(q.get())   # 在队列为空的时候会发生阻塞
print(q.get())   # 在队列为空的时候会发生阻塞
try:
    print(q.get_nowait())   # 在队列为空的时候 直接报错
except queue.Empty:
    pass
```



### 9.6生产者消费者模型

#### 9.6.1什么是生产者消费者模式

生产者消费者模式是通过一个容器来解决生产者和消费者的强耦合问题。生产者和消费者彼此之间不直接通讯，而通过阻塞队列来进行通讯，所以生产者生产完数据之后不用等待消费者处理，直接扔给阻塞队列，消费者不找生产者要数据，而是直接从阻塞队列里取，阻塞队列就相当于一个缓冲区，平衡了生产者和消费者的处理能力。

#### 9.6.2为什么要使用生产者和消费者模式

在线程世界里，生产者就是生产数据的线程，消费者就是消费数据的线程。在多线程开发当中，如果生产者处理速度很快，而消费者处理速度很慢，那么生产者就必须等待消费者处理完，才能继续生产数据。同样的道理，如果消费者的处理能力大于生产者，那么消费者就必须等待生产者。为了解决这个问题于是引入了生产者和消费者模式。

#### 9.6.3解耦 (修改  复用  可读性)

- 把写在一起的大的功能分开成多个小的功能处理
- 登陆  注册

#### 9.6.4队列

生产者和消费者之间的容器就是队列

```python
import time
import random
from multiprocessing import Process,Queue

def producer(q,name,food):	# 生产者
    for i in range(10):
        time.sleep(random.random())
        fd = '%s%s'%(food,i)
        q.put(fd)
        print('%s生产了一个%s'%(name,food))

def consumer(q,name):	# 消费者
    while True:
        food = q.get()
        if not food:break
        time.sleep(random.randint(1,3))
        print('%s吃了%s'%(name,food))


def cp(c_count,p_count):	# 设置消费者和生产者个数
    q = Queue(10)	# 队列(容器)长度
    for i in range(c_count):
        Process(target=consumer, args=(q, 'alex')).start()
    p_l = []
    for i in range(p_count):
        p1 = Process(target=producer, args=(q, 'wusir', '泔水'))
        p1.start()
        p_l.append(p1)
    for p in p_l:p.join()	# 等待所有子进程结束
    for i in range(c_count):	# 给每个消费者发送None,消费者接受到None时停止接受并退出.
        q.put(None)
if __name__ == '__main__':
    cp(2,3)
```

- 注意：结束信号None，不一定要由生产者发，主进程里同样可以发，但主进程需要等生产者结束后才应该发送该信号 , 但上述解决方式，在有多个生产者和多个消费者时，我们则需要用一个很low的方式去解决

#### 9.6.5joinablequeue

- 创建可连接的共享进程队列。这就像是一个Queue对象，但队列允许项目的使用者通知生产者项目已经被成功处理。通知进程是使用共享的信号和条件变量来实现的。 
- 除了与Queue对象相同的方法之外，还具有以下方法：
  - q.task_done() 
    使用者使用此方法发出信号，表示q.get()返回的项目已经被处理。如果调用此方法的次数大于从队列中删除的项目数量，将引发ValueError异常。
  - q.join() 
    生产者将使用此方法进行阻塞，直到队列中所有项目均被处理。阻塞将持续到为队列中的每个项目均调用q.task_done()方法为止。 
    下面的例子说明如何建立永远运行的进程，使用和处理队列上的项目。生产者将项目放入队列，并等待它们被处理。

```python
import time
import random
from multiprocessing import JoinableQueue,Process

def producer(q,name,food):  # 设置生产者
    for i in range(10):
        time.sleep(random.random())
        fd = '%s%s'%(food,i)
        q.put(fd)
        print('%s生产了一个%s'%(name,food))
    q.join()

def consumer(q,name):   # 设置消费者
    while True:
        food = q.get()
        time.sleep(random.random())
        print('%s吃了%s'%(name,food))
        q.task_done()

if __name__ == '__main__':
    jq = JoinableQueue()
    p = Process(target=producer, args=(jq, 'wusir', '泔水'))
    p.start()
    c = Process(target=consumer, args=(jq, 'alex'))
    c.daemon = True
    c.start()
    p.join()
```

### 9.7进程之间的数据共享

- mulprocessing中有一个manager类, 封装了所有和进程相关的 数据共享 数据传递相关的数据类型 , 但是对于 字典 列表这一类的数据操作的时候会产生数据不安全 , 需要加锁解决问题，并且需要尽量少的使用这种方式.

```python
from multiprocessing import Manager,Process,Lock

def func(dic,lock):
    with lock:
        dic['count'] -= 1

if __name__ == '__main__':
    # m = Manager()
    with Manager() as m:
        l = Lock()
        dic = m.dict({'count':100})
        p_l = []
        for i in range(100):
            p = Process(target=func,args=(dic,l))
            p.start()
            p_l.append(p)
        for p in p_l:p.join()
        print(dic)
```

### 9.8线程的理论

#### 9.8.1.理论

- 线程  开销小  数据共享  是进程的一部分
- 进程  开销大  数据隔离  是一个资源分配单位
- cpython解释器   不能实现多线程利用多核

#### 9.8.2.锁:GIL  全局解释器锁

- 保证了整个python程序中,只能有一个线程被CPU执行
- 原因 : cpython解释器中特殊的垃圾回收机制
- GIL锁导致了线程不能并行,可以并发.
- 所以使用锁线程并不影响高io型的操作,只会对高计算型的程序由效率上的影响,遇到高计算 : 多进程 + 多线程 ---分布式

- cpython  pypy有
- jpython  iron  python无

### 9.9thread类

#### 9.9.1threading模块

- multiprocessing 是完全仿照threading的类写的 , 二者在使用层面，有很大的相似性，因而不再详细介绍.

```python
import os
import time
from threading import Thread
def func():
	print('start son thread')
	time.sleep(1)
	print('end son thread',os.getpid())
# 启动线程 start
Thread(target=func).start()
print('start',os.getpid())
time.sleep(0.5)
print('end',os.getpid())
```

#### 9.9.2开启多个子线程

```python
def func(i):
    print('start son thread',i)
    time.sleep(1)
    print('end son thread',i,os.getpid())
for i in range(10):
    Thread(target=func,args=(i,)).start()
print('main')
```

- 主线程什么时候结束？等待所有子线程结束之后才结束
- 主线程如果结束了，主进程也就结束了

#### 9.9.3join方法

- 阻塞 直到子线程执行结束

```python
def func(i):
    print('start son thread',i)
    time.sleep(1)
    print('end son thread',i,os.getpid())
t_l = []
for i in range(10):
    t = Thread(target=func,args=(i,))
    t.start()
    t_l.append(t)
for t in t_l:t.join()
print('子线程执行完毕')
```

#### 9.9.4使用面向对象的方式启动线程

```python
class MyThread(Thread):
    def __init__(self,i):
        self.i = i
        super().__init__()
    def run(self):
        print('start',self.i,self.ident)
        time.sleep(1)
        print('end',self.i)

for i in range(10):
    t = MyThread(i)
    t.start()
    print(t.ident)
```

#### 9.9.5线程里的一些其他方法

- current_thread()  在哪个线程中被调用,就返回当前线程的对象
- enumerate()  返回当前活着的线程的对象列表(包括主线程)
- active_count()  返回当前活着的线程的个数(包括主线程)

```python
from threading import current_thread,enumerate,active_count
def func(i):
    t = current_thread()
    print('start son thread',i,t.ident)
    time.sleep(1)
    print('end son thread',i,os.getpid())

t = Thread(target=func,args=(1,))
t.start()
print(t.ident)
print(current_thread().ident)   # 水性杨花 在哪一个线程里，current_thread()得到的就是这个当前线程的信息
print(enumerate())
print(active_count())   # =====len(enumerate())
```

#### 9.9.6terminate 结束进程

- 在线程中不能从主线程结束一个子线程

#### 9.9.7测试进程和线程的效率差

```python
def func(a,b):
    c = a+b
import time
from multiprocessing import Process
from threading import Thread
if __name__ == '__main__':
    start = time.time()
    p_l = []
    for  i in range(500):
        p = Process(target=func,args=(i,i*2))
        p.start()
        p_l.append(p)
    for p in p_l:p.join()
    print('process :',time.time() - start)

    start = time.time()
    p_l = []
    for i in range(500):
        p = Thread(target=func, args=(i, i * 2))
        p.start()
        p_l.append(p)
    for p in p_l: p.join()
    print('thread :',time.time() - start)
```

#### 9.9.8数据隔离还是共享？

```python
from threading import Thread
n = 100
def func():
    global n    # 不要在子线程里随便修改全局变量
    n-=1
t_l = []
for i in range(100):
    t = Thread(target=func)
    t_l.append(t)
    t.start()
for t in t_l:t.join()
print(n)
```

#### 9.9.9守护线程daemon

```python
import time
from threading import Thread
def son1():
    while True:
        time.sleep(0.5)
        print('in son1')
def son2():
    for i in range(5):
        time.sleep(1)
        print('in son2')
t =Thread(target=son1)
t.daemon = True
t.start()
Thread(target=son2).start()
time.sleep(3)
```

- 守护线程一直等到所有的非守护线程都结束之后才结束 , 除了守护了主线程的代码之外也会守护子线程
- 结束顺序 ：非守护线程结束 -->主线程结束-->主进程结束-->守护线程结束

### 9.10池

- 池 : 预先的开启固定个数的进程数，当任务来临的时候，直接提交给已经开好的进程,让这个进程去执行就可以了,节省了进程(线程的开启 关闭 切换都需要时间),并且减轻了操作系统调度的负担.

#### 9.10.1进程池(from concurrent.futures import ProcessPoolExecutor)

- .submit(函数,参数)   提交
- .shutdown()    关闭池  阻塞直到所有的任务执行完毕

```python
import os
import time
from concurrent.futures import ProcessPoolExecutor

def func():
    print('start', os.getpid())
    time.sleep(1)
    print('end', os.getpid())

if __name__ == '__main__':
    pp = ProcessPoolExecutor(5)
    for i in range(20):
        pp.submit(func)     # 提交
    pp.shutdown()   # 关闭池之后就不能继续提交任务，并且会阻塞，直到已经提交的任务完成
    print('done')

```

- 任务的参数  +  返回值
  - ret.result()获取返回值

```python
import os
import time
from concurrent.futures import ProcessPoolExecutor

def func(i):
    print('start', os.getpid())
    time.sleep(1)
    print('end', os.getpid())
    return i**2

if __name__ == '__main__':
    p = ProcessPoolExecutor(5)      # 创建5个进程池
    lst = []
    for i in range(15):
        s = p.submit(func, i)
        lst.append(s)
    for i in lst:
        print(i.result())           # .result输出函数返回值
    print('done', os.getpid())
```

- 进程池的开销大,一个池中的任务个数限制了我们程序的并发个数

#### 9.10.2线程池(from concurrent.futures import ThreadPoolExecutor)

```python
import os
import time
from concurrent.futures import ThreadPoolExecutor

def func(i):
    print('start', os.getpid())
    time.sleep(1)
    print('end', os.getpid())
    return i**2

t = ThreadPoolExecutor(10)
lst = []
for i in range(10):
    res = t.submit(func, i)
    lst.append(res)
t.shutdown()
for i in lst:
    print(i.result())
print('Done')
```

或者使用map函数

- tp.map(func,iterable) 迭代获取iterable中的内容，作为func的参数，让子线程来执行对应的任务

```python
from concurrent.futures import ThreadPoolExecutor
def func(i):
    print('start', os.getpid())
    time.sleep(random.randint(1,3))
    print('end', os.getpid())
    return '%s * %s'%(i,os.getpid())
tp = ThreadPoolExecutor(20)
ret = tp.map(func,range(20))
for i in ret:
    print(i)
ret_l = []
for i in range(10):
    ret = tp.submit(func,i)
    ret_l.append(ret)
tp.shutdown()
print('main')
```

#### 9.10.3回调函数

- ret.add_done_callback(函数)    给当前任务绑定回调函数 , 先执行完的执行()里的函数.并且ret的结果会作为参数返回给绑定的函数

```python
import requests
from concurrent.futures import ThreadPoolExecutor
def get_page(url):
    res = requests.get(url)
    return {'url':url,'content':res.text}
def parserpage(ret):
    dic = ret.result()
    print(dic['url'])
tp = ThreadPoolExecutor(5)
url_lst = [
    'http://www.baidu.com',   # 3
    'http://www.cnblogs.com', # 1
    'http://www.douban.com',  # 1
    'http://www.tencent.com',
    'http://www.cnblogs.com/Eva-J/articles/8306047.html',
    'http://www.cnblogs.com/Eva-J/articles/7206498.html',
]
ret_l = []
for url in url_lst:
    ret = tp.submit(get_page,url)
    ret_l.append(ret)
    ret.add_done_callback(parserpage)	# 回调函数
```



#### 9.10.4开线程/进程还是池?

- 如果只是开启一个子线程做一件事,就可以单独开线程
- 有大量的任务等待程序去做,要达到一定的并发数,开启进程池
- 根据程序的io操作判断用不用池:
  - 以下情况不用: socket的server端,大量阻塞io,recv,recvfrom,socketserver
  - 以下用:爬虫

#### 9.10.5回调函数

- 执行完子线程任务之后直接调用对应的回调函数
- 爬取网页 需要等待数据传输和网络上的响应高IO的 -- 子线程
- 分析网页 没有什么IO操作 -- 这个操作没必要在子线程完成，交给回调函数

#### 9.10.6ThreadPoolExecutor常用方法

- tp = ThreadPoolExecutor(cpu*5)
- obj = submit (需要在子线程执行的函数名,参数)
- obj
  - 1.获取返回值 obj.result() 是一个阻塞方法
  - 2.绑定回调函数 obj.add_done_callback(子线程执行完毕之后要执行的代码对应的函数)
- ret = map(需要在子线程执行的函数名,iterable)
  - 1.迭代ret,总是能得到所有的返回值
- shutdown
  - tp.shutdown()  阻塞整个池

### 9.11多线程起多进程

```python
import os
from multiprocessing import Process
from threading import Thread

def tfunc():
    print('线程', os.getpid())
def pfunc():
    print('进程-->',os.getpid())
    for i in range(10):
        Thread(target=tfunc).start()    # 进程函数中开线程

if __name__ == '__main__':
    Process(target=pfunc).start()       # 开进程
```



### 9.12协程

- 协程 : 用户级别的,由我们自己写的python代码来控制切换的 , 是操作系统不可见的
- 在一个线程下的多个任务之间来回切换,那么每一个任务都是一个协程.
- 在Cpython解释器下 (协程和线程都不能利用多核,都是在一个CPU上轮流执行.)
  - 由于多线程本身就不能利用多核,所以即便是开启了多个线程也只能轮流在一个CPU上执行.
  - 协程如果把所有任务的IO操作都规避掉,只剩下需要使用CPU的操作,就意味着协程就可以做到提高CPU利用率的效果.
- 线程和协程
  - 线程  切换需要操作系统,开销大,操作系统不可控,给操作系统的压力大
    - 操作系统对IO操作的感知更加灵敏
  - 协程  切换需要python代码,开销小,用户操作可控,完全不会增加操作系统的压力
    - 用户级别能够对IO操作的感知比较低

### 9.13切换问题和greenlet模块

- 两种切换方式:
  - 原生python完成  yield  asyncio
  - C语言完成的python模块

```python
import time
from  greenlet import greenlet

def eat():
    print('wusir is eating')
    time.sleep(0.5)
    g2.switch()         # 执行g2
    print('wusir finished eat')

def sleep():
    print('小马哥 is sleeping')
    time.sleep(0.5)
    print('小马哥 finished sleep')
    g1.switch()         # 继续g1刚才的位置执行

g1 = greenlet(eat)
g2 = greenlet(sleep)
g1.switch()     # 打开开关
```



### 9.14gevent模块

- .spawn(函数) 创建协程任务
- from gevent import monkey
  - monkey.patch_all()	让gevent识别所有io操作(默认不识别)
- join()   阻塞 直到任务完成

```python
import time
import gevent
from gevent import monkey
monkey.patch_all()	# 规避所有io操作
def eat():
    print('wusir is eating')
    time.sleep(1)       # 遇到io自动切换
    print('wusir finished eat')

def sleep():
    print('小马哥 is sleeping')
    time.sleep(1)       # 遇到io自动切换
    print('小马哥 finished sleep')

g1 = gevent.spawn(eat)  # 创造一个协程任务
g2 = gevent.spawn(sleep)  # 创造一个协程任务
g1.join()   # 阻塞 直到g1任务完成为止
g2.join()   # 阻塞 直到g1任务完成为止
```

- gevent.joinall([g1,g2])	阻塞列表中的协程
- joinall([])   阻塞列表所有协程

```python
import time
import gevent
from gevent import monkey
monkey.patch_all()
def eat():
    print('wusir is eating')
    time.sleep(1)
    print('wusir finished eat')

def sleep():
    print('小马哥 is sleeping')
    time.sleep(1)
    print('小马哥 finished sleep')

# g1 = gevent.spawn(eat)  # 创造一个协程任务
# g3 = gevent.spawn(eat)  # 创造一个协程任务
# g2 = gevent.spawn(sleep)  # 创造一个协程任务
# gevent.joinall([g1,g2,g3])
g_l = []
for i in range(10):		# 起10个协程
    g = gevent.spawn(eat)
    g_l.append(g)	# 加到列表
gevent.joinall(g_l)
```

- .value    获取返回值

```python
import time
import gevent
from gevent import monkey
monkey.patch_all()
def eat():
    print('wusir is eating')
    time.sleep(1)
    print('wusir finished eat')
    return 'wusir***'

def sleep():
    print('小马哥 is sleeping')
    time.sleep(1)
    print('小马哥 finished sleep')
    return '小马哥666'

g1 = gevent.spawn(eat)
g2 = gevent.spawn(sleep)
gevent.joinall([g1,g2])
print(g1.value)		# wusir***
print(g2.value)		# 小马哥666
```

### 9.15asyncio模块

- 是一个消息循环。我们从`asyncio`模块中直接获取一个`EventLoop`的引用，然后把需要执行的协程扔到`EventLoop`中执行，就实现了异步IO。

```python
import asyncio

async def hello():
    print("Hello world!")
    # 异步调用asyncio.sleep(1):
    await asyncio.sleep(1)
    print("Hello again!")

# 获取EventLoop:
loop = asyncio.get_event_loop()
# 执行coroutine
loop.run_until_complete(hello())
loop.close()
```

- 执行多个任务

```python
import asyncio

async def hello():
    print("Hello world!")
    await asyncio.sleep(1)
    print("Hello again!")
    return 'done'

loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.wait([hello(),hello()]))
loop.close()
```

- 获取单个返回值

```python
import asyncio

async def hello():
    print("Hello world!")
    await asyncio.sleep(1)
    print("Hello again!")
    return 'done'

loop = asyncio.get_event_loop()
task = loop.create_task([hello()])
loop.run_until_complete(task)
ret = task.result()
print(ret)
```

- 获取多个任务返回值

```python
import asyncio

async def hello(i):
    print("Hello world!")
    await asyncio.sleep(i)  # 睡i秒
    print("Hello again!")
    return 'done',i

loop = asyncio.get_event_loop()
task1 = loop.create_task(hello(2))
task2 = loop.create_task(hello(1))
task3 = loop.create_task(hello(3))
task_l = [task1,task2,task3]
tasks = asyncio.wait(task_l)
loop.run_until_complete(tasks)
for t in task_l:
    print(t.result())
```

- 按返回顺序获取返回值

```python
import asyncio

async def hello(i):
    print("Hello world!")
    await asyncio.sleep(i)
    print("Hello again!")
    return 'done',i


async def main():
    tasks = []
    for i in range(10):
        tasks.append(asyncio.ensure_future(hello(10-i)))
    for res in asyncio.as_completed(tasks):
        result = await res
        print(result)
loop = asyncio.get_event_loop()
loop.run_until_complete(main())
```

- asyncio使用协程完成http访问

```python
import asyncio

async def get_url():
    reader,writer = await asyncio.open_connection('www.baidu.com',80)
    writer.write(b'GET / HTTP/1.1\r\nHOST:www.baidu.com\r\nConnection:close\r\n\r\n')
    all_lines = []
    async for line in reader:
        data = line.decode()
        all_lines.append(data)
    html = '\n'.join(all_lines)
    return html

async def main():
    tasks = []
    for url in range(20):
        tasks.append(asyncio.ensure_future(get_url()))
    for res in asyncio.as_completed(tasks):
        result = await res
        print(result)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())  # 处理一个任务
    loop.run_until_complete(asyncio.wait([main()]))  # 处理多个任务

    task = loop.create_task(main())  # 使用create_task获取返回值
    loop.run_until_complete(task)
    loop.run_until_complete(asyncio.wait([task]))
```







## 第十章 数据库



## 第十一章 前端开发



## 第十二章 Django框架



## 附录 常见错误和单词

|      |      |      |
| ---- | ---- | ---- |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |

## 错误记录:

