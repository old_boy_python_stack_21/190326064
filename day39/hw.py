#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""

# 创建学生表
create  table student(
sid int primary key,
sname char(4),
gender enum('male','female'),
class_id int,
foreign key(class_id) references class(cid)
);


# 创建老师表
create table teacher(
tid int primary key,
tname char(5)
);


# 创建课程表
create table course(
cid int primary key,
cname char(9) unique,
teacher_id int,
foreign key(teacher_id) references teacher(tid)
);


# 创建成绩表
create table score(
sid int primary key auto_increment,
student_id int,
foreign key(student_id) references student(sid),
corse_id int,
foreign key(corse_id) references course(cid),
number int
);

"""
