# day39笔记

## 1.回顾

### 1.存储引擎:

- MyISAM:适合做读,插入数据比较频繁的，对修改和删除涉及少的，不支持事务、行级锁和外键。有表级锁索引和数据分开存储的，mysql5.5以下默认的存储引擎.
- InnoDB:适合并发比较高的，对事务一致性要求高的，相对更适应频繁的修改和删除操作，有行级锁，外键且支持事务索引和数据是存在一起的，mysql5.6以上默认的存储引擎.
- MEMORY:数据存在内存中，表结构存在硬盘上，查询速度快，重启数据丢失

### 2.数据类型

- 数字类型:int,float(总位数,小数位数)
- 字符串类型:char(宽度),varchar(宽度)
- 时间类型:datetime,date,time
- enum和set:enum(单选项1,单选项2),set(多选项1,多选项2)

### 3.创建表

- create table 表名(字段名 数据类型 (宽度/选项))

### 4.库相关

- create database 库名;  创建库
- show databases;   查看库
- select database();   查看所在库
- use 库名;   切换库

### 5.函数

- database()   当前库
- user()   当前用户
- now()   时间
- concat()
- password()

## 2.约束

### 1.约束

- not  null 某一个字段不能为空
- default 给某个字段设置默认值
- unique 设置某字段不能重复
- auto_increment 设置某一个int类型的字段,自动增加(自增)
  - auto increment自带not null效果
  - 设置条件 int unique
- primary key 设置某一个字段非空且不能重复
  - 约束力相当于 not null+unique
  - 一张表只能有一个主键
- foreign key 外键
  - references
  - 级联删除和更新
- unsigned 设置一个数字无符号

### 2.not null / default

```python
create table t2(
  id int not null,			# 设置非空
  name char(12) not null,
  age int default 18,		#默认18
  gender enum('male','female') not null default 'male'		# 非空并默认male
);
```

### 3.unique 不重复

```Python
create table t3(
    id int unique,		# 不能重复
    username char(12) unique,	# 不能重复
    password char(18)
);
```

### 4.联合唯一

```Python
create table t4(
    id int,
    ip char(15),
    server char(10),
    port int,
    unique(ip,port)		# 设置ip+端口不能重复
);
```

### 5.自增

- auto_increment  自增字段  必须是数字 且必须是唯一的

```Python
create table t5(
    id int unique auto_increment,	# 设置不重复数字且自增
    username char(10),
    password char(18)
);
insert into t5(username,password) values('alex','alex3714');	# 存数据时不需要写自增字段
```

### 6.primary key 主键

- 一张表只能设置一个主键
- 一张表最好设置一个主键
- 约束这个字段 非空(not null)  且唯一(unique)
- 指定的第一个非空且唯一的字段会被定义成主键
- not null + unique = primary key

```Python
create table t6(
    id int not null unique,     # 你指定的第一个非空且唯一的字段会被定义成主键
    name char(12) not null unique
)

create table t7(
    id int primary key,     # 你指定的第一个非空且唯一的字段会被定义成主键
    name char(12) not null unique
)
```

### 7.联合主键

```Python
create table t4(
    id int,
    ip char(15),
    server char(10),
    port int,
    primary key(ip,port)	# 相当于分别给ip和port设置了not null和unique,ip+port为主键
);
```

### 8.外键 foreign key

- 涉及到两张表

- 部门表

  ```Python
  create table post(
      pid  int  primary key,		# 主键
      postname  char(10) not null unique,		# 非空不重复
      comment   varchar(255),
      phone_num  char(11)
  );
  ```

- 员工表

  ```Python
  create table staff(
  id int primary key auto_increment,	# 自增主键
  age int,
  gender  enum('male','female'),	# 性别单选
  salary  float(8,2),		# 薪资xxxxxx.xx
  hire_date date,			# 雇佣日期
  post_id int,
  foreign key(post_id) references post(pid)	# 设置外键post_id关联post表的pid项
  )
  ```

### 9.级联删除和级联更新

```Python
create table staff2(
id  int primary key auto_increment,		# 主键自增
age int,
gender  enum('male','female'),		# 性别单选
salary  float(8,2),
hire_date date,					# 入职日期:年月日
post_id int,
foreign key(post_id) references post(pid) on update cascade on delete set null		# 设置外键post_id关联post表的pid项,随之更新,删除时为空.
);
```



## 3.修改表

- 创建项目之前 和 项目开发,运行过程中
- add添加字段
  - alter table 表名 add 字段名
- add添加字段并给属性和位置
  - alter table 表名 add 字段名 数据类型(宽度) 约束 first/after name
- drop删除字段
  - alter table 表名 drop 字段名
- modify更改字段属性
  - alter table 表名 modify 字段名 类型 宽度 约束
- modify改变字段属性和位置
  - alter table 表名 modify age int not null after id;	# 把age放到id后
  - alter table 表名 modify age int not null first;     # 把age放到第一个

- change字段更名
  - alter table 表名 change 字段名 新名 类型 宽度 约束



## 4.表关系

### 1.多对一

- foreign key  永远是在多的那张表中设置外键
  - 多个学生都是同一个班级的
  - 学生表 关联 班级表
  - 学生是多 班级是一

```Python
create table author(
    aid primary key auto_increment,
    name char(12),
    birthday date,
    gender enum('male','female')   
);
create table book(
    id int primary key,
    name char(12) not null,
    price float(5,2)
    author_id int,
    foreign key(author_id) references author(aid)
);
```



### 2.一对一

- foreign key + unique 后出现的后一张表中的数据作为外键,并且要约束这个外键是唯一的
  - 客户关系表 : 手机号,招生老师,上次联系时间,备注信息
  - 学生表 : 姓名,入学日期,缴费日期,结业

```python
create table author(
    aid primary key auto_increment,
    name char(12),
    birthday date,
    gender enum('male','female')   
);
create table book(
    id int primary key,
    name char(12) not null,
    price float(5,2)
    author_id int unique,		# 比一对多加了unique
    foreign key(author_id) references author(aid)
);
```



### 3.多对多

- 产生第三张表,把两个关联关系的字段作为第三张表的外键
  - 书,作者
  - 出版社,书

```python
create table author(
    aid primary key auto_increment,
    name char(12),
    birthday date,
    gender enum('male','female')   
);
create table book(
    id int primary key,
    name char(12) not null,
    price float(5,2)
);
create table book_author(
	id int primary key auto_increment,
    book_id int not null,
    author_id not null,
    foreign key(book_id) renferences book(id),
    foreign key(authoe_id) renferences author(aid),
)
```





## 5.数据操作

### 1.增加 insert

- insert into 表名 values(值...)
  - 所有的在这个表中的字段都需要按照顺序被填写在这里
- insert into 表名(字段名...) values(值...)
  - 所有在字段位置填写了名字的字段和后面的值必须是一一对应
- insert into 表名(字段名,字段名...) values (值),(值)...
  - 所有在字段位置填写了名字的字段和后面的值必须是一一对应

- value单数 一次性写入一行数据
- values复数  一次性写入多行数据

```Python
insert into t1 value (1,'alex',83)
insert into t1 values (1,'alex',83),(2,'wusir',74)

insert into t1(name,age) value ('alex',83)
insert into t1(name,age) values ('alex',83),('wusir',74)
```

- 第一个角度
  - 写入一行内容还是写入多行
  - insert into 表名 values (值....)
  - insert into 表名 values (值....)，(值....)，(值....)
- 第二个角度
  - 是把这一行所有的内容都写入
  - insert into 表名 values (值....)
  - 指定字段写入
  - insert into 表名(字段1，字段2) values (值1，值2)

### 2.删除 delete

- delete from 表 where 条件;

### 3.修改/更新 update

- update 表 set 字段=新值 where 条件;

### 4.查询 select

- select * from 表
- select 字段,字段... from 表
- select distinct 字段,字段... from 表     # 按照查出来的字段去重
- select 字段*5 from 表     # 按照查出来的字段计算(加减乘除)
- select 字段 as 新名字,字段 as 新名字 from 表  # 按照查出来的字段操作并给一个新名字显示
- select 字段 新名字 from 表  # 把查出来的字段给新名字显示







