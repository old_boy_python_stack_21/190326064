#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.变量命名
"""
name = 'alex'  # 变量

NAME = 'eric'  # 全局变量


def func():  # 函数
    pass
"""

# 2.运算符
'''
v = 1 or 2  # 1
v = 0 or 2  # 2
v = 1 and 2  # 2
v = 0 and 2  # 0

val =  v  if v else 666
val = v or 666 # 源码
'''

# 3.数据类型
'''
int
bool
str
list
dict
tuple
set
bytes
None
'''

# 4.编码
'''
常见的编码
ascii
unicode
utf-8
gbk
gb2312
'''

# 5.深浅拷贝
'''
浅拷贝：第一层
深拷贝：所有层(可变类型)


import copy

v = [11, 22, 33, [11, 22, 3]]
v1 = copy.copy(v)  # 浅
v2 = copy.deepcopy(v)   # 深
'''

# 6.py2和py3区别
'''
字符串类型不同
默认解释器编码
输入输出
int
long
除法
range和xrang
模块和包
字典
map/filter
'''

# 7.内置函数
'''
常用：open / id / type / len / range ...
'''

# 8.自定义函数
'''
函数式编程：增加代码的可读性和重用性。
小高级:做参数,做变量,做返回值(闭包,装饰器)
生成器:
def func():
    yield 1
    yield 2
    yield 3
    
v = func()  # 生成器
# 循环v时或v.__next__() 时输出1,2,3。

def base():
    yield 88
    yield 99

def func():
    yield 1
    yield 2
    yield from base()   # 调用base输出88,99
    yield 3

result = func()

for item in result:
    print(item)
    
    
列表推导式:
v1 = [i for i in range(10)] # 立即循环创建所有元素。
print(v1)

生成器推导式:
v2 = (i for i in range(10)) # 创建了一个生成器，内部循环为执行。


def func():
    result = []
    for i in range(10):
        result.append(i)
    return result
v1 = func()
for item in v1:
   print(item)  # 0-9
   
  
def func():
    for i in range(10):
        def f():
            return i
        yield f


v1 = func()
for item in v1:
    print(item())   # 0-9


v1 = [i for i in range(10)]  # 列表推导式，立即循环创建所有元素。
v2 = (lambda: i for i in range(10))  # 生成器推导式
for item in v2:
    print(item())
'''

# 9.模块
'''
常见内置模块：json / datetime / time / os / sys
第三方模块: requests / xlrd
自定义模块: 文件 / 文件夹/包

导入:
import 模块
from 模块.模块 import 模块
from 模块.模块.模块 import 函数

注意：文件和文件夹的命名不能是导入的模块名称相同，否则就会直接在当前目录中查找。

调用模块内部元素:
函数()
模块.函数()
'''

