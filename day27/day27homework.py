#!/usr/bin/env python
# -*- coding:utf-8 -*-

import re


def cheng(suanshi):
    try:
        while 1:
            x = re.search(r'\d+(\.\d+)?[*/]-?\d+(\.\d+)?', suanshi)
            a, b, c = re.split(r'([*/])', x.group())
            a = float(a)
            c = float(c)
            if b == '*':
                res = a * c
            elif b == '/':
                res = float(a / c)
            suanshi = suanshi.replace(x.group(), str(res), 1)

    except Exception:
        return suanshi


def jia(suanshi):
    try:
        while 1:
            x = re.search(r'-?\d+(\.\d+)?[-+]\d+(\.\d+)?', suanshi)
            lst = re.split(r'([-+])', x.group())
            if len(lst) == 5:
                q, w, a, b, c = lst
                if b == '+':
                    res = - float(a) + float(c)
                elif b == '-':
                    res = -(float(a) + float(c))
            elif len(lst) == 3:
                a, b, c = lst
                if b == '+':
                    res = float(a) + float(c)
                elif b == '-':
                    res = float(a) - float(c)
            suanshi = suanshi.replace(x.group(), str(res), 1)
    except Exception:
        return suanshi


def fuhao(user):
    user = user.replace('--', '+')
    user = user.replace('+-', '-')
    return user


# user = input('请输入算式:')
user = '1 - 2 * ( (60-30 +(-40/5) * (9-2*5/3 + 7 /3*99/4*2998 +10 * 568/14 )) - (-4*3)/ (16-3*2) )'
user = user.replace(' ', '')
while 1:
    lst = re.findall('\([^()]+\)', user)
    if not lst:
        print(jia(fuhao(cheng(user))))
        break
    if not lst:
        res = jia(cheng(user))[1:-1]
        user = fuhao(user)
        continue
    for i in lst:
        user = user.replace(i, cheng(i), 1)
    user = fuhao(user)
    lst2 = re.findall('\([^()]+\)', user)
    for j in lst2:
        user = user.replace(j, jia(j), 1)
    user = fuhao(user)
    for i in lst:
        user = user.replace(i, str(i[1:-1]))
    user = fuhao(user)
