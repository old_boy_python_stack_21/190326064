#!/usr/bin/env python
# -*- coding:utf-8 -*-

# server

import socket

sk = socket.socket()
sk.bind(('192.168.12.21', 9000))
sk.listen()

conn, addr = sk.accept()
conn.send(b'hello')
msg = conn.recv(1024)
print(msg)
conn.close()
sk.close()

# client
# import socket
#
# sk = socket.socket()
#
# sk.connect(('127.0.0.1', 9000))
#
# msg = sk.recv(1024)
# print(msg)
# sk.send(b'2351')
# sk.close()