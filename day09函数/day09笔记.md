# day09笔记

## 一.三元运算(三目运算)

v = 前面 if 条件 else 后面

## 二.函数

场景:1.代码重复执行

​	2.代码量超过一屏,可用函数分割代码.

本质:将代码块放到前面,定义一个变量,后期调用变量执行代码块.

### 2.1函数的基本结构

``` python
def 函数名()	#定义
	代码
函数名() 	#调用
```

### 2.2参数

def func(形参):

​	代码块

func(实参)

### 2.3返回值

def func(形参):

​	代码块

​	return x(默认None)

print(func(实参))	#打印出的是返回值x,不设x则为None



