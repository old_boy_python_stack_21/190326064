#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.将函数部分知识点，整理到自己笔记中。（搞明白课上讲的案例。）

# 2.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新列表返回。
'''
def func(a):
    new = a[1::2]
    return new

print(func())
'''

# 3.写函数，判断用户传入的一个对象（字符串或列表或元组任意）长度是否大于5，并返回真假。
'''
def func(a):
    if len(a)>5:
        return True
    else:
        return False

print(func('sdg'))
'''

# 4.写函数，接收两个数字参数，返回比较大的那个数字。
'''
def func(a, b):
    res = a if a > b else b
    return res

print(func(82, 86))
'''

# 5.写函数，函数接收四个参数分别是：姓名，性别，年龄，学历。用户通过输入这四个内容，然后将这四个内容传入到函数中，此函数接收到
# 这四个内容，将内容根据"*"拼接起来并追加到一个student_msg文件中。
'''
def func(name, gender, age, edu):
    s = name + '*' + gender + '*' + age + '*' + edu
    with open('student_msg', mode='a', encoding='utf-8') as f:
        f.write(s)


name = input('姓名:')
gender = input('性别:')
age = input('年龄:')
edu = input('学历:')
func(name, gender, age, edu)
'''

# 6.写函数，在函数内部生成如下规则的列表 [1,1,2,3,5,8,13,21,34,55…]（斐波那契数列），并返回。 注意：函数可接收一个参数用于指
# 定列表中元素最大不可以超过的范围。
'''
def func(n):
    lst = [1, ]
    a = 1
    count = 0
    while 1:
        if a>n:
            break
        lst.append(a)
        a += lst[count]
        count += 1
    return lst

print(func(1000))
'''


# 7.写函数，验证用户名在文件 data.txt 中是否存在，如果存在则返回True，否则返回False。（函数有一个参数，用于接收用户输入的用户
# 名）
'''
def func(user):
    with open('data.txt', mode='r', encoding='utf-8') as f:
        res = False
        for line in f:
            if user == line.split('|')[1]:
                res = True

    return res


a = input('用户名:')
print(func(a))
'''

